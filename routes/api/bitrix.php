<?php

use Illuminate\Support\Facades\Route;

Route::prefix('bitrix')->name('bitrix.')->group(function() {
    Route::post('lead/add/{company}', 'BitrixController@leadAdd')->name('lead.add');
});