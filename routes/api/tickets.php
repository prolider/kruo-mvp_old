<?php

use Illuminate\Support\Facades\Route;

Route::prefix('ticket')->name('ticket.')->group(function() {
    Route::get('', 'TicketController@index')->name('index');

    Route::prefix('{ticket}')->middleware('invited.forbid')->group(function() {
        Route::get('', 'TicketController@show')->name('show');
        Route::post('comment', 'TicketController@comment')->name('comment');
        Route::post('status', 'TicketController@changeStatus')->name('status');
        Route::post('return', 'TicketController@returnToManager')->name('return');
        Route::post('send-invite', 'TicketController@sendInvite')->name('sendInvite');
    });
});