<?php

use Illuminate\Support\Facades\Route;

Route::prefix('comments')->name('comments.')->group(function() {
    Route::post('{comment}/change-status', 'CommentController@changeStatus')->name('changeStatus');
    Route::post('change-types', 'CommentController@changeTypes')->name('changeTypes');
});
