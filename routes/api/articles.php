<?php

use Illuminate\Support\Facades\Route;

Route::prefix('articles')->name('articles.')->group(function() {
    Route::get('{category}/extra-fields', 'ArticleController@getExtraFields')->name('extra-fields');
});