<?php

use Illuminate\Support\Facades\Route;

Route::prefix('extra-fields')->name('extra_fields.')->group(function() {
    Route::post('save', 'ExtraFieldController@save')->name('save');

    Route::prefix('{article}')->group(function() {
        Route::post('attach', 'ExtraFieldController@attach')->name('attach');
        Route::post('sync', 'ExtraFieldController@sync')->name('sync');
        Route::post('detach/{field}', 'ExtraFieldController@detach')->name('detach');
        Route::post('deleteFile/{field}', 'ExtraFieldController@deleteFile')->name('deleteFile');
    });

    Route::prefix('{field}')->group(function() {
        Route::get('', 'ExtraFieldController@get')->name('get');
        Route::post('update', 'ExtraFieldController@update')->name('update');
        Route::post('delete', 'ExtraFieldController@delete')->name('delete');
    });

    Route::prefix('steps')->name('steps.')->group(function() {
        Route::post('save', 'ExtraFieldController@saveStep')->name('save');
        Route::post('{step}/update', 'ExtraFieldController@updateStep')->name('update');
        Route::post('{step}/delete', 'ExtraFieldController@deleteStep')->name('delete');
    });
});