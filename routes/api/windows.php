<?php

use Illuminate\Support\Facades\Route;

Route::prefix('windows')->name('windows.')->group(function() {
    Route::get('list', 'WindowController@list')->name('list');
});