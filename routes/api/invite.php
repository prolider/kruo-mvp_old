<?php

use Illuminate\Support\Facades\Route;

Route::prefix('invite')->name('invite.')->group(function() {
    Route::get('', 'InviteController@index')->name('index');
    Route::get('available', 'InviteController@available')->name('available');
});