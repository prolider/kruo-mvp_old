<?php

use Illuminate\Support\Facades\Route;

Route::post('articles.search', 'ArticleController@search')->name('articles.search');
Route::post('partners.search', 'PartnerController@search')->name('partners.search');
Route::middleware('auth')->group(function () {
    Route::includeApiRoutes('tickets.php');
    Route::includeApiRoutes('extra_fields.php');
    Route::includeApiRoutes('comments.php');
    Route::includeApiRoutes('invite.php');
    Route::includeApiRoutes('user.php');
    Route::includeApiRoutes('windows.php');
    Route::includeApiRoutes('articles.php');
    Route::includeApiRoutes('bitrix.php');

    Route::post('events/{event}/deleteFile', 'EventController@deleteFile')->name('events.file.delete');
    Route::delete('notifications/{notification}', 'NotificationController@destroy')->name('notifications.delete');

    Route::get('discussions', 'DiscussionController@paginatedIndex')->middleware('can:viewAppeals,App\Models\User');
});
