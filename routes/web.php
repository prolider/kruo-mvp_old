<?php

use Illuminate\Support\Facades\{
    Route,
    Auth
};

Auth::routes(['verify' => true]);

Route::includeWebRoutes('auth.php');
Route::includeWebRoutes('admin.php');
Route::includeWebRoutes('articles.php');
Route::includeWebRoutes('measures.php');
Route::includeWebRoutes('pages.php');
Route::includeWebRoutes('partners.php');
Route::includeWebRoutes('services.php');
Route::includeWebRoutes('windows.php');
Route::includeWebRoutes('slug.php');
