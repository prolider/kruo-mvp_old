<?php

use Illuminate\Support\Facades\Route;

Route::prefix('events')->name('events.')->group(function () {
    Route::get('', 'EventController@index')->name('index');
    Route::middleware('can:canCreateEvents,App\Models\Event')->group(function () {
        Route::get('create', 'EventController@create')->name('create');
        Route::post('save', 'EventController@save')->name('save');
    });
    Route::prefix('{event}')->group(function () {
        Route::get('show', 'EventController@show')->middleware('can:canShowEvent,event')->name('show');

        Route::middleware('can:canEditEvent,event')->group(function () {
            Route::get('edit', 'EventController@edit')->name('edit');
            Route::post('update', 'EventController@update')->name('update');
        });
    });
});
