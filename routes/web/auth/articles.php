<?php

use Illuminate\Support\Facades\Route;

Route::prefix('articles')->name('articles.')->group(function () {
    Route::get('', 'ArticleController@index')->name('index');

    Route::get('create', 'ArticleController@create')->name('create');

    Route::middleware('auth')->group(function () {
        Route::post('save', 'ArticleController@save')->name('save');
    });
});