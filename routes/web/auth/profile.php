<?php

use Illuminate\Support\Facades\Route;

Route::name('profile.')->prefix('profile')->middleware('invited.forbid')->group(function () {
    Route::get('', 'ProfileController@profile')->name('index');
    Route::post('', 'ProfileController@updateProfile')->name('update');
    Route::get('operator', 'ProfileController@operator')->name('operator');
    Route::get('company', 'ProfileController@company')->middleware('can:createUserRelated,App\Models\User')->name('company');
    Route::get('contacts', 'ProfileController@contacts')->name('contacts');
    Route::get('agent', 'ProfileController@agent')->name('agent');

    Route::prefix('password')->name('password.')->group(function () {
        Route::get('', 'ProfileController@changePassword')->name('index');
        Route::post('', 'ProfileController@updatePassword')->name('update');
    });

    Route::get('settings', 'ProfileController@settings')->name('settings');
});
