<?php

use Illuminate\Support\Facades\Route;

Route::prefix('discussions')->name('discussions.')->middleware('invited.forbid')->group(function () {
    Route::get('', 'DiscussionController@index')->middleware('can:viewAppeals, App\Models\User')->name('index');
    Route::get('new', 'DiscussionController@create')->middleware('can:canCreateDiscussions')->name('create');
    Route::get('{discussion}', 'DiscussionController@show')->name('show');
    Route::post('', 'DiscussionController@store')->middleware('can:canCreateDiscussions')->name('store');

    Route::prefix('messages')->group(function () {
        Route::get('{message}/read', 'DiscussionController@read')->name('read');
        Route::post('{discussion}', 'DiscussionController@send')->name('send');
    });
});
