<?php

use Illuminate\Support\Facades\Route;

Route::prefix('companies')->name('companies.')->group(function () {
    Route::middleware('invited.forbid')->group(function () {
        Route::post('save', 'CompanyController@save')->name('save');
        Route::get('search', 'CompanyController@search')->name('search');
        Route::get('info', 'CompanyController@info')->name('info');

        Route::prefix('{company}')->group(function () {
            Route::prefix('invite')->group(function () {
                Route::get('', 'CompanyController@invite')->name('invite');
                Route::post('send', 'CompanyController@sendInvitation')->name('invite.send');
                Route::post('remove', 'CompanyController@removeInvited')->name('invite.remove');
            });
        });
    });

    Route::get('{company}', 'CompanyController@show')->middleware('can:see,company')->name('show');
});
