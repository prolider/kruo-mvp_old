<?php

use Illuminate\Support\Facades\Route;

Route::prefix('ticket')->name('ticket.')->group(function () {
    Route::get('', 'TicketController@index')->name('index');
    Route::post('', 'TicketController@window')->name('window');
    Route::get('{ticket}', 'TicketController@show')->middleware('can:viewTicket,ticket')->name('show');
    Route::post('{ticket}/comment', 'CommentController@store')->middleware('invited.forbid')->name('comment.new');
});