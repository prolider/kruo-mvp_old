<?php

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

Route::prefix('admin')->group(function () {
    Voyager::routes();

    Route::middleware('admin.user')->group(function () {
        Route::prefix('tickets/{ticket}')->group(function() {
            Route::post('comment', 'CommentController@operatorStore')->name('operator.add.ticket');
            Route::post('comment/set-all-readed', 'CommentController@setAllReaded')->name('operator.read.tickets');
        });

        Route::namespace('Api')->group(function() {
            Route::prefix('services/{service}/document')->group(function() {
                Route::post('', 'DocumentController@store');
                Route::put('{serviceDocument}', 'DocumentController@update');
                Route::delete('{serviceDocument}', 'DocumentController@delete');
            });

            Route::put('model/{id}/save', 'VoyagerBaseController@update');
        });
    });
});
