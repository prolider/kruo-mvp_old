<?php

use Illuminate\Support\Facades\Route;

Route::prefix('measures')->name('measures.')->group(function () {
    Route::get('', 'ArticleController@measures')->name('index');
});
