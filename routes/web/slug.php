<?php

use Illuminate\Support\Facades\Route;
use App\Models\Category;

Route::get('{tag}/{slugs?}', 'SlugController@slugWithTag')->name('slug')->where([
    'slugs' => '(.|\s)*\S(.|\s)*',
    'tag'   => implode('|', Category::TAGS),
]);

Route::get('{slugs}', 'SlugController@slugWithoutTag')->name('slug')->where([
    'slugs' => '(.|\s)*\S(.|\s)*',
]);
