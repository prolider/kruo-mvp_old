<?php

use Illuminate\Support\Facades\Route;

Route::prefix('windows')->name('windows.')->group(function () {
    Route::get('', 'WindowController@index')->name('index');
    Route::get('{window}', 'WindowController@show')->name('show');
});