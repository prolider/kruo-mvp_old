<?php

use Illuminate\Support\Facades\Route;

Route::prefix('articles')->name('articles.')->group(function() {
    Route::get('create', 'ArticleController@create')->name('create');
});