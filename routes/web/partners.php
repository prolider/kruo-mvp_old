<?php

use Illuminate\Support\Facades\Route;

Route::prefix('partners')->name('partners.')->group(function () {
    Route::get('', 'PartnerController@index')->name('index');

    Route::middleware('can:createBusinessPartner, App\Models\User')->group(function () {
        Route::get('create', 'PartnerController@create')->name('create');
        Route::post('save', 'PartnerController@save')->name('save');
    });

    Route::get('success', 'PartnerController@success')->name('success');

});
