<?php

use Illuminate\Support\Facades\Route;

Route::get('verify-authentication/resend', 'Auth\TwoFactorController@resend')
    ->middleware('twofactor')
    ->name('verify-authentication.resend');
Route::resource('verify-authentication', 'Auth\TwoFactorController')
    ->middleware('twofactor')
    ->only(['index', 'store']);

Route::middleware(['auth', 'twofactor', 'verified'])->group(function () {
    Route::includeWebRoutes('auth/articles.php');
    Route::includeWebRoutes('auth/events.php');
    Route::includeWebRoutes('auth/discussions.php');

    Route::includeWebRoutes('auth/profile.php');
    Route::includeWebRoutes('auth/ticket.php');
    Route::includeWebRoutes('auth/companies.php');

    Route::prefix('main-companies')->name('main_companies.')->group(function() {
        Route::get('', 'MainCompanyController@index')->name('index');
    });

    Route::get('messages', 'HomeController@messages')->name('messages.index');
    Route::get('notices', 'HomeController@notices')->name('notices.index');


    Route::prefix('projects')->name('projects.')->middleware('forbid.not.admin')->group(function() {
        Route::get('', 'ProjectController@index')->name('index');
    });
});
