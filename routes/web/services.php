<?php

use Illuminate\Support\Facades\Route;

Route::prefix('services')->name('services.')->group(function () {
    Route::get('', 'ServiceController@index')->name('index');
    Route::get('category-{serviceCategory}/service-{service}', 'ServiceController@show')->name('show');
    Route::get('category-{serviceCategory}', 'ServiceCategoryController@show')->name('categories.show');

    Route::middleware('auth')->group(function() {
        Route::prefix('{service}')->group(function() {
            Route::get('form/{ticket?}', 'ServiceController@showForm')
                ->middleware('can:createUserRelated, App\Models\User')
            ->name('form');

            Route::post('form/{ticket?}', 'ServiceController@storeForm')->name('store-form');
            Route::post('draft/{ticket?}', 'ServiceController@storeDraft')->name('store-draft');
        });
    });
});
