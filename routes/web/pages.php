<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('index');
Route::get('auth/confirm/{user}', 'Voyager\UserController@confirmation')->name('confirmation.auth');

Route::prefix('search')->name('search.')->group(function () {
    Route::get('', 'SearchController@index')->name('index');
});

Route::get('/login/registered', 'ProfileController@registered')->name('registered');
Route::get('/login/resetted', 'ProfileController@resetted')->name('resetted');

Route::get('/privacy-policy', function () {
    return view('policy');
})->name('policy');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/faq', function () {
    return view('faq/faq');
})->name('faq');
