<div class="card-body pt-1" id="articles-list">
    <!--begin::Item-->
    @foreach($models as $model)
        <div class="d-flex align-items-center mb-1">
            <!--begin::Text-->
            <div class="d-flex flex-column flex-grow-1">
                @if($model instanceof \App\Models\Service)
                    <a href="{{ route('services.show', ['serviceCategory' => $model->category, 'service' => $model]) }}" class="text-dark text-hover-primary mb-1 font-size-lg font-weight-bolder">{{ $model->name }}</a>
                @else
                    <a href="{{ route('slug', $model->getSlugHierarchy()) }}" class="text-dark text-hover-primary mb-1 font-size-lg font-weight-bolder">{{ $model->name }}</a>
                @endif
            </div>
            <!--end::Text-->
            <!--begin::Section-->
            <div class="d-flex align-items-center py-2">
                <!--begin::Btn-->
                @if($model instanceof \App\Models\Service)
                    <a href="{{ route('services.show', ['serviceCategory' => $model->category, 'service' => $model]) }}" class="btn btn-icon btn-icon-primary btn-light">  <i class="la la-angle-right"></i></a>
                @else
                    <a href="{{ route('slug', $model->getSlugHierarchy()) }}" class="btn btn-icon btn-icon-primary btn-light">  <i class="la la-angle-right"></i></a>
                @endif
                <!--end::Btn-->
            </div>
            <!--end::Section-->
        </div>
    @endforeach
    <!--end::Item-->
</div>
