@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <!--begin::Subheader-->
        <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title--> 
                        <h2 class="text-dark font-weight-bold my-1 mr-5">Результаты поиска</h2>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Row-->
                <div class="row pt-4">
                    <div class="card card-custom gutter-b col-4">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="la la-filter text-primary"></i>
                                </span>
                                <h3 class="card-label">Область поиска</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('search.index') }}" method="get">
                                <div class="form-group">
                                    <label class="form-label" for="search">Поиск</label>
                                    <input name="search" class="form-control" id="search" type="text" value="{{ old('search') ?? $search ?? '' }}">
                                </div>
                                <div class="form-group">
                                    @foreach ($searchable as $key => $checkbox)
                                        <div class="form-check">
                                            <input class="form-check-input" {{ $checkbox['checked'] ? 'checked' : '' }} type="checkbox" name="categories[]" id="{{ $key }}" value="{{ $key }}">
                                            <label class="form-check-label" for="{{ $key }}">{{ $checkbox['name'] }}</label>
                                        </div>
                                    @endforeach
                                </div>
                                <button type="submit" class="btn btn-primary">Найти</button>
                            </form>
                        </div>
                    </div>
                    <div class="row pl-4 col-8">
                    @foreach ($searchCategories as $key => $category)
                        <div class="col-12">
                            <!--begin::List Widget 1-->
                            <div class="card card-custom card-stretch gutter-b" id="{{ $key }}">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">{{ $category['key'] }}</h3>
                                    </div>
                                </div>
                                <!--begin::Body-->
                                @include('search.list', ['models' => $category['data'], 'paginationId' => $key])
                                <!--end::Body-->
                            </div>
                            <!--end::List Widget 1-->
                            <div class="w-100"></div>
                        </div>
                    @endforeach
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->
    </div>
@endsection
