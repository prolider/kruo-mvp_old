@extends('layouts.app')

@section('content')

    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h2 class="text-dark font-weight-bold my-1 mr-5">{{ $serviceCategory->name ?? 'Default' }}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="content d-flex flex-column flex-column-fluid">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-custom card-stretch gutter-b pt-8">
                            <div class="card-body pt-1">
                                @forelse($serviceCategory->services as $service)
                                    <div class="d-flex align-items-center mb-7">
                                        <div class="d-flex flex-column flex-grow-1" style="flex:1;">
                                            <a href="{{ route('services.show', compact('serviceCategory', 'service')) }}" class="text-dark text-hover-primary mb-1 font-size-h5 font-weight-bold">{{ $service->name }}</a>
                                        </div>
                                        <div class="d-flex align-items-center py-2">
                                            <a href="{{ route('services.show', compact('serviceCategory', 'service')) }}" class="btn btn-icon btn-icon-primary btn-light">  <i class="la la-angle-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                @empty
                                    <p>Нет услуг в данной категории</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 flex-row-auto offcanvas-mobile">
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">Путеводитель инвестора</h3>
                                </div>
                            </div>
                            <div class="card-body pt-4">
                                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                    @forelse($categories as $category)
                                        <div class="navi-item mb-1">
                                            <a class="navi-link py-3 @if(Request::url() === ($route = route('services.categories.show', $category)))current-category @endif" href="{{ $route }}">{{ $category->name }}</a>
                                        </div>
                                    @empty
                                        <p>Нет категорий</p>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

