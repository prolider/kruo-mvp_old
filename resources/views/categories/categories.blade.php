@extends('layouts.app')

@section('content')
    <div class="container content">
        <h1  class="mb-4">Выберите направление</h1>
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="row">
                    @forelse($categories as $category)
                        <div class="col-xs-6 col-sm-6 col-md-3">
                            <a class="category-link" href="{{ route('services.categories.show', ['category_id' => $category->id]) }}">
                                <div class="category rounded custom-class">
                                    <!--ToDo-->
                                    <!--<div class="category-image">{{ $category->image }}</div>-->
                                    <div class="category-image"><i class="fa fa-home"></i></div>
                                    <div class="category-description">{{ $category->name }}</div>                                
                                </div>
                            </a>
                        </div>
                    @empty
                        <p>Нет категорий</p>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{ $services->links() }}
            </div>
        </div>  
    </div>

  
@endsection