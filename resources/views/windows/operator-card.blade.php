<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
    <div class="card card-custom gutter-b card-stretch">
        <div class="card-body pt-4">
            <div class="d-flex align-items-end py-2">
                <div class="d-flex align-items-center">
                    <div class="d-flex flex-shrink-0 mr-5">
                        <div class="symbol symbol-lg-75">
                            <img src="{{ $minuser->avatarUrl }}" alt="image">
                        </div>
                    </div>
                    <div class="d-flex flex-column">
                        <a href="#" class="text-dark font-weight-bold font-size-h4 mb-0">{{ $minuser->name }} {{ $minuser->firstname }}</a>
                        <span class="text-muted font-weight-bold">{{ $minuser->position }}</span>
                    </div>
                </div>
            </div>
            <div class="py-2">
                <div class="d-flex align-items-center mb-2">
                    <span class="flex-shrink-0 mr-3">
                        <a href="https://t.me/{{ $minuser->telegram }}" target="_blank" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="Telegram"><i class="icon-lg la fab la-telegram-plane"></i></a>
                    </span>
                    <span class="flex-shrink-0 mr-3">
                        <a href="viber://chat?number={{ $minuser->phone }}" target="_blank" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="Viber"><i class="icon-lg la fab la-viber"></i></a>
                    </span>
                    <span class="flex-shrink-0 mr-3">
                        <a href="https://api.whatsapp.com/send?phone={{ $minuser->phone }}" target="_blank" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="WhatsApp"><i class="icon-xl la fab la-whatsapp-square"></i></a>
                    </span>
                </div>
                <div class="d-flex align-items-center mb-2">
                    <a href="tel:{{ $minuser->phone ?? '-' }}" class="text-muted text-hover-primary font-weight-bold">{{ $minuser->phone ?? '-' }}</a>
                </div>
                <div class="d-flex align-items-center mb-2">
                    <a href="mailto:{{ $minuser->email ?? '-' }}" class="text-muted text-hover-primary font-weight-bold">{{ $minuser->email ?? '-' }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
