@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">                  
                    <h2 class="text-dark font-weight-bold my-1 mr-5">
                        Министерства и ведомства
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">  
        <div class="container">
            <div class="row">
                @forelse($windows as $window)
                <div class="col-lg-4">
                    <!--begin::Tizer Widget 1-->
                    <div class="card card-custom card-stretch gutter-b bg-diagonal">
                        
                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Section-->																										
                            <div class="d-flex flex-column justify-content-between ">
                                        <!--begin::Text-->
                                        <h4><span class="card-label ">{{ $window->name }}</span></h4>
                                        
                                        <!--end::Text-->
                                <!--begin::Footer-->
                                <div class="d-flex align-items-center justify-content-between mt-5">
                                <a href="{{ route('windows.show', $window) }}" class="btn btn-primary btn-shadow font-size-md">Посмотреть профиль</a>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Section-->												
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Tizer Widget 1-->
                </div>  
                    
                @empty
                    <p>Список пуст</p>    
                @endforelse
            </div>
        </div>
    </div>
@endsection
