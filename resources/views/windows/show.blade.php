@extends('layouts.app')

@section('content')
    @if($window)
        <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">
                            {{ $window->name }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="d-flex flex-row">
                    <div class="flex-row-auto col-xl-6 col-lg-6 col-md-6 col-sm-6" id="kt_content_sidebar">
                        <!--begin::Nav Panels Wizard 1-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body pt-5">
                                <!--begin::User-->
                                <div class="text-left mb-7">
                                    <h4 class="font-weight-bold my-2">{{ $window->name }}</h4>
                                </div>
                                <!--end::User-->
                                <!--begin::Nav-->
                                @if($window->description)
                                    <div class="text-left text-lg my-3">
                                        {!! $window->description !!}
                                    </div>
                                @else
                                    <div class="text-left text-lg my-3">
                                        Описание отсутствует
                                    </div>
                                @endif
                            </div>
                            <!--end::Nav-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Nav Panels Wizard 1-->
                    @can('canSeeMinusers')
                        <div class="flex-row-fluid ml-lg-8 d-none d-sm-block">
                                <div class="row">
                                    @each('windows.operator-card', $window->users, 'minuser')
                                </div>
                            <!--
                            <div class="card mb-4">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Категории услуг
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <ul>
                                        @each('windows.category-card', $window->categories, 'category')
                                    </ul>
                                </div>
                            </div>-->
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    @endif
@endsection
