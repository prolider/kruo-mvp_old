@extends('layouts.app')

@section('content')
    <div class="container content">
        @if($notifications->count() > 0)
        <h1 class="mb-4">Новые оповещения</h1>
        @foreach($notifications as $notification)
            <div class="notice row">
              <div class="col-md-8">
                <h5>
                    <a href="{{ route('ticket.show', $notification->notificable) }}">{{ $notification->name }}</a>
                </h5>
                <p>{{ $notification->notificable->formResult->form->service->name }}</p>
                <p>{{ $notification->description }}</p>
              </div> 
              <div class="col-md-4 text-right">
                <a href="{{ route('ticket.show', $notification->notificable) }}" class="btn btn-brown" data-id="{{ $notification->id }}"><i class="fa fa-folder-open"></i></a>  
                <a href="#" class="btn btn-success remove-notice" data-id="{{ $notification->id }}"><i class="fa fa-check"></i></a>
              </div>
            </div>
        @endforeach
        @else
            <h1>Новых уведомлений нет</h1>
        @endif
        </div>
    </div>
@stop

@push('javascript')
    <script>
        $('.remove-notice').on('click', function (e) {
          e.preventDefault();
          const id = $(e.target).data('id');
          $.ajax(`/api/notifications/${id}`, {
            method: 'POST',
            data: {
              _method: 'DELETE',
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: (response) => {
              $(e.target).parent().parent().remove();
              $('meta[name="csrf-token"]').attr('content', response.token);
            }
          })
        })
    </script>
@endpush
