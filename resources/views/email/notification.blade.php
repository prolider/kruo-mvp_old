@component('mail::message')
    {{ $notification->name }}
    <br>
    {{ $notification->description }}
@endcomponent
