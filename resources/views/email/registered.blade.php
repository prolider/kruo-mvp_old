@component('mail::message')
    <p>{{ $user->full_name }}</p>
    <br>
    <p>Поздравляем с регистрацией на портале "Личный кабинет инвестора"!</p>
    <p>Подтвердите регистрацию, пройдя по <a href="{{ route('confirmation.auth', $user->id) }}">ссылке</a>.</p>
@endcomponent