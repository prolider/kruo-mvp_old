@component('mail::message')
    <p>Вас пригласили в качестве наблюдателя в компанию {{ $user->inviterCompany->name }}.</p>
    <br>
    <p>Для входа в учетную запись наблюдателя используйте следующие данные:</p> 
    <p>Логин: {{ $user->email }}</p>
    <p>Пароль: {{ $password }}</p>
    <div>
        <a href="{{ route('confirmation.auth', $user->id) }}" class="btn btn-primary">Авторизация</a>
    </div>
@endcomponent
