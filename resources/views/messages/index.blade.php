@extends('layouts.app')

@section('content')
    <div class="container content">
        <div class="card">
            <div class="card-body">
                @if ($tickets->count() > 0)
                    <h1 class="mb-4">Сообщения</h1>
                    @foreach($tickets as $ticket)
                        <p>
                            <a href="{{ route('ticket.show', $ticket) }}">
                                {{ $ticket->formResult->form->service->name }} ({{ $ticket->new_comments_count }} новых сообшений)
                            </a>
                        </p>
                    @endforeach
                @else
                    <h1 class="mb-4">Новых сообщений нет</h1>
                @endif
            </div>
        </div>
    </div>
@stop
