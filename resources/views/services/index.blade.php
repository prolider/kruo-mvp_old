@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h2 class="text-dark font-weight-bold my-1 mr-5">{{ __('app.investor_guide') }}</h2>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>

    <div class="container">
        <div class="row">
            @forelse($categories as $category)
                <div class="col-lg-4">
                    <!--begin::Tizer Widget 1-->
                    <div class="card card-custom card-stretch gutter-b bg-diagonal bg-diagonal-light-primary">

                        <!--begin::Body-->
                        <div class="card-body">
                            <!--begin::Section-->
                            <div class="d-flex flex-column justify-content-between ">
                                <!--begin::Text-->
                                <h4><span class="card-label ">{{ $category->name }}</span></h4>
                                <p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">{{ $category->class_name }}</p>
                                <!--end::Text-->
                                <!--begin::Footer-->
                                <div class="d-flex align-items-center justify-content-between mt-5">
                                    <a href="{{ route('services.categories.show', $category) }}" class="btn btn-primary btn-shadow font-size-md">Подробнее</a>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Tizer Widget 1-->
                </div>
            @empty
                <p>Нет категорий</p>
            @endforelse
        </div>
    </div>
@endsection
