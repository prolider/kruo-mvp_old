@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            {{ __('breadcrumbs.You_are_here') }}
                        </li>
                        @if($serviceCategory)
                            <li class="breadcrumb-item">
                                <a href="{{ route('services.categories.show', compact('serviceCategory')) }}">{{ $serviceCategory->name ?? 'Default'}}</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <h3 class="card-title">{{ $service->name }}</h3>
                    </div>
                    <div class="card-body">
                        @guest
                            <div class="alert alert-custom alert-light-info fade show mb-5" role="alert">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text">Для отправки заявки необходима регистрация на портале.</div>
                            </div>
                        @endguest
                        @auth
                            @php $user = auth()->user(); @endphp
                            @if (!($user->role && $user->role->name === 'user'))
                                <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon-warning"></i>
                                    </div>
                                    <div class="alert-text">
                                        Для Вашего аккаунта установлено ограничение на создание заявок.
                                    </div>
                                </div>
                            @endif
                            @if (!$user->company && $user->role->name === 'user')
                                <div class="alert alert-custom alert-light-info fade show mb-5" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon-warning"></i>
                                    </div>
                                    <div class="alert-text">
                                        Для отправки заявок Вам необходимо создать компанию и подтвердить полномочия представителя на подачу заявок от лица компании. Для создания Профиля компании Вы можете перейти по <a href="{{ route('profile.company') }}">ссылке</a>.
                                    </div>
                                </div>
                            @elseif (($user->company && $user->company->isVerified()) && $user->role->name === 'user') )
                                <div class="alert alert-custom alert-light-success fade show mb-5" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon-warning"></i>
                                    </div>
                                    <div class="alert-text">Все условия выполнены успешно. Вы можете подавать заявки на получение услуг.</div>
                                </div>
                            @elseif ($user->company && $user->role->name === 'user')
                                <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon-warning"></i>
                                    </div>
                                    <div class="alert-text">
                                        Проверка полномочий представителя не пройдена или не назначен персональный менеджер. Вы не можете создавать заявки. Пожалуйста, пройдите проверу полномочий представителя в разделе <a href="{{ route('profile.company')}}">Профиль компании</a> и дождитесь результатов проверки.
                                    </div>
                                </div>
                            @endif
                        @endauth
                        <h5 class="title title text-muted">{{ $service->official_name }}</h5>
                        <div class="row">
                            <div class="col-12">
                                {!! $service->description !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        @if($service->show_button)
                            @auth
                                @can('canCreateTickets')
                                    <div class="button-use">
                                        <a class="btn btn-primary btn-shadow font-size-md mb-5" href="{{ route('services.form', $service) }}">Подать заявку&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                                    </div>
                                @endcan
                            @else
                                <div class="button-use">
                                    <a class="btn btn-primary btn-shadow font-size-md mr-3" href="{{ route('register') }}">
                                        Создать кабинет инвестора
                                    </a>
                                    <a class="btn btn-primary btn-shadow font-size-md" href="{{ route('login') }}">
                                        Войти
                                    </a>
                                </div>
                            @endauth
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4 flex-row-auto" id="">
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Ответственные организации
                        </h3></div>
                    </div>
                    <div class="card-body pt-4">
                        <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                            <div class="navi-item mb-2">
                                <div class="navi-link py-4 ">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <i class="icon-xl la la-institution"></i>
                                        </span>
                                    </span>
                                    <span class="navi-text">Корпорация развития Ульяновской области</span>
                                </div>
                            </div>
                            <div class="navi-item mb-2">
                                <div class="navi-link py-4">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <i class="icon-xl la la-institution"></i>
                                        </span>
                                    </span>
                                    <span class="navi-text">
                                        @if ($service->window_id)
                                            {{ $service->window->name }}
                                        @else
                                            не указано
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Файлы на скачивание</h3>
                        </div>
                    </div>
                    <div class="card-body pt-4">
                        <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                            @auth
                                @forelse($service->documents as $document)
                                    <div class="navi-item mb-2">
                                        <a href="{{ $document->documentUrl }}" download="{{ $document->download_name }}" class="navi-link py-4">
                                            <span class="navi-icon mr-2">
                                                <span class="svg-icon">
                                                    <i class="icon-xl la la-file-download"></i>
                                                </span>

                                            </span>
                                            <span class="navi-text">
                                                {{ $document->name }}
                                            </span>
                                        </a>
                                    </div>
                                @empty
                                    <p>Нет файлов на скачивание</p>
                                @endforelse
                            @else
                                <p><a class="" href="{{ route('login') }}" id="" target="_blank">Авторизуйтесь</a> для доступа к
                                    файлам</p>
                            @endauth
                        </div>
                    </div>
                </div>
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Ссылки</h3>
                        </div>
                    </div>
                    <div class="card-body pt-4">
                        <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                            @auth
                                @forelse($service->links_array as $link)
                                    <div class="navi-item mb-2">
                                        <a href="{{ $link['url'] }}" class="navi-link py-4" target="_blank">
                                            <span class="navi-icon mr-2">
                                                <span class="svg-icon">
                                                    <i class="icon-xl la la-external-link"></i>
                                                </span>
                                            </span>
                                            <span class="navi-text">
                                                {{ $link['name'] }}
                                            </span>
                                        </a>
                                    </div>
                                @empty
                                    <p>Нет ссылок</p>
                                @endforelse
                            @else
                                <p><a class="" href="{{ route('login') }}" id="" target="_blank">Авторизуйтесь</a> для доступа к
                                    ссылкам</p>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
