@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            {{ __('breadcrumbs.You_are_here') }}
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ route('services.index') }}">Получить услугу</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <span class="text-muted">{{ $service->name }}</span>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            Создание заявки
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">

        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <h3 class="card-title">Создать заявку</h3>
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">{{ $service->name }}</h4>

                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach

                            @if ($service->form->is_valid)
                                <div class="alert alert-custom alert-light-info fade show mb-5" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon-warning"></i>
                                    </div>
                                    <div class="alert-text">Для отправки заявки заполните все поля формы и загрузите необходимые файлы.</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">
                                                <i class="ki ki-close"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>

                                <form method="POST" action="{{ route('services.store-form', [$service, $ticket]) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div id="form"></div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 flex-row-auto" id="kt_profile_aside">
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Ответственные организации
                            </h3></div>
                        </div>
                        <div class="card-body pt-4">
                            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                <div class="navi-item mb-2">
                                    <div class="navi-link py-4">
                                        <span class="navi-icon mr-2">
                                            <span class="svg-icon">
                                                <i class="icon-xl la la-institution"></i>
                                            </span>
                                        </span>
                                        <span class="navi-text">Корпорация развития Ульяновской области</span>
                                    </div>
                                </div>
                                <div class="navi-item mb-2">

                                    <div class="navi-link py-4">
                                        <span class="navi-icon mr-2">
                                            <span class="svg-icon">
                                                <i class="icon-xl la la-institution"></i>
                                            </span>
                                        </span>
                                        <span class="navi-text">
                                            @if ($service->window_id)
                                                {{ $service->window->name }}
                                            @else
                                                не указано
                                            @endif

                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Файлы на скачивание</h3>
                            </div>
                        </div>
                        <div class="card-body pt-4">
                            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                @auth
                                    @forelse($service->documents as $document)
                                        <div class="navi-item mb-2">
                                            <a href="{{ $document->documentUrl }}" download="{{ $document->download_name }}" class="navi-link py-4">
                                                <span class="navi-icon mr-2">
                                                    <span class="svg-icon">
                                                        <i class="icon-xl la la-file-download"></i>
                                                    </span>

                                                </span>
                                                <span class="navi-text">
                                                    {{ $document->name }}
                                                </span>
                                            </a>
                                        </div>
                                    @empty
                                        <p>Нет файлов на скачивание</p>
                                    @endforelse
                                @else
                                    <p>
                                        <a class="" href="{{ route('login') }}">Авторизуйтесь</a> для доступа к
                                        файлам
                                    </p>

                                @endauth
                            </div>
                        </div>

                    </div>
                    <div class="card card-custom gutter-b">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label">Ссылки</h3>
                            </div>
                        </div>
                        <div class="card-body pt-4">
                            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                @auth
                                    @forelse($service->links_array as $link)
                                        <div class="navi-item mb-2">
                                            <a href="{{ $link['url'] }}" class="navi-link py-4">
                                                <span class="navi-icon mr-2">
                                                    <span class="svg-icon">
                                                        <i class="icon-xl la la-external-link"></i>
                                                    </span>
                                                </span>
                                                <span class="navi-text">
                                                    {{ $link['name'] }}
                                                </span>
                                            </a>
                                        </div>
                                    @empty
                                        <p>Нет ссылок</p>
                                    @endforelse
                                @else
                                    <p><a class="" href="{{ route('login') }}">Авторизуйтесь</a> для доступа к
                                        ссылкам</p>
                                @endauth
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        window.form = {!! $service->form->form_data !!};
        window.serviceType = '{{ $service->use_button }}';
        @if ($ticket && $ticket->formResult->result)
        window.result = {!! $ticket->formResult->result !!};
        @endif
        window.user = {!! json_encode(auth()->user()) !!};
    </script>
    <script src="/js/forms/form-renderer.js"></script>
@endsection
