@if(isset($submenu) && $submenu)
    <div class="menu-submenu">
        <ul class="menu-subnav">
@endif
@foreach($items as $menu_item)
    @php
        $isOpen = false;

        foreach($menu_item->children as $child) {
            if(Route::is($child->route)) $isOpen = true;
        }
    @endphp
    @if(isset($parent))
        <li class="menu-item menu-item-parent" aria-haspopup="true">
            <span class="menu-link">
                <span class="menu-text">{{ $parent->title }}</span>
            </span>
        </li>
    @endif
    @php
        $routeName = str_replace('.index', '', $menu_item->route)
    @endphp
    <li class="menu-item @if(Str::startsWith(Request::route()->getName(), $routeName    )) menu-item-active @endif @if(!$menu_item->children->isEmpty()) menu-item-submenu @endif   @if($isOpen) menu-item-open" @endif aria-haspopup="true">
        <a @if(!$menu_item->children->isEmpty()) class="menu-link menu-toggle" href="javascript:;" @else class="menu-link"  href="{{ $menu_item->link() }}"  @endif>
            @if ($menu_item->icon_class)                
                <i class="{{ $menu_item->icon_class }}"><span></span></i>                
            @else
                <i class="menu-bullet menu-bullet-dot"><span></span></i>                
            @endif            
            <span class="menu-text">
                {{ $menu_item->title }}
            </span>
            @if(!$menu_item->children->isEmpty())
                <i class="menu-arrow"><span></span></i>
            @endif
        </a>

        @if(!$menu_item->children->isEmpty())
            @include('menu_items', ['items' => $menu_item->children, 'submenu' => true, 'parent' => $menu_item])
        @endif
    </li>
@endforeach
@if(isset($submenu) && $submenu)
        </ul>
    </div>
@endif
