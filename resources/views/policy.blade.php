@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">{{ __('app.policy') }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body">
                        @if(app()->getLocale() === 'en')
                            {!! setting('site.policy-eng') !!}
                        @else 
                            {!! setting('site.policy') !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection