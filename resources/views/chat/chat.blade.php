<div class="chatbox">
	<div id="ChatSidebar" class="chatsidebar">
        <button class="openchatbtn" id="ChatBtn" onclick="openchatNav()" title="Чат с оператором"><i class="fa fa-comments"></i></button>
        <button class="closechatbtn" id="CloseChatBtn" onclick="closechatNav()" title="Свернуть панель"><i class="fa fa-times"></i></button>
        @auth
        <div class="px-4 py-3 messages" id="messages">
 			<div class="user-message text-right pb-2">
 				<div class="user-name small">Максимов Алексей Юрьевич</div>
 				<div class="date small">19.10.2020 15:27</div> 				
 				<div class="message">Подскажите, как оформить Устав для ООО?</div>
 				<hr>
 			</div>
 			<div class="operator-message pb-2">
 				<div class="operator-name small">Трифонов Михаил Сергеевич</div>
 				<div class="date small">19.10.2020 15:28</div> 
 				<div class="message">Перейдите в раздел "Начало бизнеса" и выберите услугу открытие ООО.</div>
 				<hr>			
 			</div>
    	</div>        
        <div class="px-4 py-3 chat">
        	<h5>Задать вопрос персональному менеджеру</h5>		
	    	<textarea class="w-100"></textarea>
		    <div class="text-center">
		    	<button class="btn btn-sm btn-coral" id="" onclick="submit" title="отправить">Отправить</button>
	    	</div>
    	</div>
        @else
            <div class="px-4 py-3">
            	<p>Для использования сервиса, пожалуйста, авторизуйтесь на портале.</p>
            	<div class="col-md-12 justify-content-center">
            <div class="text-center py-3">
                <a class="btn btn-sm btn-coral"  href="#login" id="navbarDropdownMenuLink" role="button" data-toggle="modal" aria-haspopup="true" aria-expanded="false">
                    Войти
                </a>             
            </div>
            <div class="text-center pb-3">
                @if (Route::has('register'))
                    <a class="btn btn-lg btn-brown" href="{{ route('register') }}">Зарегистрироваться</a>
                @endif             
            </div>           
        </div> 
            </div>
        @endauth
    </div>
</div>
<div id="WraperChatblack" class="wraperblack" onclick="hide()"></div>

<style>
    #messages::-webkit-scrollbar {
        width: 7px;
        background-color: #f9f9fd;
    }
    #messages::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: var(--corall);
    }

    #messages::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2);
        border-radius: 10px;
        background-color: #eee;
    }    
    .chatbox {
        overflow: hidden;        
    }
    .chat {
        bottom: 0;
        position: fixed;
        width: inherit;
        background:#eee;
        max-height: 150px;
        overflow: hidden;
    }
    .messages {
    height: calc(100% - 150px);
    overflow-y: auto;
    }
</style>

@push('javascript')
    <script>

      function openchatNav() {
        document.getElementById("ChatSidebar").style.right = "0px";
        document.getElementById("WraperChatblack").style.display = "block";
        document.getElementById("CloseChatBtn").style.display = "block";
        document.getElementById("CloseChatBtn").style.right = "15px";
        document.getElementById("ChatBtn").style.visibility = "hidden";
        document.getElementById("section-main").style.overflow = "hidden";
      }

      function closechatNav() {
        document.getElementById("ChatSidebar").style.right = "-400px";
        document.getElementById("WraperChatblack").style.display = "none";
        document.getElementById("ChatBtn").style.visibility = "visible";
        document.getElementById("CloseChatBtn").style.right = "-50px";
        document.getElementById("section-main").style.overflow = "auto";

      }
      function hide() {
        document.getElementById("WraperChatblack").style.display = "none";
        document.getElementById("ChatSidebar").style.right = "-400px";
        document.getElementById("CloseChatBtn").style.right = "-50px";
        document.getElementById("ChatBtn").style.visibility = "visible";
        document.getElementById("section-main").style.overflow = "auto";
      }


    </script>
@endpush