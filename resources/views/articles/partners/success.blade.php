@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Площадка кооперации</h2>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    @if (session('status'))
        <div class="d-flex flex-column-fluid">
            <div class="container">           
                <div class="row">
                    <div class="col-md-8 flex-row-auto">
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">Заявка принята</h3>
                                </div>
                            </div>                            
                            <div class="card-body pt-4">                              
                                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                    Спасибо за участие. Анкета будет опубликована после прохождения модерации.
                                </div>
                            </div>
                            <div class="card-footer pt-4">                              
                                <div class="d-flex align-items-center justify-content-between mt-5">
                                    <a href="{{ route('partners.index') }}" class="btn btn-primary btn-shadow font-size-md">Вернуться в раздел</a>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>               
            </div>
        </div>
    @else
        <div class="d-flex flex-column-fluid">
            <div class="container">           
                <div class="row">
                    <div class="col-md-8 flex-row-auto">
                        Переадресация в раздел
                    </div>
                </div>
            </div>   
        </div>
        <script>window.location = "{{ route('partners.index') }}";</script>           
    @endif
@endsection
