<div class="row">
    @foreach($step->fields as $field)
        <div class="form-group {{ $field->container_class ?? 'col-md-12' }}">
            <label>{{ $field->name }}</label>

            @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT or $field->type == \App\Models\ExtraField::TYPE_SELECT)
                <select
                    class="form-control form-control-solid @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT) select2 @endif"
                    name="extraFields[{{ $field->id }}][value]@if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)[]@endif"
                    @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT) multiple @endif
                    @if($field->is_required) required @endif
                >
                    @foreach($field->details['options'] ?? [] as $option)
                        <option>{{ $option }}</option>
                    @endforeach
                </select>
            @elseif($field->type == \App\Models\ExtraField::TYPE_NUMBER)
                <input
                    type="number"
                    name="extraFields[{{ $field->id }}][value]"
                    placeholder="{{ $field->placeholder }}"
                    @if($field->is_required) required @endif
                >
            @elseif($field->type == \App\Models\ExtraField::TYPE_FILE)
                <input
                    type="file"
                    name="extraFields[{{ $field->id }}][value]"
                    @if($field->is_required) required @endif
                >
            @elseif($field->type == \App\Models\ExtraField::TYPE_TEXT_AREA)
                <textarea
                    class="form-control form-control-solid"
                    name="extraFields[{{ $field->id }}][value]"
                    placeholder="{{ $field->placeholder }}"
                    @if($field->is_required) required @endif
                ></textarea>
            @else
                <input
                    class="form-control form-control-solid"
                    name="extraFields[{{ $field->id }}][value]"
                    placeholder="{{ $field->placeholder }}"
                    @if($field->is_required) required @endif
                >
            @endif
            <span class="form-text text-muted">{{ $field->helper_message }}</span>
        </div>
    @endforeach
</div>