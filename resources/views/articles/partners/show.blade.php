@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        @include('articles.breadcrumbs', ['category' => $article->category])
        <div class="subheader pb-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    @php
                        $orgName = $article->extraFields->where('name', 'Полное наименование компании')->first();
                        $offer = $article->extraFields->where('name', 'Ваше предложение партнеру')->first();
                    @endphp
                    <div class="col-lg-8 col-md-12 col-sm-12">
                        <div class="card card-custom gutter-b">
                            <div class="card-header align-items-center flex-wrap justify-content-between py-5 h-auto">
                                <div class="d-flex align-items-center my-2">
                                    <a href="{{ back()->getTargetUrl() }}" class="btn btn-clean btn-icon btn-sm mr-6" data-inbox="back">
                                        <i class="flaticon2-left-arrow-1"></i>
                                    </a>
                                </div>
                                <div class="d-flex align-items-center justify-content-end text-right my-2">
                                    <span class="font-size-h3 font-weight-bold mr-4" data-toggle="dropdown">
                                        {{ $article->name }}
{{--                                        @if($orgName){{ $orgName->pivot->value }}@endif  --}}
                                    </span>
                                </div>
                            </div>
                            <form class="form">
                                <div class="card-body">
                                    <div class="font-size-lg mb-5">Дата публикации: {{ $article->created_at->format('d.m.Y') }}</div>
                                    <div class="font-size-lg">
                                        <h5>Предложение партнеру:</h5>
                                        <div class="bg-gray-100 rounded-sm mb-5 mt-5">
                                            <div class="p-4">
                                                <span class="mt-2">
                                                    @if($offer){{ $offer->pivot->value }}@endif
                                                </span>
                                            </div>
                                        </div>
                                        <p></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card card-custom">
                            <div class="card-header card-header-tabs-line">
                                <div class="card-toolbar">
                                    <ul class="nav nav-tabs nav-boldest nav-tabs-line">
                                        @foreach($article->category->extraFieldGroups as $group)
                                            @foreach($group->steps as $step)
                                                <li class="nav-item">
                                                    <a class="nav-link @if($loop->first)active @endif" data-toggle="tab" href="#step-{{ $step->id }}">{{ $step->name }}</a>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    @foreach($article->category->extraFieldGroups as $group)
                                        @foreach($group->steps as $step)
                                            <div class="tab-pane fade @if($loop->first)active @endif show" id="step-{{ $step->id }}" role="tabpanel">
                                                <div class="font-size-lg mb-5">
                                                    @foreach($step->fields as $field)
                                                        @php $value = $field->articleValues()->where('article_id', $article->id)->first() @endphp
                                                        @if(!$value) @continue @endif
                                                        @if($field->is_hidden) @continue @endif
                                                        <label class="font-weight-bold">{{ $field->name }}</label>
                                                        <p class="text-dark-50 px-5">
                                                            @if($field->type == \App\Models\ExtraField::TYPE_DATE)
                                                                {{ \Carbon\Carbon::parse($value->pivot->value)->format('d M Y') }}
                                                            @elseif($field->type == \App\Models\ExtraField::TYPE_URL)
                                                                <a href="{{ $value->pivot->value }}">Ссылка</a>
                                                            @elseif($field->type == \App\Models\ExtraField::TYPE_FILE)
                                                                @foreach($value->pivot->value ?? [] as $value)
                                                                    <a download="{{ $value->original_name }}" href="{{ $value->download_link }}">
                                                                        {{ $value->original_name }}
                                                                    </a>
                                                                @endforeach
                                                            @elseif($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)
                                                                {{ implode(', ', json_decode($value->pivot->value) ?? []) }}
                                                            @else
                                                                {{ $value->pivot->value }}
                                                            @endif
                                                        </p>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
