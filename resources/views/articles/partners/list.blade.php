<div id="articles-list" class="" style="">
    @foreach($partners as $article)
        <div class="d-flex align-items-center mb-3">
            <div class="d-flex flex-column flex-grow-1">
                <div class="d-block">
                    <span class="text-muted font-weight-bold mt-2">{{ $article->created_at->format('d.m.Y') }}</span>
                </div>                
                <a href="{{ route('slug', $article->getSlugHierarchy()) }}" class="text-dark text-hover-primary mb-1 font-size-h5 font-weight-bold">{{ $article->name }}</a>
                    <!--<span style="width: 80px; text-muted" class="mr-4">{{ $article->created_at->format('d.m.Y') }}</span>
                    <span style="width: fit-content;" class="label font-weight-bold label-lg  label-light-primary label-inline">{{ $article->category->name }}</span>-->
            </div>
            <div class="d-flex align-items-center py-2">
                <a href="{{ route('slug', $article->getSlugHierarchy()) }}" class="btn btn-icon btn-icon-primary btn-light">  <i class="la la-angle-right"></i>
                </a>
            </div>
        </div>
    @endforeach
    {{ $partners->links() }}
</div>
