<script>
    "use strict";

    // Class definition
    var KTWizard2 = function () {
        // Base elements
        var _wizardEl;
        var _formEl;
        var _wizardObj;
        var _validations = [];

        // Private functions
        var _initWizard = function () {
            // Initialize form wizard
            _wizardObj = new KTWizard(_wizardEl, {
                startStep: 1, // initial active step number
                clickableSteps: false  // allow step clicking
            });

            // Validation before going to next page
            _wizardObj.on('change', function (wizard) {
                if (wizard.getStep() > wizard.getNewStep()) {
                    return; // Skip if stepped back
                }

                // Validate form before change wizard step
                var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step

                if (validator) {
                    validator.validate().then(function (status) {
                        if (status == 'Valid') {
                            wizard.goTo(wizard.getNewStep());

                            $('.select2').select2();
                            KTUtil.scrollTop();
                        } else {
                            KTUtil.scrollTop();
                        }
                    });
                }

                return false;  // Do not change wizard step, further action will be handled by he validator
            });

            // Change event
            _wizardObj.on('changed', function (wizard) {
                KTUtil.scrollTop();
            });

            // Submit event
            _wizardObj.on('submit', function (wizard) {
                let validator = _validations[wizard.getStep() - 1];
                console.log(validator)
                validator.validate().then(function (status) {
                    console.log(status)
                    if (status == 'Valid') {
                        _formEl.submit(); // Submit form
                    } else {
                        KTUtil.scrollTop();
                    }
                });
            });
        }

        var _initValidation = function () {
            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            // Step 1
            @foreach($category->extraFieldGroups as $fieldGroup)
                @foreach($fieldGroup->steps as $step)
                    _validations.push(FormValidation.formValidation(
                        _formEl,
                        {
                            fields: {
                                @foreach($step->fields->where('is_required', true) as $field)
                                    'extraFields[{{ $field->id }}][value]': {
                                        validators: {
                                            notEmpty: {
                                                message: 'Поле ' + '{{ $field->name }}' + ' должно быть заполнено.',
                                            }
                                        }
                                    },
                                @endforeach
                            },
                            plugins: {
                                // trigger: new FormValidation.plugins.Trigger(),
                                bootstrap: new FormValidation.plugins.Bootstrap()
                            }
                        }
                    ));
                @endforeach
            @endforeach
        }

        return {
            // public functions
            init: function () {
                _wizardEl = KTUtil.getById('kt_wizard');
                _formEl = KTUtil.getById('kt_form');

                _initWizard();
                _initValidation();
            }
        };
    }();

    jQuery(document).ready(function () {
        KTWizard2.init();
    });

</script>