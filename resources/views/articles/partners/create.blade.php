@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('partners.index') }}" class="text-muted">Площадка кооперации</a>
                        </li>

                        <li class="breadcrumb-item text-muted">
                            <a href="" class="text-muted">Анкета участника площадки кооперации</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom">
                <div class="card-body p-0">
                    <div class="wizard wizard-2" id="kt_wizard" data-wizard-state="first" data-wizard-clickable="false">
                        <!--begin: Wizard Nav-->
                        <div class="wizard-nav flex-lg-shrink-0 w-lg-300px w-xl-375px border-right py-8 px-8 py-lg-20 px-lg-10">
                            <div class="wizard-steps">
                                @foreach($category->extraFieldGroups as $group)
                                    @foreach($group->steps as $step)
                                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="@if($loop->first) current @else pending @endif">
                                            <div class="wizard-icon">
                                                <span class="wizard-number">{{ $step->number }}</span>
                                                <span class="wizard-check">
                                                    <span class="svg-icon svg-icon-2x">
                                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Check.svg-->
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002)"></path>
                                                            </g>
                                                        </svg>
                                                        <!--end::Svg Icon-->
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="wizard-label">
                                                <h3 class="wizard-title">{{ $step->name }}</h3>
                                                <div class="wizard-desc">{{ $step->description }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                        <!--end: Wizard Nav-->
                        <!--begin: Wizard Body-->
                        <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                            <!--begin: Wizard Form-->
                            <div class="row">
                                <div class="offset-xxl-2 col-xxl-8">
                                    <form enctype="multipart/form-data" class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{ route('partners.save') }}" id="kt_form">
                                        @csrf
                                        <div class="form-group">
                                            <h4 class="mb-5 font-weight-bold text-dark">Заголовок Вашего предложения</h4>
                                            <input type="visible" class="form-control form-control-solid" name="name" value="" placeholder="Ограничение заголовка 200 символов" maxlength="200">
                                            <span class="form-text text-muted">Кратко опишите Ваше предложение парнтнеру</span></div>
                                        <input type="hidden" name="category_id" value="{{ $category->id }}">

                                        @foreach($category->extraFieldGroups as $group)
                                            @foreach($group->steps as $step)
                                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                    <h4 class="mb-5 font-weight-bold text-dark">{{ $step->helper_message }}</h4>
                                                    @include('articles/partners/tab')
                                                </div>
                                            @endforeach
                                        @endforeach

                                        @include('articles/partners/actions')
                                    </form>
                                </div>
                                <!--end: Wizard-->
                            </div>
                            <!--end: Wizard Form-->
                        </div>
                        <!--end: Wizard Body-->
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
    <script src="{{ asset('theme/assets/js/pages/features/forms/widgets/select2.js') }}"></script>

    @include('articles/partners/wizard-script')
@endsection
