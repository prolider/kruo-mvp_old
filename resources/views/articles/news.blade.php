@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        @include('articles.breadcrumbs', ['category' => $category])
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Row-->
                <div class="row">
                    <div class="col-md-9">
                        <!--begin::List Widget 1-->
                        <div class="card card-custom card-stretch gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">{{ isset($category) ? $category->name : 'Статьи' }}</h3>
                                </div>
                            </div>
                            <!--begin::Body-->
                            @include('articles.list')
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 1-->
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@section('js')

    <script>
        $(document).ready(function() {
            $('.fields-select').on('change', function() {
                let form = $('#fields-select-form');

                let action = form.attr('action');
                let data = form.serializeArray();

                $.post(action, data, function(response) {
                    $('#articles-list').replaceWith(response.view);
                });
            });
        });
    </script>
@endsection
