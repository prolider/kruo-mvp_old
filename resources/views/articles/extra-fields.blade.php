<div class="form-group" id="extra-fields">
    @foreach($category->extraFieldGroups ?? [] as $group)
        @foreach($group->fields as $field)
            <label>{{ $field->name }}</label>
            <input class="form-control" name="extraFields[{{$field->id}}][value]">
        @endforeach
    @endforeach
</div>