<div class="col-md-4">
    @if($category && $category->extraFieldGroups->count() > 0)
        <div class="card card-custom mb-2">
            <div class="card-body px-5">
                <div class="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                    <div class="card-body px-5">
                        <form id="fields-select-form" action="{{ route('api.articles.search') }}">
                            @csrf
                            @if($category)
                                <input type="hidden" name="category_id" value="{{ $category->id }}">
                            @endif
                            @foreach($category->extraFieldGroups as $extraFieldGroup)
                                @foreach($extraFieldGroup->fields as $field)
                                    @if($field->type !== \App\Models\ExtraField::TYPE_FILE)
                                        <div class="row">
                                            <div class="col-md-6">
                                                {{ $field->name }}
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control fields-select" name="fields[{{ $field->id }}]">
                                                    <option></option>
                                                    @foreach($field->articleValues as $value)
                                                        @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)
                                                            @foreach($value->pivot->value as $option)
                                                                <option value="{{ $option }}">{{ $option }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="{{ $value->pivot->value }}">{{ $value->pivot->value }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="card card-custom card-stretch">
        <div class="card-body px-5">
            <div class="navi navi-hover navi-active navi-link-rounded navi-bold navi-icon-center navi-light-icon">
                <div class="card-body px-5">
                    @include('articles.categories-list')
                </div>
            </div>
        </div>
    </div>
</div>
