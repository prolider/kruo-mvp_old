<ul>
    @foreach($categories as $rootCategory)
        <li>
            <a href="{{ route('articles.index', ['category_id' => $rootCategory->id]) }}">{{ $rootCategory->name }}</a>
        </li>

        @if($rootCategory->childCategories->count() > 0)
            @include('articles.categories-list', ['categories' => $rootCategory->childCategories])
        @endif
    @endforeach
</ul>
