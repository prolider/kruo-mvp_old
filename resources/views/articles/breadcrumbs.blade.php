<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
        <!--<h5 class="text-dark font-weight-bold my-1 mr-5">Список статей</h5>-->
        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">
            <li class="breadcrumb-item text-muted">
                {{ __('breadcrumbs.You_are_here') }}
            </li>
            @if (isset($category) && $category)
                @foreach($category->getHierarchy()  as $hiearchyCategory)
                    @if(!$loop->last || isset($article))
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('slug', $hiearchyCategory->getSlugHierarchy()) }}">{{ $hiearchyCategory->name }}</a>
                        </li>
                    @else
                        <li class="breadcrumb-item text-muted">
                            {{ $hiearchyCategory->name }}
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>
    </div>
</div>
