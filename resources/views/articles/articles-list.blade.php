@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        @include('articles.breadcrumbs', ['category' => $category])
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Row-->
                <div class="row">
                    @if ($category->tag !== \App\Models\Category::TAG_NEWS)
                        @if(isset($category) && $category && $category->extraFieldGroups->count() > 0)
                            <div class="col-md-4">                            
                                <div class="card card-custom gutter-b">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="la la-filter text-primary"></i>
                                            </span>
                                            <h3 class="card-label">{{ __('support-measures.selection_by_parameters') }}</h3>
                                        </div>
                                    </div>
                                    <div class="card-body">

                                        <form id="fields-select-form" class="mb-3" action="{{ route('api.articles.search') }}">
                                            @csrf
                                            <input type="hidden" name="category_id" value="{{ $category->id }}">
                                            <input id="path" type="hidden" name="path">

                                            @foreach($category->extraFieldGroups as $extraFieldGroup)
                                                @foreach($extraFieldGroup->filterableFields() as $field)
                                                    @if($field->type !== \App\Models\ExtraField::TYPE_FILE)
                                                        <div class="form-group">
                                                            <label>{{ $field->name }}</label>
                                                            @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)
                                                                <select class="form-control fields-select select2" name="fields[{{ $field->id }}][]" multiple="multiple">
                                                            @else
                                                                <select class="form-control fields-select" name="fields[{{ $field->id }}][]">
                                                                    <option>Все</option>
                                                            @endif
                                                                @foreach($field->details['options'] ?? [] as $option => $optionValue)
                                                                    <option value="{{ $optionValue }}">{{ $optionValue }}</option>
                                                                @endforeach
{{--                                                                @else--}}
{{--                                                                    @foreach($field->articleValues->unique('pivot.value') as $value)--}}
{{--                                                                        <option value="{{ $value->pivot->value }}">{{ $value->pivot->value }}</option>--}}
{{--                                                                    @endforeach--}}
{{--                                                                @endif--}}
                                                            </select>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endforeach

                                            <button type="reset" onclick="resetFilter()" class="btn btn-primary btn-shadow font-size-md">Сбросить фильтр</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-md-4"> 
                                <div class="card card-custom gutter-b">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <span class="card-icon">
                                                <i class="la la-filter text-primary"></i>
                                            </span>
                                            <h3 class="card-label">{{ __('support-measures.selection_by_parameters') }}</h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>{{ __('support-measures.filtering_parameters') }}</p>
                                    </div>
                                </div>
                            </div>
                        @endif                        
                    @endif
                    <div class="col-md-8">
                        <!--begin::List Widget 1-->
                        <div class="card card-custom card-stretch gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    @if ($category->tag !== \App\Models\Category::TAG_NEWS)
                                        <h3 class="card-label">{{ $category->name }}</h3>
                                    @else
                                        <h3 class="card-label">{{ __('app.news') }}</h3>
                                    @endif
                                </div>
                            </div>
                            <!--begin::Body-->
                            @include('articles.list')
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 1-->
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@section('js')

    <script>
        $(document).ready(function() {
            $('.fields-select').on('change', function() {
                filterContent()
            });
        });

        $(document).ready(function() {
            $('.select2').select2({
                placeholder: "Все",
            });
        });

        function resetFilter() {
            $('#fields-select-form').trigger('reset');
            $('.select2').val('').trigger('change');

            filterContent();
        }

        function filterContent() {
            $('#path')[0].value = window.location.pathname;

            let form = $('#fields-select-form');
            let action = form.attr('action');
            let data = form.serializeArray();
            data = data.filter(item => item.value != "Все");

            $.post(action, data, function(response) {
                $('#articles-list').replaceWith(response.view);
            });
        }
    </script>
@endsection
