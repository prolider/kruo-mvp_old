@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        @include('articles.breadcrumbs', ['category' => $article->category])
        <div class="subheader pb-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">{{ $article->name }} </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card card-custom gutter-b">
                            <div class="card-body font-size-lg">
                                @if ($article->extraFields->count() > 0)
                                    <div class="bg-gray-100 mb-5">
                                        <div class="p-4">
                                            @foreach($article->category->extraFieldGroups as $group)
                                                @foreach($group->fields as $field)
                                                    @php $value = $field->articleValues()->where('article_id', $article->id)->first() @endphp
                                                    @if($field->is_hidden || !$value) @continue @endif
                                                    <p class="mb-2">
                                                    @if($field->type == \App\Models\ExtraField::TYPE_DATE)
                                                        <b>{{ $field->name }}</b>: {{ \Carbon\Carbon::parse($value->pivot->value)->format('d.m.Y') }}
                                                    @elseif($field->type == \App\Models\ExtraField::TYPE_URL)
                                                        @if(json_decode($value->pivot->value)->url != "")
                                                        <b>{{ $field->name }}</b>:
                                                            <a href="//{{ json_decode($value->pivot->value)->url }}" target="_blank">
                                                                {{ json_decode($value->pivot->value)->name ?? json_decode($value->pivot->value)->url }}
                                                            </a>
                                                        @endif
                                                    @elseif($field->type == \App\Models\ExtraField::TYPE_FILE)
                                                        <b>{{ $field->name }}</b>:
                                                        @foreach($value->pivot->value ?? [] as $file)
                                                            <a style="display: block" download="{{ $file->original_name }}" href="{{ $file->download_link }}">
                                                                {{ $file->original_name }}
                                                            </a>
                                                        @endforeach
                                                    @elseif($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)
                                                        <b>{{ $field->name }}</b>: {{ implode(', ', json_decode($value->pivot->value)) }}
                                                    @else
                                                        <b>{{ $field->name }}</b>: {{ $value->pivot->value }}
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                                @if ($article->category->tag === \App\Models\Category::TAG_NEWS)
                                    <div class="d-block">
                                        <span class="text-muted font-weight-bold mt-2">{{ $article->created_at->format('d.m.Y') }}</span>
                                    </div>
                                @endif
                                <p style="word-wrap: anywhere">
                                    {!! $article->content !!}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 flex-row-auto" id="kt_profile_aside">
                        @if ($articles)
                            <div class="card card-custom gutter-b">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">{{ $article->category->name }}</h3>
                                    </div>
                                </div>
                                <div class="card-body pt-4">
                                    <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                        @foreach($articles as $other)
                                        <div class="navi-item mb-1 @if ($article->id == $other->id)current-article @endif">
                                            @if ($article->category->tag === \App\Models\Category::TAG_NEWS)
                                                <span class="text-muted font-weight-bold pl-4 mt-2">{{ $other->created_at->format('d.m.Y') }}</span>
                                            @endif
                                            <a href="{{ route('slug', $other->getSlugHierarchy()) }}" class="navi-link py-3">
                                                <span class="navi-text">{{ $other->name }}</span>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card card-custom gutter-b">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">{{ __('support-measures.categories') }}</h3>
                                    </div>
                                </div>
                                @if($parentCategory)
                                    <div class="card-body pt-4">
                                        <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                            @foreach($parentCategory->categories as $category)
                                            <div class="navi-item mb-1">
                                                <a href="{{ route('slug', $category->getSlugHierarchy()) }}" class="navi-link py-3">
                                                    <span class="navi-text">{{ $category->name }}</span>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
