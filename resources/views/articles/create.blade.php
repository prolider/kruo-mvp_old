@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h1 class="text-dark font-weight-bold my-1 mr-5">Анкета участника</h1>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    

    <div class="d-flex flex-column-fluid" style="padding-bottom: 25px;">
        <!--begin::Container-->
        <div class="container">
            <div class="card card-custom">
                <div class="card-body p-0">
                    <!--begin: Wizard-->
                    <div class="wizard wizard-2" id="kt_wizard" data-wizard-state="first" data-wizard-clickable="false">
                        <!--begin: Wizard Nav-->
                        <div class="wizard-nav flex-lg-shrink-0 w-lg-300px w-xl-375px border-right py-8 px-8 py-lg-20 px-lg-10">
                            <!--begin::Wizard Step 1 Nav-->
                            <div class="wizard-steps">
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                    <div class="wizard-icon">
                                        <span class="wizard-number">1</span>
                                        <span class="wizard-check">
                                            <span class="svg-icon svg-icon-2x">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Check.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">Об участнике</h3>
                                        <div class="wizard-desc">Данные о своей компании</div>
                                    </div>
                                </div>
                                <!--end::Wizard Step 1 Nav-->
                                <!--begin::Wizard Step 2 Nav-->
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="pending">
                                    <div class="wizard-icon">
                                        <span class="wizard-number">2</span>
                                        <span class="wizard-check">
                                            <span class="svg-icon svg-icon-2x">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Check.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </div>
                                    <div class="wizard-label">
                                        <h3 class="wizard-title">Для партнеров</h3>
                                        <div class="wizard-desc">Требования и ожидания от партнеров</div>
                                    </div>
                                </div>
                                <!--end::Wizard Step 2 Nav-->
                                
                                
                                
                                
                            </div>
                        </div>
                        <!--end: Wizard Nav-->
                        <!--begin: Wizard Body-->
                        <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                            <!--begin: Wizard Form-->
                            <div class="row">
                                <div class="offset-xxl-2 col-xxl-8">
                                    <form class="form fv-plugins-bootstrap fv-plugins-framework" action="{{ route('articles.save') }}" id="kt_form">
                                    @csrf
                                        <!--begin: Wizard Step 1-->
                                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                            <h4 class="mb-10 font-weight-bold text-dark">Заполните данные о своей компании</h4>
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Полное наименование компании</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg" name="name" placeholder="Укажите юридическое название" value="">
                                                <span class="form-text text-muted">Например, ООО "Фирма Плюс"</span>
                                            <div class="fv-plugins-message-container"></div></div>
                                            <!--end::Input-->
                                            
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Контактный телефон</label>
                                                        <input type="tel" class="form-control form-control-solid form-control-lg" name="phone" placeholder="+7 900 000-00-00" value="">
                                                        
                                                    <div class="fv-plugins-message-container"></div></div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control form-control-solid form-control-lg" name="phone" placeholder="Email" value="">
                                                        
                                                    <div class="fv-plugins-message-container"></div></div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Веб-сайт</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="phone" placeholder="Укажите веб-сайт" value="">
                                                        <span class="form-text text-muted">Например, www.site.com</span>
                                                    <div class="fv-plugins-message-container"></div></div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group fv-plugins-icon-container">
                                                        <label>Год основания</label>
                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="phone" placeholder="Укажите год" value="">
                                                        
                                                    <div class="fv-plugins-message-container"></div></div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>Сферы деятельности</label>
                                                
                                                <select class="form-control form-control-solid select2 form-control-lg select2-hidden-accessible" id="kt_select2_3" name="category_id" multiple="" data-select2-id="kt_select2_3" tabindex="-1" aria-hidden="true">
                                                    <optgroup label="Доступен множественный выбор">
                                                        <option value="AK" selected="selected" data-select2-id="2">Легкая промышленность</option>
                                                        <option value="HI">Тяжелая промышленность</option>
                                                        <option value="AZ">Логистика</option>
                                                        <option value="CO">Мебельное производство</option>
                                                        <option value="ID">Производство продуктов питания</option>
                                                        <option value="MT">Строительство</option>
                                                        <option value="CA">Переработка</option>
                                                        <option value="NV">Сельское хозяйство</option>
                                                        <option value="OR">Производство комплектующих</option>
                                                        <option value="WA">Дистрибуция</option>
                                                    </optgroup>
                                                    
                                                </select>
                                                
                                                <span class="form-text text-muted">Выберите сферы Вашей компании</span>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                    <label for="exampleTextarea">Производимая продукция</label>
                                                    <textarea class="form-control form-control-solid form-control-lg" id="exampleTextarea" name="content" placeholder="Перечислите виды продукции..."></textarea>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Объемы производства</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg" name="fname" placeholder="Укажите с единицами измерений и периодом..." value="">
                                                
                                            <div class="fv-plugins-message-container"></div></div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                    <label for="exampleTextarea">Основные клиенты</label>
                                                    <textarea class="form-control form-control-solid form-control-lg" id="exampleTextarea" placeholder="Перечислите основных клиентов Вашей компании..."></textarea>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                    <label for="exampleTextarea">Перспективные планы увеличения мощности производства/объема продаж в РФ</label>
                                                    <textarea class="form-control form-control-solid form-control-lg" id="exampleTextarea" placeholder="Указать целевой год и показатель..."></textarea>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group fv-plugins-icon-container">
                                                <label>Планируемый объем инвестиций по локализации в РФ, млн. руб</label>
                                                <input type="text" class="form-control form-control-solid form-control-lg" name="fname" placeholder="Укажите значение..." value="">
                                                
                                            <div class="fv-plugins-message-container"></div></div>
                                            <!--end::Input-->
                                            
                                        </div>
                                        <!--end: Wizard Step 1-->
                                        <!--begin: Wizard Step 2-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <h4 class="mb-10 font-weight-bold text-dark">Заполните требования к партнерам или ожиданиях от сотрудничества</h4>
                                            <!--begin::Input-->
                                            <!--begin::Input-->
                                            <p>TODO: Поля из БД</p>

                                            @include('articles.extra-fields')
                                            <!--end::Input-->
                                        </div>
                                        <!--end: Wizard Step 2-->
                                        
                                        <!--begin: Wizard Actions-->
                                        <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                            <div class="mr-2">
                                                <button type="button" id="prev-step" class="btn btn-light-primary font-weight-bolder px-10 py-3" data-wizard-type="action-prev">
                                                <span class="svg-icon svg-icon-md mr-3">
                                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-left.svg-->
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                            <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                                                        </g>
                                                    </svg>
                                                    <!--end::Svg Icon-->
                                                </span>Шаг назад</button>
                                            </div>
                                            <div>
                                                <button type="button" class="btn btn-success font-weight-bolder px-10 py-3" data-wizard-type="action-submit">Отправить
                                                <span class="svg-icon svg-icon-md ml-3">
                                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Check.svg-->
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                            <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002)"></path>
                                                        </g>
                                                    </svg>
                                                    <!--end::Svg Icon-->
                                                </span></button>
                                                <button type="button" id="next-step" class="btn btn-primary font-weight-bolder px-10 py-3" data-wizard-type="action-next">Далее
                                                <span class="svg-icon svg-icon-md ml-3">
                                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
                                                            <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                        </g>
                                                    </svg>
                                                    <!--end::Svg Icon-->
                                                </span></button>
                                            </div>
                                        </div>
                                        <!--end: Wizard Actions-->
                                    <div></div><div></div><div></div><div></div><div></div></form>
                                </div>
                                <!--end: Wizard-->
                            </div>
                            <!--end: Wizard Form-->
                        </div>
                        <!--end: Wizard Body-->
                    </div>
                    <!--end: Wizard-->
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>

@endsection

@section('js')
    <link href="{{ asset('theme/assets/css/pages/wizard/wizard-2.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ asset('theme/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/features/forms/editors/ckeditor-classic.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/features/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/features/forms/widgets/bootstrap-select.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/custom/wizard/wizard-2.js') }}"></script>
    <script>
        $(document).ready(function() {
            ClassicEditor
                .create( document.querySelector( '#article-content' ) )
                .then( editor => {
                    console.log( editor );
                } )
                .catch( error => {
                    console.error( error );
                });

            loadFields()

            $('#category-select').on('change', fn => loadFields());
            var width = $('.form').width();
            $('.select2').css('width', width);
        });

        $(window).resize(function() {
            var width = $('.form').width();
            $('.select2').css('width', width);
        });

        function loadFields() {
            let categoryId = $('#category-select').val();

            $.get('/api/articles/' + categoryId + '/extra-fields', [], function(data) {
                $('#extra-fields').replaceWith(data.view);
            });
        }
    </script>
@endsection