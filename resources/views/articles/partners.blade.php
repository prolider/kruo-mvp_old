@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Площадка кооперации</h2>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <div class="d-flex flex-column-fluid mb-5">
        <!--begin::Container-->
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="la la-filter text-primary"></i>
                                    </span>
                                    <h3 class="card-label">Подбор по параметрам</h3>
                                </div>
                            </div>
                            <div class="card-body px-5">
                                <form class="form" id="fields-select-form" action="{{ route('api.partners.search') }}">
                                    @csrf
                                    @php $isFilterable = false; @endphp
                                    @if($category && $category->extraFieldGroups->count() > 0)
                                        <input type="hidden" name="category_id" value="{{ $category->id }}">
                                        @foreach($category->extraFieldGroups as $extraFieldGroup)
                                            @foreach($extraFieldGroup->filterableFields() as $field)
                                                @if($field->type !== \App\Models\ExtraField::TYPE_FILE)
                                                    <div class="row">
                                                        <div class="col-md-12 mb-2">
                                                            {{ $field->name }}
                                                        </div>
                                                        <div class="col-md-12 mb-5">
                                                            <select class="form-control fields-select" name="fields[{{ $field->id }}]">
                                                                <option>Все</option>
                                                                @foreach($field->articleValues->unique('pivot.value') as $value)
                                                                    <option value="{{ $value->pivot->value }}">{{ $value->pivot->value }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    @if ($field->is_filterable)
                                                        @php $isFilterable = true; @endphp
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endforeach

                                        @if ($isFilterable)
                                            <button type="reset" onclick="resetFilter()" class="btn btn-primary btn-shadow font-size-md">Сбросить фильтр</button>
                                        @else
                                            <p>Параметры фильтрации не заданы</p>
                                        @endif
                                    @else
                                        <p>Параметры фильтрации не заданы</p>
                                    @endif
                                </form>
                            </div>
                        </div>
                </div>

                <!--begin::Card-->
                <div class="col-md-8">
                    <div class="card card-custom">
                        <div class="card-body">

                            @can('canCreatePartners')
                                <!--begin::Search Form-->
                                <div class="mb-7">
                                    <div class="row align-items-center mb-5">
                                        <div class="col-lg-3 col-xl-3 mt-5 mb-5 mt-lg-0">
                                            <a href="{{ route('partners.create') }}" class="btn btn-primary btn-shadow font-size-md"><i class="la la-plus"></i>Стать участником</a>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Search Form-->
                            @endcan

                            @include('articles.partners.list')
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>


@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('.fields-select').on('change', function() {
                filterContent()
            });
        });

        function resetFilter() {
            $('#fields-select-form').trigger('reset');

            filterContent();
        }

        function filterContent() {
            let form = $('#fields-select-form');

            let action = form.attr('action');
            let data = form.serializeArray();
            data = data.filter(item => item.value != "Все");

            $.post(action, data, function(response) {
                $('#articles-list').replaceWith(response.view);
            });
        }
    </script>
    <link href="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css">


@endsection
