@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content d-flex flex-column flex-column-fluid">
            @include('articles.breadcrumbs')

            <div class="d-flex flex-row row">
                @if ($category->tag !== \App\Models\Category::TAG_NEWS)
                    @include('articles.filter')
                @endif

                @include('articles.list')
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.fields-select').on('change', function() {
                let form = $('#fields-select-form');

                let action = form.attr('action');
                let data = form.serializeArray();

                $.post(action, data, function(response) {
                    $('#articles-list').replaceWith(response.view);
                });
            });
        });
    </script>
@endsection
