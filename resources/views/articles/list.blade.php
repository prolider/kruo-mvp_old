<div class="card-body pt-1" id="articles-list">
    @php
        $route = isset($category) && $category && $category->tag === \App\Models\Category::TAG_MEASURES ? 'measures.show' : 'articles.show';
    @endphp
    @foreach($articles as $article)
        <div class="d-flex align-items-center mb-3">
            <div class="d-flex flex-column flex-grow-1">
                @if (isset($category) && $category->tag === \App\Models\Category::TAG_NEWS)
                    <div class="d-block">
                        <span class="text-muted font-weight-bold mt-2">{{ $article->created_at->format('d.m.Y') }}</span>
                    </div>
                @endif
                <a href="{{ route('slug', $article->getSlugHierarchy()) }}" class="text-dark text-hover-primary mb-1 font-size-lg font-weight-bolder">{{ $article->name }}</a>
            </div>
            <div class="d-flex align-items-center py-2">
                <a href="{{ route('slug', $article->getSlugHierarchy()) }}" class="btn btn-icon btn-icon-primary btn-light">  <i class="la la-angle-right"></i>
                </a>
            </div>
        </div>
    @endforeach
    <div class="d-flex justify-content-between align-items-center flex-wrap">
        {{ $articles->links() }}
    </div>
</div>
