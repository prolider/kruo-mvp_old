@php
    $withMenu =  isset($withMenu) ? $withMenu : true;
    $withHeader =  isset($withHeader) ? $withHeader :  true;
    $withFooter =  isset($withFooter) ? $withFooter : true;
    $withWrapper =  isset($withWrapper) ? $withWrapper : true;
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>{{ setting('site.title') }}</title>
        <meta name="description" content="{{ setting('site.description') }}">
        <meta name="keywords" content="">
        <meta name="twitter:title" content="{{ setting('site.title') }}"/>
        <meta name="twitter:description" content="{{ setting('site.description') }}"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700" />
        <script src="https://kit.fontawesome.com/1b8e599b2e.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="/css/titan.css">
        <link href="{{ asset('theme/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
        <!-- <link href="{{ asset('theme/assets/plugins/global/style.bundle.table.css') }}" rel="stylesheet" type="text/css" /> -->
        <link href="{{ asset('theme/assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/assets/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/assets/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('theme/assets/css/pages/login/login-1.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('theme/assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/wizard.css') }}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
        <link rel="stylesheet" href="/css/errors.css">        
        <script src="/js/app.js"></script>
    </head>
    <body id="kt_body" class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-minimize-hoverable page-loading">
    <div id="preloader" style="display:none;z-index: 9999; position: fixed; width: 100%; height: 100%; background-color: grey; opacity: 60%">
        <div class="spinner-border" role="status" style="position: fixed; left: 50%; top:50%; color: black; opacity: 100% !important;">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    @php
        $logoData = json_decode(setting('site.logo'));
        $logo = $logoData[0]->download_link ?? asset('images/logo-1.svg');

        if ($user = auth()->user()) {
            if ($user->role->name !== 'admin' && $user->role->name !== 'leader') {
                $user = $user->role->name;
            } else {
                $user = 'operator';
            }
        } else {
            $user = null;
        }
    @endphp
    @if($withWrapper)
        <div id="kt_header_mobile" class="header-mobile align-items-center header-mobile-fixed">
            <a href="/">
                @if(app()->getLocale() === 'en')
                    <img alt="logo-mobile" src="{{ Voyager::image(json_decode(setting('site.mobile-logo-eng'))[0]->download_link ?? '')  }}" class="h-30px mt-1" />
                @else
                    <img alt="logo-mobile" src="{{ Voyager::image(json_decode(setting('site.mobile-logo'))[0]->download_link ?? '')  }}" class="h-30px mt-1" />
                @endif
            </a>
            <div class="d-flex align-items-center">
                <button class="btn btn-hover-text-primary p-0 ml-3" id="kt_aside_mobile_toggle">
                    <i class="icon-xl la la-bars menu-icon text-light"></i>
                </button>
                <!--<button class="btn p-0 burger-icon ml-5" id="kt_header_mobile_toggle">
                    <span></span>
                </button>-->
                <button class="btn btn-hover-text-primary p-0 ml-3" id="kt_header_mobile_topbar_toggle">
                    <i class="icon-xl la la-user-circle menu-icon text-light"></i>
                </button>
            </div>
        </div>
    @endif
    <div class="d-flex flex-column flex-root">
        @if($withMenu)
            <div class="d-flex flex-row flex-column-fluid page">
                <div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
                    <div class="brand flex-column-auto" id="kt_brand">
                        @if(app()->getLocale() === 'en')                        
                            <a href="/" class="brand-logo">
                                <img alt="Logo" src="{{ Voyager::image(json_decode(setting('site.logo-eng'))[0]->download_link ?? '')  }}" class="h-85px mt-14" />
                            </a>
                        @else 
                            <a href="/" class="brand-logo">
                                <img alt="Logo" src="{{ Voyager::image(json_decode(setting('site.logo'))[0]->download_link ?? '')  }}" class="h-85px mt-14" />
                            </a>
                        @endif

                    </div>
                        <div class="aside-menu-wrapper flex-column-fluid mt-10" id="kt_aside_menu_wrapper">
                            <div id="kt_aside_menu" class="aside-menu my-4 scroll" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
                                @if(app()->getLocale() === 'en')
                                    {{ menu('engsite', 'menu') }}
                                @else 
                                    {{ menu('site', 'menu') }}
                                    {{ $user ? menu($user, 'menu') : '' }}
                                @endif
                            </div>
                        </div>
                </div>
        @endif
            @if($withWrapper)<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">@endif
                @if($withHeader)
                    <div id="kt_header" class="header header-fixed">
                        <div class="container d-flex align-items-stretch justify-content-end">
                            <div class="topbar">
                                <div class="topbar-item ml-4">
                                    @auth
                                        @php
                                            $newMessagesCount = \App\Models\User::newMessagesCount();
                                            $notifications = \App\Models\Notification::user()->where('status', \App\Models\Notification::STATUS_NEW)->count();
                                            $events = \App\Models\Event::forUser()->current()->count();
                                        @endphp
                                        <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="false">
                                            <div class="btn btn-icon btn-clean btn-lg btn-dropdown">
                                                <i class="icon-xl la la-search text-info mr-1"></i>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="">
                                            <div class="quick-search quick-search-dropdown" id="">
                                                <form id="search_form" action="{{ route('search.index') }}" method="get">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="icon-xl la la-search text-info mr-1"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" name="search" class="form-control" placeholder="{{ __('app.search') }}" onsubmit="$('#search_form').submit()">
                                                        <button class="ml-2 btn btn-sm btn-primary">{{ __('app.search') }}</button>
                                                    </div>
                                                </form>
                                                <div class="quick-search-wrapper scroll ps" data-scroll="true" data-height="325" data-mobile-height="200" style="height: 325px; overflow: hidden;"><div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                                            </div>
                                        </div>

                                        <div class="btn btn-icon btn-clean btn-lg">
                                            <a class="nav-link" href="{{ route('notices.index') }}" alt="Уведомления" data-toggle="tooltip" data-placement="bottom" title="Уведомления">
                                                <i class="icon-xl la la-bell text-info"></i>
                                                <span class="label label-rounded @if ($notifications) label-danger @else label-primary @endif">{{ $notifications }}</span>
                                            </a>
                                        </div>

                                        <div class="btn btn-icon btn-clean btn-lg">
                                            <a class="nav-link" href="{{ route('events.index') }}" alt="События" data-toggle="tooltip" data-placement="bottom" title="События">
                                                <i class="icon-xl la la-calendar text-info mr-1"></i>
                                                <span class="label label-rounded @if ($events) label-danger @else label-primary @endif">{{ $events }}</span>
                                            </a>
                                        </div>

                                        <div class="btn btn-icon btn-clean btn-lg">
                                            <a class="nav-link" href="{{ route('discussions.index') }}" alt="Сообщения" data-toggle="tooltip" data-placement="bottom" title="Сообщения">
                                                <i class="icon-xl la la-envelope-o text-info"></i>
                                                    <span class="label label-rounded @if ($newMessagesCount) label-danger @else label-primary @endif">{{ $message_count }}</span>
                                            </a>
                                        </div>

                                        <div class="btn btn-icon btn-clean btn-lg" id="kt_quick_user_toggle">
                                            <a class="nav-link" alt="Профиль" data-toggle="tooltip" data-placement="bottom" title="Профиль">
                                                <i class="icon-xl la la-user-circle text-info mr-1"></i>
                                            </a>
                                        </div>
                                    @else
                                        <div class="topbar-item  mr-3" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="false">
                                            <div class="btn btn-icon btn-clean btn-lg btn-dropdown">
                                                <i class="icon-xl la la-search text-info mr-1"></i>
                                            </div>
                                        </div>

                                        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg" style="">
                                            <div class="quick-search quick-search-dropdown" id="test">
                                                <form id="search_form" action="{{ route('search.index') }}" method="get">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="icon-xl la la-search text-info mr-1"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" name="search" class="form-control" placeholder="{{ __('app.search') }}" onsubmit="$('#search_form').submit()">

                                                        <button class="ml-2 btn btn-sm btn-primary">{{ __('app.search') }}</button>
                                                    </div>
                                                </form>
{{--                                                <div class="quick-search-wrapper scroll ps" data-scroll="true" data-height="325" data-mobile-height="200" style="height: 325px; overflow: hidden;"><div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>--}}
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-center flex-wrap" >
                                            <a href="{{ route('login') }}" class="d-none d-sm-flex btn btn-fixed-height btn-bg-secondary btn-text-dark-50 btn-hover-text-primary btn-icon-primary font-size-lg px-5 my-1 mr-3">
                                                <i class="icon-lg la la-sign-in"></i>
                                                {{ __('app.signin') }}</a>
                                            <a href="{{ route('register') }}" id="kt_login_signup" class="d-none d-sm-flex btn btn-fixed-height btn-primary font-size-lg px-5 my-1">{{ __('app.signup') }}</a>
                                        </div>


                                        <div class="d-flex align-items-center flex-wrap" >
                                            <a href="{{ route('login') }}" class="d-flex d-sm-none btn btn-fixed-height btn-bg-secondary btn-text-dark-50 btn-hover-text-primary btn-icon-primary font-size-lg px-5 my-1 mr-3">
                                                <i class="icon-lg la la-sign-in"></i>
                                                </a>
                                            <a href="{{ route('register') }}" id="kt_login_signup" class="d-flex d-sm-none btn btn-fixed-height btn-primary font-size-lg px-5 my-1">
                                                <i class="icon-lg la la-user-plus"></i>
                                            </a>
                                        </div>
                                        <div class="dropdown ml-3">
                                            <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
                                                <div class="btn btn-icon btn-clean btn-dropdown btn-lg">
                                                    <img class="h-20px w-20px rounded-sm" src="{{ asset('media/svg/flags/' . app()->getLocale() . '.svg') }}" alt="">
                                                </div>
                                            </div>
                                        <div class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-122px, 72px, 0px);">
                                            <ul class="navi navi-hover py-4">
                                                @foreach(LaravelLocalization::getSupportedLocales() as $locale => $attributes)
                                                    @if(app()->getLocale() !== $locale)
                                                        <li class="navi-item">
                                                            <a href="{{ LaravelLocalization::getLocalizedURL($locale, null, [], true) }}" class="navi-link">
                                                                <span class="symbol symbol-20 mr-3">
                                                                    <img src="{{ asset('media/svg/flags/' . $locale . '.svg') }}" alt="">
                                                                </span>
                                                                <span class="navi-text">{{ $attributes['native'] }}</span>
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @endauth
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="content d-flex flex-column flex-column-fluid" style="padding: 0px;" id="kt_content">

                        @if (isset($exception))
                            <div class="flex-center position-ref full-height">
                                <div class="code">
                                    @yield('code')
                                </div>

                                <div class="message" style="padding: 10px;">
                                    @yield('message')
                                </div>
                            </div>
                        @else
                        <!--
                            
                            <div class="alert alert-custom alert-light-danger mb-0" role="alert">
                                <div class="alert-icon">
                                    <i class="flaticon-warning"></i>
                                </div>
                                <div class="alert-text">
                                Внимание! Портал работает в тестовом режиме. Просим не указывать информацию и не загружать документы, распространение которых может принести ущерб Вам или компании.
                                </div>
                                <button type="button" class="btn btn-primary btn-sm acceptcookies">
                                    Прочитано
                                </button>
                            </div>-->
                            <!-- END Bootstrap-Cookie-Alert -->
                            @yield('content')
                        @endif
                </div>
                @if($withFooter)
                    <div class="footer bg-white py-4 d-flex flex-lg-column mt-5" id="kt_footer">
                        <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
                            <div class="text-dark order-2 order-md-1 text-right">
                                <span class="text-muted font-weight-bold mr-2">2021 - {{ date('Y') }}©</span>
                                @if(app()->getLocale() === 'en')
                                    {{ setting('site.title-eng') }}                                    
                                    <div></br>
                                        <span class="">Support team</span>
                                        </br>
                                        <span class=""><a href="mailto:support@ulregion.com">support@ulregion.com</a></span>
                                    </div>
                                @else
                                    {{ setting('site.title') }}
                                    <div></br>
                                        <span class="">Есть вопросы? Напишите нам</span>
                                        </br>
                                        <span class=""><a href="mailto:support@ulregion.com">support@ulregion.com</a></span>
                                    </div>
                                @endif
                                
                            </div>
                            <div class="d-flex align-items-center justify-content-start">
                                <img class="mr-3" style="height: 100px" src="{{ asset('images/ulgov-logo.png') }}">
                                @if(app()->getLocale() === 'en')      
                                    <div class="nav nav-dark flex-column">
                                        <a target="_self" data-action="page" class="nav-link pr-2" href="{{ route('about') }}"> About</a>
                                        <a target="_self" data-action="page" class="nav-link pr-2" href="{{ route('faq') }}"> Common information</a>
                                        <a target="_self" data-action="page" data-id="17" class="nav-link" href="{{ route('policy') }}"> Privacy</a>
                                    </div>
                                @else                             
                                    <div class="nav nav-dark flex-column">
                                        <a target="_self" data-action="page" class="nav-link pr-2" href="{{ route('about') }}"> О портале</a>
                                        <a target="_self" data-action="page" class="nav-link pr-2" href="{{ route('faq') }}"> Справочная информация</a>
                                        <a target="_self" data-action="page" data-id="17" class="nav-link" href="{{ route('policy') }}"> Политика конфиденциальности</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            @if($withWrapper)
            </div>
            @endif
        </div>
    </div>
    @if($withWrapper)
        <div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
            <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
                <h3 class="font-weight-bold m-0">Профиль
                    <small class="text-muted font-size-sm ml-2"></small></h3>
                <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
                    <i class="ki ki-close icon-xs text-muted"></i>
                </a>
            </div>
            <div class="offcanvas-content pr-5 mr-n5">
                @auth
                <div class="d-flex align-items-center mt-5">
                    <div class="symbol symbol-100 mr-5">
                        <div class="symbol-label" style="background-image:url('{{ optional(auth()->user())->avatarUrl }}')"></div>
                    </div>
                    <div class="d-flex flex-column">
                        <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{ auth()->user()->name }} {{ auth()->user()->firstname }}</a>
                        <div class="navi mt-1">
                            <a href="#" class="navi-item">
                                <span class="navi-link p-0 pb-2">
                                    <span class="navi-icon mr-1">
                                        <span class="svg-icon svg-icon-lg svg-icon-primary">
                                            <!--begin::Svg Icon | path:theme/assets/media/svg/icons/Communication/Mail-notification.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                                    <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="navi-text text-muted text-hover-primary">{{ optional(auth()->user())->email }}</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                @endauth
                <div class="separator separator-dashed mt-8 mb-5"></div>
                <div class="navi navi-spacer-x-0 p-0">
                    @auth
                    <a href="{{ route('profile.index') }}" class="navi-item">
                        <div class="navi-link">
                            <div class="symbol symbol-40 bg-light mr-3">
                                <div class="symbol-label">
                                        <span class="svg-icon svg-icon-md svg-icon-danger">
                                            <!--begin::Svg Icon | path:theme/assets/media/svg/icons/Communication/Adress-book2.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24" />
                                                    <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
                                                    <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                </div>
                            </div>
                            <div class="navi-text">
                                <div class="font-weight-bold font-size-lg">Профиль</div>
                                <div class="text-muted">редактировать данные</div>
                            </div>
                        </div>
                    </a>
                    @if(auth()->user()->can('browse_admin'))
                        <a href="{{ route('voyager.tickets.index') }}" class="navi-item">
                            <div class="navi-link">
                                <div class="symbol symbol-40 bg-light mr-3">
                                    <div class="symbol-label">
                                        <span class="svg-icon svg-icon-md svg-icon-success">
                                            <!--begin::Svg Icon | path:/keen/theme/demo1/dist/assets/media/svg/icons/General/Settings-1.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
                                                    <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                </div>
                                <div class="navi-text">
                                    <div class="font-weight-bold font-size-lg">Панель управления</div>
                                    <div class="text-muted">настройки портала</div>
                                </div>
                            </div>
                        </a>
                    @endif
                    <span class="navi-item mt-2">
                        <span class="navi-link">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-sm btn-light-primary font-weight-bolder py-3 px-6">Выход</a>
                        </span>
                    </span>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    @endauth
                </div>
                <div class="separator separator-dashed my-7"></div>
            </div>
        </div>
    @endif
    <div id="kt_scrolltop" class="scrolltop">
        <span class="svg-icon">
            <!--begin::Svg Icon | path:/keen/theme/demo1/dist/assets/media/svg/icons/Navigation/Up-2.svg-->
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                    <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1"></rect>
                    <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero"></path>
                </g>
            </svg>
            <!--end::Svg Icon-->
        </span>
    </div>

    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3E97FF", "secondary": "#E5EAEE", "success": "#08D1AD", "info": "#844AFF", "warning": "#F5CE01", "danger": "#FF3D60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#DEEDFF", "secondary": "#EBEDF3", "success": "#D6FBF4", "info": "#6125E1", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
    <script src="{{ asset('theme/assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/widgets.js') }}"></script>
    <script src="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="/js/cookies.js"></script>
    <script>
        function disableScroll() {
            $('body').addClass('no-scroll');
        }

        function enableScroll() {
            $('body').removeClass('no-scroll');
        }

        $( document ).ajaxStart(function() {
            $('#preloader').show();
            disableScroll();
        });

        $( document ).ajaxStop(function() {
            $('#preloader').hide();
            enableScroll();
        });
    </script>

    
    @yield('js')

    @stack('javascript')
    </body>
</html>
