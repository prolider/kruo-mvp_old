@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        @include('articles.breadcrumbs')
        <div class="subheader pb-6 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5"> 
                        @if (isset($category) && $category)
                            {{ $category->name }}
                        @else 
                            {{ __('support-measures.support_measures') }}
                        @endif
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    @foreach($categories as $category)
                        <div class="col-lg-4">
                            <div class="card card-custom card-stretch gutter-b bg-diagonal bg-diagonal-light-primary">
                                <div class="card-body">
                                    <div class="d-flex flex-column justify-content-between ">
                                        <h4><span class="card-label ">{{ $category->name }}</span></h4>
                                        <p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">
                                            {{ $category->description }}
                                        </p>
                                        <div class="d-flex align-items-center justify-content-between mt-5">
                                            <a href="{{ route('slug', $category->getSlugHierarchy()) }}" class="btn btn-primary btn-shadow font-size-md">{{ __('support-measures.more_details') }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
