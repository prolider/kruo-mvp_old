@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('events.index') }}" class="text-muted">События</a>
                        </li>

                        <li class="breadcrumb-item text-muted">
                            <span class="text-muted">Создание</span>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Dashboard-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-lg-6">

                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">
                                Создать событие
                            </h3>
                        </div>

                        <!--begin::Form-->
                        <form method="POST" action="{{ route('events.save') }}" class="form" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Название (заголовок) события</label>
                                    <input name="name" class="form-control" placeholder="Введите название" value="{{ old('name') }}" required>
                                    @if($errors->has('name'))
                                        @foreach ($errors->get('name') as $error)
                                            <div class="alert-text text-danger">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Место</label>
                                    <input name="address" class="form-control" placeholder="Укажите место или платформу"  value="{{ old('address') }}" required>
                                    @if($errors->has('address'))
                                        @foreach ($errors->get('address') as $error)
                                            <div class="alert-text text-danger">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                        <label class="col-form-label">Тип события</label>
                                        <div class="dropdown bootstrap-select form-control dropup">
                                            <select name="event_type" class="form-control selectpicker" tabindex="null">
                                                @foreach(\App\Models\Event::EVENT_TYPE_NAMES as $value => $name)
                                                    <option value="{{ $value }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if($errors->has('event_type'))
                                            @foreach ($errors->get('event_type') as $error)
                                                <div class="alert-text text-danger">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                        <label class="col-form-label">Вид приглашения</label>
                                        <div class="dropdown bootstrap-select form-control dropup">
                                            <select name="invite_type" class="form-control selectpicker" tabindex="null">
                                                @foreach(\App\Models\Event::INVITE_TYPE_NAMES as $value => $name)
                                                    <option value="{{ $value }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if($errors->has('invite_type'))
                                            @foreach ($errors->get('invite_type') as $error)
                                                <div class="alert-text text-danger">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                        <label class="col-form-label">Дата начала события</label>
                                        <input name="start_at" type="datetime-local" class="form-control" value="{{ old('start_at') }}" placeholder="Выберите дату">
                                        @if($errors->has('start_at'))
                                            @foreach ($errors->get('start_at') as $error)
                                                <div class="alert-text text-danger">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 pl-0 mb-3">
                                        <label class="col-form-label">Дата окончания события</label>
                                        <input name="end_at" type="datetime-local" class="form-control" value="{{ old('end_at') }}" placeholder="Выберите дату">
                                        @if($errors->has('end_at'))
                                            @foreach ($errors->get('end_at') as $error)
                                                <div class="alert-text text-danger">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label pl-4">Выберите получателей</label>
                                    <div class="col-12">
                                        <select class="form-control select2" name="users[]" multiple="" tabindex="-1" aria-hidden="true">
                                            @foreach($userGroups as $group => $users)
                                                <optgroup label="{{ $group ?: 'Компания не указана' }}">
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->fullName }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        @if($errors->has('users'))
                                            @foreach ($errors->get('users') as $error)
                                                <div class="alert-text text-danger">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                                <div id="kt_repeater_1">
                                    <label>Прикреление файлов</label>
                                    <div class="row px-4">
                                        <div data-repeater-list="files" class="col-8">
                                            <div data-repeater-item class="form-group row align-items-center">
                                                <div class="custom-file">
                                                    <input type="file" name="file" class="custom-file-input" id="customFile">
                                                    <label class="custom-file-label text-muted selected" for="customFile"></label>
                                                </div>
                                                <div class="col-4 pt-3 pl-0">
                                                    <a href="javascript:;" data-repeater-delete="" class="btn font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>Удалить</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label> </label>
                                            <a href="javascript:;" data-repeater-create="" class="btn font-weight-bolder btn-light-primary">
                                                <i class="la la-plus"></i>Добавить поле</a>
                                        </div>

                                        @if($errors->has('files'))
                                            @foreach ($errors->get('files') as $error)
                                                <div class="alert-text text-danger">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="exampleTextarea">Текст анонса</label>
                                    <textarea name="text" class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 81px;">{{ old('text') }}</textarea>
                                    @if($errors->has('text'))
                                        @foreach ($errors->get('text') as $error)
                                            <div class="alert-text text-danger">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    @endif
                                </div>



                                <div class="form-group">
                                    <label for="exampleTextarea">Подробное описание</label>
                                    <textarea name="description" class="richTextBox" id="text-editor">{{ old('description') }}</textarea>
                                    @if($errors->has('description'))
                                        @foreach ($errors->get('description') as $error)
                                            <div class="alert-text text-danger">
                                                {{ $error }}
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary btn-shadow font-size-md">Создать</button>
                                <button type="reset" class="btn btn-secondary ml-3">Отмена</button>
                            </div>
                        </form>
                        <!--end::Form-->

                    </div>


                </div>

            </div>

            <!--end::Row-->

            <!--end::Dashboard-->
        </div>
        <!--end::Container-->
    </div>
@endsection

@section('js')
    <script src="{{ asset('theme/assets/plugins/custom/tinymce/tinymce.bundle.js') }}"></script>
    <script src="{{ asset('theme/assets/js/pages/features/forms/editors/tinymce.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#kt_repeater_1').repeater({
                initEmpty: false,

                show: function () {
                    $(this).slideDown();
                },

                hide: function (deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });

            tinymce.init({
                selector: '#text-editor',
                plugins: 'code, image, media',
                extended_valid_elements: 'span, image',
            })

            $('.select2').select2();
        })
    </script>
@endsection
