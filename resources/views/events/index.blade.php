@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Все события</h2>
                    @can('canCreateEvents', App\Models\Event::class)
                        <a href="{{ route('events.create') }}" class="btn btn-primary"><i class="la la-plus"></i> Создать событие</a>
                    @endcan
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-12">
                    <div class="accordion accordion-solid accordion-panel accordion-svg-toggle pb-5" id="accordionExample8">
                        @foreach($events as $event)
                            @php $mutedClass = $event->isPassed() ? 'bg-secondary' : ''; @endphp
                            <div class="card {{ $mutedClass }}">
                                <div class="card-header @if($event->isPassed()) bg-secondary @endif" id="headingTwo8">
                                    <div class="card-title {{ $mutedClass }} collapsed" data-toggle="collapse" data-target="#collapse-{{ $event->id }}" aria-expanded="false">

                                        <div class="card-title {{ $mutedClass }}"><i class="icon-md text-dark la la-calendar-check pr-1"></i>{{ $event->event_dates }}</div>
                                        <div class="card-title {{ $mutedClass }}">{{ $event->event_type_name }}</div>
                                        @if($event->isPassed())
                                            <div class="card-title {{ $mutedClass }} text-muted">
                                                Событие завершено
                                            </div>
                                        @endif
                                        <div class="card-label">
                                            <span class="mr-0">
                                                <i class="icon-md text-dark la la-map-marker pr-1"></i>
                                            </span>
                                            {{ $event->address }}
                                        </div>

                                        <span class="svg-icon">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <div class="col-12 card-label font-size-h5 py-4 pl-5 text-dark bg-secondary">{{ $event->name }}</div>

                                </div>
                                <div id="collapse-{{ $event->id }}" class="collapse" data-parent="#accordionExample8" style="">
                                    <div class="card-body pt-3">
                                        <p class="font-size-h6"><i class="icon-md text-dark la la-clock pr-1"></i>Время: {{ $event->event_times }}</p>
                                        <p class="">{!! $event->text !!}</p>
                                        <div class="btn-group mr-3">
                                            <a class="btn btn-light-primary font-weight-bold px-6" href="{{ route('events.show', $event) }}">
                                                Подробнее
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <!--end::Row-->
            </div>
            <!--end::Container-->
        </div>

        <!--end::Entry-->
    </div>
@endsection
