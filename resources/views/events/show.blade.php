@extends('layouts.app')

@section('content')
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-1">
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">

                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">

                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('events.index') }}" class="text-muted">Все события</a>
                    </li>

                    <li class="breadcrumb-item text-muted">
                        <span class="text-muted">{{ $event->name }}</span>
                    </li>
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page Heading-->
        </div>
    </div>
    <div class="subheader pb-lg-8 subheader-transparent" id="kt_subheader">

        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h2 class="text-dark font-weight-bold my-1 mr-5">
                        {{ $event->name }}                        
                    </h2>
                    @if($event->isCurrentUserIsAuthor())
                    <a class="btn btn-primary" href="{{ route('events.edit', $event) }}"><i class="la la-sync"></i> Редактировать</a>
                    @endif
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Row-->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
                                <h3 class="card-label pr-3"><i class="icon-lg text-dark la la-calendar-check pr-2"></i>{{ $event->event_dates }}</h3>
                                <h3 class="card-label pr-3"><i class="icon-lg text-dark la la-clock pr-2"></i>Время: {{ $event->event_times }}</h3>

                            </div>

                        </div>
                        <div class="card-body font-size-lg">
                            <p><b>Дата публикации:</b> {{ $event->created_at->format('d.m.Y') }}</p>
                            <p><b>Место проведения:</b> {{ $event->address }}</p>
                            <p><b>Тип мероприятия:</b> {{ $event->event_type_name }}</p>
                            <p><b>Вид приглашения:</b> {{ $event->invite_type_name }}</p>
                            {!! $event->text !!}

                            @if($event->fileUrls)
                                <p class="mt-2"><b>Прикрепленные файлы:</b></p>
                                <div class="custom-file mt-2">
                                    @foreach($event->fileUrls as $fileName => $url)
                                    <a download="{{ $fileName }}" href="{{ $url }}" class="py-1 mr-3">
                                        <span class="mr-0">
                                            <i class="icon-xl text-info la la-file-download"></i>
                                        </span>
                                        <span class="navi-text">{{ $fileName }}</span>
                                    </a>
                                    @endforeach
                                </div>
                            @endif
                            @if($event->description)
                                <div>{!! $event->description !!}</div>
                            @endif
                        </div>

                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Container-->
            </div>

            <!--end::Entry-->
        </div>
        <!--end::Content-->

    </div>
@endsection