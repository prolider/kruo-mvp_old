<div id="form-builder"></div>
<input type="hidden" name="{{ isset($row) ? $row->field : 'form_builder' }}" value="">

@push('javascript')
    <script>
      window.formBuilderData = {!! $content !!}
      $('form').on('submit', function (e) {
        $('input[name="{{ isset($row) ? $row->field : 'form_builder' }}"]').val(JSON.stringify(window.formBuilder.getData()));
      });
    </script>

    <script src="/js/forms/form-builder.js"></script>
@endpush
