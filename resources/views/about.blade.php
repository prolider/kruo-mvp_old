@extends('layouts.app')

@section('content')    
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5"> 
                    <h2 class="text-dark font-weight-bold my-1 mr-5">{{ __('app.about') }}</h2> 
                </div>
            </div>                
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container"> 
            <div class="d-flex row">
                <div class="flex-row-auto col-12 col-sm-6 col-md-4 col-xl-3" id="kt_content_sidebar">
                    <div class="card card-custom gutter-b">
                        <div class="card-body pt-15">
                                <div class="text-center mb-7">
                                    <div class="">
                                        <img src="{{ asset('images/logo-qw.svg') }}" alt="image">                                                
                                    </div>
                                    <h4 class="font-weight-bold my-2">Корпорация развития Ульяновской области</h4>
                                    <div class="text-muted mb-2">Акционерное общество</div>
                                    
                                </div>
                                <div class="mb-7 text-center">
                                    <a href="#" target="_blank" class="btn btn-icon btn-circle btn-light-facebook mr-2">
                                        <i class="socicon-facebook icon-1x"></i>
                                    </a>
                                    <a href="#" target="_blank" class="btn btn-icon btn-circle btn-light-twitter mr-2">
                                        <i class="socicon-twitter icon-1x"></i>
                                    </a>
                                    <a href="#" target="_blank" class="btn btn-icon btn-circle btn-light-instagram">
                                        <i class="socicon-instagram icon-1x"></i>
                                    </a>
                                </div>
                                <div class="text-center text-lg my-3">
                                    <span class="my-2">432071, г. Ульяновск, <br>ул. Рылеева, д. 41</span>
                                </div>
                                <div class="text-center text-lg font-weight-bolder my-3">
                                    <span class="my-2"><a href="tel:+78422737001" target="_blank">+7 (8422) 73-70-01</a></span>
                                </div>
                                <div class="text-center text-lg font-weight-bolder my-3">
                                <a href="//www.ulregion.com" target="_blank">www.ulinvest.com</a>
                                </div>
                                <div class="text-center text-lg font-weight-bolder my-3">
                                    <a href="mailto:info@ulregion.com" target="_blank">info@ulregion.com</a>                                
                            </div>                            
                            <div class="pt-4">
                                <a href="tel:+78422737001" target="_blank" class="btn btn-light-primary font-weight-bolder text-center btn-block">Связаться с Корпорацией</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex-row-auto col-12 col-sm-6 col-md-8 col-xl-9">                                                
                    <div class="card card-custom gutter-b">
                        <div class="card-body pt-15">                        
                            @if(app()->getLocale() === 'en')      
                                {!! setting('site.about-eng') !!}
                            @else                       
                                {!! setting('site.about') !!}
                            @endif 
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>      
@endsection
