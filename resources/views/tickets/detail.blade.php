@extends('layouts.app')

@php
    $formData = $ticket->formResult->result;
    $formData = is_array($formData) ? $formData : json_decode($formData, true);

    $form = $ticket->formResult->form->form_data;
    $form = is_array($form) ? $form : json_decode($form, true);
    $form = isset($form[0][0]) ? array_reduce($form, function ($a, $c) {
        return array_merge($a, $c);
    }, []) : $form;
@endphp

@section('content')
	<div aria-live="polite" aria-atomic="true" style="position: relative;">
		<div style="position: absolute; top: 10px; right: 10px;">
			@if (session('comment') || ($errored = $errors->first()))
				<div class="toast show alert alert-{{ isset($errored) ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
					Комментарий {{ isset($errored) ? 'не был добавлен' : 'успешно добавлен' }}
					<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif
		</div>
	</div>

	<div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
		<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center flex-wrap mr-1">
				<!--begin::Page Heading-->
				<div class="d-flex align-items-baseline flex-wrap mr-5">

					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
						<li class="breadcrumb-item text-muted">
							<a href="{{ route('ticket.index') }}" class="text-muted">Мои заявки</a>
						</li>

						<li class="breadcrumb-item text-muted">
							<a href="#" class="text-muted">Заявка</a>
						</li>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Page Heading-->
			</div>
		</div>
	</div>

	<div class="container ticket-detail">
		<div class="row">
			<div class="col-lg-6">
				<div class="content">
					<div class="card card-custom gutter-b">
						<div class="card-header">
							<h3 class="card-title">
							Заявка #{{ $ticket->id }} от {{ $ticket->created_at ? $ticket->created_at->format('d.m.Y'): 'Неизвестно' }}
							</h3>
							<div class="d-flex align-items-center mr-2">
								<span class="label label-light-success label-lg font-weight-bold label-inline">{{ $ticket->status_name }}</span>
							</div>
						</div>
						<div class="card-body">
							<h4 class="card-title">{{ $ticket->formResult->form->service->name }}</h4>
							<div class="result">
								@foreach($form['steps'] as $step)
									@foreach($step['data'] as $row)
										@foreach($row['cols'] as $col)
											@foreach($col['data'] as $field)

												@if (isset($field['value']))
												<div>
													<label>{{ $field['title'] }}</label>
													@if (isset($formData[$field['name']]))
														<div class="custom-file ml-2">
															@php
															$res = [];
															if (isset($field['options'])) {
																$res = collect($formData[$field['name']])
																	->map(function ($val) use ($field) {

																		$r = collect($field['options'])
																			->first(function ($v) use ($val) {
																				return $v['value'] === $val;
																			});
																		return $r['name'];
																	})
																	->toArray();
															}
															@endphp
															@if (isset($field['options']))
																@foreach($res as $opt)
																	<p class="user-selected-options" style="margin-bottom: 1rem;">{{ $opt }}</p>
																@endforeach
															@else
																@if (isset($field['type']) && $field['type'] === 'file')
																	@foreach($formData[$field['name']] as $file)
																		<p class="user-file" style="margin-bottom: 1rem;">
																			<a href="/storage/{{ $file['path'] }}" target="_blank"><i class="icon-xl text-info la la-file-download"></i> {{ $file['name'] }}</a>
																		</p>
																	@endforeach
																@else
																	{{ $formData[$field['name']] }}
																@endif
															@endif
														</div>
													@else
														<div>-</div>
													@endif
												</div>
												@endif
											@endforeach
										@endforeach
									@endforeach
								@endforeach
							</div>
						</div>
					</div>
				</div>

				<div class="card card-custom gutter-b">
					<div class="d-flex card-header align-items-center justify-content-between flex-wrap card-spacer-x py-5">
						<div class="d-flex align-items-center mr-2 py-2">
							<div class="font-weight-bold font-size-lg mr-3">Комментарии</div>
						</div>
					</div>
					@forelse($ticket->comments as $comment)
						<div class="mb-3">
							<div class="shadow-xs" data-inbox="message">
								<div class="d-flex align-items-center card-spacer-x pt-6 pb-2">
									<span class="symbol symbol-50 mr-4">
										<span class="symbol-label" style="background-image: url('{{ $comment->user->avatarUrl }}')"></span>
									</span>
									<div class="d-flex flex-column flex-grow-1 flex-wrap mr-2">
										<div class="d-flex">
											<span class="font-size-lg font-weight-bolder text-dark-75 mr-2">{{ optional($comment->user)->name}} {{optional($comment->user)->firstname }}</span>
										</div>
										<span class="text-muted">{{ optional($comment->user)->organization_name }}</span>

									</div>
									<div class="d-flex align-items-center">
										@if($comment->user->id != auth()->user()->id)
											<select class="form-control selectpicker" id="status-select-{{ $comment->id }}" onchange="changeStatus('{{ $comment->id }}')">
												<option class="py-3" value="R" @if($comment->status == 'R')  @endif>Прочитано</option>
												<option class="py-3" value="N" @if($comment->status == 'N') selected @endif>Не прочитано</option>
											</select>
										@endif
									</div>
								</div>
								<div class="d-flex align-items-center card-spacer-x py-1 font-weight-bold text-muted mr-2">{{ $comment->created_at->format('d.m.Y, H:i') }}</div>
								<div class="card-spacer-x py-3 ">
									<p>{!! $comment->text !!}</p>
									<div class="d-flex align-items-center py-3">
										@foreach($comment->attachments as $attachment)
											<div class="mr-4">
												<a href="{{ asset("storage/{$attachment['path']}") }}" class="py-1">
													<span class="mr-0">
														<i class="icon-xl text-info la la-file-download"></i>
													</span>
													<span class="navi-text">{{ $attachment['name'] }}</span>
												</a>
											</div>
										@endforeach
									</div>

								</div>
							</div>


						</div>
					@empty
						<div class="card-spacer-x py-5">
							<p>Пока нет комментариев по текущей заявке.</p>
						</div>
					@endforelse
					<!--begin::Reply-->
					<div class="mb-3" id="kt_inbox_reply">

						<div class="card-body p-0">
							<!--begin::Form-->
							<form id="kt_inbox_reply_form" action="{{ route('ticket.comment.new', $ticket) }}" method="POST" enctype="multipart/form-data">
								{{ csrf_field() }}
								<!--begin::Body-->
								<textarea name="text" class="form-control border-0 ql-editor ql-blank px-8" placeholder="Введите текст..." style="min-height:200px" id="comment"></textarea>
								<!--end::Body-->
								<!--begin::Footer-->
								<div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
									<!--begin::Actions-->
									<div class="d-flex align-items-center mr-3">
										<!--begin::Send-->
										<div class="btn-group mr-3">
											<button class="btn btn-primary font-weight-bold px-6" type="submit" class="btn btn-brown">Отправить</button>


										</div>
										<!--end::Send-->
										<!--begin::Other-->
										<div class="custom-file" style="margin: 10px 0;">
										<label class="btn btn-light-primary mx-2" for="customFile">
											<i class="la la-paperclip"></i>Выберите файлы
											<input type="file" style="display: none;" class="custom-file-input" id="customFile" name="attachment[]" multiple>
											</label>
										</div>

										<!--end::Other-->
									</div>
									<div class="d-flex align-items-center mr-3">
										<!--begin::Send-->

										<button type="submit" form="discussion"class="btn btn-outline-warning mr-2 ml-5" data-toggle="tooltip" title="" data-original-title="В случае возникновения проблем по заявке или на проекте Вы можете обратиться к руководству через тревожную кнопку!">
											<i class="la la la-warning"></i>Возникли сложности?
										</button>
										<!--end::Other-->
									</div>
									<!--end::Actions-->

								</div>
								<!--end::Footer-->
							</form>
							<form action="{{ route('discussions.create') }}" id="discussion">
								<input type="hidden" name="is_alert" value="true">
								<input type="hidden" name="name" value="Заявка #{{ $ticket->id }} {{ $ticket->formResult->form->service->name }}">
							</form>
							<!--end::Form-->
						</div>

					</div>
					<!--end::Reply-->
				</div>
			</div>

			<div class="col-lg-4 flex-row-auto" id="kt_profile_aside">

				<!--begin::Nav Panels Wizard 1-->
				<div class="card card-custom gutter-b">
					<!--begin::Body-->
					<div class="card-body pt-4">

						<!--begin::User-->
						<div class="d-flex align-items-center py-5">
							@if ( is_null(auth()->user()->company->personal_manager))
								<div>Персональный менеджер не назначен</div>
							@else
								<div class="symbol symbol-60 symbol-xxl-90 mr-5 align-self-start align-self-xxl-center">
									<div class="symbol-label" style="background-image:url('{{ auth()->user()->company->personal_manager->avatarUrl }}')"></div>
									<i class="symbol-badge bg-success"></i>
								</div>
								<div>
									<span class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">{{ auth()->user()->company->personal_manager->name }} {{ auth()->user()->company->personal_manager->firstname }}</span>
									<div class="text-muted">АО "Корпорация развития Ульяновской области"</div>
									<div class="navi navi-bold navi-hover navi-active navi-link-rounded">
										<div class="navi-item mt-2">
											<div class="d-flex align-items-center mb-2">
												<a  href="{{ route('discussions.create') }}" class="text-hover-primary font-weight-bold navi-link py-4 active">Создать обращение</a>
												<a href="tel:{{ auth()->user()->company->personal_manager->phone }}" class="btn btn-icon btn-primary btn-lg mx-2" data-toggle="tooltip" data-original-title="Позвонить">
													<i class="la la-phone"></i>
												</a>

											</div>
										</div>
									</div>
								</div>
							@endif
						</div>
						<!--end::User-->
					</div>
					<!--end::Body-->
				</div>
				<!--end::Nav Panels Wizard 1-->

				<div class="accordion accordion-solid accordion-panel accordion-svg-toggle pb-5" id="accordion">
					<div class="card">
						<div class="card-header" id="headingOne8">
							<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne8" aria-expanded="false">
								<div class="card-label">Приглашение</div>
								<span class="svg-icon">
									<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon points="0 0 24 0 24 24 0 24"></polygon>
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
										</g>
									</svg>
									<!--end::Svg Icon-->
								</span>
							</div>
						</div>
						<div id="collapseOne8" class="collapse" data-parent="#accordion" style="">
							<div class="card-body">
								<div class="navi navi-bold navi-hover navi-active navi-link-rounded">

									@if($ticket->status === \App\Models\Ticket::STATUS_INVITE && !is_null($ticket->invite))
										<p>Дата визита {{ $ticket->invite->invite_time->format('d.m.Y') }}</p>
										<p>Время визита {{ $ticket->invite->invite_time->format('H:i') }}</p>
										<p>{{ optional(optional($ticket->invite)->window)->name ?? 'Министерство не указано' }}</p>
									@else
										<p>У вас нет приглашений</p>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style>
		.ticket-detail .custom-file-label::after {
			content: 'Выбрать';
		}

		.comment.new .card-header {
			background: var(--success);
		}
	</style>
@endsection

@section('js')
	<script>
		$('input[type="file"]').on('change', function (event) {
			var fileList = Array.from(event.target.files).map(f => f.name).join(', ');
			$(event.target).siblings('label').text(fileList);
		});

		function changeStatus(commentId) {
			const status = $('#status-select-' + commentId).val();

			const data = {
				status: status,
				_token: $('meta[name="csrf-token"]').attr('content')
			};

			$.post('/api/comments/' + commentId + '/change-status', data, function() {

			});
		}
	</script>

@endsection
@push('javascript')
	<script>
		$('.toast').toast('show');
	</script>
@endpush