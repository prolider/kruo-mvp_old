@extends('layouts.app')

@section('content')
    <div class="container content">
        @if(session('formSend') === 'success')
            <div id="ticketadd-alert" class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Заявка успешно создана!
            </div>
        @endif
        <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h1 class="text-dark font-weight-bold my-1 mr-5">Мои заявки</h1>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-body">
                        <!--begin::Search Form-->
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-3 col-xl-2 mt-5 mt-lg-0">
                                    <a href="{{ route('categories.index') }}" class="btn btn-primary"><i class="la la-plus"></i>Создать заявку</a>
                                </div>
                                    <div class="col-lg-9 col-xl-10">
                                        <div class="row align-items-center">
                                            <div class="col-md-4 my-2 my-md-0">
                                                <div class="input-icon">
                                                    <input type="text" class="form-control" placeholder="Поиск..." id="kt_datatable_search_query">
                                                    <span>
                                                        <i class="flaticon2-search-1 text-muted"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-4 my-2 my-md-0">
                                                <div class="d-flex align-items-center">
                                                    <label class="mr-3 mb-0 d-none d-md-block">Статус:</label>
                                                    <select name="status" class="form-control" id="kt_datatable_search_status">
                                                        <option value>Любой</option>
                                                        @foreach (App\Models\Ticket::STATUS_NAMES as $key => $status)
                                                            <option value="{{ $key }}">{{ $status }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <!--end::Search Form-->
                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" id="kt_datatable" style="">
                            <table class="datatable-table" style="" id="usertickets">
                                <thead class="datatable-head">
                                <tr class="datatable-row" style="left: 0px;">
                                    <th data-field="OrderID" class="datatable-cell datatable-cell-sort">
                                        <span style="width: 50px;">№</span>
                                    </th>
                                    <th data-field="CompanyName" class="datatable-cell datatable-cell-sort">
                                        <span style="width: 250px;">Наименование услуги</span>
                                    </th>
                                    <th data-field="ShipDate" class="datatable-cell datatable-cell-sort">
                                        <span style="width: 80px;">Дата</span>
                                    </th>
                                    <th data-field="Status" class="datatable-cell datatable-cell-sort">
                                        <span style="width: 150px;">Статус</span>
                                    </th>
                                    <th data-field="Minuser" class="datatable-cell datatable-cell-sort">
                                        <span style="width: 150px;">Менеджер корпорации</span>
                                    </th>
                                    <th data-field="Window" class="datatable-cell datatable-cell-sort">
                                        <span style="width: 150px;">Министерство</span>
                                    </th>
                                    <th class="datatable-cell datatable-cell-sort">
                                        <span style="width: 30px;"></span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="datatable-body">
                                @forelse($tickets as $ticket)
                                    <tr class="datatable-row">
                                        <td data-field="OrderID" class="datatable-cell" style="width: 50px;" >
                                            <span style="width: 50px;">{{ $ticket->id }}</span>
                                        </td>
                                        <td data-field="CompanyName" class="datatable-cell" style="width : 250px;">
                                            <span style="width: 250px;">
                                                <a href={{ route('ticket.show', $ticket) }}>
                                                    @if ($ticket->status === \App\Models\Ticket::STATUS_DRAFT || $ticket->status === \App\Models\Ticket::STATUS_CLIENT_WRITE)
                                                        {{ $ticket->formResult->form->service->name }}
                                                    @else
                                                        {{ $ticket->formResult->form->service->name }}
                                                    @endif
                                                </a>
                                            </span>
                                        </td>
                                        <td data-field="ShipDate" class="datatable-cell" style="width: 80px;">
                                            <span style="width: 80px;">
                                                {{ $ticket->created_at ? $ticket->created_at->format('d.m.Y'): 'Неизвестно' }}
                                            </span>
                                        </td>
                                        <td data-field="Status" arial-label="1" class="datatable-cell" style="width: 150px;">
                                            <span style="width: 150px;">
                                                <span class="label font-weight-bold label-lg
                                                @switch($ticket->status)
                                                @case('N')
                                                    label-light-primary
                                                    @break

                                                @case('C')
                                                    label-light-danger
                                                    @break

                                                @case('D')
                                                    label-light-success
                                                    @break

                                                @case('W')
                                                    label-light-warning
                                                    @break
                                                @default

                                                @endswitch
                                                    label-inline">
                                                {{ __("ticket.status.$ticket->status") }}
                                                </span>
                                            </span>
                                        </td>
                                        <td class="datatable-cell"  style="width : 150px">
                                            <span style="width:150px;">
                                                @if(isset(optional($ticket->operator)->name))
                                                    {{ optional($ticket->operator)->name }} {{ optional($ticket->operator)->firstname }}
                                                @else
                                                    Не назначен
                                                @endif
                                            </span>
                                        </td>
                                        <td class="datatable-cell"  style="width: 150px;">
                                            <span style="width: 150px;">
                                                @if(isset(optional($ticket->window)->name))
                                                    {{ (optional($ticket->window)->name) }}
                                                @else
                                                    Не указано
                                                @endif
                                            </span>
                                        </td>
                                        <td data-field="Actions" data-autohide-disabled="false" aria-label="null" class="datatable-cell" style="width: 30px;">
                                            <span style="overflow: visible; position: relative; width: 30px;">
                                                <div class="dropdown dropdown-inline">
                                                    <a href="javascript:;" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                                        <span class="svg-icon svg-icon-md">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <circle fill="#000000" cx="12" cy="5" r="2"></circle>
                                                                    <circle fill="#000000" cx="12" cy="12" r="2"></circle>
                                                                    <circle fill="#000000" cx="12" cy="19" r="2"></circle>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                                        @if ($ticket->status === \App\Models\Ticket::STATUS_NEW)
                                                            <form method="POST" action="{{ route('api.ticket.status', $ticket) }}">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="status" value="{{ \App\Models\Ticket::STATUS_CANCEL_REQUEST }}">
                                                                <button type="submit" class="btn btn-sm btn-primary">Запросить отмену</button>
                                                            </form>
                                                        @endif
                                                        @if (in_array($ticket->status, [\App\Models\Ticket::STATUS_DRAFT, \App\Models\Ticket::STATUS_NEED_DATA, \App\Models\Ticket::STATUS_CLIENT_WRITE]))
                                                            <a class="btn btn-sm btn-primary" href="{{ route('services.form', [$ticket->formResult->form->service, $ticket]) }}">
                                                                    Редактировать черновик
                                                                </a>
                                                        @endif
                                                        @if ($ticket->status === \App\Models\Ticket::STATUS_INVITE && !is_null($ticket->invite))
                                                            <p>Приглашение назначено на {{ $ticket->invite->invite_time->format('d.m.Y H:i') }} в {{ @$ticket->invite->window->name }}.</p>
                                                        @else

                                                        @endif
                                                    </div>
                                                </div>
                                            </span>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="8">У вас пока нет заявок</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="datatable-pager datatable-paging-loaded">
                                <ul class="datatable-pager-nav my-2 mb-sm-0">
                                    <li>
                                        <a title="First" class="datatable-pager-link datatable-pager-link-first datatable-pager-link-disabled" data-page="1" disabled="disabled">
                                            <i class="flaticon2-fast-back"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Previous" class="datatable-pager-link datatable-pager-link-prev datatable-pager-link-disabled" data-page="1" disabled="disabled">
                                            <i class="flaticon2-back"></i>
                                        </a>
                                    </li>
                                    <li style="display: none;">
                                        <input type="text" class="datatable-pager-input form-control" title="Page number">
                                    </li>
                                    <li>
                                        <a class="datatable-pager-link datatable-pager-link-number datatable-pager-link-active" data-page="1" title="1">1</a>
                                    </li>
                                    <li>
                                        <a title="Next" class="datatable-pager-link datatable-pager-link-next datatable-pager-link-disabled" data-page="1" disabled="disabled">
                                            <i class="flaticon2-next"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Last" class="datatable-pager-link datatable-pager-link-last datatable-pager-link-disabled" data-page="1" disabled="disabled">
                                            <i class="flaticon2-fast-next"></i>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#usertickets').DataTable({
                paging: true,
                ordering: true,
            });
        });
    </script>
    <link href="{{ asset('theme/assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('theme/assets/js/ticket/scripts.bundle.js') }}"></script>

@endsection
