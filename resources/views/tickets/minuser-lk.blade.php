@extends('layouts.app')

@section('content')
    <div class="container mt-2">
        <div class="content d-flex flex-column flex-column-fluid" id="app"></div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/minuser-lk.js') }}"></script>
@endsection
