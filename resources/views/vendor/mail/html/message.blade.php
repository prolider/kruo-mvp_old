@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
        <div style="background-color:#003366; color:#fff; padding:20px;">
            <h1 style="text-transform:uppercase;">{{ config('app.name') }}</h1>
        </div>
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
        <div style="background-color:#cccccc; padding: 20px;">
            Сообщение сформировано системой оповещений портала<br>
            {{ config('app.name') }}
        </div>
        <div style="background-color:#fff; padding: 20px;">
            <a href="https://ulinvestor.site" style="display: inline-block;">
                <img src="https://ulinvestor.site/images/message-logo-200.png" class="logo" alt="Личный кабинет инвестора Ульяновскоой области">
            </a>
        </div>           
        @endcomponent
    @endslot
@endcomponent
