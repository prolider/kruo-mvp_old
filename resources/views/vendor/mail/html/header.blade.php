<tr>
    <td class="header">
        <div>
            @if (trim($slot) === 'Laravel')
                <img src="https://ulinvestor.site/images/message-logo-200.png" class="logo" alt="Личный кабинет инвестора Ульяновскоой области">
            @else
                {{ $slot }}
            @endif
        </div>
    </td>
</tr>
