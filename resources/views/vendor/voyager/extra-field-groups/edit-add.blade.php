@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        </div><!-- panel-body -->

                        <div class="panel-footer text-right">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-success save" onclick="update(event)" title="Применить изменения">Применить</button>
                                <button type="submit" class="btn btn-primary save" name="_index" value="1" title="Сохранить и закрыть">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
                </div>
                @if($dataTypeContent->with_steps)
                <div class="panel panel-bordered form-edit-add">
                    @if($edit)
                        <div class="panel-heading">
                            <h4 class="m-2">Шаги формы</h4>
                        </div>
                        <div class="panel-body">
                            <div class="table steps">
                                <div class="flex fixed-top">
                                    <div class="flex-step-column">Название</div>
                                    <div class="flex-step-column">Номер</div>
                                    <div class="flex-step-column">Описание</div>
                                    <div class="flex-step-column">Текст-подсказка</div>
                                    <div class="flex-step-column"></div>
                                </div>

                                @foreach($dataTypeContent->steps as $step)
                                    <form method="POST" class="form" action="{{ route('api.extra_fields.steps.update', $step) }}">
                                        @csrf
                                        <div class="flex steps-row" id="step-row-{{ $step->id }}">
                                            <div class="flex-step-column">
                                                <input class="form-control" name="name" value="{{ $step->name }}">
                                            </div>
                                            <div class="flex-step-column">
                                                <input class="form-control" name="number" type="number" value="{{ $step->number }}">
                                            </div>
                                            <div class="flex-step-column">
                                                <input class="form-control" name="description" value="{{ $step->description }}">
                                            </div>
                                            <div class="flex-step-column">
                                                <input class="form-control" name="helper_message" value="{{ $step->helper_message }}">
                                            </div>
                                            <div class="flex-step-column">
                                                <button type="submit" class="btn btn-sm btn-success" title="Сохранить">
                                                    <i class="voyager-check"></i>
                                                </button>
                                                <button onclick="deleteStep({{ $step->id }})" type="submit" class="btn btn-sm btn-danger" title="Удалить">
                                                    <i class="voyager-trash"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                @endforeach
                                <form method="POST" id="add_step_form" class="form" action="{{ route('api.extra_fields.steps.save') }}" style="display: none">
                                    @csrf
                                    <div class="flex steps-row">
                                        <input type="hidden" name="extra_field_group_id" value="{{ $dataTypeContent->getKey() }}" required>
                                        <div class="flex-step-column">
                                            <input class="form-control" name="name" required>
                                        </div>
                                        <div class="flex-step-column">
                                            <input class="form-control" type="number" name="number" value="{{ optional($dataTypeContent->steps->last())->number + 1 }}" required>
                                        </div>
                                        <div class="flex-step-column">
                                            <input class="form-control" name="description" required>
                                        </div>
                                        <div class="flex-step-column">
                                            <input class="form-control" name="helper_message" required>
                                        </div>
                                        <div class="btn-group-sm">
                                            <button type="submit" class="btn btn-sm btn-warning" title="Сохранить">
                                                <i class="voyager-check"></i>
                                            </button>
                                            <button style="display: none" id="hide_add_step_form_btn" type="button" class="btn btn-sm btn-danger" title="Отменить">
                                                <i class="voyager-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                                <button id="show_add_step_form_btn" class="btn btn-warning" title="Добавить шаг">
                                    Добавить шаг
                                </button>
                                <button style="display: none" id="hide_add_step_form_btn" type="button" class="btn btn-danger" title="Отменить">
                                    Отменить
                                </button>
                            </div>
                            <p>Поля отмеченные цветом обязательны для заполнения</p>
                        </div>
                    @endif
                </div>
                @endif
                <div class="panel panel-bordered form-edit-add">
                    @if($edit)
                        <div class="panel-heading">
                            <h4>Поля свойств</h4>
                        </div>
                        <div class="panel-body">
                            <div class="table fields">
                                <div class="table">
                                    <div class="flex fixed-top">
                                        <div class="flex-table-column">Название</div>
                                        <div class="flex-table-column">Тип</div>
                                        <div class="flex-table-column">Значение по умолчанию</div>
                                        <div class="flex-table-column">Описание</div>
                                        <div class="flex-table-column">Обязательное</div>
                                        <div class="flex-table-column">Только чтение</div>
                                        <div class="flex-table-column">Поле фильтра</div>
                                        <div class="flex-table-column">Класс (col-md-*)</div>
                                        <div class="flex-table-column">Заглушка</div>
                                        <div class="flex-table-column">Подсказка</div>
                                        <div class="flex-table-column">Скрытый</div>
                                        @if($dataTypeContent->with_steps) <div class="flex-table-column">Шаг</div>@endif
                                        <div class="flex-table-column"></div>
                                    </div>

                                    @foreach($dataTypeContent->fields as $field)
                                        <form method="POST" id="field-form-{{ $field->id }}" class="form" action="{{ route('api.extra_fields.update', $field) }}" data-id="{{ $field->id }}">
                                            @csrf
                                            <div id="field-row-{{ $field->id }}" class="flex group-field">
                                                <div class="flex-table-column">
                                                    <input class="form-control" name="name" value="{{ $field->name }}">
                                                </div>
                                                <div class="flex-table-column">
                                                    <select class="form-control field-select" name="type" required>
                                                        @foreach(\App\Models\ExtraField::TYPES as $type => $name)
                                                            <option value="{{ $type }}" @if($type == $field->type) selected @endif>{{ $name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-control" name="default_value" value="{{ $field->default_value }}">
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-control" name="description" value="{{ $field->description }}">
                                                </div>

                                                <div class="flex-table-column">
                                                    <input class="form-check-input" name="is_required" type="checkbox" value="1" {{ $field->is_required ? 'checked' : '' }}>
                                                </div>

                                                <div class="flex-table-column">
                                                    <input class="form-check-input" name="read_only" type="checkbox" value="1" {{ $field->read_only  ? 'checked' : '' }}>
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-check-input" name="is_filterable" type="checkbox" value="1" {{ $field->is_filterable  ? 'checked' : '' }}>
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-control" name="container_class" placeholder="col-md-4 col-md-6" value="{{ $field->container_class }}">
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-control" name="placeholder" value="{{ $field->placeholder }}">
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-control" name="helper_message" value="{{ $field->helper_message }}">
                                                </div>
                                                <div class="flex-table-column">
                                                    <input class="form-check-input" name="is_hidden" type="checkbox" value="1" {{ $field->is_hidden  ? 'checked' : '' }}>
                                                </div>
                                                @if($dataTypeContent->with_steps)
                                                    <div class="flex-table-column">
                                                    <select class="form-control" name="extra_field_step_id" required>
                                                        @foreach($dataTypeContent->steps as $step)
                                                            <option value="{{ $step->id }}" @if($step->id == $field->step_id) selected @endif>{{ $step->name }} ({{ $step->number }})</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                @endif
                                                <div class="btn-group-sm">
                                                    <button type="button" onclick="updateExtraField({{ $field->id }})" class="btn btn-sm btn-success" title="Сохранить">
                                                        <i class="voyager-check"></i>
                                                    </button>
                                                    <button onclick="deleteExtraField({{ $field->id }})" type="button" class="btn btn-sm btn-danger" title="Удалить">
                                                        <i class="voyager-trash"></i>
                                                    </button>
                                                </div>
                                            </div>


                                            <div id="option-{{ $field->id }}" @if($field->type != \App\Models\ExtraField::TYPE_MULTISELECT && $field->type != \App\Models\ExtraField::TYPE_SELECT) style="display: none" @endif>
                                                <h5>Опции выбора:</h5>

                                                @foreach($field->details['options'] ?? [] as $option)
                                                    <div class="row form-group">
                                                        <div class="col-md-2">
                                                            <input class="form-control" name="options[]" value="{{ $option }}" readonly>
                                                        </div>
                                                        <div class="btn-group-sm">
                                                            <button type="button" onclick="$(this).parent().parent().remove()" class="btn btn-sm btn-danger" title="Удалить"><i class="voyager-trash"></i></button>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <button id="option-button-{{ $field->id }}" type="button" onclick="addOption('{{ $field->id }}')" class="btn btn-sm btn-warning"  @if($field->type != \App\Models\ExtraField::TYPE_MULTISELECT && $field->type != \App\Models\ExtraField::TYPE_SELECT) style="display: none" @endif>
                                                Добавить поле
                                            </button>
                                        </form>
                                    @endforeach
                                </div>
                                <form method="POST" id="add_field_form" class="form" action="{{ route('api.extra_fields.save') }}" style="display: none">
                                    @csrf
                                    <div class="flex group-field">
                                        <input type="hidden" name="extra_field_group_id" value="{{ $dataTypeContent->getKey() }}" required>
                                        <div class="flex-table-column">
                                            <input class="form-control" name="name" required>
                                        </div>
                                        <div class="flex-table-column">
                                            <select class="form-control" name="type" required id="fieldTypeSelect">
                                                @foreach(\App\Models\ExtraField::TYPES as $type => $name)
                                                    <option value="{{ $type }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-control" name="default_value">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-control" name="description">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-check-input" type="checkbox" name="is_required" value="1">
                                        </div>

                                        <div class="flex-table-column">
                                            <input class="form-check-input" type="checkbox" name="read_only" value="1">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-check-input" type="checkbox" name="is_filterable" value="1">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-control" name="container_class">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-control" name="placeholder">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-control" name="helper_message">
                                        </div>
                                        <div class="flex-table-column">
                                            <input class="form-check-input" type="checkbox" name="is_hidden" value="1">
                                        </div>
                                        @if($dataTypeContent->with_steps)
                                            <div class="flex-table-column">
                                                <select class="form-control" name="extra_field_step_id" required id="fieldTypeSelect">
                                                    @foreach($dataTypeContent->steps as $step)
                                                        <option value="{{ $step->id }}">{{ $step->name }} ({{ $step->number }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endif

                                            <div class="btn-group-sm">
                                                <button type="submit" class="btn btn-sm btn-warning" title="Сохранить">
                                                    <i class="voyager-check"></i>
                                                </button>
                                                <button style="display: none" id="hide_add_field_form_btn" type="button" class="btn btn-sm btn-danger" title="Отменить">
                                                    <i class="voyager-trash"></i>
                                                </button>
                                            </div>

                                    </div>
                                    <div id="select-options" style="display: none">
                                        <h5>Опции выбора:</h5>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <input class="form-control" name="options[]">
                                            </div>
                                            <div class="btn-group-sm">
                                                <button type="button" onclick="deleteSelectOption($(this))" class="btn btn-sm btn-danger" title="Удалить"><i class="voyager-trash"></i></button>


                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button id="show_add_field_form_btn" class="btn btn-warning">
                                    Добавить поле
                                </button>
                                <button id="select-options-button" style="display: none" type="button" onclick="addSelectOption()" class="btn btn-sm btn-warning">
                                    Добавить поле
                                </button>
                                <button style="display: none;" id="hide_add_field_form_btn" type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Отменить">
                                    Отменить
                                </button>
                            </div>
                            <p>Поля отмеченные цветом обязательны для заполнения</p>
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            $('#show_add_field_form_btn').on('click', function() {
                $('#add_field_form').show();
                $('#hide_add_field_form_btn').show();
                $('#show_add_field_form_btn').hide();
            });

            $('#hide_add_field_form_btn').on('click', function() {
                $('#add_field_form').trigger('reset');
                $('#add_field_form').hide();
                $('#hide_add_field_form_btn').hide();
                $('#show_add_field_form_btn').show();
            });

            $('#show_add_step_form_btn').on('click', function() {
                $('#add_step_form').show();
                $('#hide_add_step_form_btn').show();
                $('#show_add_step_form_btn').hide();
            });

            $('#hide_add_step_form_btn').on('click', function() {
                $('#add_step_form').trigger('reset');
                $('#add_step_form').hide();
                $('#hide_add_step_form_btn').hide();
                $('#show_add_step_form_btn').show();
            });

            $('#fieldTypeSelect').on('change', function() {
                $('#default-value-input').removeAttr('disabled');

                if ($(this).val() == {{ \App\Models\ExtraField::TYPE_SELECT }} || $(this).val() == {{ \App\Models\ExtraField::TYPE_MULTISELECT }}) {
                    $('#select-options').show();
                    $('#select-options-button').show();
                } else if($(this).val() == {{ \App\Models\ExtraField::TYPE_FILE }}) {
                    $('#default-value-input').attr('disabled', true);
                } else {
                    $('#select-options').hide();
                    $('#select-options-button').hide();
                }
            })

            $('form :input').on('change', function() {
                const fieldId = $(this).closest('form').data('id');

                const fieldType = $(this).closest('form').find('.field-select').val();

                if (fieldType == {{ \App\Models\ExtraField::TYPE_MULTISELECT}} || fieldType == {{ \App\Models\ExtraField::TYPE_SELECT}}) {
                    $('#option-' + fieldId).show();
                    $('#option-button-' + fieldId).show();
                } else {
                    $('#option-' + fieldId).hide();
                    $('#option-button-' + fieldId).hide();
                }
            });
        });

        function deleteSelectOption(button) {
            button.closest('div.row')[0].remove();
        }

        function addSelectOption() {
            $('#select-options').append('<div class="row form-group"><div class="col-md-2"><input class="form-control" name="options[]"></div><button type="button" onclick="deleteSelectOption($(this))" class="btn btn-sm btn-danger"><i class="voyager-trash"></i></button></div>')
        }

        function addOption(field) {
            $('#option-' + field).append('<div class="row form-group"><div class="col-md-2"><input class="form-control" name="options[]"></div><div class="btn-group-sm"><button type="button" onclick="$(this).parent().parent().remove()" class="btn btn-sm btn-danger"><i class="voyager-trash"></i></button></div></div>')
        }
        function deleteExtraField(fieldId) {
            $.post('/api/extra-fields/' + fieldId + '/delete', [], function (data) {
                $('#field-row-' + fieldId).remove();
            })
        }

        function updateExtraField(fieldId) {
            let form = $('#field-form-' + fieldId).serializeArray();

            $.post('/api/extra-fields/' + fieldId + '/update', form, function (data) {
                $('body').append('<div id="toast-container" class="toast-top-right"><div class="toast toast-success" aria-live="polite" style="opacity: 0.741711;"><div class="toast-message">Успешное обновлено</div></div></div>');
                setTimeout(function() {
                    $('#toast-container').remove();
                }, 1000);

            });
        }
        function deleteStep(stepId) {
            $.post('/api/extra-fields/steps/' + stepId + '/delete', [], function (data) {
                $('#step-row-' + stepId).remove();
            })
        }
    </script>
@stop
