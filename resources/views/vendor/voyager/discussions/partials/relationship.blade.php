@if(isset($options->model) && isset($options->type))

    @if(class_exists($options->model))

        @php
            $relationshipData = (isset($data)) ? $data : $dataTypeContent;
            $model = app($options->model);

            $selected_values = isset($options->loads) ? $model::with($options->loads) : $model::select();

            $selected_values = $selected_values->orderBy('created_at', 'asc')->where($options->column, '=', $relationshipData->{$options->key})->get()->map(function ($item, $key) use ($options) {
                $attributes = $options->attributes ?? [$options->label => $item->{$options->label}];
                $plucked = [];

                foreach ($attributes as $key => $value) {
                    if ($value->type === 'file') {
                        $plucked[$value->name] = json_decode($item->{$key});
                    } else {
                        $plucked[$value->name] = $item->{$key};
                    }
                }

                if (isset($options->loads)) {
                    return [
                        'data' => $plucked,
                        'model' => $options->load_model::find($item->{$options->load_key}),
                        'id' => $item->id,
                    ];
                }

                return ['data' => $plucked];
            })->all();
        @endphp

        @if(empty($selected_values))
            <p>{{ __('voyager::generic.no_results') }}</p>
        @else
            <ul>
                @foreach($selected_values as $selected_value)
                    <li>
                        <a href="{{ route($options->route_prefix . 'show', $selected_value['id']) }}">Сообщение</a> от {{ $selected_value['model']->{$options->load_label} }}
                        <ul>
                        @foreach ($selected_value['data'] as $key => $item)
                            <li>{{ $key }}:
                                @if (is_array($item))
                                    @if (!empty($item))
                                        @foreach ($item as $file)
                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) }}">
                                                {{ $file->name }}
                                            </a>
                                        @endforeach
                                    @else
                                        отсутствуют
                                    @endif
                                @else
                                    {{ $item }}
                                @endif
                            </li>
                        @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        @endif

    @else

        cannot make relationship because {{ $options->model }} does not exist.

    @endif

@endif
