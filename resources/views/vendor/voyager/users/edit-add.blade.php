@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop

@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <form class="form-edit-add" role="form"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data" autocomplete="off">
                            <!-- PUT Method if we are editing -->
                            @if(isset($dataTypeContent->id))
                                {{ method_field("PUT") }}
                            @endif
                            {{ csrf_field() }}

                            <div class="">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#params" data-toggle="tab" data-tab="params">Персональные данные</a>
                                    </li>
                                    <li>
                                        <a href="#passport" data-toggle="tab" data-tab="form">Паспортные данные</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active panel panel-bordered" id="params">
                                        <div class="row">
                                            @include('vendor.voyager.users.partials.params')
                                        </div>
                                    </div>
                                    <div class="tab-pane panel panel-bordered" id="passport">
                                        <div class="row">
                                            @include('vendor.voyager.users.partials.passport')
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary pull-right save">
                                {{ __('voyager::generic.save') }}
                            </button>
                            <button type="submit" class="btn btn-primary save" onclick="update(event, {{ $edit }})">Применить</button>
                        </form>

                        <iframe id="form_target" name="form_target" style="display:none"></iframe>
                        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
                            {{ csrf_field() }}
                            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $('select[name=role_id]').on('change', function() {
                let roleId = $('select[name=role_id]').val();

                if (roleId == '{{ \App\Models\Role::where("name", \App\Models\Role::ROLE_MINUSER)->value("id") }}') {
                    $('#window-select').show();
                } else {
                    $('#window-select').hide();
                }
            })
        });
    </script>
@stop
