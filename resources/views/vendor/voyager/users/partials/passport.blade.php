<div class="panel panel-bordered">
    <div class="panel-body row">
        <div class="form-group col-md-3">
            <label for="name">Viber</label>
            <input type="text" class="form-control" id="inn" name="viber" placeholder="Viber"
                   value="{{ old('viber', $dataTypeContent->viber ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Telegram</label>
            <input type="text" class="form-control" id="telegram" name="telegram" placeholder="Telegram"
                   value="{{ old('telegram', $dataTypeContent->telegram ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">Whatsapp</label>
            <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="Whatsapp"
                   value="{{ old('whatsapp', $dataTypeContent->whatsapp ?? '') }}">
        </div>
        <div class="form-group col-md-3">
            <label for="name">Должность</label>
            <input type="text" class="form-control" id="position" name="position" placeholder="Должность"
                   value="{{ old('position', $dataTypeContent->position ?? '') }}">
        </div>
    </div>
</div>
