<div class="panel panel-bordered">
    <div class="panel-body row">
        <div class="form-group col-md-3">
            <label for="name">ИНН</label>
            <input type="text" class="form-control" id="company_inn" name="company_inn" placeholder="ИНН"
                   value="{{ old('company_inn', $dataTypeContent->company_inn ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="name">ОГРН (ГОРНИП)</label>
            <input type="text" class="form-control" id="company_ogrn" name="company_ogrn" placeholder="ОГРН (ГОРНИП)"
                   value="{{ old('company_ogrn', $dataTypeContent->company_ogrn ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="company_kpp">КПП</label>
            <input type="text" class="form-control" id="company_kpp" name="company_kpp" placeholder="Полное наименование"
                   value="{{ old('company_name', $dataTypeContent->company_kpp ?? '') }}">
        </div>

        <div class="form-group col-md-3">
            <label for="company_registration_date">Дата регистрации</label>
            <input type="date" class="form-control" id="company_registration_date" name="company_registration_date" placeholder="Полное наименование"
                   @if(!is_null($dataTypeContent->company_registration_date))
                   value="{{ \Carbon\Carbon::parse($dataTypeContent->company_registration_date)->format('Y-m-d') }}"
                   @endif
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            <label for="company_name">Полное наименование</label>
            <input type="text" class="form-control" id="company_name" name="company_name" placeholder="ОГРН (ГОРНИП)"
                   value="{{ old('company_ogrn', $dataTypeContent->company_name ?? '') }}">
        </div>

        <div class="form-group col-md-6">
            <label for="company_address">Юридический адрес</label>
            <input type="text" class="form-control" id="company_address" name="company_address" placeholder="ИНН"
                   value="{{ old('company_inn', $dataTypeContent->company_address ?? '') }}">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            <label for="company_management_post">Должность руководителя</label>
            <input type="text" class="form-control" id="company_management_post" name="company_management_post" placeholder="ОГРН (ГОРНИП)"
                   value="{{ old('company_management_post', $dataTypeContent->company_management_post ?? '') }}">
        </div>

        <div class="form-group col-md-6">
            <label for="company_management_name">ФИО Руководителя</label>
            <input type="text" class="form-control" id="company_management_name" name="company_management_name" placeholder="ИНН"
                   value="{{ old('company_management_name', $dataTypeContent->company_management_name ?? '') }}">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label for="company_verification_status">Статус компании</label>
            <select id="company_verification_status" name="company_verification_status" class="form-control">
                <option
                    value="{{ \App\Models\Company::COMPANY_VERIFICATION_STATUS_NOT_VERIFIED }}"
                    @if($dataTypeContent->company_verification_status === \App\Models\Company::COMPANY_VERIFICATION_STATUS_NOT_VERIFIED)
                        selected
                    @endif
                >Не подтверждена</option>
                <option
                    value="{{ \App\Models\Company::COMPANY_VERIFICATION_STATUS_ON_CHECK }}"
                    @if($dataTypeContent->company_verification_status === \App\Models\Company::COMPANY_VERIFICATION_STATUS_ON_CHECK)
                        selected
                    @endif
                >На проверке</option>
                <option
                    value="{{ \App\Models\Company::COMPANY_VERIFICATION_STATUS_REJECT }}"
                    @if($dataTypeContent->company_verification_status === \App\Models\Company::COMPANY_VERIFICATION_STATUS_REJECT)
                        selected
                    @endif
                >Отклонена</option>
                <option
                    value="{{ \App\Models\Company::COMPANY_VERIFICATION_STATUS_ACCEPT }}"
                    @if($dataTypeContent->company_verification_status === \App\Models\Company::COMPANY_VERIFICATION_STATUS_ACCEPT)
                        selected
                    @endif
                >Подтверждена</option>
            </select>
        </div>
    </div>

    @if(!is_null($dataTypeContent->signature_file) && !is_null($dataTypeContent->statement_file))
    <div class="row">
        <div class="col-md-6">
            <p>
                <a href="/storage/{{ $dataTypeContent->statement_file['path'] }}" download="{{ $dataTypeContent->statement_file['name'] }}">Заявление</a>
            </p>
            <p>
                <a href="/storage/{{ $dataTypeContent->signature_file['path'] }}" download="{{ $dataTypeContent->signature_file['name'] }}">Подпись</a>
            </p>
        </div>
    </div>
    @endif
</div>
