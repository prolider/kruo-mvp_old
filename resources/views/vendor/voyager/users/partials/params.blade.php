

    <div class="panel panel-bordered">
        {{-- <div class="panel"> --}}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name">Фамилия</label>
                        <input type="text" class="form-control" id="name" name="firstname" placeholder="Фамилия"
                            value="{{ old('firstname', $dataTypeContent->firstname ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('voyager::generic.name') }}</label>
                        <input type="text" class="form-control" id="name" name="name"
                            placeholder="{{ __('voyager::generic.name') }}"
                            value="{{ old('name', $dataTypeContent->name ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="name">Отчество</label>
                        <input type="text" class="form-control" id="name" name="patronymic" placeholder="Отчество"
                            value="{{ old('patronymic', $dataTypeContent->patronymic ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label for="is_confirmed">Подтверждён</label>
                        <input type="checkbox" name="is_confirmed"
                            data-on="0" {!! $dataTypeContent->is_confirmed ? 'checked="checked"' : '' !!}
                            data-off="1">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name">Телефон</label>
                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="Телефон"
                            value="{{ old('phone', $dataTypeContent->phone ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="email">{{ __('voyager::generic.email') }}</label>
                        <input type="email" class="form-control" id="email" name="email"
                            placeholder="{{ __('voyager::generic.email') }}"
                            value="{{ old('email', $dataTypeContent->email ?? '') }}">
                    </div>

                    <div class="form-group">
                        <label for="password">{{ __('voyager::generic.password') }}</label>

                        <input type="password" class="form-control" id="password" name="password" value=""
                            autocomplete="new-password">
                        @if(isset($dataTypeContent->password))
                            <br>
                            <small>{{ __('voyager::profile.password_hint') }}</small>
                        @endif
                    </div>
                    <div class="form-group" id="window-select" @if(optional($dataTypeContent->role)->name != \App\Models\Role::ROLE_MINUSER) style="display: none" @endif>
                        <label for="default_role">Министерство</label>
                        @php
                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                            $row     = $dataTypeRows->where('field', 'user_belongsto_window_relationship')->first();
                            $options = $row->details;
                        @endphp
                        @include('voyager::formfields.relationship')
                    </div>

                    <div class="form-group">
                        <label for="default_role">{{ __('voyager::profile.role_default') }}</label>
                        @php
                            $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                            $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                            $options = $row->details;
                        @endphp
                        @include('voyager::formfields.relationship')
                    </div>
                    {{--<div class="form-group col-md-4">
                            <label for="additional_roles">{{ __('voyager::profile.roles_additional') }}</label>
                            @php
                                $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                                $options = $row->details;
                            @endphp
                            @include('voyager::formfields.relationship')
                        </div>--}}
                            {{--@php
                                    if (isset($dataTypeContent->locale)) {
                                        $selected_locale = $dataTypeContent->locale;
                                    } else {
                                        $selected_locale = config('app.locale', 'en');
                                    }
                                @endphp
                                <div class="form-group col-md-4">
                                    <label for="locale">{{ __('voyager::generic.locale') }}</label>
                                    <select class="form-control select2" id="locale" name="locale">
                                        @foreach (Voyager::getLocales() as $locale)
                                            <option value="{{ $locale }}"
                                                    {{ ($locale == $selected_locale ? 'selected' : '') }}>{{ $locale }}</option>
                                        @endforeach
                                    </select>
                                </div>--}}

                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        @if(isset($dataTypeContent->avatarUrl))
                            <img src="{{ url(old('avatar', $dataTypeContent->avatarUrl ?? '')) }}"
                                style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                        @endif
                        <input type="file" data-name="avatar" name="avatar">
                    </div>
                    </div>
                </div>
        </div>
    </div>



