<footer class="app-footer">
    <div class="site-footer-right">
        Разработано в студии <a href="https://mrtim.ru" target="_blank">MrTim</a>        
        @php $version = Voyager::getVersion(); @endphp
        @if (!empty($version))
            - {{ $version }}
        @endif
    </div>
</footer>
