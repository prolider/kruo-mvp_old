@if ($action === 'browse')
    <p>{{ is_null($data->window) ? '' : $data->window->name }}</p>
@else
    @php
        $windows = !is_null($dataTypeContent->service_category_id) ? $dataTypeContent->category->windows->pluck('name', 'id') : \App\Models\Window::all()->pluck('name', 'id');
    @endphp
    <select name="window_id" class="select2">
        @foreach($windows as $key => $window)
            <option value="{{ $key }}" @if($content === $key) selected @endif>{{ $window }}</option>
        @endforeach
    </select>
@endif

@if ($action !== 'browse')
    @push('javascript')
        <script>
          $('select[name="service_category_id"]').on('change', function(event) {
            var windowField = $('select[name="window_id"]');
            windowField.find('option').remove();

            window.windows.filter(function(win) {
                return win.categories.reduce(function(accum, category) {
                  return accum || category.id == event.target.value;
                }, false);
              })
              .map(function(win) {
                windowField.append(new Option(win.name, win.id));
              });

            windowField.val(null).trigger('change');
          });

          window.windows = {!! json_encode(\App\Models\Window::forSelectChange()) !!};
        </script>
    @endpush
@endif
