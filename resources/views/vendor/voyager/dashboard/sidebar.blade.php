<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('voyager.tickets.index') }}">
                    <div class="logo-icon-container">
                        @php
                            $logoData = json_decode(setting('site.logo'));
                            $logo = $logoData[0]->download_link ?? asset('/images/logo-1.svg');
                        @endphp
                            <img src="{{ !json_last_error() ? Voyager::image($logo) : $logo }}" alt="Logo Icon">
                    </div>
                </a>
            </div><!-- .navbar-header -->
            <div class="navbar-title">
                <h4>Панель Администратора</h4>
            </div>
            <!--<div class="panel widget center">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <h4>Панель Администратора</h4>
                    <img src="{{ $user_avatar }}" class="avatar" alt="{{ Auth::user()->name }} avatar">
                    <h4>{{ ucwords(Auth::user()->name) }}</h4>
                    <p>{{ Auth::user()->email }}</p>
                    <a href="{{ route('voyager.profile') }}" class="btn btn-primary">{{ __('voyager::generic.profile') }}</a>
                    <div style="clear:both"></div>
                </div>
            </div>-->
        </div>
        <div id="adminmenu">
            <admin-menu :items="{{ menu('admin', '_json') }}"></admin-menu>
        </div>
    </nav>
</div>
