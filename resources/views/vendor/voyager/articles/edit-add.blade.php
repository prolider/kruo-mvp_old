@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        </div><!-- panel-body -->

                        <div class="panel-footer text-right">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-success save" onclick="update(event, {{ $edit }})">Применить</button>
                                <button type="submit" name="_index" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
                </div>
                <div class="panel panel-bordered form-edit-add">
                    <div class="panel-heading">
                        <h4>Свойства</h4>
                    </div>
                    @if($edit)
                        <div class="panel-body">
                            <div>


                                <form enctype="multipart/form-data" method="POST" action="{{ route('api.extra_fields.sync', $dataTypeContent->id) }}">
                                @csrf
                                @foreach(optional($dataTypeContent->category)->extraFieldGroups ?? [] as $group)
                                    @foreach($group->fields as $field)
                                        <div class="form-group row">
                                            <label class="col-md-2 col-form-label">
                                                {{ $field->name }}
                                            </label>
                                            <div class="col-md-4 col-form-label">
                                                @php
                                                    $value = optional(optional($dataTypeContent->extraFields->find($field))->pivot)->value;
                                                @endphp
                                                @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT || $field->type == \App\Models\ExtraField::TYPE_SELECT)
                                                    <select
                                                            class="form-control form-control-solid @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT) select-2 @endif"
                                                            name="extraFields[{{ $field->id }}][value]@if ($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)[]@endif"
                                                            @if($field->type == \App\Models\ExtraField::TYPE_MULTISELECT) multiple @endif
                                                            @if($field->is_required) required @endif
                                                    >
                                                        @foreach($field->details['options'] ?? [] as $option)
                                                            @if ($field->type == \App\Models\ExtraField::TYPE_MULTISELECT)
                                                                <option @if(in_array($option, json_decode($value) ?? [])) selected @endif>{{ $option }}</option>
                                                            @else
                                                                <option @if($option == $value) selected @endif>{{ $option }}</option>
                                                            @endif

                                                        @endforeach
                                                    </select>
                                                @elseif($field->type == \App\Models\ExtraField::TYPE_NUMBER)
                                                    <input
                                                            type="number"
                                                            name="extraFields[{{ $field->id }}][value]"
                                                            class="form-control form-control-solid"
                                                            placeholder="{{ $field->placeholder }}"
                                                            value="{{ $value }}"
                                                            @if($field->is_required) required @endif
                                                    >
                                                @elseif($field->type == \App\Models\ExtraField::TYPE_DATE)
                                                    <input
                                                            type="date"
                                                            name="extraFields[{{ $field->id }}][value]"
                                                            class="form-control form-control-solid"
                                                            placeholder="{{ $field->placeholder }}"
                                                            value="{{ $value }}"
                                                            @if($field->is_required) required @endif
                                                    >
                                                @elseif($field->type == \App\Models\ExtraField::TYPE_FILE)
                                                    @if (is_array($value))
                                                        @foreach($value as $file)
                                                            <div id="file-row-{{ $loop->index }}">
                                                                <a download="{{ $file->original_name }}" href="{{ $file->download_link }}">
                                                                    {{ $file->original_name }}
                                                                </a>
                                                                <button type="button" onclick="removeFile({{ $field->id }}, '{{ $file->original_name }}', {{ $loop->index }})" class="btn btn-sm btn-danger">
                                                                    <i class="voyager-trash"></i>
                                                                </button>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                    <input
                                                            type="file"
                                                            name="extraFields[{{ $field->id }}][value]"
                                                            @if($field->is_required) required @endif
                                                            multiple
                                                    >
                                                @elseif($field->type == \App\Models\ExtraField::TYPE_TEXT_AREA)
                                                    <textarea
                                                            class="form-control form-control-solid"
                                                            name="extraFields[{{ $field->id }}][value]"
                                                            placeholder="{{ $field->placeholder }}"
                                                            @if($field->is_required) required @endif
                                                    >{{ $value }}</textarea>
                                                @elseif($field->type == \App\Models\ExtraField::TYPE_URL)
                                                    <label for="url_name_{{$field->id}}">Название ссылки</label>
                                                    <input
                                                        id="url_name_{{$field->id}}"
                                                        class="form-control form-control-solid"
                                                        name="extraFields[{{ $field->id }}][name]"
                                                        placeholder="{{ $field->placeholder }}"
                                                        value="{{ json_decode($value)->name ?? null}}"
                                                        @if($field->is_required) required @endif
                                                    >
                                                    <label for="url_{{$field->id}}">Ссылка</label>
                                                    <input
                                                        id="url_{{$field->id}}"
                                                        class="form-control form-control-solid"
                                                        name="extraFields[{{ $field->id }}][url]"
                                                        placeholder="{{ $field->placeholder }}"
                                                        value="{{ json_decode($value)->url ?? $value}}"
                                                        @if($field->is_required) required @endif
                                                    >
                                                @else
                                                    <input
                                                            class="form-control form-control-solid"
                                                            name="extraFields[{{ $field->id }}][value]"
                                                            placeholder="{{ $field->placeholder }}"
                                                            value="{{ $value }}"
                                                            @if($field->is_required) required @endif
                                                    >
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach

                                <button type="submit" class="btn btn-primary pull-right">
                                    Сохранить
                                </button>
                            </form>
                            <br>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            $('#show_add_field_form_btn').on('click', function() {
                $('#add_field_form').show();
                $('#hide_add_field_form_btn').show();
                $('#show_add_field_form_btn').hide();
            });

            $('#hide_add_field_form_btn').on('click', function() {
                $('#add_field_form').hide();
                $('#hide_add_field_form_btn').hide();
                $('#show_add_field_form_btn').show();
            });

            if ($('#extra_field_id').val() == {{ \App\Models\ExtraField::TYPE_DATE }}) {
                $('#extra_field_input').attr('type', 'date');
            }

            $('#extra_field_id').on('change', function() {
                let fieldId = $(this).val();
                $.get('/api/extra-fields/' + fieldId, function(field) {
                    $('#extra_field_input').replaceWith('<input id="extra_field_input" name="value">');
                    if (field.type == {{ \App\Models\ExtraField::TYPE_DATE }}) {
                        $('#extra_field_input').attr('type', 'date');
                    } else if (field.type == {{ \App\Models\ExtraField::TYPE_NUMBER }}) {
                        $('#extra_field_input').attr('type', 'number');
                    } else if (field.type == {{ \App\Models\ExtraField::TYPE_FILE }}) {
                        $('#extra_field_input').attr('type', 'file');
                    } else if (field.type == {{ \App\Models\ExtraField::TYPE_MULTISELECT }} || field.type == {{ \App\Models\ExtraField::TYPE_SELECT }}) {
                        let selectClass = (field.type == {{ \App\Models\ExtraField::TYPE_MULTISELECT }}) ? 'select-2' : '';
                        let multipleClass = (field.type == {{ \App\Models\ExtraField::TYPE_MULTISELECT }}) ? 'multiple' : ''
                        let requiredClass = field.is_required  ? ' required' : '';
                        let options = field.details.options ? field.details.options : [];
                        let optionsHtml = '';

                        options.forEach(option => optionsHtml += '<option>' + option+ '</option>');

                        let html = '<select id="extra_field_input" class="form-control form-control-solid ' + selectClass
                        + '" name="value"' + multipleClass + requiredClass + '>' + optionsHtml + '</select>'

                        $('#extra_field_input').replaceWith(html);

                        $('.select2').select2();
                    } else {
                        $('#extra_field_input').removeAttr('type');
                    }
                });
            });

            $('.select-2').select2();
        });

        function deleteExtraField(articleId, fieldId) {
            $.post('/api/extra-fields/' + articleId + '/detach/' + fieldId, [], function (data) {
                $('#field-row-' + fieldId).remove();
            })
        }

        function removeFile(fieldId, file, fileIndex) {
            $.post('/api/extra-fields/' + {{ $dataTypeContent->id }} + '/deleteFile/' + fieldId, {fileName: file}, function (data) {
                $('#file-row-' + fileIndex).remove();
            });
        }
    </script>
@stop
