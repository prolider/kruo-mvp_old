@foreach($categories as $category)
    @php $level = $loop->parent ? $level : 1; @endphp
    <li class="category-select-item" data-value="{{ $category->name }}" data-level="{{ $level }}" data-default-selected="">
        <a href="#">{{ $category->name }}</a>
    </li>
    @if($category->categories->count() > 0)
        @php $level = $level + 1; @endphp
        @include('voyager::articles.category-select-items', ['categories' => $category->categories])
    @endif
@endforeach