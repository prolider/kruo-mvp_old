<div class="panel-body">
    <div class="files-table">
        <div style="display: flex; justify-content: space-between;">
            <h4>Прикрепленные файлы</h4>
            <button type="button" class="btn btn-success" id="add-file-modal">Добавить</button>
        </div>
        @include('vendor.voyager.services.partials.documents-table', ['documents' => $dataTypeContent->documents])
    </div>
    <div class="links-table" style="margin-bottom: 50px;">
        <div style="display: flex; justify-content: space-between;">
            <h4>Ссылки</h4>
            <button type="button" class="btn btn-success add-link">Добавить</button>
        </div>

        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Ссылка</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $links = json_decode($dataTypeContent->links, true) ?? [];
                @endphp
                @forelse($links as $key => $link)
                <tr>
                    <td>
                        <input class="form-control" name="links[{{ $key }}][name]" value="{{ $link['name'] }}">
                    </td>
                    <td>
                        <input class="form-control" name="links[{{ $key }}][url]" value="{{ @$link['url'] }}">
                    </td>
                    <td>
                        <button class="btn btn-danger remove-link" type="button">Удалить</button>
                    </td>
                </tr>
                @empty
                    <tr class="empty-links">
                        <td colspan="3" class="text-center">Пока нет ссылок</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>


</div>

@push('javascript')
    <div class="modal" tabindex="-1" role="dialog" id="file-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Новый документ</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" id="file-form" action="">
                        <div class="form-group">
                            <label>Название файла</label>
                            <input type="text" name="document[name]" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Файл</label>
                            <input type="file" name="document[file]" required>
                            <span class="text-muted">Размер файла не должен превышать 15 мб.</span>
                        </div>
                        <div class="form-group">
                            <label>Позиция</label>
                            <input type="number" name="document[sort]" value="100" min="0" class="form-control" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save-file">Сохранить</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var serviceId = +'{{ $dataTypeContent->id }}';
        var nextLinkIdx = {{ count($links) }};

        $(document).ready(function () {
          var editedFile = null;

          $('.files-table #add-file-modal').on('click', function () {
            $('#file-modal input[name="document[name]"]').val('');
            $('#file-modal input[name="document[sort]"]').val('');
            $('#file-modal input[name="document[file]"]').val('');
            $('#file-modal').modal('show');
          });

          $(document).on('click', '#save-file', function () {
            $(this).parent().parent().find('form').submit();
          });

          $('.files-table').on('click', '.btn-warning', function () {
            var tds = $(this).parent().parent().find('td');

            editedFile = $(this).data('id');

            $('#file-modal input[name="document[name]"]').val(tds[0].innerText);
            $('#file-modal input[name="document[sort]"]').val(tds[2].innerText);
            $('#file-modal input[name="document[file]"]').val('');

            $('#file-modal').modal('show');
          });

          $('.files-table').on('click', '.btn-danger', function () {
            if (confirm()) {
              var _this = $(this);
              $.ajax({
                url: `/admin/services/${serviceId}/document/${$(this).data('id')}`,
                type: 'DELETE',
                success: function () {
                  toastr.success('Файл успешно удален.');
                  _this.parent().parent().remove();
                }
              });
            }
          });

          $(document).on('submit', '#file-form', function (e) {
            e.preventDefault();

            var _this = $(this);
            var fd = new FormData();

            var url;
            if (editedFile !== null) {
              url = `/admin/services/${serviceId}/document/${editedFile}`
              fd.append('_method', 'PUT');
            } else {
              url = `/admin/services/${serviceId}/document`;
            }

            var file = _this.find('input[name="document[file]"]')[0].files[0];

            if (file && file.size > 15728640) {
                toastr.error("Размер файла не должен превышать 15 мб.");

                return;
            }

            fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
            fd.append('name', _this.find('input[name="document[name]"]').val());
            fd.append('sort', _this.find('input[name="document[sort]"]').val());
            fd.append('file', file);

            $.ajax({
              url: url,
              data: fd,
              processData: false,
              contentType: false,
              type: 'POST',
              success: function (response) {
                if (editedFile !== null) {
                  toastr.success('Документ успешно изменен.');
                } else {
                  toastr.success('Документ успешно добавлен.');
                }

                $('#file-modal').modal('hide');
                $('.files-table table').html(response.table);
              },
              error: function (response) {
                var errors = Object.values(JSON.parse(response.responseText).errors);
                toastr.error(errors.join('\n'));
              }
            });
          });

          $('.links-table .add-link').on('click', function () {
            $('.links-table table tbody').append(`
                <tr>
                    <td>
                        <input class="form-control" name="links[${nextLinkIdx}][name]" value="">
                    </td>
                    <td>
                        <input class="form-control" name="links[${nextLinkIdx}][url]" value="">
                    </td>
                    <td>
                        <button class="btn btn-danger remove-link" type="button">Удалить</button>
                    </td>
                </tr>
            `);
            $('.empty-links').hide();
            nextLinkIdx++;
          });

          $('.links-table').on('click', '.remove-link', function (e) {
            e.preventDefault();
            $(e.target).parent().parent().remove();
            if ($('.links-table table tbody tr').length === 1) {
              $('.empty-links').show();
            }
          });
        });
    </script>
@endpush
