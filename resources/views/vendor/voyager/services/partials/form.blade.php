<div class="panel-body">
@php
    if ($dataTypeContent->form) {
        $formData = !is_array($dataTypeContent->form->form_data) ? $dataTypeContent->form->form_data : json_encode($dataTypeContent->form->form_data);
    } else {
        $formData = "";
    }
@endphp

@include('form-builder.form', ['content' => $formData, 'action' => ($edit ? 'edit' : 'add'), 'onTab' => true])
</div>