<form>
    <div class="btn-group hierarchy-select" data-resize="auto" id="category-select">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="selected-label pull-left">&nbsp;</span>
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu open">
            <div class="hs-searchbox">
                <input type="text" class="form-control" autocomplete="off">
            </div>
            <ul class="dropdown-menu inner" role="menu">
                <li class="category-select-item" data-value="" data-level="1" data-default-selected="">
                    <a href="#">Все категории</a>
                </li>
                @foreach($categories as $category)
                    <li class="category-select-item" data-level="1" data-value="{{ $category->name }}">
                        <a href="#">{{ $category->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        <input id="category-id" class="hidden hidden-field" name="category-id" aria-hidden="true" type="text"/>
    </div>
</form>