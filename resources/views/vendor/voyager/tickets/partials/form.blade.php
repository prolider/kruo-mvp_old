@php
    $formData = $dataTypeContent->formResult->result;
    $formData = is_array($formData) ? $formData : json_decode($formData, true);

    $form = $dataTypeContent->formResult->form ? $dataTypeContent->formResult->form->form_data : [];
    $form = is_array($form) ? $form : json_decode($form, true);

    $user = $dataTypeContent->formResult->user;
@endphp

<div class="form-result">
    @if ($dataTypeContent->formResult->form)
    <h3>{{ $dataTypeContent->formResult->form->name }}</h3>
    @endif

    <div class="row">
        <div class="col-md-6 col-12">
            <h4>Пользователь:</h4>
            @if ($user)
            <table class="table table-bordered">
                <tr>
                    <th>Имя</th>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <th>E-mail</th>
                    <td>{{ $user->email }}</td>
                </tr>
            </table>
            @else
                <p>Не авторизован</p>
            @endif
        </div>
        <div class="col-md-6 col-12">
            <h4>Заполненные поля:</h4>
            @if($dataTypeContent->formResult->form)
            <table class="table table-bordered">
                @foreach($form as $page)
                    @foreach($page as $field)
                        @if (isset($field['name']) )
                        <tr>
                            <th>
                                {{ $field['label'] ?? 'Скрытое поле' }}
                            </th>
                            <td>
                                @if (isset($formData[$field['name']]))
                                    @if (is_array($formData[$field['name']]))
                                        {{ implode(", ", $formData[$field['name']]) }}
                                    @else
                                        {{ $formData[$field['name']] }}
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @endif
                    @endforeach
                @endforeach
            </table>
            @else
                <p>Форма не заполнена</p>
            @endif
        </div>
    </div>
</div>

    <style>
        .form-result th {
            font-weight: bold;
        }
    </style>
