<div class="comments">
    @forelse($dataTypeContent->comments as $comment)
        <div class="comment @if($comment->isNew) new @endif">
            <p>Пользователь: {{ $comment->user->name }}</p>
            <p>Добавлен: {{ $comment->created_at->format('d.m.Y H:i:s') }}</p>
            {!! $comment->text !!}

            @foreach($comment->attachments as $attachment)
                <p>
                    <a href="{{ asset("storage/{$attachment['path']}") }}">
                        {{ $attachment['name'] }}
                    </a>
                </p>
            @endforeach
        </div>
    @empty
        <p>Пока нет комментариев</p>
    @endforelse

    </form>
    <h3>Новый комментарий</h3>
    <form action="{{ route('operator.add.ticket', $dataTypeContent) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Тип</label>
            <select name="visibility" class="form-control">
                <option value="{{ \App\Models\Comment::VISIBILITY_ALL }}" selected>Публичный</option>
                <option value="{{ \App\Models\Comment::VISIBILITY_OPERATOR }}">Внутренний</option>
            </select>
        </div>

        <div class="form-group">
            <label class="control-label" for="name">Вложения</label>
            <input type="file" name="attachment[]" multiple="multiple">
        </div>

        <textarea name="text" class="richTextBox"></textarea>

        <button class="btn btn-primary" type="submit">Отправить</button>
    </form>
</div>

<style>
    .ticket-detail .custom-file-label::after {
        content: 'Выбрать';
    }

    .comment.new {
        background: #38C172;
    }
</style>

@push('javascript')
    <script>
      // Initiliaze rich text editor
      tinymce.init(window.voyagerTinyMCE.getConfig());

      $('.nav-tabs a').on('shown.bs.tab', function (event) {
        if (event.target.dataset.tab === 'comments') {
          $.ajax('{{ route('operator.read.tickets', $dataTypeContent) }}', {
            method: 'POST'
          });
        }
      });
    </script>
@endpush
