<div class="panel-body" style="padding: 0;">

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    

<!-- Adding / Editing -->

    <div class="form-group col-md-12">
        @if ($dataTypeContent->formResult->form)
            <lable class="control-lable " style="font-weight: 600;">Услуга</lable>
            <div>
                <lable class="control-lable">{{ $dataTypeContent->formResult->form->name }}</lable>
            </div>
        @endif
    </div>
    @php
        $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
    @endphp

    @foreach($dataTypeRows as $row)
    <!-- GET THE DISPLAY OPTIONS -->
        @php
            $display_options = $row->details->display ?? NULL;
            if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
            }
        @endphp
        @if($row->getTranslatedAttribute('display_name') == "Minuser Id")
                @continue;
            @endif
        @if (isset($row->details->legend) && isset($row->details->legend->text))
            <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
        @endif

        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
            {{ $row->slugify }}
            
            <label class="control-label" for="name">
                @if($row->getTranslatedAttribute('display_name') == "Назначенный оператор")
                    Менеджер корпорации
                @elseif($row->getTranslatedAttribute('display_name') == "Window Id")
                    Министерство
                @else
                    {{ $row->getTranslatedAttribute('display_name') }}
                @endif
            </label>
            @include('voyager::multilingual.input-hidden-bread-edit-add')
            @if (isset($row->details->view))
                @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
            @elseif ($row->type == 'relationship')
                @include('voyager::formfields.relationship', ['options' => $row->details])
            @else
                @php
                $windows = DB::select('select * from windows order by id');
                $options = array(null => "Выбрать из списка");
                for ($i = 1; $i <= count($windows); $i++) {
                    $options[$i] = $windows[$i -1]->name;
                }
                if($row->getTranslatedAttribute('display_name') == "Window Id") {
                    $row->type = "select_dropdown";
                    $row->details = (object) array("options" => $options);
                }
                
                @endphp
                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
            @endif

            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
            @endforeach
            @if ($errors->has($row->field))
                @foreach ($errors->get($row->field) as $error)
                    <span class="help-block">{{ $error }}</span>
                @endforeach
            @endif
        </div>
    @endforeach

</div><!-- panel-body -->
