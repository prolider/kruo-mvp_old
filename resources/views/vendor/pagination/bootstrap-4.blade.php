@if ($paginator->hasPages())
    <div class="d-flex flex-wrap py-2 mr-3">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <a href="#" class="btn btn-icon btn-sm btn-light mr-2 my-1 disabled"  aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <i class="ki ki-bold-arrow-back icon-xs"></i>
                </a>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')" class="btn btn-icon btn-sm btn-light mr-2 my-1"  aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <i class="ki ki-bold-arrow-back icon-xs"></i>
                </a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <a href="#" class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1 disabled" aria-disabled="true">{{ $element }}</a>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a href="#" aria-current="page" class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1 btn-hover-primary active">{{ $page }}</a>
                        @else
                            <a href="{{ $url }}" class="btn btn-icon btn-sm border-0 btn-light mr-2 my-1">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')" class="btn btn-icon btn-sm btn-light mr-2 my-1"><i class="ki ki-bold-arrow-next icon-xs"></i></a>
            @else
                <a href="#" rel="next" aria-label="@lang('pagination.next')" class="btn btn-icon btn-sm btn-light mr-2 my-1 disabled"><i class="ki ki-bold-arrow-next icon-xs"></i></a>
            @endif
        </ul>
    </div>
@endif
