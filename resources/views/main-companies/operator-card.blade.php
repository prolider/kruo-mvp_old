
<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
    <div class="card card-custom gutter-b card-stretch">
        <div class="card-body pt-4 ribbon ribbon-clip ribbon-left">
            @if($operator->id == optional(optional(auth()->user())->company)->personal_manager_id)
            <div class="ribbon-target" style="top: 12px;">
                <span class="ribbon-inner bg-primary"></span>ПМ
            </div>
            @endif
            <div class="d-flex align-items-center py-2">
                <div class="d-flex align-items-start">
                    <div class="d-flex flex-shrink-2 mr-5">
                        <div class="symbol symbol-50 symbol-xl-75">
                            <div class="symbol-label" style="background-image:url('{{ $operator->avatarUrl }}')" style="width:80px"></div>
                        </div>
                    </div>
                    <div class="d-flex flex-shrink-1 flex-column">
                        <span class="text-dark font-weight-bold font-size-h4 mb-0">{{ $operator->name }} {{ $operator->firstname }}</span>
                        <span class="text-muted font-weight-bold">{{ $operator->position }}</span>
                    </div>
                </div>
            </div>
            <div class="py-2">
                <div class="d-flex align-items-center mb-2">
                    <span class="flex-shrink-0 mr-3">
                    <a href="https://t.me/{{ $operator->telegram }}" target="_blank" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="Telegram"><i class="icon-lg la fab la-telegram-plane"></i></a>
                    </span>
                    <span class="flex-shrink-0 mr-3">
                    <a href="viber://chat?number={{ $operator->phone }}" target="_blank" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="Viber"><i class="icon-lg la fab la-viber"></i></a>
                    </span>
                    <span class="flex-shrink-0 mr-3">
                    <a href="https://api.whatsapp.com/send?phone={{ $operator->phone }}" target="_blank" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="WhatsApp"><i class="icon-xl la fab la-whatsapp-square"></i></a>
                    </span>
                </div>
                <div class="d-flex align-items-center mb-2">
                    <a href="tel:{{ $operator->phone ?? '-' }}" target="_blank" class="text-muted text-hover-primary font-weight-bold">{{ $operator->phone ?? '-' }}</a>
                </div>
                <div class="d-flex align-items-center mb-2">
                    <a href="mailto:{{ $operator->email ?? '-'}}" target="_blank" class="text-muted text-hover-primary font-weight-bold">{{ $operator->email ?? '-'}}</a>
                </div>
            </div>
            @if($operator->id == optional(optional(auth()->user())->company)->personal_manager_id)
               <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                  <div class="navi-item mt-2">
                       <div class="d-flex align-items-center mb-2">
                           <a href="{{ route('discussions.create') }}" class="text-hover-primary font-weight-bold navi-link py-4 active">Создать обращение</a>
                            <a href="tel:{{ $operator->phone ?? '-' }}" class="btn btn-icon btn-primary btn-lg mx-2" data-toggle="tooltip" data-original-title="Заказать обратный звонок">
                               <i class="la la-phone"></i>
                           </a>

                       </div>
                   </div>
                </div>
            @endif
        </div>
    </div>
</div>
