
    @if($mainCompany)
        <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">
                            Сотрудники Корпорации развития Ульяновской области
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="d-flex row">
                    <div class="flex-row-auto col-12 col-sm-6 col-md-4 col-xl-3" id="kt_content_sidebar">
                        <div class="card card-custom gutter-b">
                            <div class="card-body pt-15">
                                {!! $mainCompany->description !!}
                            </div>
                        </div>
                    </div>
                    <div class="flex-row-auto col-12 col-sm-6 col-md-8 col-xl-9">
                        <div class="row">
                            @each('main-companies.operator-card', $mainCompany->operators, 'operator')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="card-header">
            Сведения отсутсвуют
        </div>
    @endif

