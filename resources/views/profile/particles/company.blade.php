<form class="@if($company && $company->isVerified() || $company && $company->isOnCheck() || $company && $company->isOnCancellation()) input-disabled @endif">
    {{ csrf_field() }}
    <div class="card card-custom card-stretch mb-5">
        <!--begin::Header-->
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Реквизиты</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Редактирование</span>
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Form-->

        <!--begin::Body-->
        <div class="card-body">

            <div class="form-group row">
            </div>
            <div class="form-group row">
                <label class="company_name col-xl-3 col-lg-3 col-form-label text-right">Наименование организации</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_name" aria-describedby="info"
                           placeholder="Полное наименование" name="name"
                           value="{{ optional($company)->name }}" required disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label text-right" for="company_inn">ИНН</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="mask-company-inn form-control form-control-lg form-control-solid" id="company_inn" placeholder="ИНН"
                           name="inn"
                           value="{{ optional($company)->inn }}"
                           required disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label text-right" for="company_kpp">КПП</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="mask-company_kpp form-control form-control-lg form-control-solid" id="company_kpp" placeholder="КПП"
                           name="kpp"
                           value="{{ optional($company)->kpp }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="company_ogrn" class="col-xl-3 col-lg-3 col-form-label text-right">ОГРН</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="mask-company-ogrn form-control form-control-lg form-control-solid" id="company_ogrn"
                           placeholder="ОГРН" name="ogrn"
                           value="{{ optional($company)->ogrn }} " disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label text-right" for="organization_date">Дата регистрации</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="date" class="form-control form-control-solid" id="company_registration_date"
                           placeholder="01.01.1945"
                           name="registration_date"
                           @if(!is_null(optional($company)->registration_date))
                           value="{{ optional($company)->registration_date->format('Y-m-d') }}"
                            @endif
                           disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="company_adress col-xl-3 col-lg-3 col-form-label text-right">Юридический адрес</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_adress" placeholder="Юридический адрес"
                           name="address" value="{{ optional($company)->address }}" disabled>
                </div>
            </div>
            <div class="row">
                <label class="col-xl-3"></label>
                <div class="col-lg-9 col-xl-6">
                    <h5 class="font-weight-bold mt-10 mb-6">Данные о руководителе</h5>
                </div>
            </div>
            <div class="form-group row">
                <label class="company_ceo col-xl-3 col-lg-3 col-form-label text-right">Должность руководителя</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_ceo"
                           placeholder="Генеральный диретор / Директор" name="management_post"
                           value="{{ optional($company)->management_post }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="company_ceo_fio col-xl-3 col-lg-3 col-form-label text-right">ФИО руководителя</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_ceo_fio"
                           placeholder="Фамилия Имя Отчество" name="management_name"
                           value="{{ optional($company)->management_name }}" disabled>
                </div>
            </div>
            <div class="row">
                <label class="col-xl-3"></label>
                <div class="col-lg-9 col-xl-6">
                    <h5 class="font-weight-bold mt-10 mb-6">Контакты представителя</h5>
                </div>
            </div>
            <div class="form-group row">
                <label class="company_site col-xl-3 col-lg-3 col-form-label text-right">ФИО представителя</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_site"
                           placeholder="ФИО представителя" name="site"
                           value="{{ optional($company)->site }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="company_phone col-xl-3 col-lg-3 col-form-label text-right">Телефон</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_phone"
                           placeholder="Телефон" name="phone"
                           value="{{ optional($company)->phone }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="company_email col-xl-3 col-lg-3 col-form-label text-right">E-mail</label>
                <div class="col-lg-9 col-xl-6">
                    <input type="text" class="form-control form-control-lg form-control-solid" id="company_email"
                           placeholder="E-mail" name="email"
                           value="{{ optional($company)->email }}" required>
                </div>
            </div>
        </div>
        <!--end::Body-->
    </div>
</form>
<!--end::Form-->
