
@if($company)
    @if($company->isOnCancellation())
        <div class="verify-company">
            <div class="card card-custom mb-5">
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">                                            
                        <h3 class="card-label font-weight-bolder text-dark">Статус полномочий</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">запрос на отмену</span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
                        <div class="alert-icon">
                            <i class="flaticon-warning"></i>
                        </div>
                        <div class="alert-text">Проверка начата. Вы не можете редактировать реквизиты организации и создавать заявки. Дождитесь результата расмотрения запроса на отмену полномочий со стороны Администратора портала. <br/>По результату рассмотрения на Ваш email будет отправлено письмо с результатом рассмотрения обращения.</div>
                        
                    </div>
                </div>
            </div>
        </div>
    @elseif($company->verification_status !== \App\Models\Company::COMPANY_VERIFICATION_STATUS_ACCEPT)
        @if (in_array($company->verification_status, [\App\Models\Company::COMPANY_VERIFICATION_STATUS_REJECT, \App\Models\Company::COMPANY_VERIFICATION_STATUS_NOT_VERIFIED]))
            <div class="verify-company">
                <form method="post" enctype="multipart/form-data" action="{{ route('companies.verification') }}">
                    @csrf
                    <div class="card card-custom mb-5">
                        <div class="card-header py-3">
                            <div class="card-title align-items-start flex-column">
                                <h3 class="card-label font-weight-bolder text-dark">Статус проверки полномочий</h3>
                                @if ($company->verification_status === \App\Models\Company::COMPANY_VERIFICATION_STATUS_REJECT)
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">отклонено</span>
                                @else
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">не запрошено</span>
                                @endif
                            </div>
                            <div class="card-toolbar">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary mr-2">Отправить</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-xl-3"></label>
                                <div class="col-lg-9 col-xl-6">
                                    <h5 class="font-weight-bold mb-6">Подтвердите полномочия на компанию</h5>
                                </div>
                            </div>
                            @if (!empty($company->statement_url))
                                <p>
                                    <a href="{{ $company->statement_url }}" download="{{ $company->statement_name }}">Заявление</a>
                                </p>
                                <p>
                                    <a href="{{ $company->signature_url }}" download="{{ $company->signature_name }}">Подпись</a>
                                </p>
                            @endif
                            <!-- <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Подтверждающий документ</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="custom-file">
                                        <input type="file" name="statement" id="docfile" class="form-control" required>
                                        <label class="custom-file-label text-muted" for="docfile"></label>
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Файл ЭЦП</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="custom-file">
                                        <input type="file" name="signature" id="sigfile" class="form-control" accept=".sig" required>
                                        <label class="custom-file-label text-muted" for="sigfile"></label>
                                    </div>
                                    <span class="form-text text-muted">Загрузите файл ЭЦП в формате .sig</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @else
            <div class="verify-company">
                <div class="card card-custom mb-5">
                    <div class="card-header py-3">
                        <div class="card-title align-items-start flex-column">
                            <h3 class="card-label font-weight-bolder text-dark">Запрос на подтверждении полномочий</h3>
                            <span class="text-muted font-weight-bold font-size-sm mt-1">на проверке</span>
                        </div>
                        <div class="card-toolbar">
                            <div class="text-right">
                                <form method="post" action="{{ route('companies.cancel-verification', $company) }}">
                                    @csrf
                                    <input type="hidden" name="canceled" value="verification-canceled">
                                    <button type="submit" class="btn btn-light-warning">Запрос на отмену полномочий</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-3">
                        <div class="alert alert-custom alert-light-warning fade show mb-5" role="alert">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text">Файлы для подтверждения полномочий успешно загружены. Ожидайте подтверждения от администратора. Вы не можете редактировать реквизиты организации и не можете подавать заявки. Для редактирования реквизитов организации необходимо отправить запрос на отмену полномочий. По результату рассмотрения на Ваш email будет отправлено письмо с результатом рассмотрения обращения.</div>                            
                        </div>
                        <!-- <p>
                            <a href="{{ $company->statement_url }}" download="{{ $company->statement_name }}"><i class="icon-xl text-info la la-file-download"></i> Заявление</a>
                        </p> -->
                        <p>
                            <a href="{{ $company->signature_url }}" download="{{ $company->signature_name }}"><i class="icon-xl text-info la la-file-download"></i> Подпись</a>
                        </p>
                    </div>
                </div>
            </div>
        @endif
    @else
        <div class="verify-company">
            <div class="card card-custom mb-5">
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Полномочия подтверждены</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">все готово к работе</span>
                    </div>                
                    <div class="card-toolbar">
                        <div class="text-right">
                            <form method="post" action="{{ route('companies.cancel-verification', $company) }}">
                                @csrf
                                <input type="hidden" name="canceled" value="verification-canceled">
                                <button type="submit" class="btn btn-light-danger">Запрос на отмену полномочий</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-custom alert-light-info fade show mb-5" role="alert">
                        <div class="alert-icon">
                            <i class="flaticon-warning"></i>
                        </div>
                        <div class="alert-text">Вы не можете редактировать реквизиты организации. <br/>Для редактирования реквизитов организации необходимо отправить запрос на отмену полномочий. <br/>По результату рассмотрения на Ваш email будет отправлено письмо с результатом рассмотрения обращения.</div>
                        
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif