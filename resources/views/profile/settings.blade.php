@extends('layouts.app')

@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center flex-wrap mr-1">
				<!--begin::Page Heading-->
				<div class="d-flex align-items-baseline flex-wrap mr-5">
					<!--begin::Breadcrumb-->
					<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">
						<li class="breadcrumb-item text-muted">
							<a href="#" class="text-muted">Профиль пользователя</a>
						</li>
						<li class="breadcrumb-item text-muted">
							<span class="text-muted">Настройки</span>
						</li>
					</ul>
					<!--end::Breadcrumb-->
				</div>
				<!--end::Page Heading-->
			</div>
		</div>
	<!--begin::Subheader-->
	<div class="subheader pb-lg-8 subheader-transparent" id="kt_subheader">
		<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
			<!--begin::Info-->
			<div class="d-flex align-items-center flex-wrap mr-1">
				<!--begin::Page Heading-->
				<div class="d-flex align-items-baseline flex-wrap mr-5">
					<!--begin::Page Title-->
					<h2 class="text-dark font-weight-bold my-1 mr-5">Профиль пользователя - Настройки</h2>
					<!--end::Page Title-->
				</div>
				<!--end::Page Heading-->
			</div>
			<!--end::Info-->
		</div>
	</div>
	<!--end::Subheader-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<div class="container">
			<!--begin::Profile Personal Information-->
			<div class="d-flex flex-row row">
				<!--begin::Aside-->
				<div class="col-md-4" id="kt_profile_aside">
					<!--begin::Nav Panels Wizard 2-->
					<div class="card card-custom gutter-b">
						<!--begin::Body-->
						<div class="card-body pt-8">
							<!--begin::Nav-->
							<div class="navi navi-bold navi-hover navi-active navi-link-rounded">
								<div class="navi-item mb-2">
									<a href="{{ route('profile.index') }}" class="navi-link py-4">
										<span class="navi-icon mr-2">
											<i class="icon-xl la la-user-circle"></i>
											</span>
										<span class="navi-text font-size-lg">Личные данные</span>
									</a>
								</div>
								<div class="navi-item mb-2">
									<a href="{{ route('profile.contacts') }}" class="navi-link py-4 active">
										<span class="navi-icon mr-2">
											<i class="icon-xl la la-phone-square"></i>
											</span>
										<span class="navi-text font-size-lg">Контакты</span>
									</a>
								</div>

								<div class="navi-item mb-2">
									<a href="{{ route('profile.password.index') }}" class="navi-link py-4">
										<span class="navi-icon mr-2">
											<i class="icon-xl la la-user-shield"></i>
										</span>
										<span class="navi-text font-size-lg">Изменение пароля</span>
									</a>
								</div>
							</div>
							<!--end::Nav-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::Nav Panels Wizard 2-->
				</div>
				<!--end::Aside-->
				<!--begin::Content-->
				<div class="col-md-8">
					<!--begin::Card-->

                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header py-3">
                                <div class="card-title align-items-start flex-column">
                                    <h3 class="card-label font-weight-bolder text-dark">Настройки</h3>
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Редактирование</span>
                                </div>
                                <div class="card-toolbar">

                                    <button type="submit" class="btn btn-primary mr-2">Сохранить</button>

                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Form-->

                                <!--begin::Body-->

                                <div class="card-body">
                                    В разработке...
                                </div>
                                <!--end::Body-->
                            <!--end::Form-->
                        </div>

				</div>
				<!--end::Content-->
			</div>
			<!--end::Profile Personal Information-->
		</div>
		<!--end::Container-->
	</div>
</div>
@endsection

