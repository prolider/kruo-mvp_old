@extends('layouts.app')

@section('content')
    <div aria-live="polite" aria-atomic="true" style="position: relative;">
  		<div style="position: absolute; top: 10px; right: 10px;">	
            @if (session('profile_update') || ($errored = $errors->first()))
                <div class="toast show alert alert-{{ isset($errored) ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
                    Контактная информация {{ isset($errored) ? 'не была обновлёна' : 'успешно обновлена' }}
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Профиль пользователя</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <span class="text-muted">Контакты</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="subheader py-5 subheader-transparent my-5" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">Профиль пользователя - Контакты</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">           
                <div class="d-flex flex-row row">
                    <div class="col-12 col-md-4 col-xl-4" id="kt_profile_aside">
                        <div class="card card-custom gutter-b">
                            <div class="card-body pt-8">
                                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                    <div class="navi-item mb-2">
                                        <a href="{{ route('profile.index') }}" class="navi-link py-4">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-user-circle"></i>
                                                </span>
                                            <span class="navi-text font-size-lg">Личные данные</span>
                                        </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                        <a href="{{ route('profile.contacts') }}" class="navi-link py-4 active">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-phone-square"></i>
                                                </span>
                                            <span class="navi-text font-size-lg">Контакты</span>
                                        </a>
                                    </div>

                                    <div class="navi-item mb-2">
                                        <a href="{{ route('profile.password.index') }}" class="navi-link py-4">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-user-shield"></i>
                                            </span>
                                            <span class="navi-text font-size-lg">Изменение пароля</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-8 col-xl-8">
                        <form class="mfcb" method="POST" action="{{ route('profile.update') }}"  enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card card-custom card-stretch">
                                <div class="card-header py-3">
                                    <div class="card-title align-items-start flex-column">
                                        <h3 class="card-label font-weight-bolder text-dark">Контакты</h3>
                                        <span class="text-muted font-weight-bold font-size-sm mt-1">Редактирование</span>
                                    </div>
                                    <div class="card-toolbar">
                                        <button type="submit" class="btn btn-primary mr-2">Сохранить</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <label class="col-xl-3"></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <h5 class="font-weight-bold mb-6">Контактная информация</h5>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Телефон</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-phone"></i>
                                                    </span>
                                                </div>
                                                <input type="tel" class="mask-phone form-control form-control-lg form-control-solid" placeholder="+7-XXX-XXX-XX-XX"
                                                        name="phone" value="{{ $user->phone }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Email</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-at"></i>
                                                    </span>
                                                </div>
                                                <input type="email" class="form-control form-control-lg form-control-solid" placeholder="{{ Auth::user()->email }}"
                                                        name="email" value="{{ $user->email }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Viber</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la fab la-viber"></i>
                                                    </span>
                                                </div>
                                                <input type="text" name="viber" class="form-control form-control-lg form-control-solid" value="{{ $user->viber }}" placeholder="{{ Auth::user()->viber }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Telegram</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la fab la-telegram"></i>
                                                    </span>
                                                </div>
                                                <input type="text" name="telegram" class="form-control form-control-lg form-control-solid" value="{{ $user->telegram }}" placeholder="{{ Auth::user()->telegram }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">WhatsApp</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group input-group-lg input-group-solid">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la fab la-whatsapp"></i>
                                                    </span>
                                                </div>
                                                <input type="text" name="whatsapp" class="form-control form-control-lg form-control-solid" value="{{ $user->whatsapp }}" placeholder="{{ Auth::user()->whatsapp }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
	<script>
		$('.toast').toast('show');
	</script>
@endpush