@extends('layouts.app')

@section('content')
	<div aria-live="polite" aria-atomic="true" style="position: relative;">
  		<div style="position: absolute; top: 10px; right: 10px;">	
            @if (session('profile_update') || ($errored = $errors->first()))
                <div class="toast show alert alert-{{ isset($errored) ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
					Личные данные {{ isset($errored) ? 'не были обновлёны' : 'успешно обновлены' }}
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>			
	<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
		
		<!--<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
				
				<div class="d-flex align-items-center flex-wrap mr-1">
					<div class="d-flex align-items-baseline flex-wrap mr-5">						
						<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">
							<li class="breadcrumb-item text-muted">
								<a href="#" class="text-muted">Профиль пользователя</a>
							</li>
							<li class="breadcrumb-item text-muted">
								<span class="text-muted">Личные данные</span>
							</li>
						</ul>
					</div>
				</div>
			</div>-->
		<div class="subheader py-5 subheader-transparent my-5" id="kt_subheader">
			<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
				<div class="d-flex align-items-center flex-wrap mr-1">
					<div class="d-flex align-items-baseline flex-wrap mr-5">
						<h2 class="text-dark font-weight-bold my-1 mr-5">Профиль пользователя - Личные данные</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="d-flex flex-column-fluid">
			<div class="container">
				<div class="d-flex flex-row row">
					<div class="col-md-4" id="kt_profile_aside">
						<div class="card card-custom gutter-b">
							<div class="card-body pt-8">
								<div class="navi navi-bold navi-hover navi-active navi-link-rounded">
									<div class="navi-item mb-2">
										<a href="investor-profile-edit.html" class="navi-link py-4 active">
											<span class="navi-icon mr-2">
												<i class="icon-xl la la-user-circle"></i>
												</span>
											<span class="navi-text font-size-lg">Личные данные</span>
										</a>
									</div>
									<div class="navi-item mb-2">
										<a href="{{ route('profile.contacts') }}" class="navi-link py-4">
											<span class="navi-icon mr-2">
												<i class="icon-xl la la-phone-square"></i>
												</span>
											<span class="navi-text font-size-lg">Контакты</span>
										</a>
									</div>
									<div class="navi-item mb-2">
										<a href="{{ route('profile.password.index') }}" class="navi-link py-4">
											<span class="navi-icon mr-2">
												<i class="icon-xl la la-user-shield"></i>
											</span>
											<span class="navi-text font-size-lg">Изменение пароля</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<form class="mfcb" method="POST" action="{{ route('profile.update') }}"  enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="card card-custom card-stretch">
								<div class="card-header py-3">
									<div class="card-title align-items-start flex-column">
										<h3 class="card-label font-weight-bolder text-dark">Личные данные</h3>
										<span class="text-muted font-weight-bold font-size-sm mt-1">Редактирование</span>
									</div>
									<div class="card-toolbar">
										<button type="submit" class="btn btn-primary mr-2">Сохранить</button>
									</div>
								</div>
								<div class="card-body">
									<!--<div class="row">
										<label class="col-xl-3"></label>
										<div class="col-lg-9 col-xl-6">
											<h5 class="font-weight-bold mb-6">Пользователь</h5>
										</div>
									</div>-->

									<div class="form-group row">
										<label class="col-xl-3 col-lg-3 col-form-label text-right">Фото</label>
										<div class="col-lg-9 col-xl-6">												
											<div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url('{{ optional(auth()->user())->avatarUrl }}')">
												<div class="image-input-wrapper"></div>
												<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Изменить фото">
													<i class="fa fa-pen icon-sm text-muted"></i>
													<input id="avatar-upload" type="file" name="avatar" accept=".png, .jpg, .jpeg">
													<input type="hidden" name="avatar_remove">
												</label>
												<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="" data-original-title="Отменить">
													<i class="ki ki-bold-close icon-xs text-muted"></i>
												</span>
												<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="" data-original-title="Удалить фото">
													<i class="ki ki-bold-close icon-xs text-muted"></i>
												</span>
											</div>
											<span class="form-text text-muted d-block mt-5">Поддерживаемые форматы: png, jpg, jpeg.</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-xl-3 col-lg-3 col-form-label text-right">Фамилия</label>
										<div class="col-lg-9 col-xl-6">
											<input type="text" class="form-control form-control-lg form-control-solid" placeholder="Фамилия" name="firstname" value="{{ $user->firstname }}">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-xl-3 col-lg-3 col-form-label text-right">Имя</label>
										<div class="col-lg-9 col-xl-6">
											<input type="text" class="form-control form-control-lg form-control-solid" placeholder="{{ ucwords(Auth::user()->name) }}"
														name="name"
														value="{{ $user->name }}">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-xl-3 col-lg-3 col-form-label text-right">Отчество</label>
										<div class="col-lg-9 col-xl-6">
											<input type="text" class="form-control form-control-lg form-control-solid" placeholder="Отчество"
														name="patronymic" value="{{ $user->patronymic }}">
											<span class="form-text text-muted">При наличии</span>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-xl-3 col-lg-3 col-form-label text-right">Должность</label>
										<div class="col-lg-9 col-xl-6">
											<input type="text" class="form-control form-control-lg form-control-solid" placeholder="Должность"
														name="position" value="{{ $user->position }}">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('javascript')
	<script>
		$('.toast').toast('show');

		$('#avatar-upload').on('change', function() {
			let img = URL.createObjectURL(this.files[0])
			$('#kt_profile_avatar').attr('style', 'background-image: url("' + img + '"');
		})
	</script>
@endpush