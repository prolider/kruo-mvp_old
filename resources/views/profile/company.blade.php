@extends('layouts.app')

@section('content')
    <div aria-live="polite" aria-atomic="true" style="position: relative;">
  		<div style="position: absolute; top: 10px; right: 10px;">
            @if (session('upload_success') || ($errored = $errors->first()))
                <div class="toast show alert alert-{{ isset($errored) && $errored  ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
                    Файлы для подтверждения полномочий {{ isset($errored) && $errored  ? 'не были загружены.' : 'успешно загружены. Ожидайте подтверждения от администратора.' }}
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif (session('verification_canceled'))
                <div class="toast show alert alert-{{ isset($errored) && $errored  ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
                    Запрос на отмену полномочий {{ isset($errored) && $errored  ? 'не отправлен.' : 'успешно отправлен. Ожидайте подтверждения от администратора.' }}
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session()->has('profile_update'))
                <div class="toast show alert alert-{{ isset($errored) && $errored ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
                    Данные компании {{ isset($errored) && $errored ? 'не были обновлены.' : 'успешно обновлены.' }}
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
		</div>
	</div>

    <!--<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">

                    <li class="breadcrumb-item text-muted">
                        <span class="text-muted">Профиль компании инвестора</span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <span class="text-muted">Редактирование</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>-->
    <div class="subheader pb-lg-8 subheader-transparent mb-5" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    @if($company)
                        <h2 class="text-dark font-weight-bold my-1 mr-5">Профиль компании инвестора - {{ optional($company)->name }}</h2>
                    @else
                        <h2 class="text-dark font-weight-bold my-1 mr-5">Профиль компании инвестора</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="d-flex flex-row row">
                <div class="col-md-4" id="kt_profile_aside">
                    <div class="card card-custom gutter-b">
                        <div class="card-body pt-8">
                            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">

                                <div class="navi-item mb-2">
                                    <a href="{{ route('profile.company') }}" class="navi-link py-4 active">
                                        <span class="navi-icon mr-2">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-list"></i>
                                            </span>
                                        </span>
                                        <span class="navi-text font-size-lg">Реквизиты</span>
                                    </a>
                                </div>
                                <div class="navi-item mb-2">
                                    @if($company)
                                        <a href="{{ route('companies.invite', $company) }}" class="navi-link py-4">
                                            <span class="navi-icon mr-2">
                                                <span class="navi-icon mr-2">
                                                    <i class="icon-xl la la-user-secret"></i>
                                                </span>
                                            </span>
                                            <span class="navi-text font-size-lg">Наблюдатели</span>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    @include('profile.particles.company')
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        $(document).ready(function() {
            $('.input-disabled :input').prop('disabled', true);
            $('#docfile, #sigfile').change(function (e) {
                $('label[for="' + this.id + '"]').text(this.files[0].name);
            });
        });

    </script>
    <script>
	    $('.toast').toast('show');
    </script>
@endsection
