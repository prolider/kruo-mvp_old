@extends('layouts.app')

@section('content')

    <div aria-live="polite" aria-atomic="true" style="position: relative;">
  		<div style="position: absolute; top: 10px; right: 10px;">		
            @if (session('password_success') || ($errored = $errors->first()))
                <div class="toast show alert alert-{{ isset($errored) ? 'danger' : 'success' }}" role="alert" data-autohide="true" data-delay="2000">
                    Пароль {{ isset($errored) ? 'не был обновлён' : 'был успешно обновлён' }}
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
		</div>
	</div>

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
       <!-- <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">Профиль пользователя</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <span class="text-muted">Изменение пароля</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="subheader py-5 subheader-transparent my-5" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">Профиль пользователя - Изменение пароля</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="d-flex flex-row row">
                    <div class="col-md-4" id="kt_profile_aside">
                        <div class="card card-custom gutter-b">
                            <div class="card-body pt-8">
                                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                    <div class="navi-item mb-2">
                                        <a href="{{ route('profile.index') }}" class="navi-link py-4">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-user-circle"></i>
                                                </span>
                                            <span class="navi-text font-size-lg">Личные данные</span>
                                        </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                        <a href="{{ route('profile.contacts') }}" class="navi-link py-4">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-phone-square"></i>
                                                </span>
                                            <span class="navi-text font-size-lg">Контакты</span>
                                        </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                        <a href="{{ route('profile.password.index') }}" class="navi-link py-4 active">
                                            <span class="navi-icon mr-2">
                                                <i class="icon-xl la la-user-shield"></i>
                                            </span>
                                            <span class="navi-text font-size-lg">Изменение пароля</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">                       
                        <form class="mfcb" id="change-password" method="POST" action="{{ route('profile.password.update') }}"  enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card card-custom card-stretch">
                                <div class="card-header py-3">
                                    <div class="card-title align-items-start flex-column">
                                        <h3 class="card-label font-weight-bolder text-dark">Изменение пароля</h3>
                                        <span class="text-muted font-weight-bold font-size-sm mt-1">Редактирование</span>
                                    </div>
                                    <div class="card-toolbar">
                                        <button type="submit" class="btn btn-primary mr-2">Сохранить</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <label class="col-xl-3"></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <h5 class="font-weight-bold mb-6">Введите пароль</h5>
                                            <span id="old_password" class="form-text text-danger">
                                                @foreach ($errors->get('old_password') as $error)
                                                    {{ $error }}<br>
                                                @endforeach
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Старый пароль</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group" id="show_hide_password">
                                                <input type="password" name="old_password" class="form-control form-control-lg form-control-solid" value="" placeholder="Введите пароль">
                                                <div class="input-group-append">
                                                    <a class="input-group-text btn btn-primary" style="min-width: 54px;">
                                                        <i class="fa fa-eye-slash"></i>
                                                    </a>
                                                </div>
                                            </div>    
                                             <!--<div class="input-group input-group-lg input-group-solid">
                                                <input type="password" name="old_password" class="form-control form-control-lg form-control-solid" value="" placeholder="Введите пароль">
                                            
                                            </div>
                                            <div class="text-right mt-5"><a href="{{ route('register') }}" target="_blank">Забыли пароль?</a></div>-->
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <label class="col-xl-3"></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <h5 class="font-weight-bold mb-6">Введите новый пароль</h5>
                                            <span id="password" class="form-text text-danger">
                                                @foreach ($errors->get('password') as $error)
                                                    {{ $error }}<br>
                                                @endforeach
                                            </span>
                                            <span id="password_confirmation" class="form-text text-danger">
                                                @foreach ($errors->get('password_confirmation') as $error)
                                                    {{ $error }}<br>
                                                @endforeach
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Пароль</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group" id="show_hide_password_2">
                                                <input type="password" name="password" class="form-control form-control-lg form-control-solid" value="" placeholder="Введите новый пароль">
                                                <div class="input-group-append">
                                                    <a class="input-group-text btn btn-primary" style="min-width: 54px;">
                                                        <i class="fa fa-eye-slash"></i>
                                                    </a>
                                                </div>
                                            </div>                                            
                                            <span class="form-text text-muted"><small class="text-muted">{{ __('validation.strong_password') }}</small></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Подтвердите пароль</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="input-group" id="show_hide_password_3">
                                            <input type="password" name="password_confirmation" class="form-control form-control-lg form-control-solid" value="" placeholder="Повторите новый пароль">
                                                <div class="input-group-append">
                                                    <a class="input-group-text btn btn-primary" style="min-width: 54px;">
                                                        <i class="fa fa-eye-slash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script>
        $(document).ready(function() {
            $('#change-password').submit(function(e) {
                let newPassword = $('input[name="password"]').val();
                let passwordConfirmation = $('input[name="password_confirmation"]').val();
                let oldPassword = $('input[name="old_password"]').val();

                $('#password').text('');
                $('#old_password').text('');
                $('#password_confirmation').text('');

                if (newPassword === oldPassword) {
                    e.preventDefault();
                    $('#password').text('Новый пароль совпадает со старым паролем.');
                }

                if (newPassword.length < 8) {
                    e.preventDefault();
                    $('#password').text('Пароль должен содержать минимум 8 символов.');
                }

                if (newPassword != passwordConfirmation) {
                    e.preventDefault();
                    $('#password').text('Пароль и подтверждение пароля должны совпадать.');
                }
            })
        })
    </script>
    <script>
	    $('.toast').toast('show');
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                    $('#show_hide_password i').removeClass( "fa-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password i').addClass( "fa-eye" );
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password_2 a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password_2 input').attr("type") == "text"){
                    $('#show_hide_password_2 input').attr('type', 'password');
                    $('#show_hide_password_2 i').addClass( "fa-eye-slash" );
                    $('#show_hide_password_2 i').removeClass( "fa-eye" );
                }else if($('#show_hide_password_2 input').attr("type") == "password"){
                    $('#show_hide_password_2 input').attr('type', 'text');
                    $('#show_hide_password_2 i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password_2 i').addClass( "fa-eye" );
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password_3 a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password_3 input').attr("type") == "text"){
                    $('#show_hide_password_3 input').attr('type', 'password');
                    $('#show_hide_password_3 i').addClass( "fa-eye-slash" );
                    $('#show_hide_password_3 i').removeClass( "fa-eye" );
                }else if($('#show_hide_password_3 input').attr("type") == "password"){
                    $('#show_hide_password_3 input').attr('type', 'text');
                    $('#show_hide_password_3 i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password_3 i').addClass( "fa-eye" );
                }
            });
        });
    </script>
@endpush
