@extends('layouts.app')

@section('content')
<div class="card mb-3">
    <div class="card-body">
        <div class="row ">
            <div class="col-md-3">
                <img src="{{ Voyager::image($operator->avatar) }}" style="width:80px">
            </div>
            <div class="col-md-9">
                <div class="">
                    {{ $operator->name }} {{ $operator->firstname }}
                </div>
                <div>
                    <i class="fa fa-phone"></i> {{ $operator->phone ?? '-' }}
                </div>
                <div>
                    <i class="fa fa-at"></i> {{ $operator->email ?? '-'}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
