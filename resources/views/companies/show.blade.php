@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader pb-lg-8 subheader-transparent mt-3" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">{{ $company->name }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-3" id="kt_content_sidebar">
                        <div class="card card-custom gutter-b">
                            <div class="card-body ribbon ribbon-clip ribbon-left">
                                @if($company->verification_status === 'N')
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-warning"></span>Проверка не запрошена
                                        </div>
                                    @elseif($company->verification_status === 'C') 
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-primary"></span>Проверка начата
                                        </div>                                    
                                    @elseif($company->verification_status === 'R') 
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-danger"></span>Проверка отклонена
                                        </div>
                                    @elseif($company->verification_status === 'Q') 
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-warning"></span>Запрос на отмену
                                        </div>
                                    @else
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-success"></span>Подтверждено
                                        </div>
                                @endif 
                                <div class="text-left mb-7 mt-10">
                                    <h4 class="font-weight-bold my-2">{{ $company->name }}</h4>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>ИНН:</b> {{ $company->inn }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>ОГРН:</b> {{ $company->ogrn }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>КПП:</b> {{ $company->kpp }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>Адрес:</b> {{ $company->address }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>Номер:</b> {{ $company->phone }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>Сайт:</b> {{ $company->site }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>E-mail:</b> {{ $company->email }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>Дата регистрации:</b> {{ $company->registration_date }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>Должность руководителя:</b> {{ $company->management_post }}</span>
                                </div>
                                <div class="text-left text-lg my-3">
                                    <span class="my-2"><b>Руководитель:</b> {{ $company->management_name }}</span>
                                </div>
                                    @auth
                                        @php $user = auth()->user(); @endphp
                                        @if ($user->role && $user->role->name === 'admin')
                                            <div class="text-left text-lg mt-6 mb-5">
                                                @if($company->isVerified())
                                                    <button id="bitrix-btn" onclick="sendToBitrix()" class="btn @if(!$company->is_sent_to_bitrix) btn-primary @endif btn-shadow font-size-md @if($company->is_sent_to_bitrix) btn-warning @endif">
                                                        @if(!$company->is_sent_to_bitrix)
                                                            Передать в Битрикс24
                                                        @else
                                                            Повторить передачу в Битрикс24
                                                        @endif
                                                    </button>
                                                    @if($company->is_sent_to_bitrix)
                                                        <span class="text-muted d-block mt-3">Передано {{ $company->sent_to_bitrix_at->format('d.m.Y H:i') }}</span>
                                                    @endif                                                                                                        
                                                @else
                                                    <button class="btn btn-primary btn-shadow font-size-md disabled">Отправить в Битрикс24</button>
                                                @endif
                                                
                                            </div>
                                        @endif
                                    @endauth
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                <div class="card card-custom gutter-b card-stretch">
                                    <div class="card-body pt-4">
                                        <div class="d-flex align-items-end py-2">
                                            <div class="d-flex align-items-center">
                                                <div class="d-flex flex-shrink-0 mr-5">
                                                    <div class="symbol symbol-lg-75">
                                                        <img src="{{ Voyager::image($creator->avatar) }}" style="width: 150px" alt="image">
                                                        <!-- <img src="{{ asset('media/svg/flags/013-russia.svg') }}" style="width: 150px" alt="image"> -->
                                                        <!-- <i class="symbol-badge symbol-badge-bottom bg-success"></i> -->
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <span class="text-dark font-weight-bold font-size-h4 mb-0">{{ $creator->fullName }}</span>
                                                    <span class="text-muted font-weight-bold">{{ (optional($creator->role)->name) ?? 'Должность не указана' }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="py-2">
                                            <div class="d-flex align-items-center mb-2">
														<span class="flex-shrink-0 mr-3">
														<a href="#" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="Telegram"><i class="icon-lg la fab la-telegram-plane"></i></a>
														</span>
                                                <span class="flex-shrink-0 mr-3">
														<a href="#" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="Viber"><i class="icon-lg la fab la-viber"></i></a>
														</span>
                                                <span class="flex-shrink-0 mr-3">
														<a href="#" class="text-muted text-hover-primary font-weight-bold" data-toggle="tooltip" data-original-title="WhatsApp"><i class="icon-xl la fab la-whatsapp-square"></i></a>
														</span>
                                            </div>
                                            <div class="d-flex align-items-center mb-2">

                                                <a href="#" class="text-muted text-hover-primary font-weight-bold">{{ ($creator->phone) }}</a>
                                            </div>
                                            <div class="d-flex align-items-center mb-2">

                                                <a href="#" class="text-muted text-hover-primary font-weight-bold">{{ ($creator->email) }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function sendToBitrix() {
            $.post('{{ route('api.bitrix.lead.add', $company) }}', {'_token': '{{ csrf_token() }}' }, function(response) {
                if (!response.success) {
                    alert('Возникла ошибка!');
                }
                $('#bitrix-btn').addClass('disabled');
                location.reload();
            });
        }
    </script>
@endsection