@extends('layouts.app')

@section('content')
<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-center flex-wrap mr-1">
        <div class="d-flex align-items-baseline flex-wrap mr-5">
            <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-6 font-size-sm">
                <li class="breadcrumb-item text-muted">
                    <a href="#" class="text-muted">Профиль компании инвестора</a>
                </li>
                <li class="breadcrumb-item text-muted">
                    <span class="text-muted">Редактирование</span>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="subheader pb-lg-8 subheader-transparent" id="kt_subheader">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                @if(isset($company))
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Профиль компании инвестора - {{ optional($company)->name }}</h2>
                @else
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Профиль компании инвестора</h2>
            @endif
            </div>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        @if($invitation = session('invitation') ?? $errors->get('email'))
            <div id="ticketadd-alert" class="alert {{ isset($invitation['class']) ? $invitation['class'] : 'alert-danger' }} alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ isset($invitation['class']) ? $invitation['text'] : $invitation[0] }}
            </div>
        @endif
        <div class="d-flex flex-row row">
            <div class="col-md-4" id="kt_profile_aside">
                <div class="card card-custom gutter-b">
                    <div class="card-body pt-8">
                        <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                            <div class="navi-item mb-2">
                                <a href="{{ route('profile.company') }}" class="navi-link py-4">
                                    <span class="navi-icon mr-2">
                                        <span class="navi-icon mr-2">
                                            <i class="icon-xl la la-list"></i>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Реквизиты</span>
                                </a>
                            </div>
                            <div class="navi-item mb-2">
                                <span href="{{ route('companies.invite', $company) }}" class="navi-link py-4 active">
                                    <span class="navi-icon mr-2">
                                        <span class="navi-icon mr-2">
                                            <i class="icon-xl la la-user-secret"></i>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Наблюдатели</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card card-custom card-stretch">
                    <div class="card-header py-3">
                        <div class="card-title align-items-start flex-column">
                            <h3 class="card-label font-weight-bolder text-dark">Наблюдатели</h3>
                            <span class="text-muted font-weight-bold font-size-sm mt-1">Редактирование</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="form" method="POST" action="{{ route('companies.invite.send', $company) }}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Укажите email наблюдателя</label>

                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">
                                        <input name="email" type="text" required class="form-control" placeholder="Введите Email...">                                        
                                    </div>
                                    <span class="form-text text-muted">На указанный email будет выслано приглашение с инструкцией</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Укажите имя наблюдателя</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">
                                        <input name="name" type="text" required class="form-control" placeholder="Введите Имя...">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Укажите фамилию наблюдателя</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="input-group">
                                        <input name="surname" type="text" required class="form-control" placeholder="Введите Фамилию...">
                                    </div>
                                </div>
                            </div>                            
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right"></label>
                                <div class="col-lg-9 col-xl-6">
                                <button class="btn btn-primary justify-content-center" type="submit">Пригласить</button>
                                </div>
                            </div>
                        </form>
                        <div id="kt_repeater_1"></div>
                        <form method="POST" action="{{ route('companies.invite.remove', $company) }}">
                            @csrf
                            <div class="row">
                                <label class="col-xl-3"></label>
                                <div class="col-lg-9 col-xl-6">
                                    <h5 class="font-weight-bold mt-10 mb-3">Приглашенные наблюдатели</h5>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Действия с наблюдателями</label>
                                <div class="col-lg-9 col-xl-9 pt-3">
                                    <div class="checkbox-list">
                                        @foreach ($company->invitedUsers as $invited)
                                            <label class="checkbox">
                                            <input type="checkbox" name="invited[]" value="{{ $invited->id }}">
                                            <span></span>{{ $invited->name }} {{ $invited->firstname }} {{ $invited->email }} (добавлен: {{ $invited->created_at->format('d.m.Y, m:h') }})</label>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right"></label>
                            @if (!$company->invitedUsers) 
                                <div class="col-lg-9 col-xl-6">
                                    <span class="text-muted">Наблюдатели не добавлены</span>
                                </div>                                
                            @else
                                <div class="col-lg-9 col-xl-6">
                                    <button type="submit" class="btn btn-warning mr-2">Исключить выбранных</button>
                                </div>
                            @endif    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
