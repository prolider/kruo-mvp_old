@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Создание обращения - #</h2>
                </div>
                <div class="d-flex align-items-center flex-wrap">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card mb-5">
            <div class="d-flex flex-row">
                <div class="flex-row-fluid d-block" id="kt_inbox_view">
                    <div class="card card-custom shadow-sm">
                        <div class="card-body p-0">
                            <form id="kt_inbox_reply_form" action="{{ route('discussions.store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div class="d-block">
                                    @if ($isAlert)
                                        <input type="hidden" name="is_alert" value="true">
                                    @endif
                                    <div class="border-bottom">
                                        <input class="form-control border-0 px-8 min-h-45px" name="name" placeholder="Тема обращения" value="{{ $head ?? '' }}">
                                    </div>
                                    <textarea class="form-control border-0 ql-container ql-snow ql-editor ql-blank px-8" name="text" placeholder="Введите текст сообщения..." style="height: 200px;"></textarea>
                                </div>
                                <div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
                                    <div class="d-flex align-items-center mr-3">
                                        <button type="submit" class="btn btn-primary btn-shadow font-weight-bold px-6">Отправить</button>
                                        <input type="file" multiple id="files_id" style="display:none;" name="files[]" />
                                        <label for="files_id" style="margin-bottom:0px;margin-left:8px;" class="btn btn-light-primary mr-2"><i class="la la-paperclip"></i>Прикрепить файл</label>
                                        <span id="files-error"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#files_id').change(function (e) {
            var resultingSize = 0;
            let fileNames = '';

            this.files.forEach(element => {
                if (this.value === "") {
                    return;
                }

                fileNames += element.name + '<br>';

                if (element.size > 2097152) {
                    this.value = "";

                    $('#files-error').text("Размер одного файла не должен превышать 2 мегабайт");

                    return;
                }

                resultingSize += element.size;

                let maxSize = 5242880;

                if (maxSize < resultingSize) {
                    this.value = "";

                    $('#files-error').text("Общий размер файлов не должен превышать 5 мегабайт");

                    return;
                }
            });

            $('#files-error').html(fileNames);
        });
    </script>
@endsection
