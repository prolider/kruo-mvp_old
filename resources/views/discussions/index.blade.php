@extends('layouts.app')

@section('content')
<div class="container content">
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h2 class="text-dark font-weight-bold my-1 mr-5">Все обращения</h2>
                </div>
                <div class="d-flex align-items-center flex-wrap">
                    @can('canCreateDiscussions')
                        <div class="align-items-center ml-3 ">
                        <a href="{{ route('discussions.create') }}" class="btn btn-fixed-height btn-primary btn-shadow font-weight-bolder font-size-sm px-5 my-1">
                        Cоздать обращение</a>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-row">
        <div class="container">
            <div class="flex-row-fluid d-block mb-5" id="kt_inbox_list">
                <div class="card card-custom card-stretch">
                    <div class="card-body table-responsive px-5">                        
                        @forelse($discussions as $discussion)
                            <div class="list list-hover min-w-500px mb-3" data-inbox="list">
                                @php
                                    $isRead = $discussion->messages_count > 0 ? "font-weight-bolder" : "";
                                @endphp
                                <div class="d-flex align-items-start list-item px-3 py-3" onclick="window.location = '{{ route('discussions.show', $discussion)}}';">
                                    <div class="d-flex align-items-center">
                                        <div class="d-flex align-items-center flex-wrap mr-3" style="min-width:200px;" data-toggle="view">
                                            <span class="symbol symbol-35 mr-3">
                                                <span class="symbol-label" style="background-image: url( {{ optional($discussion->owner)->avatarUrl }} )"></span>
                                            </span>
                                            <div class="d-block" style="max-width:150px;">
                                                <span class="font-weight-bold text-dark-75 d-block">{{ $discussion->owner->name }} {{ $discussion->owner->firstname }}</span>
                                                <span class="text-muted d-block">{{ $discussion->owner->company->name }}</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 mt-2 mr-2" data-toggle="view">
                                        <div>
                                            <span class="{{ $isRead }} font-size-lg mr-2">#{{ $discussion->id }} - {{ $discussion->name }}</span>
                                        </div>
                                    </div>                                    
                                    @if ($discussion->is_alert)
                                        <div class="mt-2 mr-2">
                                            <span class="label w-120px label-light-danger font-weight-bold label-inline">Требует внимания</span>
                                            </div>
                                        @endif
                                    @if ($isRead)
                                        <div class="mt-2 mr-2">
                                            <span class="label font-weight-bold label-lg  label-danger label-inline">{{ $discussion->messages_count }}</span>
                                        </div>
                                    @endif    
                                    <div class="mt-2 mr-3 {{ $isRead }} w-100px text-right" data-toggle="view">{{ $discussion->created_at->format('d.m.Y') }}</div>
                                </div>
                            </div>
                            @empty
                                <div class="d-flex align-items-start list-item card-spacer-x py-3">
                                У вас пока нет обращений
                                </div>
                            @endforelse                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('theme/assets/js/pages/widgets.js') }}"></script>
@endsection
