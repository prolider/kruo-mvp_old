@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h2 class="text-dark font-weight-bold my-1 mr-5">
                        Обращение #{{ $discussion->id }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="card card-custom  mb-5">
            <div class="card-header align-items-center flex-wrap justify-content-start py-5 h-auto">
                <div class="d-flex align-items-center my-2">
                    <a href="{{ route('discussions.index') }}" class="btn btn-clean btn-icon btn-sm mr-6" data-inbox="back" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Вернуться к списку обращений">
                        <i class="flaticon2-left-arrow-1"></i>
                    </a>
                </div>
                <div class="d-flex align-items-center justify-content-start pl-2 py-6">
                    <span class="symbol symbol-50 mr-4">
                        <span class="symbol-label" style="background-image: url('{{$discussion->owner->avatarUrl}}')"></span>
                    </span>
                    <div class="d-flex flex-column flex-grow-1 flex-wrap">
                        <div class="d-block">
                            <span class="font-size-lg font-weight-bolder text-dark-75 d-block">{{ $discussion->owner->name }} {{ $discussion->owner->firstname }}</span>
                            <span class="text-muted d-block">{{ $discussion->owner->company->name }}</span>
                        </div>
                    </div>                        
                </div>
                <div class="ml-auto">
                    <div>
                        @if ($discussion->is_alert)
                                <span class="label label-light-danger font-weight-bold label-inline">Требует внимания</span>
                        @endif
                    </div>
                    <div class="font-weight-bold text-muted ml-auto">
                        {{ $discussion->created_at->format('d.m.Y, m:h') }}
                    </div>
                </div>
            </div>
            <div class="card-body p-0 pb-5">
                <div class="d-block align-items-center justify-content-between flex-wrap card-spacer-x py-5">  
                    <div class="d-block align-items-center mr-2 py-2">  
                        <div class="font-weight-bold font-size-h5 mr-3">Тема обращения:</div>                    
                        <div class="font-weight-bold font-size-h5 mr-3">{{ $discussion->name }}</div>
                    </div>                    
                </div>
                <div class="card-spacer-x py-3 ">
                    <div class="font-weight-bold font-size-h5 mr-3">Текст обращения: </div>
                    <p>{!! $discussion->text !!}</p>
                    @if (is_array($discussion->filesUrls))
                        <div class="d-flex align-items-center py-3">
                            @foreach ($discussion->filesUrls as $key => $url)
                                <div class="mr-4">
                                    <a href="{{ $url }}" class="py-1">
                                        <span class="mr-0">
                                            <i class="icon-xl text-info la la-file-download"></i>
                                        </span>
                                        <span class="navi-text">{{ $key }}</span>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                <hr>
                @foreach ($discussion->messages as $message)
                    <div class="shadow-xs" data-inbox="message">
                        <div class="d-flex align-items-center card-spacer-x py-6">
                            <span class="symbol symbol-50 mr-4">
                                <span class="symbol-label" style="background-image: url('{{ $message->author->avatarUrl }}')"></span>
                            </span>
                            <div class="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                                <div class="d-flex">
                                    <span class="font-size-lg font-weight-bolder text-dark-75 mr-2">
                                        {{ auth()->user()->id === $message->user_owner_id ? 'Вы' : $message->author->name }} {{ auth()->user()->id === $message->user_owner_id ? '' : $message->author->firstname }}
                                    </span>
                                </div>
                            </div>
                            @if ($user->id !== $message->user_owner_id && $message->status === App\Models\DiscussionMessage::STATUS_UNREAD)
                                <div class="d-flex align-items-center mr-5">
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-light-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Не прочитано</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="">
                                            <a class="dropdown-item" href="{{ route('discussions.read', $message) }}">Отметить как "Прочитанно"</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="d-flex align-items-center">
                                <div class="font-weight-bold text-muted mr-2">
                                    {{ $message->created_at->format('d.m.Y, m:h') }}
                                </div>
                            </div>
                        </div>

                        <div class="card-spacer-x py-3 ">
                            <p>{{ $message->text }}</p>
                            @if ($message->filenames)
                                @foreach ($message->filesUrls as $key => $url)
                                    <div>
                                        <a href="{{ $url }}"><i class="icon-xl text-info la la-file-download"></i> {{ $key }}</a>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="card card-custom shadow-sm mb-5">
            <div class="card-body p-0">
                <form id="kt_inbox_reply_form" action="{{ route('discussions.send', $discussion) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="d-block">
                        <textarea placeholder="Введите сообщение ..." style="height: 200px;" name="text" class="form-control p-10" id="text"></textarea>
                    </div>
                    @error('text')
                        <span class="label label-light-danger font-weight-bold label-inline">{{ $message }}</span>
                    @enderror
                    <div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
                        <div class="d-flex align-items-center mr-3">
                            <div class="btn-group mr-3">
                                <button type="submit" class="btn btn-primary btn-shadow font-weight-bold px-6">Отправить</button>
                            </div>
                            <div class="custom-file" style="margin: 10px 0;">
                                <input type="file" class="custom-file-input" id="customFile" name="files[]" multiple>
                                <label class="custom-file-label" for="customFile">Выберите файлы</label>
                            </div>
                        </div>
                        @error('files.*')
                            <span class="label label-light-danger font-weight-bold label-inline">{{ $message }}</span>
                        @enderror
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
