@extends('layouts.app')

@section('content')
    <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
        <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h2 class="text-dark font-weight-bold my-1 mr-5">
                        Уведомления
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
        @if($notifications->count() > 0)
            @foreach($notifications as $notification)
                @if ($notification->notificable instanceof \App\Models\User)
                    <div class="card card-custom gutter-b notice">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-11">
                                    <span class="text-muted">{{ $notification->created_at->Format('d.m.Y H:i:s') }}</span>
                                    <h5>
                                        <a href="{{ route('profile.company') }}">{{ $notification->name }}</a>
                                    </h5>
                                    <p>{{ $notification->description }}</p>
                                </div>
                                <div class="col-md-1 text-right">
                                    <!--<a href="{{ route('voyager.users.edit', $notification->notificable) }}" class="btn btn-brown" data-id="{{ $notification->id }}"><i class="fa fa-folder-open"></i></a>-->
                                    <button class="btn btn-icon btn-light btn-hover-danger btn-sm remove-notice" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                @elseif ($notification->notificable instanceof \App\Models\Company)

                    <div class="card card-custom gutter-b notice">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-11">
                                    <span class="text-muted">{{ $notification->created_at->Format('d.m.Y H:i:s') }}</span>
                                    <h5>
                                        @if(auth()->user()->role->name == 'admin')
                                            <a href="{{ route('voyager.companies.edit', $notification->notificable) }}">{{ $notification->name }}</a>
                                        @elseif (auth()->user()->role->name == 'operator')
                                            <a href="{{ route('projects.index') }}">{{ $notification->name }}</a>
                                        @else
                                            <a href="{{ route('profile.company') }}">{{ $notification->name }}</a>
                                        @endif
                                    </h5>
                                    <p>{{ $notification->description }}</p>
                                </div>
                                <div class="col-md-1 text-right">
                                    <!--<a href="{{ route('voyager.companies.edit', $notification->notificable) }}" class="btn btn-brown" data-id="{{ $notification->id }}"><i class="fa fa-folder-open"></i></a>-->
                                    <button class="btn btn-icon btn-light btn-hover-danger btn-sm remove-notice" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>

                @elseif ($notification->notificable instanceof \App\Models\Discussion)

                    <div class="card card-custom gutter-b notice">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-11">
                                    <span class="text-muted">{{ $notification->created_at->Format('d.m.Y H:i:s') }}</span>
                                    <h5>
                                        <a href="{{ route('discussions.show', $notification->notificable) }}">{{ $notification->name }}</a>
                                    </h5>
                                    <p>{{ $notification->description }}</p>
                                </div>
                                <div class="col-md-1 text-right">
                                    <!--<a href="{{ route('voyager.companies.edit', $notification->notificable) }}" class="btn btn-brown" data-id="{{ $notification->id }}"><i class="fa fa-folder-open"></i></a>-->
                                    <button class="btn btn-icon btn-light btn-hover-danger btn-sm remove-notice" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>

                    @elseif ($notification->notificable instanceof \App\Models\Article)

                        <div class="card card-custom gutter-b notice">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-11">
                                        <span class="text-muted">{{ $notification->created_at->Format('d.m.Y H:i:s') }}</span>
                                        <h5>
                                            <a href="{{ route('voyager.articles.edit', $notification->notificable) }}">
                                                {{ $notification->name }}
                                            </a>
                                        </h5>
                                        <p>{{ $notification->description }}</p>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <button class="btn btn-icon btn-light btn-hover-danger btn-sm remove-notice" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>

                    @elseif ($notification->notificable instanceof \App\Models\Event)

                        <div class="card card-custom gutter-b notice">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-11">
                                        <span class="text-muted">{{ $notification->created_at->Format('d.m.Y H:i:s') }}</span>
                                        <h5>
                                            <a href="{{ route('events.show', $notification->notificable) }}">
                                                {{ $notification->name }}
                                            </a>
                                        </h5>
                                        <p>{{ $notification->description }}</p>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <button class="btn btn-icon btn-light btn-hover-danger btn-sm remove-notice" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>

                    @else

                    <div class="card card-custom gutter-b notice">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-11">
                                    @if($notification->notificable)
                                        <span class="text-muted">{{ $notification->created_at->Format('d.m.Y H:i:s') }}</span>
                                        <h5>
                                            <a href="{{ route('ticket.show', $notification->notificable) }}">{{ $notification->name }}</a>
                                        </h5>
                                    @endif
                                    <p>{{ $notification->notificable->formResult->form->service->name ?? '' }}</p>
                                    <p>{{ $notification->description }}</p>
                                </div>
                                <div class="col-md-1 text-right">
                                    <button class="btn btn-icon btn-light btn-hover-danger btn-sm remove-notice" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Удалить" data-id="{{ $notification->id }}"><i class="fa fa-trash"></i></button>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>

                @endif
            @endforeach
            @else
                <h5>Нет уведомлений для просмотра</h5>
            @endif
        </div>
    </div>
@stop

@push('javascript')
    <script>
        $('.remove-notice').on('click', function (e) {
          e.preventDefault();
          const id = $(e.currentTarget).data('id');
          $.ajax(`/api/notifications/${id}`, {
            method: 'POST',
            data: {
              _method: 'DELETE',
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: (response) => {
              $(e.currentTarget).parent().parent().parent().parent().remove();
              $('meta[name="csrf-token"]').attr('content', response.token);
            }
          })
        })
    </script>
@endpush
