@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="text-dark font-weight-bold my-1 mr-5">Мои проекты</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    @foreach($companies as $company)
                        <div class="col-md-4">
                            <div class="card card-custom gutter-b">
                                <div class="card-header ribbon ribbon-clip ribbon-left">
                                    @if($company->verification_status === 'N')
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-warning"></span>Проверка не запрошена
                                        </div>
                                    @elseif($company->verification_status === 'C')
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-primary"></span>Проверка начата
                                        </div>
                                    @elseif($company->verification_status === 'R')
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-danger"></span>Проверка отклонена
                                        </div>
                                    @elseif($company->verification_status === 'Q')
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-warning"></span>Запрос на отмену
                                        </div>
                                    @else
                                        <div class="ribbon-target" style="top: 12px;">
                                            <span class="ribbon-inner bg-success"></span>Подтверждено
                                        </div>
                                    @endif
                                    <h3 class="card-title mt-15">{{ $company->name }}</h3>
                                </div>

                                <div class="card-body">
                                    <div class="d-flex flex-column justify-content-between ">

                                        <p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0"><b>ИНН:</b>
                                            {{ $company->inn }}</p>
                                        <p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0"><b>Дата создания:</b>
                                            {{ $company->created_at->format('d.m.Y, H:i') }}</p>
                                        <div class="d-flex align-items-center justify-content-between mt-5">
                                            <a href="{{ route('companies.show', $company) }}" class="btn btn-primary btn-shadow font-size-md">Посмотреть профиль</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
