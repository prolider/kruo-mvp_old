@extends('layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
		<div class="subheader py-6 py-lg-8 subheader-transparent" id="kt_subheader">
			<div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">

				<div class="d-flex align-items-center flex-wrap mr-1">
					<div class="d-flex align-items-baseline flex-wrap mr-5">
						<h2 class="text-dark font-weight-bold my-1 mr-5">{{ __('app.main_title') }}</h2>

					</div>
				</div>
			</div>
		</div>
		<div class="d-flex flex-column-fluid">
			<div class="container">
				<div class="card card-custom gutter-b">
					<div class="card-body d-flex align-items-center py-5">
						<div class="d-flex flex-center position-relative ml-5 mr-15 ml-lg-9">
							<span class="svg-icon svg-icon-5x svg-icon-primary position-absolute opacity-15">
								<svg xmlns="http://www.w3.org/2000/svg" width="70px" height="70px" viewBox="0 0 70 70" fill="none">
									<g stroke="none" stroke-width="1" fill-rule="evenodd">
										<path d="M28 4.04145C32.3316 1.54059 37.6684 1.54059 42 4.04145L58.3109 13.4585C62.6425 15.9594 65.3109 20.5812 65.3109 25.5829V44.4171C65.3109 49.4188 62.6425 54.0406 58.3109 56.5415L42 65.9585C37.6684 68.4594 32.3316 68.4594 28 65.9585L11.6891 56.5415C7.3575 54.0406 4.68911 49.4188 4.68911 44.4171V25.5829C4.68911 20.5812 7.3575 15.9594 11.6891 13.4585L28 4.04145Z" fill="#000000"></path>
									</g>
								</svg>
							</span>
							<i class="fa fa-info icon-xl mr-1 text-primary"></i>
						</div>
						<div class="m-0 font-size-lg">
						@if(app()->getLocale() === 'en')
							{!! setting('site.mainpage-about-eng') !!}
						@else
							{!! setting('site.mainpage-about') !!}
						@endif	
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="card card-custom card-stretch gutter-b bg-diagonal bg-diagonal-light-primary">
							<div class="card-body">
								<div class="d-flex flex-column justify-content-between ">
									<h4><span class="card-label ">{{ __('app.cooperation_platform') }}</span></h4>
									<p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">Доступ к базе предложений сотрудничества от проверенных инвесторов для совместной реализации проектов и стартапов.</p>
									<div class="d-flex align-items-center justify-content-between mt-5">
										<a href="{{ route('partners.index') }}" class="btn btn-primary btn-shadow font-size-md">{{ __('app.select') }}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card card-custom card-stretch gutter-b bg-diagonal">
							<div class="card-body">
								<div class="d-flex flex-column justify-content-between ">
									<h4><span class="card-label ">{{ __('app.investor_guide') }}</span></h4>
									<p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">Подбор и получение услуг для Вашего инвестиционного проекта. Подберите подходящее направление и актуальную услугу.</p>
									<div class="d-flex align-items-center justify-content-between mt-5">
										<a href="{{ route('services.index') }}" class="btn btn-primary btn-shadow font-size-md">{{ __('app.select') }}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="card card-custom card-stretch gutter-b bg-diagonal bg-diagonal-light-warning">
							<div class="card-body">
								<div class="d-flex flex-column justify-content-between ">
											<h4><span class="card-label ">{{ __('app.support_measures') }}</span></h4>
											<p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">Подбор льгот или мер поддержки для Вашего инвестиционного проекта с фильтром и рубрикатором.</p>
									<div class="d-flex align-items-center justify-content-between mt-5">
									<a href="{{ route('measures.index') }}" class="btn btn-primary btn-shadow font-size-md">{{ __('app.select') }}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="card card-custom card-stretch gutter-b">
							<div class="card-body">
								@if(app()->getLocale() === 'en')
									
								@else	
									<div id="stats-widget-slider-1" class="carousel slide">
										<div class="d-flex align-items-center justify-content-between flex-wrap">
											<span class="font-size-h4 text-muted text-uppercase pr-2">Категории услуг</span>
											<div class="p-0">
												<a href="#stats-widget-slider-1" class="btn btn-light btn-sm" role="button" data-slide="prev"><i class="la la-angle-left "></i></a>
												<a href="#stats-widget-slider-1" class="btn btn-light btn-sm" role="button" data-slide="next"><i class="la la-angle-right "></i></a>
											</div>
										</div>
										<div class="carousel-inner pt-9">
											@foreach ($serviceCategories as $category)
												@if ($loop->first)
													<div class="carousel-item active">
														<div class="d-flex flex-column justify-content-between h-100">
															<h3 class="font-size-h4 text-dark-75 text-hover-primary font-weight-bold cursor-pointer">
																<a href="{{ route('services.categories.show', ['serviceCategory' => $category]) }}">{{ $category->name }}</a>
															</h3>
															<p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">{{ $category->class_name }}</p>
														</div>
													</div>
												@else
													<div class="carousel-item ">
														<div class="d-flex flex-column justify-content-between h-100">
															<h3 class="font-size-h4 text-dark-75 text-hover-primary font-weight-bold cursor-pointer">
																<a href="{{ route('services.categories.show', ['serviceCategory' => $category]) }}">{{ $category->name }}</a>
															</h3>
															<p class="text-dark-75 font-size-lg font-weight-normal pt-2 mb-0">{{ $category->class_name }}</p>
														</div>
													</div>
												@endif
											@endforeach
										</div>
									</div>
									<div class="d-flex align-items-center justify-content-between mt-5">
										<a href="{{ route('services.index') }}" class="btn btn-primary btn-shadow font-size-md">Смотреть все</a>
									</div>
								@endif	
							</div>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="card card-custom card-stretch gutter-b">
							<div class="card-body">
								<div id="stats-widget-slider-2" class="carousel slide" data-ride="carousel" data-interval="8000">
									<div class="d-flex align-items-center justify-content-between flex-wrap">
										<span class="font-size-h4 text-muted text-uppercase pr-2">{{ __('app.news') }}</span>
										<div class="p-0">
											<a href="#stats-widget-slider-2" class="btn btn-light btn-sm" role="button" data-slide="prev"><i class="la la-angle-left "></i></a>
											<a href="#stats-widget-slider-2" class="btn btn-light btn-sm" role="button" data-slide="next"><i class="la la-angle-right "></i></a>
										</div>
									</div>
									<div class="carousel-inner pt-9">
										@foreach ($mainNews as $news)
											@if ($loop->first)
												<div class="carousel-item active">
													<div class="d-flex flex-column justify-content-between h-100">
														<h3 class="font-size-h4 text-dark-75 text-hover-primary cursor-pointer">
														<a href="{{ route('slug', $news->getSlugHierarchy()) }}">{{ $news->name }}</a></h3>
														<span class="text-muted font-weight-bold mt-2">{{ $news->created_at->format('d.m.Y') }}</span>
													</div>
												</div>
											@else
												<div class="carousel-item">
													<div class="d-flex flex-column justify-content-between h-100">
														<h3 class="font-size-h4 text-dark-75 text-hover-primary cursor-pointer">
															<a href="{{ route('slug', $news->getSlugHierarchy()) }}">{{ $news->name }}</a>
														</h3>
														<span class="text-muted font-weight-bold mt-2">{{ $news->created_at->format('d.m.Y') }}</span>
													</div>
												</div>
											@endif
										@endforeach
									</div>
								</div>
								@if($mainNews->first())
									<div class="d-flex align-items-center justify-content-between mt-5">
										<a href="{{ route('slug', $mainNews->first()->category->getSlugHierarchy()) }}" class="btn btn-primary btn-shadow font-size-md">{{ __('app.more') }}</a>
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

