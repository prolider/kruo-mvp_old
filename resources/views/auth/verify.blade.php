@extends('auth.login')

@section('verification')
    <div class="card mb-5">
        <div class="card-body">
            @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    Новая ссылка для подтверждения была успешно отправлена на Ваш email указанный при регистрации
                </div>
            @endif
            <p>Указанный email уже зарегистрирован в системе, но пока не был подтвержден. Пожалуйста, проверьте вашу почту на наличие письма со ссылкой на подтверждение email адреса.</p>
            <p>Если Вы не получили письмо со ссылкой, повторите отправку</p>
            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-primary">Выслать письмо еще раз</button>.
            </form>
        </div>

    </div>
@endsection
