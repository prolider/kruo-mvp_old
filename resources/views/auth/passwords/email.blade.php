@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <!--begin::Content-->
        <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
            <!--begin::Content body-->
            <div class="d-flex flex-column-fluid flex-center">
                <div class="card mb-3">
                    <div class="card-body p-5">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <!--begin::Forgot-->
                        <div class="login-form login-forgot">
                            <!--begin::Form-->
                            <form class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{ route('password.email') }}" novalidate="novalidate" id="kt_login_forgot_form">
                                @csrf
                                <!--begin::Title-->
                                <div class="pb-5 pt-lg-0 pt-5">
                                    <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Забыли пароль ?</h3>
                                    <p class="text-muted font-weight-bold font-size-h4">введите email для восстановления пароля</p>
                                </div>
                                <!--end::Title-->
                                <!--begin::Form group-->
                                <div class="form-group">
                                    <input class="form-control form-control-solid h-auto p-6 rounded-lg font-size-h6 @error('email') is-invalid @enderror" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <!--end::Form group-->
                                <!--begin::Form group-->
                                <div class="form-group d-flex flex-wrap pb-lg-0">
                                    <button type="submit" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Востановить</button>
                                </div>
                                <!--end::Form group-->
                                </form>
                                <!--end::Form-->
                        </div>
                        <!--end::Forgot-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
