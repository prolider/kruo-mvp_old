@extends('layouts.app', [
    'withMenu' => false,
    'withHeader' => false,
    'withFooter' => false,
    'withWrapper' => false
])
@section('content')
    <div class="login login-1 d-flex flex-column flex-lg-row flex-column-fluid bg-white login-signup-on" id="kt_login">
        <div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #003366;">
            <div class="d-flex flex-column-auto flex-column my-auto">
                @php
                    $logoData = json_decode(setting('site.logo'));
                    $logo = $logoData[0]->download_link ?? asset('theme/assets/media/logos/logo-1.svg');
                @endphp
                <a href="/" class="text-center mt-15 mb-10">
                    <img alt="Logo" src="{{ !json_last_error() ? Voyager::image($logo) : $logo }}" class="h-85px" />
                </a>
                <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg text-white mb-10">Портал для инвесторов
                    <br>Ульяновской области
                </h3>
                <div class="flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden px-20 pb-10 mx-auto">
                    <span class="text-light text-center font-size-h5">После регистрации Вам будет предоставлен доступ к личному кабинету. В подтверждение будет отправлено письмо на указанный при регистрации e-mail.
                    </span>
                </div>
            </div>
        </div>
        <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">

            <div class="d-flex flex-column-fluid flex-center">
                <div class="login-form login-signin">
                    @yield('verification')
                    <form method="POST" action="{{ route('login') }}" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_login_signin_form" novalidate="novalidate" >
                         @csrf
                        <div class="pb-5 pt-lg-0 pt-5">
                            <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Добро пожаловать</h3>
                            <span class="text-muted font-weight-bold font-size-h4">Впервые здесь?
                            <a href="javascript:;" id="kt_login_signup" class="text-primary font-weight-bolder">Зарегистрироваться</a></span>
                        </div>
                        <div class="form-group fv-plugins-icon-container">
                            <label class="font-size-h6 font-weight-bolder text-dark">Email</label>
                            <input  class="form-control form-control-solid h-auto p-6 rounded-lg" type="email" placeholder="Введите email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <div class="form-group fv-plugins-icon-container">
                            <div class="d-flex justify-content-between mt-n5">
                                <label class="font-size-h6 font-weight-bolder text-dark pt-5">Пароль</label>
                                <a href="javascript:;" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Забыли пароль ?</a>
                            </div>

                            <div class="input-group" id="show_hide_password">
                                <input  class="form-control form-control-solid h-auto p-6" placeholder="Введите пароль" type="password" name="password" required autocomplete="current-password">
                                <div class="input-group-append">
                                    <a class="input-group-text btn btn-primary" style="min-width: 54px;">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                </div>
                            </div>
                            @error('password')
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="fv-plugins-message-container"></div>
                        </div>

                        <div class="pb-lg-0 pb-5">
                            <button type="submit" id="kt_login_signin_submit" class="btn btn-primary btn-shadow font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Войти</button>
                        </div>
                        <input type="hidden"><div>
                        </div>
                    </form>
                </div>
                <div class="login-form login-signup">
                    <form method="POST" action="{{ route('register') }}" class="form fv-plugins-bootstrap fv-plugins-framework" novalidate="novalidate" id="kt_login_signup_form">
                        @csrf
                        <div class="pb-5 pt-lg-0 pt-5">
                            <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Зарегистрироваться</h3>
                            <p class="text-muted font-weight-bold font-size-h4">Укажите данные для регистрации</p>
                        </div>
                        <div class="form-group fv-plugins-icon-container">
                            <input class="form-control @error('name') is-invalid @enderror form-control-solid h-auto p-6 rounded-lg font-size-h6" type="text" placeholder="Имя" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <div class="form-group">
                            <input class="form-control form-control-solid h-auto p-6 rounded-lg font-size-h6 mask-phone" type="phone" placeholder="Телефон" name="phone" autocomplete="phone">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group fv-plugins-icon-container">
                            <input class="form-control form-control-solid h-auto p-6 rounded-lg font-size-h6 @error('email') is-invalid @enderror" type="email" placeholder="Email" name="email" autocomplete="email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <div class="form-group fv-plugins-icon-container">
                            <div class="input-group" id="show_hide_password_2">
                                <input  class="form-control form-control-solid h-auto p-6" placeholder="Введите пароль" type="password" name="password" required autocomplete="current-password">
                                <div class="input-group-append">
                                    <a class="input-group-text btn btn-primary" style="min-width: 54px;">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                </div>
                            </div>
                            <small class="text-muted">{{ __('validation.strong_password') }}</small>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <div class="form-group fv-plugins-icon-container">
                            <div class="input-group" id="show_hide_password_3">
                                <input  class="form-control form-control-solid h-auto p-6" placeholder="Повторите пароль" type="password" name="password_confirmation" required autocomplete="current-password">
                                <div class="input-group-append">
                                    <a class="input-group-text btn btn-primary" style="min-width: 54px;">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="fv-plugins-message-container"></div>
                        </div>
                        <div class="form-group d-flex align-items-center fv-plugins-icon-container">
                            <label class="checkbox mb-0">
                                <input type="checkbox" name="policy">
                                <span></span>
                            </label>
                            <div class="pl-2">Я согласен
                                <a href="{{ route('policy') }}" class="ml-1">с Политикой конфиденциальности</a>
                            </div>
                            <div class="fv-plugins-message-container">
                        </div>
                        </div>
                        <div class="form-group d-flex flex-wrap pb-lg-0 pb-3">
                            <button type="submit" id="kt_login_signup_submit" class="btn btn-primary btn-shadow font-weight-bolder font-size-h6 px-5 py-4 my-3 mr-3">Создать кабинет инвестора</button>
                            <button type="button" id="kt_login_signup_cancel" class="btn btn-light-primary btn-shadow font-weight-bolder font-size-h6 px-5 py-4 my-3">Уже есть кабинет?</button>
                        </div>
                        <div>
                        </div>
                    </form>
                </div>
                <div class="login-form login-forgot">
                    <form class="form fv-plugins-bootstrap fv-plugins-framework" method="POST" action="{{ route('password.email') }}" novalidate="novalidate" id="kt_login_forgot_form">
                        @csrf
                        <div class="pb-5 pt-lg-0 pt-5">
                            <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Забыли пароль?</h3>
                            <p class="text-muted font-weight-bold font-size-h4">введите email для восстановления пароля</p>
                        </div>
                        <div class="form-group">
                            <input class="form-control form-control-solid h-auto p-6 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off" />
                        </div>
                        <div class="form-group d-flex flex-wrap pb-lg-0">
                            <button type="button" id="kt_login_forgot_submit" class="btn btn-primary btn-shadow font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Отправить</button>
                            <button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary btn-shadow font-weight-bolder font-size-h6 px-8 py-4 my-3">Вспомнили пароль?</button>
                        </div>
                        </form>
                </div>
                </div>
            <div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
                <a href="{{ route('policy') }}" class="text-primary font-weight-bolder font-size-h6">Политика конфиденциальности</a>
                <a href="{{ route('about') }}" class="text-primary ml-10 font-weight-bolder font-size-h6">Справочная информация</a>
            </div>
        </div>
    </div>
    <style>
        .content {
            padding:0;
        }
    </style>
@endsection
@section('js')
    <script src="{{ asset('theme/assets/js/pages/custom/login/login.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script>
        $('.mask-phone').mask('+7(999)999-99-99');
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password input').attr("type") == "text"){
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass( "fa-eye-slash" );
                    $('#show_hide_password i').removeClass( "fa-eye" );
                }else if($('#show_hide_password input').attr("type") == "password"){
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password i').addClass( "fa-eye" );
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password_2 a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password_2 input').attr("type") == "text"){
                    $('#show_hide_password_2 input').attr('type', 'password');
                    $('#show_hide_password_2 i').addClass( "fa-eye-slash" );
                    $('#show_hide_password_2 i').removeClass( "fa-eye" );
                }else if($('#show_hide_password_2 input').attr("type") == "password"){
                    $('#show_hide_password_2 input').attr('type', 'text');
                    $('#show_hide_password_2 i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password_2 i').addClass( "fa-eye" );
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password_3 a").on('click', function(event) {
                event.preventDefault();
                if($('#show_hide_password_3 input').attr("type") == "text"){
                    $('#show_hide_password_3 input').attr('type', 'password');
                    $('#show_hide_password_3 i').addClass( "fa-eye-slash" );
                    $('#show_hide_password_3 i').removeClass( "fa-eye" );
                }else if($('#show_hide_password_3 input').attr("type") == "password"){
                    $('#show_hide_password_3 input').attr('type', 'text');
                    $('#show_hide_password_3 i').removeClass( "fa-eye-slash" );
                    $('#show_hide_password_3 i').addClass( "fa-eye" );
                }
            });
        });
    </script>
@endsection
