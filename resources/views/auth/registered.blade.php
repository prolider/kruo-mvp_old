@extends('layouts.app', [
    'withMenu' => false,
    'withHeader' => false,
    'withFooter' => false,
    'withWrapper' => false
])

@section('content')
<div class="login login-1 d-flex flex-column flex-lg-row flex-column-fluid bg-white login-signin-on" id="kt_login">
    <!--begin::Aside-->
    <div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #003366;">
        <!--begin::Aside Top-->
        <div class="d-flex flex-column-auto flex-column my-auto">
            <!--begin::Aside header-->
            @php
                $logoData = json_decode(setting('site.logo'));
                $logo = $logoData[0]->download_link ?? asset('theme/assets/media/logos/logo-1.svg');
            @endphp
            <a href="/" class="text-center mt-15 mb-10">
                <img alt="Logo" src="{{ !json_last_error() ? Voyager::image($logo) : $logo }}" class="h-85px" />
            </a>
            <!--end::Aside header-->
            <!--begin::Aside title-->
            <h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg text-white mb-10">Портал для инвесторов
                <br>Ульяновской области
            </h3>
            <!--end::Aside title-->
            <!--begin::Aside description-->
            <div class="flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden px-20 pb-10 mx-auto">
                <span class="text-light text-center font-size-h5">После регистрации Вам будет предоставлен доступ к личному кабинету. В подтверждение будет отправлено письмо на указанный при регистрации e-mail.
                </span>
            </div>
            <!--end::Aside description-->
        </div>
        <!--end::Aside Top-->
    </div>
    <!--begin::Aside-->
    <!--begin::Content-->
    <div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
        <!--begin::Content body-->
        <div class="d-flex flex-column-fluid flex-center">
            <!--begin::Signin-->
            <div class="login-form login-signin">
                <div class="flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden px-20 pb-10 mx-auto">
                    <span class="text-dark text-center font-size-h5">
                        Регистрация прошла успешно. Вам необходимо подтвердить email указанный при регистрации. На указанный email было отправлено письмо с подтверждением и ссылкой для активации аккаунта. Пожалуйста, проверьте почту и подтвердите регистрацию.
                    </span>
                </div>
                <div class="flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden px-20 pb-10 mx-auto">
                    <a href="{{ route('login')}}" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3">Авторизация</a>
                </div>
            </div>
            <!--end::Signin-->
            </div>
        <div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
            <a href="{{ route('policy') }}" class="text-primary font-weight-bolder font-size-h6">Политика конфиденциальности</a>
            <a href="{{ route('about') }}" class="text-primary ml-10 font-weight-bolder font-size-h6">Справочная информация</a>
        </div>
        <!--end::Content footer-->
    </div>
    <!--end::Content-->
</div>
<!--end::Login-->
<style>
    .content {
        padding:0;
    }
</style>
@endsection
