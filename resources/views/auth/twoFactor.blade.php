@extends('layouts.app')

@section('title')
    Подтверждение Email адреса
@endsection

@section('content')
    <div class="container content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="text-center">
                    <p class="moto-text_system_9">Двухфакторная аутентификация</p>
                </div>
                <div class="card mb-3">
                    <div class="card-body p-5">
                        @if(session()->has('message'))
                            <p class="alert alert-info">
                                {{ session()->get('message') }}
                            </p>
                        @endif
                        <form method="POST" action="{{ route('verify-authentication.store') }}">
                            {{ csrf_field() }}
                            <p class="text-muted">
                                Введите код из отравленного на вашу почту письма.
                                Если вы не получали его, нажмите <a href="{{ route('verify-authentication.resend') }}">здесь</a>.
                            </p>
                            <div class="input-group mb-3">
                                <input name="two_factor_code" type="text"
                                       class="form-control{{ $errors->has('two_factor_code') ? ' is-invalid' : '' }}"
                                       required autofocus placeholder="Код двухфакторной аутентификации">
                                @if($errors->has('two_factor_code'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('two_factor_code') }}
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary px-4">
                                        Подтвердить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
