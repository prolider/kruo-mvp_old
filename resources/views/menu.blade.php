<ul class="menu-nav">
    @if ($menuName = optional(optional($items->first())->menu)->display_name)
        <li class="menu-section text-white">
            <h4 class="menu-text">{{ $menuName }}</h4>
        </li>
    @endif

    @include('menu_items')
</ul>
