import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes/user-lk';
import App from './components/app';
import Notifications from './components/notifications/module';

Vue.use(VueRouter);
Vue.use(Notifications);

const router = new VueRouter({
    routes,
    mode: 'history',
});

new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
