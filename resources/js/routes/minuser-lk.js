export default [
    {
        path: '/:locale?/ticket',
        name: 'ticket.index',
        component: () => import('../components/minuser-lk/tickets/list'),
    },
    {
        path: '/:locale?/ticket/:id',
        name: 'ticket.show',
        component: () => import('../components/minuser-lk/tickets/show'),
        props: true,
    },
];
