import Vue from 'vue';
import App from './components/form-builder/app';

new Vue({
  el: '#form-builder',
  render: h => h(App),
});
