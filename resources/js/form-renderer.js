import Vue from 'vue';
import VTooltip from 'v-tooltip'
import App from './components/form-renderer/app';
import SimpleVueValidation from 'simple-vue-validator';

Vue.use(VTooltip);
Vue.use(SimpleVueValidation);

new Vue({
  el: '#form',
  render: h => h(App),
});
