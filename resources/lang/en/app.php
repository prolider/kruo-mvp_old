<?php

return [

    'article_null' => 'Articles are missing',
    'category_null' => 'Сategories are missing',

//buttons//
    'search' => 'Search',
    'select' => 'Select',

//titles//
    'main_title' => 'Services for investors of the Ulyanovsk region',
    'policy' => 'Privacy policy', 
    'common_information' => 'Common information',
    'cooperation_platform' => 'Cooperation platform',
    'support_measures' => 'Support measures',
    'investor_guide' => 'Get a service',
    'news' => 'News',
    'more' => 'More',
    'about' => 'About',
    'signin' => 'Sign in',
    'signup' => 'Sign up',   
];
