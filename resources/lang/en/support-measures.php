<?php

return [

    'support_measures' => 'Support measures',
    'more_details' => 'More details',
    'selection_by_parameters' => 'Selection by parameters',
    'filtering_parameters' => 'Filtering parameters are not set',
    'categories' => 'Categories'

];