<?php

return [

    'article_null' => 'Записи отсутствуют',
    'category_null' => 'Категории отсутствуют',

//buttons//
    'search' => 'Поиск',
    'select' => 'Подобрать',

//titles//
    'main_title' => 'Сервисы и услуги для инвесторов Ульяновской области',
    'cooperation_platform' => 'Площадка кооперации',
    'policy' => 'Политика конфиденциальности',
    'common_information' => 'Справочная информация',
    'support_measures' => 'Меры поддержки',
    'investor_guide' => 'Получить услугу',
    'news' => 'Новое на портале',
    'more' => 'Смотреть все',
    'about' => 'О портале',
    'signin' => 'Войти в кабинет',
    'signup' => 'Регистрация',
];
