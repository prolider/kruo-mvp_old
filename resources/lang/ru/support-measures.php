<?php

return [

    'support_measures' => 'Меры поддержки',
    'more_details' => 'Подробнее',
    'selection_by_parameters' => 'Подбор по параметрам',
    'filtering_parameters' => 'Параметры фильтрации не заданы',
    'categories' => 'Категории'
];