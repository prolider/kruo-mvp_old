<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class UserItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'user')->firstOrFail();
        MenuItem::where('menu_id', $menu->id)->delete();

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-clipboard-list menu-icon text-light',
            'route' => 'ticket.index',
            'title' => 'Мои заявки',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-envelope menu-icon text-light',
            'route' => 'discussions.index',
            'title' => 'Мои обращения',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-bell menu-icon text-light',
            'route' => 'notices.index',
            'title' => 'Уведомления',
            'order' => 3,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-calendar menu-icon text-light',
            'route' => 'events.index',
            'title' => 'События',
            'order' => 4,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-user-tie menu-icon text-light',
            'route' => 'main_companies.index',
            'title' => 'Персональный менеджер',
            'order' => 5,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-user-tie menu-icon text-light',
            'route' => 'profile.index',
            'title' => 'Профиль инвестора',
            'order' => 6,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-building menu-icon text-light',
            'route' => 'profile.company',
            'title' => 'Профиль компании',
            'order' => 7,
        ]);
    }
}
