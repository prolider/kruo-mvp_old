<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class SiteItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $siteMenu = Menu::where('name', 'site')->firstOrFail();
        MenuItem::where('menu_id', $siteMenu->id)->delete();

        $info = MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Общая информация',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-info-circle menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 1,
        ]);

        /**
         * Sub-menu begin
         */

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'О портале',
            'url'     => '',
            'route'   => 'about',
            'target'     => '_self',
            'icon_class' => 'menu-bullet menu-bullet-dot',
            'color'      => null,
            'parent_id'  => $info->id,
            'order'      => 1,
        ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Новое на портале',
            'url'     => '/news/news',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'menu-bullet menu-bullet-dot',
            'color'      => null,
            'parent_id'  => $info->id,
            'order'      => 2,
        ]);

        /**
         * Sub-menu end
         */

        // MenuItem::create([
        //     'menu_id' => $siteMenu->id,
        //     'title'   => 'Меры поддержки',
        //     'url'     => '',
        //     'route'   => 'measures.index',
        //     'target'     => '_self',
        //     'icon_class' => 'icon-xl la la-briefcase-medical menu-icon text-light',
        //     'color'      => null,
        //     'parent_id'  => null,
        //     'order'      => 2,
        // ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Получить услугу',
            'url'     => '',
            'route'   => 'services.index',
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-briefcase menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 3,
        ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Площадка кооперации',
            'url'     => '',
            'route'   => 'partners.index',
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-handshake menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 4,
        ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Справочная информация',
            'url'     => '',
            'route'   => 'faq',
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-question-circle menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 5,
        ]);
    }
}
