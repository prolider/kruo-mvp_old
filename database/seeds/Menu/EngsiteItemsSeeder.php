<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class EngsiteItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $siteMenu = Menu::where('name', 'engsite')->firstOrFail();
        MenuItem::where('menu_id', $siteMenu->id)->delete();

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'About',
            'url'     => '',
            'route'   => 'about',
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-info-circle menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 1,
        ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'News',
            'url'     => '/en/news/novosti',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-newspaper-o menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 2,
        ]);

        /**
         * Sub-menu end
         */

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Support measures',
            'url'     => '',
            'route'   => 'measures.index',
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-briefcase-medical menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 3,
        ]);

        MenuItem::create([
            'menu_id' => $siteMenu->id,
            'title'   => 'Common information',
            'url'     => '',
            'route'   => 'faq',
            'target'     => '_self',
            'icon_class' => 'icon-xl la la-question-circle menu-icon text-light',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 4,
        ]);
    }
}
