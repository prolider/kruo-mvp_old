<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class AdminItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'admin')->firstOrFail();
        MenuItem::where('menu_id', $menu->id)->delete();

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'voyager-bell',
            'route' => 'voyager.tickets.index',
            'title' => 'Заявки',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'voyager-chat',
            'route' => 'voyager.discussions.index',
            'title' => 'Обращения',
            'order' => 2,
        ]);

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title'   => 'Компании',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'voyager-pie-chart',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 3,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-helm',
            'route' => 'voyager.main-companies.index',
            'title' => 'Корпорация',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-home',
            'route' => 'voyager.companies.index',
            'title' => 'Инвесторы',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-company',
            'route' => 'voyager.windows.index',
            'title' => 'Министерства',
            'order' => 3,
        ]);
        /**
         * Sub menu end
         */

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title'   => 'Управление услугами',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'voyager-compass',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 4,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.service-categories.index',
            'title' => 'Категории услуг',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-documentation',
            'route' => 'voyager.services.index',
            'title' => 'Услуги',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-params',
            'route' => 'voyager.forms.index',
            'title' => 'Формы',
            'order' => 3,
        ]);
        /**
         * Sub menu end
         */

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title'   => 'Меры, Новости, Партнёры',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'voyager-info-circled',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 5,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.categories.index',
            'title' => 'Категории',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-news',
            'route' => 'voyager.articles.index',
            'title' => 'Статьи',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-list',
            'route' => 'voyager.extra-field-groups.index',
            'title' => 'Группы свойств',
            'order' => 3,
        ]);
        /**
         * Sub menu end
         */

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'voyager-calendar',
            'route' => 'voyager.events.index',
            'title' => 'События',
            'order' => 6,
        ]);

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title'   => 'Вопрос-ответ',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'voyager-question',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 7,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.faq_categories.index',
            'title' => 'Категории вопросов',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-news',
            'route' => 'voyager.faqs.index',
            'title' => 'Вопросы-Ответы',
            'order' => 2,
        ]);
        /**
         * Sub menu end
         */

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title'   => 'Настройки',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'voyager-helm',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 9,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-list',
            'route' => 'voyager.menus.index',
            'title' => 'Конструктор меню',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-settings',
            'route' => 'voyager.settings.index',
            'title' => 'Настройки портала',
            'order' => 2,
        ]);
        /**
         * Sub menu end
         */

        $info = MenuItem::create([
            'menu_id' => $menu->id,
            'title'   => 'Инструменты',
            'url'     => '',
            'route'   => null,
            'target'     => '_self',
            'icon_class' => 'voyager-tools',
            'color'      => null,
            'parent_id'  => null,
            'order'      => 9,
        ]);

        /**
         * Sub menu start
         */
        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-people',
            'route' => 'voyager.users.index',
            'title' => 'Пользователи',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-lock',
            'route' => 'voyager.roles.index',
            'title' => 'Роли',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-images',
            'route' => 'voyager.media.index',
            'title' => 'Медиа',
            'order' => 3,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-bread',
            'route' => 'voyager.bread.index',
            'title' => 'BREAD',
            'order' => 4,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => 'voyager-data',
            'route' => 'voyager.database.index',
            'title' => 'База данных',
            'order' => 5,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => $info->id,
            'icon_class' => '',
            'route' => 'voyager.compass.index',
            'title' => 'Compass',
            'order' => 6,
        ]);
        /**
         * Sub menu end
         */

        MenuItem::create([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-external',
            'route' => 'voyager.bitrix-settings.index',
            'title' => 'Битрикс24',
            'order' => 10
        ]);
    }
}
