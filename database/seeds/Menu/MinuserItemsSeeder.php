<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MinuserItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'minuser')->firstOrFail();
        MenuItem::where('menu_id', $menu->id)->delete();

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-clipboard-list menu-icon text-light',
            'route' => 'ticket.index',
            'title' => 'Заявки',
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-bell menu-icon text-light',
            'route' => 'notices.index',
            'title' => 'Уведомления',
            'order' => 2,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'url' => '',
            'color' => null,
            'parent_id' => null,
            'icon_class' => 'icon-xl la la-user-circle menu-icon text-light',
            'route' => 'profile.index',
            'title' => 'Мой профиль',
            'order' => 3,
        ]);
    }
}
