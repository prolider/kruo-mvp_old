<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        foreach (Role::ROLES as $key => $value) {
            $role = Role::firstOrNew(['name' => $key]);

            $role->fill([
                'display_name' => $value,
            ])->save();
        }
    }
}
