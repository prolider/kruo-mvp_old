<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        Menu::select()->delete();

        Menu::create([
            'name' => 'admin',
            'display_name' => 'Панель Администратора'
        ]);

        Menu::create([
            'name' => 'site',
            'display_name' => '',
        ]);

        Menu::create([
            'name' => 'engsite',
            'display_name' => '',
        ]);
        Menu::create([
            'name' => 'user',
            'display_name' => 'Кабинет Инвестора',
        ]);

        Menu::create([
            'name' => 'operator',
            'display_name' => 'Кабинет Модератора',
        ]);

        Menu::create([
            'name' => 'minuser',
            'display_name' => 'Кабинет Оператора',
        ]);
    }
}
