<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class VoyagerDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::select()->delete();
        DataType::select()->delete();

        $this->seed('RoleDataSeeder');
        $this->seed('UserDataSeeder');
        $this->seed('MenuDataSeeder');

        $this->seed('MenusTableSeeder');
        $this->seed('MenuItemsTableSeeder');
        $this->seed('RolesTableSeeder');
        $this->seed('PermissionsTableSeeder');
        $this->seed('PermissionRoleTableSeeder');
        $this->seed('SettingsTableSeeder');
        $this->seed('CategoryDatabaseSeeder');

        $this->seed('ArticleDataSeeder');
        $this->seed('CategoryDataSeeder');
        $this->seed('CompanyDataSeeder');
        $this->seed('FormDataSeeder');
        $this->seed('FaqDataSeeder');
        $this->seed('FaqCategoryDataSeeder');
        $this->seed('MainCompanyDataSeeder');
        $this->seed('ServiceCategoryDataSeeder');
        $this->seed('ServiceDataSeeder');
        $this->seed('TicketDataSeeder');
        $this->seed('WindowDataSeeder');
        $this->seed('ExtraFieldGroupDataSeeder');
        $this->seed('ExtraFieldDataSeeder');
        $this->seed('DiscussionMessageDataSeeder');
        $this->seed('DiscussionDataSeeder');
        $this->seed('PartnerExtraFieldSeeder');
        $this->seed('EventDataSeeder');
        $this->seed('BitrixSettingDataSeeder');
    }
}
