<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('site.title');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.site.title'),
            'value'        => 'Личный кабинет Инвестора',
            'details'      => '',
            'type'         => 'text',
            'order'        => 1,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.title-eng');
        $setting->fill([
            'display_name' => 'Site title',
            'value'        => 'Personal Account for investor',
            'details'      => '',
            'type'         => 'text',
            'order'        => 1,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.description');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.site.description'),
            'value'        => 'Портал "Личный Кабинет Инвестора" предназначен для взаимодействия Инвесторов с ведомствами Ульяновской области и их структурами, а также для предоставления Инвесторам онлайн инструментов для запуска и развития инвестиционных проектов. Информационные сервисы доступны для чтения всем посетителям, а электронные услуги и личный кабинет - только для зарегистрированных пользователей.',
            'details'      => '',
            'type'         => 'text',
            'order'        => 2,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.description-eng');
        $setting->fill([
            'display_name' => 'Site description',
            'value'        => 'The portal "Personal account for investor" is intended for interaction of Investors with the departments of the Ulyanovsk region and their structures, as well as for providing Investors with online tools for launching and developing investment projects. Information services are available for reading to all visitors, and electronic services and a personal account are available only for registered users.',
            'details'      => '',
            'type'         => 'text',
            'order'        => 2,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.logo');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.site.logo'),
            'details'      => '',
            'type'         => 'file',
            'order'        => 3,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.logo-eng');
        $setting->fill([
            'display_name' => 'Site Logo',
            'details'      => '',
            'type'         => 'file',
            'order'        => 3,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.mobile-logo');
        $setting->fill([
            'display_name' => 'Логотип мобильной версии',
            'details'      => '',
            'type'         => 'file',
            'order'        => 3,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.mobile-logo-eng');
        $setting->fill([
            'display_name' => 'Mobile site logo',
            'details'      => '',
            'type'         => 'file',
            'order'        => 3,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.google_analytics_tracking_id');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.site.google_analytics_tracking_id'),
            'value'        => '',
            'details'      => '',
            'type'         => 'text',
            'order'        => 4,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('admin.bg_image');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.admin.background_image'),
            'details'      => '',
            'type'         => 'image',
            'order'        => 5,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('admin.title');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.admin.title'),
            'value'        => 'Кабинет администратора',
            'details'      => '',
            'type'         => 'text',
            'order'        => 1,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('admin.description');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.admin.description'),
            'value'        => 'Кабинет администратора',
            'details'      => '',
            'type'         => 'text',
            'order'        => 2,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('admin.loader');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.admin.loader'),
            'value'        => '',
            'details'      => '',
            'type'         => 'image',
            'order'        => 3,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('admin.icon_image');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.admin.icon_image'),
            'details'      => '',
            'type'         => 'file',
            'order'        => 4,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('admin.google_analytics_client_id');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.admin.google_analytics_client_id'),
            'value'        => '',
            'details'      => '',
            'type'         => 'text',
            'order'        => 1,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('admin.google_analytics_tracking_id');
        $setting->fill([
            'display_name' => __('voyager::seeders.settings.site.google_analytics_tracking_id'),
            'value'        => '',
            'details'      => '',
            'type'         => 'text',
            'order'        => 1,
            'group'        => 'Admin',
        ])->save();

        $setting = $this->findSetting('site.about');
        $setting->fill([
            'display_name' => 'О портале',
            'value'        => '<p>Портал "Личный Кабинет Инвестора" предназначен для взаимодействия Инвесторов с ведомствами Ульяновской области и их структурами, а также для предоставления Инвесторам онлайн инструментов для запуска и развития инвестиционных проектов.</p>
            <h3>Информационные сервисы.</h3>
            <p>Информационные сервисы доступны для чтения всем посетителям. Кроме новостного и справочного разделов к ним относятся такие важные сервисы, как:</p>
            <ul>
            <li><strong>&laquo;Меры поддержки&raquo;.</strong> <br />Инфосервис, раскрывающий существующие меры поддержки инвесторов и инвестиционных проектов в различных отраслях.</li>
            <li><strong>&laquo;Получить услугу&raquo;</strong> в режиме чтения. <br />Сервис для получения услуг, связанных с инвестиционной деятельностью, в различных ведомствах Ульяновской области.</li>
            <li><strong>&laquo;Площадка кооперации&raquo;.</strong> <br />Сервис, позволяющий инвесторам публиковать свои предложения или проекты с подробными параметрами и требованиями для поиска партнеров, а партнерам &ndash; открыть возможности выгодного и перспективного сотрудничества и роста.</li>
            </ul>
            <h3>Расширенные сервисы.</h3>
            <p>Расширенные сервисы доступны для зарегистрированных пользователей и компаний, прошедших верификацию (проверку) в качестве инвесторов Ульяновской области.</p>
            <p>Расширенные сервисы включают следующие возможности:</p>
            <ul>
            <li><strong>&laquo;Получить услугу&raquo;</strong> с полным доступом. <br />Подачу заявки на получение услуги с возможностью дистанционной подачи документов. В ряде случаев возможно дистанционное получение услуг без передачи бумажного пакета документов и выезда в ведомство. После подачи заявки на получение услуг информация по ней будет хранится в разделе &laquo;Мои заявки&raquo;</li>
            <li><strong>&laquo;Мои заявки&raquo;.</strong> <br />Сервис для отслеживания хода получения услуг. Доступна коммуникация с представителем ведомства, ответственным за предоставление услуги, и модератором &laquo;Корпорацией развития Ульяновской области&raquo;.</li>
            <li><strong>&laquo;Персональный менеджер&raquo;.</strong> <br />Инвестору назначается персональный менеджер, который будет курировать взаимодействие с ведомствами и выступать в роли модератора, ускоряющего процесс получения услуг. С ним можно связаться удобным из доступных способов.</li>
            <li><strong>&laquo;Мои обращения&raquo;.</strong> <br />Персональному менеджеру можно направить обращение из личного кабинета и вести диалог в ветке обращений. В данном разделе будут хранится все обращения.</li>
            <li><strong>&laquo;События&raquo;.</strong> <br />Данный сервис направлен на информирование инвесторов о важных групповых или индивидуальных событиях, организованных Корпорацией Развития Ульяновской области.</li>
            <li><strong>&laquo;Профиль&raquo;</strong> инвестора и <strong>&laquo;Профиль компании&raquo;</strong>. <br />Разделы с информацией о компании инвестора и ее уполномоченного представителя.</li>
            </ul>
            <p>Если у Вас возникнут дополнительные работы по работе портала или получению услуг перед регистрацией, то можно воспользоваться разделом Справочной информации или написать нам через виджет обратной связи.</p>',
            'details'      => '',
            'type'         => 'rich_text_box',
            'order'        => 1,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.mainpage-about');
        $setting->fill([
            'display_name' => 'Главная страница О портале',
            'value'        => '<p>Приветствуем на портале "Личный Кабинет Инвестора". Портал предназначен для взаимодействия Инвесторов с ведомствами Ульяновской области и их структурами, а также для предоставления Инвесторам онлайн инструментов для запуска и развития инвестиционных проектов. Информационные сервисы доступны для чтения всем посетителям, а зарегистрированным пользователям доступен личный кабинет, получение услуг и персональный менеджер.</p>
            <p class="mt-5"><strong>Регистрируйтесь и получите доступ ко всем возможностям портала!</strong></p>',
            'details'      => '',
            'type'         => 'rich_text_box',
            'order'        => 1,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.mainpage-about-eng');
        $setting->fill([
            'display_name' => 'Main page About',
            'value'        => 'Information about the Portal is being developed.',
            'details'      => '',
            'type'         => 'rich_text_box',
            'order'        => 1,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.about-eng');
        $setting->fill([
            'display_name' => 'About the Portal',
            'value'        => 'The portal "Personal account for investor" is intended for interaction of Investors with the departments of the Ulyanovsk region and their structures, as well as for providing Investors with online tools for launching and developing investment projects. Information services are available for reading to all visitors, and electronic services and a personal account are available only for registered users.',
            'details'      => '',
            'type'         => 'rich_text_box',
            'order'        => 1,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.policy');
        $setting->fill([
            'display_name' => 'Политика конфиденциальности',
            'value'        => 'Условия использования и политика конфиденциальности в стадии разработки',
            'details'      => '',
            'type'         => 'rich_text_box',
            'order'        => 5,
            'group'        => 'Site',
        ])->save();

        $setting = $this->findSetting('site.policy-eng');
        $setting->fill([
            'display_name' => 'Terms & Policy',
            'value'        => 'Temrs of using and privacy policy is being developed',
            'details'      => '',
            'type'         => 'rich_text_box',
            'order'        => 5,
            'group'        => 'Site',
        ])->save();
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
