<?php

use \TCG\Voyager\Models\DataType;

trait HasDataTypeTrait
{
    protected function dataType(string $field, string $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
