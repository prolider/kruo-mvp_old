<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;
use App\Models\{
    Category,
    ExtraField,
    ExtraFieldGroup,
    ExtraFieldStep
};

class PartnerExtraFieldSeeder extends Seeder
{
    use Seedable;

    protected $extraFieldGroup;
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::whereNull('category_id')->tag(Category::TAG_PARTNERS)->firstOrFail();

        $extraFieldGroup = ExtraFieldGroup::firstOrCreate([
            'name' => 'Бизнес кооперации',
            'with_steps' => true,
        ]);

        $category->extraFieldGroups()->sync([$extraFieldGroup->id]);

        $this->extraFieldGroup = $extraFieldGroup;

        $domainOptions = [
            'options' => [
                'Легкая промышленность',
                'Тяжелая промышленность',
                'Логистика',
                'Мебельное производство',
                'Производство продуктов питания',
                'Строительство',
                'Переработка',
                'Сельское хозяйство',
                'Производство комплектующих',
                'Дистрибуция',
            ]
        ];

        $firstStep = ExtraFieldStep::firstOrCreate([
            'name' => 'Об участнике',
            'number' => 1,
            'description' => 'Данные о своей компании',
            'helper_message' => 'Заполните данные о своей компании',
            'extra_field_group_id' => $extraFieldGroup->id,
        ]);

        $secondStep = ExtraFieldStep::firstOrCreate([
            'name' => 'Для партнеров',
            'number' => 2,
            'description' => 'Требования и ожидания от партнеров',
            'helper_message' => 'Заполните требования к партнерам или ожиданиях от сотрудничества',
            'extra_field_group_id' => $extraFieldGroup->id,
        ]);

        $this->createExtraField('Полное наименование компании', ExtraField::TYPE_TEXT, $firstStep, true, 'col-md-12', 'Укажите юридическое название', 'Например, ООО "Фирма Плюс"');
        $this->createExtraField('Контактный телефон', ExtraField::TYPE_TEXT, $firstStep, true, 'col-md-6', '+7 900 000 00-00');
        $this->createExtraField('Email', ExtraField::TYPE_TEXT, $firstStep, true, 'col-md-6', 'Email');
        $this->createExtraField('Веб-сайт', ExtraField::TYPE_TEXT, $firstStep, true, 'col-md-6', 'Укажите веб-сайт', 'Например, www.site.com');
        $this->createExtraField('Год основания', ExtraField::TYPE_TEXT, $firstStep, true, 'col-md-6', 'Укажите год');
        $this->createExtraField('Сферы деятельности', ExtraField::TYPE_MULTISELECT, $firstStep, true, 'col-md-12', '', 'Выберите сферы вашей деятельности', $domainOptions);
        $this->createExtraField('Производимая продукция', ExtraField::TYPE_TEXT_AREA, $firstStep, false, 'col-md-12', 'Перечислите виды продукции');
        $this->createExtraField('Объемы производства', ExtraField::TYPE_TEXT, $firstStep, true, 'col-md-12', 'Укажите с единицами измерений и периодом');
        $this->createExtraField('Основные клиенты', ExtraField::TYPE_TEXT_AREA, $firstStep, false, 'col-md-12', 'Перечислите основных клиентов Вашей компании');
        $this->createExtraField('Перспективные планы увеличения мощности производства/объема продаж в РФ', ExtraField::TYPE_TEXT_AREA, $firstStep, false, 'col-md-12', 'Укажите целевой год и показатель');
        $this->createExtraField('Планируемый объем инвестиций по локализации в РФ, млн. руб', ExtraField::TYPE_TEXT, $firstStep, false, 'col-md-12', 'Укажите значение');

        $this->createExtraField('Ваше предложение партнеру', ExtraField::TYPE_TEXT_AREA, $secondStep, true, 'col-md-12', 'Укажите текст', 'Например: "Совместное производство станков для переаботки отходов"');
        $this->createExtraField('Сферы деятельности', ExtraField::TYPE_MULTISELECT, $secondStep, true, 'col-md-12', '', 'Выберите сферы деятельности компании-партнера', $domainOptions);
        $this->createExtraField('Продолжительность функционирования партнера на момент рассмотрения вопроса о партнерстве', ExtraField::TYPE_TEXT, $secondStep, true, 'col-md-12', 'Укажите количество лет или интервал', 'Например: 2,5 года');
        $this->createExtraField('Наличие иностранных учредителей', ExtraField::TYPE_SELECT, $secondStep, true, 'col-md-6', '', '', [
            'options' => [
                'Да',
                'Нет',
                'Не важно'
            ],
        ]);
        $this->createExtraField('Организационно-правовая форма', ExtraField::TYPE_SELECT, $secondStep, true, 'col-md-6', '', '', [
            'options' => [
                'ЗАО',
                'ООО',
                'АО',
                'Не важно'
            ],
        ]);
        $this->createExtraField('Локализация партнерства', ExtraField::TYPE_SELECT, $secondStep, true, 'col-md-6', '', '', [
            'options' => [
                'На территории имущественного комплекса партнера',
                'Совместно на новой производственной площадке (Совместное/несовместное инвестирование)',
                'Отдельно от партнера на площадях, построенных за свой счет/арендованных',
                'Не важно'
            ],
        ]);
        $this->createExtraField('Тип партнерства', ExtraField::TYPE_SELECT, $secondStep, true, 'col-md-6', '', '', [
            'options' => [
                'Поставки сырья/материалов',
                'Сбыт продукции',
                'Совместное предприятие',
                'Инвестирование',
                'Технологическое партнерство',
                'Не важно'
            ],
        ]);
        $this->createExtraField('Подробнее о типе партнерства', ExtraField::TYPE_TEXT_AREA, $secondStep, true, 'col-md-12', 'Укажите подробнее о желаемом типе партнерства (а именно: поставку какого сырья или сбыт какого вида продукции интересует, а также другие особенности)');
        $this->createExtraField('Организация партнерства', ExtraField::TYPE_SELECT, $secondStep, true, 'col-md-6', '', '', [
            'options' => [
                'Поставки сырья/материалов',
                'Сбыт продукции',
                'Совместное предприятие',
                'Инвестирование',
                'Технологическое партнерство',
                'Не важно'
            ],
        ]);
        $this->createExtraField('Подробнее об организации партнерства', ExtraField::TYPE_TEXT_AREA, $secondStep, true, 'col-md-12', 'Укажите долю партнера при выбранном виде организации партнерства');
    }

    protected function createExtraField($name, $type, $step, $isRequired = false, $class = '', $placeholder = '', $helperMessage = '', $options = [])
    {
        ExtraField::firstOrCreate([
            'name'                 => $name,
            'type'                 => $type,
            'extra_field_step_id'  => $step->id,
            'is_required'          => $isRequired,
            'container_class'      => $class,
            'helper_message'       => $helperMessage,
            'placeholder'          => $placeholder,
            'extra_field_group_id' => $this->extraFieldGroup->id,
        ], [
            'details'              => $options,
        ]);
    }
}
