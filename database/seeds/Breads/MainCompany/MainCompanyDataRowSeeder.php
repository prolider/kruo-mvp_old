<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class MainCompanyDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'main-companies')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Название',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'short_name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Короткое название',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'description');
        $dataRow->fill([
            'type'         => 'rich_text_box',
            'display_name' => 'Описание',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Создано',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'rich_text_box',
            'display_name' => 'Обновлено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 6,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'deleted_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Удалено',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 7,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'main_company_belongstomany_user_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Операторы',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
            'details'      => [
                'scope' => 'operator',
                'model' => 'App\\Models\\User',
                'table' => 'users',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'main_company_operators',
                'pivot' => 1,
                'taggable' => 0,
            ]
        ])->save();
    }
}
