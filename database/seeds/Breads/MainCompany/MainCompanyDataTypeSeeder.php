<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class MainCompanyDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'main-companies');

        $dataType->fill([
            'name'                  => 'main_companies',
            'display_name_singular' => 'КРУО',
            'display_name_plural'   => 'КРУО',
            'icon'                  => 'voyager-home',
            'model_name'            => 'App\\Models\\MainCompany',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'КРУО']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-home',
            'route' => 'voyager.main-companies.index',
        ])->save();
    }
}
