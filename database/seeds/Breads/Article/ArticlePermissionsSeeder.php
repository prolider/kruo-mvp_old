<?php

use Illuminate\Database\Seeder;
use \TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class ArticlePermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('articles');

        $this->seed('PermissionRoleTableSeeder');
    }
}
