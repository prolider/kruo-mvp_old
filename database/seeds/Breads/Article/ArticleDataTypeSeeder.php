<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class ArticleDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'articles');

        $dataType->fill([
            'name'                  => 'articles',
            'display_name_singular' => 'Статья',
            'display_name_plural'   => 'Статьи',
            'icon'                  => 'voyager-news',
            'model_name'            => 'App\\Models\\Article',
            'generate_permissions'  => 1,
            'description'           => '',
            'controller'            => 'App\\Http\\Controllers\\Voyager\\ArticleController',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Статьи']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-news',
            'route' => 'voyager.articles.index',
        ])->save();
    }
}
