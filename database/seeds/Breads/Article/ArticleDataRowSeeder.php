<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class ArticleDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'articles')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Название',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 2,
            'details'      => [
                'validation' => [
                    'rule' => 'required|string|min:1',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'content');
        $dataRow->fill([
            'type' => 'rich_text_box',
            'display_name' => 'Контент',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'company_belongsto_category_relationship');
        $dataRow->fill([
            'type' => 'relationship',
            'display_name' => 'Категория',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Category',
                'table' => 'categories',
                'type' => 'belongsTo',
                'column' => 'category_id',
                'key' => 'id',
                'label' => 'name',
                'pivot' => 0,
                'taggable' => 0,
                'pivot_table' => 'comments',
                'scope' => 'operator',
            ],
            'order' => 4,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'category_id');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Категория',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'on_mainpage');
        $dataRow->fill([
            'type' => 'checkbox',
            'display_name' => 'На главной',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 5,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'is_published');
        $dataRow->fill([
            'type' => 'checkbox',
            'display_name' => 'Опубликовано',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 6,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'slug');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Семантическая ссылка',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 7,
        ])->save();
    }
}
