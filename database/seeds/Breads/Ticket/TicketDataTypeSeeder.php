<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class TicketDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'tickets');

        $dataType->fill([
            'name'                  => 'tickets',
            'display_name_singular' => 'Заявка',
            'display_name_plural'   => 'Заявки',
            'icon'                  => 'voyager-ticket',
            'model_name'            => 'App\\Models\\Ticket',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Заявки']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-ticket',
            'route' => 'voyager.ticket.index',
        ])->save();
    }
}
