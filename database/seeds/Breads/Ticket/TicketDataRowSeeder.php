<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class TicketDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $companyDataType = DataType::where('slug', 'tickets')->firstOrFail();

        $dataRow = $this->dataRow($companyDataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'form_result_id');
        $dataRow->fill([
            'type'         => 'hidden',
            'display_name' => 'Form Result Id',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'operator_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Operator Id',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'window_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Window Id',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'status');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Статус',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
            'details'      => [
                'options'  => [
                    'N'    => 'Отправлена',
                    'W'    => 'Взята в работу',
                    'O'    => 'Оформление',
                    'C'    => 'Отменена',
                    'D'    => 'Услуга оказана',
                    'R'    => 'Черновик',
                    'L'    => 'Клиент заполняет',
                    'P'    => 'Запрос на отмену',
                    'E'    => 'Требует уточнения',
                    'F'    => 'Отказ',
                    'I'    => 'Приглашение'
                ],
                'default' => 'N',
                'display' => [
                    'width' => 6
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'plan_end_date');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Плановая дата завершения',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 7,
            'details'      => [
                'display'  => [
                    'width' => 6
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'fact_end_date');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Фактическая дата завершения',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
            'details'      => [
                'display'  => [
                    'width' => 6
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'fact_end_date');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Фактическая дата завершения',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
            'details'      => [
                'display'  => [
                    'width' => 6
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'archived');
        $dataRow->fill([
            'type'         => 'radio_btn',
            'display_name' => 'Архивный',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
            'details'      => [
                'default'  => "N",
                'options'  => [
                    'Y'    => 'Да',
                    'N'    => 'Нет',
                ],
                'display'  => [
                    'width' => 6,
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'attachments');
        $dataRow->fill([
            'type'         => 'file',
            'display_name' => 'Вложения',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 10,
            'details'      => [
                'display'  => [
                    'width' => 6
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'attachments');
        $dataRow->fill([
            'type'         => 'file',
            'display_name' => 'Вложения',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 10,
            'details'      => [
                'display'  => [
                    'width' => 6
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Создано',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 11,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Обновлено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 12,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'deleted_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Удалено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 13,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'ticket_belongsto_user_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Назначенный оператор',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 13,
            'details'      => [
                'display' => ['width' => '6'],
                'scope' => 'operator',
                'model' => 'App\\Models\\User',
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'operator_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'data_rows',
                'pivot' => '0',
                'taggable' => '0'
            ]
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'ticket_belongsto_window_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Министерство',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 13,
            'details'      => [
                'display' => ['width' => '6'],
                'scope' => 'operator',
                'model' => 'App\\Models\\Window',
                'table' => 'windows',
                'type' => 'belongsTo',
                'column' => 'window_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'data_rows',
                'pivot' => '0',
                'taggable' => '0'
            ]
        ])->save();
    }
}
