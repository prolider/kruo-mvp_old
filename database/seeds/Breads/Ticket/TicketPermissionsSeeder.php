<?php

use Illuminate\Database\Seeder;
use \TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class TicketPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('tickets');

        $this->seed('PermissionRoleTableSeeder');
    }
}
