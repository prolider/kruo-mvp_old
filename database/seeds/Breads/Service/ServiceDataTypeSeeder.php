<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class ServiceDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'services');

        $dataType->fill([
            'name'                  => 'services',
            'display_name_singular' => 'Услуга',
            'display_name_plural'   => 'Услуги',
            'icon'                  => 'voyager-chat',
            'model_name'            => 'App\\Models\\Service',
            'generate_permissions'  => 1,
            'description'           => '',
            'controller'            => 'App\\Http\\Controllers\\Voyager\\ServiceController',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Услуги']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-chat',
            'route' => 'voyager.services.index',
        ])->save();
    }
}
