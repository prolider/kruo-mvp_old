<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class ServiceDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'services')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Название',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'description');
        $dataRow->fill([
            'type'         => 'rich_text_box',
            'display_name' => 'Описание',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'service_category_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Категория услуг',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'window_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Министерство',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 5,
            'details'      => [
                'view' => 'vendor.voyager.formfields.window',
            ]
        ])->save();

        $dataRow = $this->dataRow($dataType, 'use_button');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Тип услуги',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 6,
            'details'      => [
                'default' => 'Y',
                'choice_disabled' => true,
                'options' => [
                    'N' => 'Информационная',
                    'Y' => 'Сопроводительная',
                ]
            ]
        ])->save();

        $dataRow = $this->dataRow($dataType, 'status');
        $dataRow->fill([
            'type'         => 'hidden',
            'display_name' => 'Тип услуги',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 7,
            'details'      => [
                'options' => [
                    'S' => 'Опубликовано',
                    'P' => 'Приватный',
                    'I' => 'Внутренний',
                ],
                'default' => 'S',
            ]
        ])->save();

        $dataRow = $this->dataRow($dataType, 'service_period');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => 'Период оказания услуг',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'service_period_type');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Тип периода',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
            'details'      => [
                'options' => [
                    'D' => 'День',
                    'W' => 'Неделя',
                    'M' => 'Месяц'
                ],
                'default' => 'D',
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Создано',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 10,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Обновлено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 11,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'deleted_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Удалено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 12,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'service_belongsto_service_category_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Категория услуги',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 13,
            'details'      => [
                'model'       => 'App\\Models\\ServiceCategory',
                'table'       => 'service_categories',
                'type'        => 'belongsTo',
                'column'      => 'service_category_id',
                'key'         => 'id',
                'label'       => 'name',
                'pivot'       => 0,
                'taggable'    => 0
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'form_id');
        $dataRow->fill([
            'type'         => 'hidden',
            'display_name' => 'Форма',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 14,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'official_name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Дополнительная информация',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 15,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'links');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Ссылки',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 16,
        ])->save();
    }
}
