<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class CategoryDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('CategoryDataTypeSeeder');
        $this->seed('CategoryDataRowSeeder');
        $this->seed('CategoryPermissionsSeeder');
    }
}
