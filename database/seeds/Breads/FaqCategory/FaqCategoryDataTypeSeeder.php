<?php

use App\Models\Menu;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\MenuItem;

class FaqCategoryDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'faq_categories');

        $dataType->fill([
            'name'                  => 'faq_categories',
            'display_name_singular' => 'Категория',
            'display_name_plural'   => 'Категории',
            'icon'                  => 'voyager-params',
            'model_name'            => 'App\\Models\\FaqCategory',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'FAQ категории']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-params',
            'route' => 'voyager.faq_categories.index',
        ])->save();
    }
}
