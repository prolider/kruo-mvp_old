<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class FaqCategoryDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faqDataType = DataType::where('slug', 'faq_categories')->firstOrFail();

        $dataRow = $this->dataRow($faqDataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.name'),
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'sort');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => __('voyager::seeders.data_rows.name'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'locale');

        $locales = [];

        foreach(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales() as $locale => $attributes) {
            $locales[$locale] = $attributes['native'];
        }
        $dataRow->fill([
            'type' => 'select_dropdown',
            'display_name' => 'Язык',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
            'details'      => [
                'options' => $locales,
                'default' => 'ru',
            ],
        ])->save();
    }
}
