<?php

use Illuminate\Database\Seeder;

class DiscussionMessageDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'discussion-messages');

        $dataType->fill([
            'name'                  => 'discussion_messages',
            'display_name_singular' => 'Сообщение',
            'display_name_plural'   => 'Сообщения',
            'icon'                  => 'voyager-params',
            'model_name'            => 'App\\Models\\DiscussionMessage',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();
    }
}
