<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class FaqDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faqDataType = DataType::where('slug', 'faqs')->firstOrFail();

        $dataRow = $this->dataRow($faqDataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.name'),
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'short_description');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Короткое описание',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'description');
        $dataRow->fill([
            'type'         => 'rich_text_box',
            'display_name' => 'Описание',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'sort');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => __('voyager::seeders.data_rows.name'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($faqDataType, 'faq_belongsto_fag_categories_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Категория',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 6,
            'details'      => [
                'display' => ['width' => '6'],
                'model' => 'App\\Models\\FaqCategory',
                'table' => 'faq_categories',
                'type' => 'belongsTo',
                'column' => 'faq_category_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'data_rows',
                'pivot' => '0',
                'taggable' => '0'
            ]
        ])->save();
    }
}
