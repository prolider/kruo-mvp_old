<?php

use App\Models\Menu;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\MenuItem;

class FaqDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'faqs');

        $dataType->fill([
            'name'                  => 'faqs',
            'display_name_singular' => 'FAQ',
            'display_name_plural'   => 'FAQ',
            'icon'                  => 'voyager-params',
            'model_name'            => 'App\\Models\\Faq',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'FAQ']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-params',
            'route' => 'voyager.faqs.index',
        ])->save();
    }
}
