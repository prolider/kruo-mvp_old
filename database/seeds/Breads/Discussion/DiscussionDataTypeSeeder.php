<?php

use Illuminate\Database\Seeder;

class DiscussionDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'discussions');
        $dataType->fill([
            'name'                  => 'discussions',
            'display_name_singular' => 'Обращение',
            'display_name_plural'   => 'Обращения',
            'icon'                  => 'voyager-params',
            'model_name'            => 'App\\Models\\Discussion',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();
    }
}
