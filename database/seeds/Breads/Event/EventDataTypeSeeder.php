<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};

class EventDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'events');
        $dataType->fill([
            'name'                  => 'events',
            'display_name_singular' => 'Событие',
            'display_name_plural'   => 'События',
            'icon'                  => 'voyager-news',
            'model_name'            => 'App\\Models\\Event',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'События']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-calendar',
            'route' => 'voyager.events.index',
        ])->save();
    }
}
