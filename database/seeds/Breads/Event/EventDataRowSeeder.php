<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use App\Models\Event;

class EventDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'events')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Название',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 2,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'address');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Место',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'event_type');
        $dataRow->fill([
            'type' => 'select_dropdown',
            'display_name' => 'Тип события',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
            'details' => [
                'options' => Event::EVENT_TYPE_NAMES,
                'default' => Event::EVENT_TYPE_MEETING,
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'invite_type');
        $dataRow->fill([
            'type' => 'select_dropdown',
            'display_name' => 'Вид приглашения',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 5,
            'details' => [
                'options' => Event::INVITE_TYPE_NAMES,
                'default' => Event::INVITE_TYPE_PUBLIC
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'start_at');
        $dataRow->fill([
            'type' => 'timestamp',
            'display_name' => 'Время начала',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 6,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'end_at');
        $dataRow->fill([
            'type' => 'timestamp',
            'display_name' => 'Время начала',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 7,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'description');
        $dataRow->fill([
            'type' => 'rich_text_box',
            'display_name' => 'Описание',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 10,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'text');
        $dataRow->fill([
            'type' => 'rich_text_box',
            'display_name' => 'Текст анонса',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 11,
        ])->save();
    }
}
