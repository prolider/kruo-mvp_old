<?php

use Illuminate\Database\Seeder;

class MenuDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'menus');
        $dataType->fill([
            'name'                  => 'menus',
            'display_name_singular' => __('voyager::seeders.data_types.menu.singular'),
            'display_name_plural'   => __('voyager::seeders.data_types.menu.plural'),
            'icon'                  => 'voyager-list',
            'model_name'            => 'TCG\\Voyager\\Models\\Menu',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();
    }
}
