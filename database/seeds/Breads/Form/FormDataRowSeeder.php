<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class FormDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $companyDataType = DataType::where('slug', 'forms')->firstOrFail();

        $dataRow = $this->dataRow($companyDataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Название',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'status');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Статус',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
            'details'      => [
                'options' => [
                    'A' => 'Включено',
                    'D' => 'Отключено'
                ],
                'default' => 'D',
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'is_public');
        $dataRow->fill([
            'type'         => 'radio_btn',
            'display_name' => 'Тип',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
            'details'      => [
                'fault'=> 'Y',
                'options' => [
                    'Y' => 'Публичная',
                    'N' => 'Приватная'
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'recaptcha');
        $dataRow->fill([
            'type'         => 'radio_btn',
            'display_name' => 'Recaptcha',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 5,
            'details'      => [
                'fault'=> 'N',
                'options' => [
                    'Y' => 'Да',
                    'N' => 'Нет'
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'form_data');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Форма',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 6,
            'details'      => [
                'view' => 'form-builder.form',
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Создано',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 7,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Обновлено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 8,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'deleted_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Удалено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 9,
        ])->save();
    }
}
