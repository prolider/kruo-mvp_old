<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class FormDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'forms');

        $dataType->fill([
            'name'                  => 'forms',
            'display_name_singular' => 'Форма',
            'display_name_plural'   => 'Формы',
            'icon'                  => 'voyager-params',
            'model_name'            => 'App\\Models\\Form',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Формы']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-params',
            'route' => 'voyager.forms.index',
        ])->save();
    }
}
