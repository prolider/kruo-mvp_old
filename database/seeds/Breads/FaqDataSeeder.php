<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class FaqDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('FaqDataTypeSeeder');
        $this->seed('FaqDataRowSeeder');
        $this->seed('FaqPermissionsSeeder');
    }
}
