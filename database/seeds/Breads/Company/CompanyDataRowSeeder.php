<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class CompanyDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $companyDataType = DataType::where('slug', 'companies')->firstOrFail();

        $dataRow = $this->dataRow($companyDataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'name');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Полное наименование',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 2,
            'details' => [
                'validation' => [
                    'rule' => 'required|string|min:1',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'inn');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'ИНН (для ООО, АО)',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'ogrn');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'ОГРН (ОРГНИП)',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'kpp');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'КПП',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'address');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Юридический адрес',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'registration_date');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Дата регистрации',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'management_post');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Должность руководителя',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 6,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'management_name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'ФИО руководителя',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 7,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'company_belongsto_user_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Владелец',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'details'      => [
                'model' => 'App\\Models\\User',
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'user_id',
                'key' => 'id',
                'label' => 'fullName',
                'pivot_table' => 'comments',
                'pivot' => 0,
                'taggable' => 0,
            ],
            'order'        => 8,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'verification_status');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Статус верификации',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'details'      => [
                "options"  => [
                    'N'    => 'Не подтверждена',
                    'C'    => 'На проверке',
                    'A'    => 'Подтверждена',
                    'R'    => 'Отклонена',
                ],
                'default' => 'N',
            ],
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'statement_file');
        $dataRow->fill([
            'type'         => 'file',
            'display_name' => 'Заявление',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 10,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'signature_file');
        $dataRow->fill([
            'type'         => 'file',
            'display_name' => 'Подпись',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 11,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'user_id');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Пользователь',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 8,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'phone');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Номер телефона',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 9,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'email');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'E-mail',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 10,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'site');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Сайт',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 11,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'is_sent_to_bitrix');
        $dataRow->fill([
            'type' => 'checkbox',
            'display_name' => 'Отправлено в Битрикс24',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 12,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'sent_to_bitrix_at');
        $dataRow->fill([
            'type' => 'timestamp',
            'display_name' => 'Время отправки в Битрикс24',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 12,
        ])->save();
    }
}
