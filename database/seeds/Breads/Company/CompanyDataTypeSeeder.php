<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class CompanyDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'companies');

        $dataType->fill([
            'name'                  => 'companies',
            'display_name_singular' => 'Инвестор',
            'display_name_plural'   => 'Инвесторы',
            'icon'                  => 'voyager-person',
            'model_name'            => 'App\\Models\\Company',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Инвесторы']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-person',
            'route' => 'voyager.companies.index',
        ])->save();
    }
}
