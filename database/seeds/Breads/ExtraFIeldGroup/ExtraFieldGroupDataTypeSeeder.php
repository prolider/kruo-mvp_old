<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class ExtraFieldGroupDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'extra-field-groups');

        $dataType->fill([
            'name'                  => 'extra_field_groups',
            'display_name_singular' => 'Группа свойств',
            'display_name_plural'   => 'Группы свойств',
            'icon'                  => 'voyager-categories',
            'model_name'            => 'App\\Models\\ExtraFieldGroup',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Группы свойств']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.extra-field-groups.index',
        ])->save();
    }
}
