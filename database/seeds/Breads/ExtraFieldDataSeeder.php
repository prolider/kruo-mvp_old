<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class ExtraFieldDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('ExtraFieldDataTypeSeeder');
        $this->seed('ExtraFieldDataRowSeeder');
        $this->seed('ExtraFieldPermissionsSeeder');
    }
}
