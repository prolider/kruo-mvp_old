<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class ExtraFieldDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'extra-fields');

        $dataType->fill([
            'name'                  => 'extra_fields',
            'display_name_singular' => 'Поле',
            'display_name_plural'   => 'Поля',
            'icon'                  => 'voyager-params',
            'model_name'            => 'App\\Models\\ExtraField',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();
    }
}
