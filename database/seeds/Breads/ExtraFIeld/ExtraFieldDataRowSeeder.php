<?php

use App\Models\ExtraField;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class ExtraFieldDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'extra-fields')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Название',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'type');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Тип',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
            'details'      => [
                'options' => ExtraField::TYPES,
                'default' => ExtraField::TYPE_URL,
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type'         => 'text_area',
            'display_name' => 'Описание',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'is_required');
        $dataRow->fill([
            'type'         => 'checkbox',
            'display_name' => 'Обязательное',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'is_filterable');
        $dataRow->fill([
            'type'         => 'checkbox',
            'display_name' => 'Показывать в фильтре',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'default_value');
        $dataRow->fill([
            'type'         => 'checkbox',
            'display_name' => 'Значение по умолчанию',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 6,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'read_only');
        $dataRow->fill([
            'type'         => 'checkbox',
            'display_name' => 'Только для чтения',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 7,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'extra_field_group_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Группа свойств',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'extra_field_belongsto_extra_field_group_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Группа свойств',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
            'details'      => [
                'model'       => 'App\\Models\\ExtraFieldGroup',
                'table'       => 'extra_field_groups',
                'type'        => 'belongsTo',
                'column'      => 'extra_field_group_id',
                'key'         => 'id',
                'label'       => 'name',
                'pivot_table' => 'article_extra_field_values',
                'pivot'       => 0,
                'taggable'    => null
            ],
        ])->save();
    }
}
