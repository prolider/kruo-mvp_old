<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class UserDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userDataType = DataType::where('slug', 'users')->firstOrFail();

        $dataRow = $this->dataRow($userDataType, 'id');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.name'),
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
            'details'      => [
                'validation' => [
                    'rule' => 'required|string|min:1',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'firstname');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Фамилия',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
            'details'      => [
                'validation' => [
                    'rule' => 'required|string|min:1',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'patronymic');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Отчество',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
            'details'      => [
                'validation' => [
                    'rule' => 'required|string|min:1',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'email');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.email'),
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
            'details'      => [
                'validation' => [
                    'rule' => 'required|unique:users,email|email',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'password');
        $dataRow->fill([
            'type'         => 'password',
            'display_name' => __('voyager::seeders.data_rows.password'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 4,
            'details'      => [
                'validation' => [
                    'rule' =>  "nullable|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@#$%^&]).*$/",
                    'messages' => [
                        'min' => 'Пароль должен быть не меньше :min символов.',
                        'regex' => 'Пароль должен состоять из символов разного регистра, цифр и спец. знаков.',
                    ]
                ]
            ],
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'remember_token');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.remember_token'),
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => __('voyager::seeders.data_rows.created_at'),
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 6,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => __('voyager::seeders.data_rows.updated_at'),
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 7,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'avatar');
        $dataRow->fill([
            'type'         => 'image',
            'display_name' => __('voyager::seeders.data_rows.avatar'),
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'user_belongsto_role_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => __('voyager::seeders.data_rows.role'),
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => [
                'model'       => 'TCG\\Voyager\\Models\\Role',
                'table'       => 'roles',
                'type'        => 'belongsTo',
                'column'      => 'role_id',
                'key'         => 'id',
                'label'       => 'display_name',
                'pivot_table' => 'roles',
                'pivot'       => 0,
            ],
            'order'        => 10,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'user_belongstomany_role_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Roles',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => [
                'model'       => 'TCG\\Voyager\\Models\\Role',
                'table'       => 'roles',
                'type'        => 'belongsToMany',
                'column'      => 'id',
                'key'         => 'id',
                'label'       => 'display_name',
                'pivot_table' => 'user_roles',
                'pivot'       => '1',
                'taggable'    => '0',
            ],
            'order'        => 11,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'settings');
        $dataRow->fill([
            'type'         => 'hidden',
            'display_name' => 'Settings',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 12,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'role_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.role'),
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'viber');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Viber',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'telegram');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Telegram',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'whatsapp');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Whatsapp',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'position');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'position',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'is_confirmed');
        $dataRow->fill([
            'type'         => 'checkbox',
            'display_name' => 'is_confirmed',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
            'details'      => [
                'on'       => 1,
                'off'      => 0,
            ]
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'window_id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Министерство',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 12,
        ])->save();

        $dataRow = $this->dataRow($userDataType, 'user_belongsto_window_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Министерство',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => [
                'model'       => 'App\\Models\\Window',
                'table'       => 'windows',
                'type'        => 'belongsTo',
                'column'      => 'window_id',
                'key'         => 'id',
                'label'       => 'name',
                'pivot_table' => '',
                'pivot'       => 0,
            ],
            'order'        => 10,
        ])->save();
    }
}
