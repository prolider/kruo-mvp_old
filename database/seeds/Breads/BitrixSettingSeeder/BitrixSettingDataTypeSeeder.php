<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class BitrixSettingDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'bitrix-settings');

        $dataType->fill([
            'name'                  => 'bitrix_settings',
            'display_name_singular' => 'Битрикс24',
            'display_name_plural'   => 'Битрикс24',
            'icon'                  => 'voyager-logbook',
            'model_name'            => 'App\\Models\\BitrixSetting',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();
    }
}
