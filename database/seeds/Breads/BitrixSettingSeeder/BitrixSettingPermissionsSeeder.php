<?php

use Illuminate\Database\Seeder;
use \TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class BitrixSettingPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('bitrix_settings');

        $this->seed('PermissionRoleTableSeeder');
    }
}
