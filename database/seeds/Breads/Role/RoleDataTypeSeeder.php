<?php

use Illuminate\Database\Seeder;

class RoleDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'roles');
        $dataType->fill([
            'name'                  => 'roles',
            'display_name_singular' => __('voyager::seeders.data_types.role.singular'),
            'display_name_plural'   => __('voyager::seeders.data_types.role.plural'),
            'icon'                  => 'voyager-lock',
            'model_name'            => 'TCG\\Voyager\\Models\\Role',
            'controller'            => 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();
    }
}
