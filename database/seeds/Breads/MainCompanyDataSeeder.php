<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class MainCompanyDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('MainCompanyDataTypeSeeder');
        $this->seed('MainCompanyDataRowSeeder');
        $this->seed('MainCompanyPermissionsSeeder');
    }
}
