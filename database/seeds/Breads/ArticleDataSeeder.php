<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class ArticleDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('ArticleDataTypeSeeder');
        $this->seed('ArticleDataRowSeeder');
        $this->seed('ArticlePermissionsSeeder');
    }
}
