<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class EventDataSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('EventDataTypeSeeder');
        $this->seed('EventDataRowSeeder');
        $this->seed('EventPermissionsSeeder');
    }
}
