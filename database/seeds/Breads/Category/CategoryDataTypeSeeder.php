<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class CategoryDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'categories');

        $dataType->fill([
            'name'                  => 'categories',
            'display_name_singular' => 'Категория',
            'display_name_plural'   => 'Категории',
            'icon'                  => 'voyager-categories',
            'model_name'            => 'App\\Models\\Category',
            'controller'            => 'App\\Http\\Controllers\\Voyager\\CategoryController',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Категории']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.categories.index',
        ])->save();
    }
}
