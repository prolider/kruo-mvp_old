<?php

use Illuminate\Database\Seeder;
use \TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class CategoryPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('categories');

        $this->seed('PermissionRoleTableSeeder');
    }
}
