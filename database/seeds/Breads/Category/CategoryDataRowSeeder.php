<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class CategoryDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'categories')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Название',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 2,
            'details'  => [
                'validation' => [
                    'rule' => 'required|string|min:1',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'description');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Описание',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 2,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'company_belongsto_category_relationship');
        $dataRow->fill([
            'type' => 'relationship',
            'display_name' => 'Категория',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Category',
                'table' => 'categories',
                'type' => 'belongsTo',
                'column' => 'category_id',
                'key' => 'id',
                'label' => 'name',
                'pivot' => 0,
                'taggable' => 0,
            ],
            'order' => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'category_id');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Категория',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 3,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'sort');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => 'Порядок сортировки',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'company_belongsto_extra_field_groups_relationship');
        $dataRow->fill([
            'type' => 'relationship',
            'display_name' => 'Группа свойств',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
            'details' => [
                'model' => 'App\\Models\\ExtraFieldGroup',
                'table' => 'extra_field_groups',
                'type' => 'belongsToMany',
                'column' => 'extra_field_group_id',
                'key' => 'id',
                'label' => 'name',
                'pivot' => 1,
                'taggable' => 0,
                'pivot_table' => 'category_extra_field_groups',
                'scope' => 'operator',
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'tag');
        $dataRow->fill([
            'type' => 'select_dropdown',
            'display_name' => 'Тег',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
            'details'      => [
                'options' => [
                    Category::TAG_MEASURES => 'Меры поддержки',
                    Category::TAG_NEWS => 'Новости',
                    Category::TAG_PARTNERS => 'Партнёры',
                    Category::TAG_NONE => 'Нет тега',
                ],
                'default' => Category::TAG_NONE,
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'slug');
        $dataRow->fill([
            'type' => 'text',
            'display_name' => 'Семантическая ссылка',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 5,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'locale');

        $locales = [];

        foreach(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales() as $locale => $attributes) {
            $locales[$locale] = $attributes['native'];
        }
        $dataRow->fill([
            'type' => 'select_dropdown',
            'display_name' => 'Язык',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
            'details'      => [
                'options' => $locales,
                'default' => 'ru',
            ],
        ])->save();
    }
}
