<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class WindowDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'windows');

        $dataType->fill([
            'name'                  => 'windows',
            'display_name_singular' => 'Министерство',
            'display_name_plural'   => 'Министерства',
            'icon'                  => 'voyager-home',
            'model_name'            => 'App\\Models\\Window',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Профили Министерств']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-home',
            'route' => 'voyager.windows.index',
        ])->save();
    }
}
