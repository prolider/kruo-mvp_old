<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class WindowDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $companyDataType = DataType::where('slug', 'windows')->firstOrFail();

        $dataRow = $this->dataRow($companyDataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Название',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'email');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Email',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'description');
        $dataRow->fill([
            'type'         => 'rich_text_box',
            'display_name' => 'Описание',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Дата создания',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 4,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Дата обновления',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'deleted_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Дата удаления',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 6,
        ])->save();

        $dataRow = $this->dataRow($companyDataType, 'window_belongstomany_service_category_relationship');
        $dataRow->fill([
            'type'         => 'relationship',
            'display_name' => 'Категории услуг',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
            'details'      => [
                'model' => 'App\\Models\\ServiceCategory',
                'table' => 'service_categories',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'service_category_window',
                'pivot' => 1,
                'taggable' => 0,
            ],
        ])->save();
    }
}
