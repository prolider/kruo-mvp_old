<?php

use Illuminate\Database\Seeder;
use \TCG\Voyager\Models\Permission;
use TCG\Voyager\Traits\Seedable;

class WindowPermissionsSeeder extends Seeder
{
    use Seedable;

    public function run()
    {
        Permission::generateFor('windows');

        $this->seed('PermissionRoleTableSeeder');
    }
}
