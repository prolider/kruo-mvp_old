<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class ServiceCategoryDataRowSeeder extends Seeder
{
    use HasDataRowTrait;

    public function run()
    {
        $dataType = DataType::where('slug', 'service-categories')->firstOrFail();

        $dataRow = $this->dataRow($dataType, 'id');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => __('voyager::seeders.data_rows.id'),
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Название',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'order');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => 'Порядок отображения',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 2,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'status');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Статус',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 4,
            'details'      => [
                'options' => [
                    'A' => 'Активно',
                    'D' => 'Неактивно'
                ],
                'default' => 'D'
            ],
        ])->save();

        $dataRow = $this->dataRow($dataType, 'created_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Создано',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 5,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'updated_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Обновлено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 6,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'deleted_at');
        $dataRow->fill([
            'type'         => 'timestamp',
            'display_name' => 'Удалено',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 7,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'image');
        $dataRow->fill([
            'type'         => 'image',
            'display_name' => 'Изображение',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'class_name');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Класс блока',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 9,
        ])->save();

        $dataRow = $this->dataRow($dataType, 'locale');

        $locales = [];

        foreach(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales() as $locale => $attributes) {
            $locales[$locale] = $attributes['native'];
        }
        $dataRow->fill([
            'type' => 'select_dropdown',
            'display_name' => 'Язык',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'order' => 4,
            'details'      => [
                'options' => $locales,
                'default' => 'ru',
            ],
        ])->save();
    }
}
