<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\{
    Menu,
    MenuItem
};
class ServiceCategoryDataTypeSeeder extends Seeder
{
    use HasDataTypeTrait;

    public function run()
    {
        $dataType = $this->dataType('slug', 'service-categories');

        $dataType->fill([
            'name'                  => 'service_categories',
            'display_name_singular' => 'Категория услуг',
            'display_name_plural'   => 'Категории услуг',
            'icon'                  => 'voyager-categories',
            'model_name'            => 'App\\Models\\ServiceCategory',
            'generate_permissions'  => 1,
            'description'           => '',
        ])->save();

        $menu = Menu::firstOrNew(['name' => 'admin']);

        $menuItem = MenuItem::firstOrNew(['menu_id' => $menu->id, 'title' => 'Категории услуг']);

        $menuItem->fill([
            'menu_id' => $menu->id,
            'target' => '_self',
            'icon_class' => 'voyager-categories',
            'route' => 'voyager.service-categories.index',
        ])->save();
    }
}
