<?php

use Illuminate\Database\Seeder;
use App\Models\{
    Category,
    ExtraFieldGroup,
    ExtraField,
};

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::firstOrNew([
            'category_id' => null,
            'tag'  => Category::TAG_PARTNERS,
        ]);
        $category->fill(['name' => 'Бизнес кооперации', 'slug' => 'partners', 'locale' => 'ru'])->save();

        $category = Category::firstOrNew([
            'category_id' => null,
            'tag' => Category::TAG_NEWS,
        ]);
        $category->fill(['name' => 'Новое на портале', 'slug' => 'news', 'locale' => 'ru'])->save();

        $category = Category::firstOrNew([
            'category_id' => null,
            'tag' => Category::TAG_MEASURES,
        ]);
        $category->fill(['name' => 'Меры поддержки', 'slug' => 'measures', 'locale' => 'ru'])->save();
    }
}
