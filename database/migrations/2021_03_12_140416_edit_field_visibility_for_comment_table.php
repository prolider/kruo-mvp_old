<?php

use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

class EditFieldVisibilityForCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `comments` CHANGE `visibility` `visibility` ENUM('A', 'O', 'M');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `comments` CHANGE `visibility` `visibility` ENUM('A', 'O');");
    }
}
