<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetNullableSomeCompanyFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('company_kpp')->nullable()->change();
            $table->string('company_address')->nullable()->change();
            $table->string('company_registration_date')->nullable()->change();
            $table->string('company_management_post')->nullable()->change();
            $table->string('company_management_name')->nullable()->change();
            $table->string('company_management_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {});
    }
}
