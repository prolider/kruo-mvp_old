<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('company_verified')->default(false);
            $table->string('company_kpp')->default('');
            $table->string('company_address')->default('');
            $table->timestamp('company_registration_date')->nullable();
            $table->string('company_management_post')->default('');
            $table->string('company_management_name')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'company_verified',
                'company_kpp',
                'company_address',
                'company_registration_date',
                'company_management_post',
                'company_management_name',
            ]);
        });
    }
}
