<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleExtraFieldValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_extra_field_values', function (Blueprint $table) {
            $table->id();

            $table->foreignId('article_id')
                ->references('id')
                ->on('articles')
            ->onDelete('CASCADE');

            $table->foreignId('extra_field_id')
                ->references('id')
                ->on('extra_fields')
            ->onDelete('CASCADE');

            $table->text('value');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_extra_field_values');
    }
}
