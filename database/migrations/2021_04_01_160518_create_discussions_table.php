<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('text');
            $table->date('date_creation');
            $table->date('date_updated');
            $table->boolean('is_alert')->default(false);
            $table->json('filenames');
            $table->foreignId('user_owner')->constrained('users')->cascadeOnDelete();
            $table->foreignId('user_recepient')->constrained('users')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions');
    }
}
