<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtraFieldStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_field_steps', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->tinyInteger('number');
            $table->text('description')->nullable();
            $table->text('helper_message')->nullable();
            $table->foreignId('extra_field_group_id')->constrained('extra_field_groups')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_field_steps');
    }
}
