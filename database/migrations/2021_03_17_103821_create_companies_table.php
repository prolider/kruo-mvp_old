<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();

            $table->string('name')->nullable();
            $table->string('inn')->nullable();
            $table->string('ogrn')->nullable();
            $table->string('kpp')->nullable();
            $table->string('address')->nullable();

            $table->date('registration_date')->nullable();

            $table->string('management_post')->nullable();
            $table->string('management_name')->nullable();

            $table->foreignId('user_id')
                ->nullable()
                ->references('id')
                ->on('users')
            ->onDelete('CASCADE');

            $table->string('verification_status')->nullable();
            $table->string('statement_file')->nullable();
            $table->string('signature_file')->nullable();

            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
