<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('inn', 'telegram');
            $table->renameColumn('snils', 'whatsapp');
            $table->renameColumn('citizenship', 'viber');
            $table->renameColumn('registration_place', 'position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('telegram', 'inn');
            $table->renameColumn('whatsapp', 'snils');
            $table->renameColumn('viber', 'citizenship');
            $table->renameColumn('position', 'registration_place');
        });
    }
}
