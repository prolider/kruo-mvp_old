<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnForUsersToDiscussionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discussions', function (Blueprint $table) {
            $table->dropForeign('discussions_user_owner_foreign');
            $table->dropForeign('discussions_user_recepient_foreign');

            $table->renameColumn('user_owner', 'user_owner_id');
            $table->renameColumn('user_recepient', 'user_recipient_id');

            $table->foreign('user_owner_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('user_recipient_id')->references('id')->on('users')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discussions', function (Blueprint $table) {
            $table->dropForeign('discussions_user_owner_id_foreign');
            $table->dropForeign('discussions_user_recipient_id_foreign');

            $table->renameColumn('user_owner_id', 'user_owner');
            $table->renameColumn('user_recipient_id', 'user_recepient');

            $table->foreign('user_owner')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('user_recepient')->references('id')->on('users')->cascadeOnDelete();
        });
    }
}
