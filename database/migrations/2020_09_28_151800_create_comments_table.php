<?php

use App\Models\Comment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ticket_id')
                ->nullable()
                ->references('id')
                ->on('tickets')
                ->onDelete('set null');
            $table->foreignId('user_id')
                ->nullable()
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->text('text');
            $table->text('attachment')->nullable();
            $table->enum('visibility', [
                Comment::VISIBILITY_ALL,
                Comment::VISIBILITY_OPERATOR,
            ]);
            $table->enum('status', [
                Comment::STATUS_NEW,
                Comment::STATUS_READED,
            ]);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
