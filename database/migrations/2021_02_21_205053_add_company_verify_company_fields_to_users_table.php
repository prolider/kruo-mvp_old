<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyVerifyCompanyFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company_verified');
            $table->string('company_verification_status', 1)
                ->default(\App\Models\Company::COMPANY_VERIFICATION_STATUS_NOT_VERIFIED);
            $table->text('statement_file')->nullable();
            $table->text('signature_file')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'statement_file',
                'signature_file',
            ]);
            $table->boolean('company_verified')->default(false);
        });
    }
}
