<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->foreignId('service_category_id')
                ->nullable()
                ->references('id')
                ->on('service_categories')
                ->onDelete('set null');
            $table->foreignId('window_id')
                ->nullable()
                ->references('id')
                ->on('windows')
                ->onDelete('set null');
            $table->enum('use_button', ['Y', 'N']);
            $table->enum('status', ['S', 'P', 'I']);
            $table->integer('service_period');
            $table->enum('service_period_type', ['D', 'W', 'M']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
