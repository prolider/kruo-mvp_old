<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('form_result_id')
                ->nullable()
                ->references('id')
                ->on('form_results')
                ->onDelete('set null');
            $table->foreignId('operator_id')
                ->nullable()
                ->references('id')
                ->on('users')
                ->onDelete('set null');
            $table->string('status');
            $table->timestamp('plan_end_date')->nullable();
            $table->timestamp('fact_end_date')->nullable();
            $table->enum('archived', ['Y', 'N']);
            $table->json('attachments');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
