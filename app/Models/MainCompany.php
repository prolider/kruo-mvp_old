<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MainCompany
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property string $description
 */
class MainCompany extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'short_name',
        'description',
    ];

    public function operators()
    {
        return $this->belongsToMany(User::class, 'main_company_operators');
    }
}
