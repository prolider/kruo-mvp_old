<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, BelongsToMany, HasMany};
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App
 *
 * @property int $id
 * @method static Builder rootCategories()
 * @method static Builder tag($tag)
 */
class Category extends Model
{
    use SoftDeletes;
    use SlugTrait;

    public const TAG_NEWS = 'news';
    public const TAG_PARTNERS = 'partners';
    public const TAG_MEASURES = 'measures';
    public const TAG_NONE = '';

    public const TAGS = [
        self::TAG_NEWS,
        self::TAG_PARTNERS,
        self::TAG_MEASURES,
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'category_id',
        'is_closed',
        'allowed_role_id',
        'is_need_moderate',
        'tag',
        'description',
        'slug',
        'locale',
        'sort',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function categories(): HasMany
    {
        return $this->hasMany(Category::class)->orderByRaw('ISNULL(sort), sort ASC');
    }

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    public function extraFieldGroups(): BelongsToMany
    {
        return $this->belongsToMany(ExtraFieldGroup::class, 'category_extra_field_groups');
    }

    public function childCategories(): HasMany
    {
        return $this->categories()->with('categories');
    }

    public function parentCategory()
    {
        return $this->category()->with('category');
    }

    public function scopeTag($query, $tag)
    {
        return $query->where('tag', $tag);
    }

    public function scopeLocale($query, $locale)
    {
        return $query->where('locale', $locale);
    }

    public function getHierarchy($recursive = true)
    {
        $categories = [$this];

        $parentCategory = $this->category;

        if (!$parentCategory) return $categories;

        $categories = array_merge($categories, $parentCategory->getHierarchy(false));

        return $recursive ? array_reverse($categories) : $categories;
    }

    public function scopeRootCategories($query)
    {
        return $query->with('childCategories')->where('category_id', null);
    }
}
