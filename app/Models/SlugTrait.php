<?php

namespace App\Models;

use Illuminate\Support\Str;

trait SlugTrait
{
    public function setSlugAttribute($slug = false)
    {
        if (!$slug) {
            $slug = Str::slug($this->name);
        }

        if (self::where('id', '!=', $this->id)->where('slug', $slug)->count() > 0) {
            $slug .= '-' . $this->id;
        }

        $this->attributes['slug'] = $slug;
    }

    public function getSlugHierarchy()
    {
        $categories = [];

        $tag = false;

        if ($category = $this->category) {
            $tag = $category->tag;
            $categories = $category->getHierarchy();
        } else {
            $tag = $this->tag ?: false;
        }

        $categories[] = $this;

        $slugs = collect($categories)->pluck('slug')->filter()->implode('/');

        if ($tag) {
            $slugs = $tag . '/' . $slugs;
        }

        return $slugs;
    }

    public function generateSlug()
    {
        $this->setSlugAttribute();
        $this->update();
    }
}