<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * Class Service
 * @package App
 *
 * @property int $id
 * @property int $service_category_id
 * @property int $window_id
 * @property int $form_id
 * @property string $name
 * @property string $description
 * @property string $use_button
 * @property int $service_period
 * @property string $service_period_type
 * @property string $official_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Form $form
 * @property ServiceCategory $category
 * @property Window $window
 * @property Collection $documents
 */
class Service extends Model
{
    use SoftDeletes;

    const USE_BUTTON = 'Y';
    const NOT_USE_BUTTON = 'N';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'service_category_id', 'window_id', 'use_button', 'status', 'service_period',
        'service_period_type', 'form_id', 'official_name', 'links',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(Form::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function window()
    {
        return $this->belongsTo(Window::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ServiceCategory::class, 'service_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function documents()
    {
        return $this->hasMany(ServiceDocument::class)->orderBy('sort');
    }

    /**
     * @return array
     */
    public function getLinksArrayAttribute()
    {
        $links = [];
        if ($this->links)
        {
            $links = json_decode($this->links, true) ?? [];
        }

        return $links;
    }

    public function getShowButtonAttribute()
    {
        return $this->use_button == self::USE_BUTTON;
    }
}
