<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 * @package App
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $description
 * @property string $notificable_type
 * @property int $notificable_id
 * @property string $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Ticket $notificable
 * @property User $client
 * @method static Builder user()
 */
class Notification extends Model
{
    const STATUS_NEW    = 'N';
    const STATUS_READED = 'R';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'description', 'notificable_type', 'notificable_id', 'status',
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeUser($query)
    {
        if (auth()->check())
        {
            $query->where('user_id', '=', auth()->id());
        }

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificable()
    {
        return $this->morphTo('notificable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class);
    }
}
