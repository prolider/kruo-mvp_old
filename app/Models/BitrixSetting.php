<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 */
class BitrixSetting extends Model
{
    use SoftDeletes;

    public const CRM_LEAD_ADD = 'crm.lead.add';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url',
    ];
}
