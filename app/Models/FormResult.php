<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FormResult
 * @package App
 *
 * @property int $id
 * @property int $form_id
 * @property int $user_id
 * @property array $result
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Form $form
 * @property User $user
 */
class FormResult extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'form_id', 'user_id', 'result',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'result' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(Form::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return array
     */
    public function getFormResultAttribute()
    {
        return is_array($this->result) ? $this->result : json_decode($this->result, true);
    }
}
