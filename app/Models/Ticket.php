<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class Ticket
 * @package App
 *
 * @property int $id
 * @property int $form_result_id
 * @property int $minuser_id
 * @property int $window_id
 * @property string $status
 * @property Carbon $plan_end_date
 * @property Carbon $fact_end_date
 * @property string $archived
 * @property array $attachments
 * @property FormResult $formResult
 * @property User $operator
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Collection $comments
 * @property Invite $invite
 */
class Ticket extends Model
{
    use SoftDeletes;

    const STATUS_DRAFT          = 'R';
    const STATUS_CLIENT_WRITE   = 'L';
    const STATUS_NEW            = 'N';
    const STATUS_IN_WORK        = 'W';
    const STATUS_FORMALIZATION  = 'O';
    const STATUS_CANCEL_REQUEST = 'P';
    const STATUS_CANCELED       = 'C';
    const STATUS_DONE           = 'D';
    const STATUS_NEED_DATA      = 'E';
    const STATUS_REJECT         = 'F';
    const STATUS_INVITE         = 'I';

    const STATUS_NAMES = [
        self::STATUS_NEW            => "Отправлена",
        self::STATUS_IN_WORK        => "Взята в работу",
        self::STATUS_FORMALIZATION  => "Оформление",
        self::STATUS_CANCELED       => "Отменена",
        self::STATUS_DONE           => "Услуга оказана",
        self::STATUS_DRAFT          => "Черновик",
        self::STATUS_CLIENT_WRITE   => "Клиент заполняет",
        self::STATUS_CANCEL_REQUEST => "Запрос на отмену",
        self::STATUS_NEED_DATA      => "Требует уточнения",
        self::STATUS_REJECT         => "Отказ",
        self::STATUS_INVITE         => "Приглашение"
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'form_result_id',
        'minuser_id',
        'window_id',
        'status',
        'plan_end_date',
        'fact_end_date',
        'archived',
        'attachments',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'plan_end_date',
        'fact_end_date',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'attachments' => 'array',
    ];

    protected $appends = [
        'status_class',
        'status_name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formResult()
    {
        return $this->belongsTo(FormResult::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function minuser()
    {
        return $this->belongsTo(User::class, 'minuser_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        $user = auth()->user();
        $user->load('role');

        if ($user->role->name === 'user') {
            return $this->hasMany(Comment::class)
                ->orderBy('created_at')
                ->where('visibility', '=', Comment::VISIBILITY_ALL);
        } else {
            return $this->hasMany(Comment::class)->orderBy('created_at');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function newComments()
    {
        return $this->hasMany(Comment::class)
            ->where('status', '=', Comment::STATUS_NEW)
            ->where('user_id', '<>', auth()->id());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeUser($query)
    {
        /** @var User $user */
        $user = auth()->user();
        if ($user->role->name === 'operator')
        {
            return $query;
            return $query->where('operator_id', '=', $user->id);
        }

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invite()
    {
        return $this->hasOne(Invite::class);
    }

    public function window(): BelongsTo
    {
        return $this->belongsTo(Window::class);
    }

    public function scopeFilter($query, Request $request)
    {
        if ($request->has('window')) {
            $query->where('window_id', $request->input('window'));
        }

        if ($request->has('operator')) {
            $query->where('operator_id', $request->input('operator'));
        }

        if ($request->has('search')) {
            $input = $request->input('search');

            $query->whereHas('formResult', function ($query) use ($input) {
                return $query->whereHas('form', function ($queryIn) use ($input) {
                    return $queryIn->where('name', 'like', '%' . $input . '%');
                });
            });
        }

        if ($request->has('status')) {
            $query->where('status', $request->input('status'));
        }

        if ($request->has('start_date')) {
            $startDate = Carbon::createFromFormat('Y-m-d', $request->input('start_date'))->startOfDay();
            $query->where('plan_end_date', '>=', $startDate);
        }

        if ($request->has('end_date')) {
            $endDate = Carbon::createFromFormat('Y-m-d', $request->input('end_date'))->endOfDay();
            $query->where('plan_end_date', '<=', $endDate);
        }

        $query->select('tickets.*');

        if ($request->has('id_s')) {
            $query->orderBy('id', $request->input('id_s'));
        }

        if ($request->has('date_s')) {
            $query->orderBy('plan_end_date', $request->input('date_s'));
        }

        if ($request->has('status_s')) {
            $query->orderBy('status', $request->input('status_s'));
        }

        if ($request->has('manager_s')) {
            $query->leftJoin('users as managers', 'tickets.minuser_id', '=', 'managers.id')
                    ->orderBy('managers.firstname', $request->input('manager_s'));
        }

        if ($request->has('window_s')) {
            $query->leftJoin('windows', 'tickets.window_id', '=', 'windows.id')
                    ->orderBy('windows.name', $request->input('window_s'));
        }

        if ($request->hasAny(['name_s', 'manager_s', 'company_s'])) {
            $query->leftJoin('form_results', 'tickets.form_result_id', '=', 'form_results.id')
                    ->leftJoin('forms', 'form_results.form_id', '=', 'forms.id');

            if ($request->has('name_s')) {
                $query->orderBy('forms.name', $request->input('name_s'));
            }

            if ($request->has('company_s')) {
                $query->leftJoin('users', 'form_results.user_id', 'users.id')
                        ->leftJoin('companies', 'users.id', 'companies.user_id')
                        ->orderBy('companies.name', $request->input('company_s'));
            }
        }

        return $query;
    }

    public function getStatusClassAttribute()
    {
        switch ($this->status) {
            case self::STATUS_CANCELED:
                $class = 'label-light-danger';
                break;
            case self::STATUS_DONE:
                $class = 'label-light-success';
                break;
            case self::STATUS_IN_WORK:
                $class = 'label-light-warning';
                break;
            case self::STATUS_NEW:
            default:
                $class = 'label-light-primary';
                break;
        }

        return $class;
    }

    public function getStatusNameAttribute()
    {
        return self::STATUS_NAMES[$this->status] ?? '';
    }
}
