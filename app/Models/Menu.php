<?php

namespace App\Models;

class Menu extends \TCG\Voyager\Models\Menu
{
    protected $fillalble = [
        'name',
        'display_name',
    ];
}
