<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Model,
    SoftDeletes
};
use Illuminate\Database\Eloquent\Relations\{
    BelongsToMany,
    HasMany
};

/**
 * Class ExtraFieldGroup
 * @package App
 *
 * @property int $id
 */
class ExtraFieldGroup extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'with_steps',
    ];

    public function fields(): HasMany
    {
        return $this->hasMany(ExtraField::class);
    }

    public function steps(): HasMany
    {
        return $this->hasMany(ExtraFieldStep::class);
    }

    public function filterableFields()
    {
        return $this->fields->where('is_filterable', true);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
