<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraFieldStep extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'number',
        'description',
        'helper_message',
        'extra_field_group_id',
    ];

    public function fields()
    {
        return $this->hasMany(ExtraField::class);
    }
}
