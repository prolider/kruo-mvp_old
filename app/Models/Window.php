<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Window extends Model
{
    use SoftDeletes;

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function forSelect()
    {
        return Window::all()->pluck('name', 'id');
    }

    public static function forSelectChange()
    {
        return Window::query()->with('categories')->get()->toArray();
    }

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(ServiceCategory::class);
    }

    /**
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function tickets(): HasMany
    {
        return $this->hasMany(Ticket::class);
    }
}
