<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

/**
 * Class Article
 * @package App
 *
 * @property int $id
 * @method static Builder filter($request)
 * @method static Builder published()
 */
class Article extends Model
{
    use SoftDeletes;
    use SlugTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'content',
        'category_id',
        'on_mainpage',
        'is_published',
        'slug',
    ];

    protected $casts = [
        'is_published' => 'boolean',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeFilter(Builder $query, Request $request)
    {
        if ($category = $request->category_id) {
            $query->where('category_id', $category);
        }

        if ($fields = $request->fields) {
            foreach ($fields as $fieldId => $values) {
                if (!$values) continue;
                $values = preg_replace('/"/', '\\\\\"', $values);

                $query->whereHas('extraFields', function ($extraFields) use ($values, $fieldId) {
                    return $extraFields->where('extra_fields.id', $fieldId)->whereIn('value', $values)
                        ->orWhere(function ($query) use ($values) {
                            $conditions = collect($values)->map(function ($value) {
                                return ['value', 'like', '%' . $value . '%'];
                            })->toArray();

                            foreach ($conditions as $condition) {
                                $query->orWhere(...$condition);
                            }
                        });
                });
            }
        }

        return $query;
    }

    public function extraFields()
    {
        return $this->belongsToMany(ExtraField::class, 'article_extra_field_values')
            ->using(ArticleExtraFieldValuePivot::class)
            ->withPivot('value');
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('is_published', true);
    }
}
