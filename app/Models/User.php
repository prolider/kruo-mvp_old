<?php

namespace App\Models;

use App\Mail\UserRegistered;
use App\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User as VoyagerUser;

/**
 * Class User
 * @package App
 *
 * @method static Builder operator()
 * @method static Builder administrator()
 *
 * @property int $id
 * @property string $firstname
 * @property string $name
 * @property string $patronymic
 * @property string $company_name
 * @property string $company_inn
 * @property string $company_verification_status
 * @property array $statement_file
 * @property array $signature_file
 * @property string $full_name
 * @property Company $company
 * @property bool $canCreateTickets
 */
class User extends VoyagerUser implements MustVerifyEmail
{
    use Notifiable, SoftDeletes;

    const SEX_MALE = 'M';
    const SEX_FEMALE = 'F';

    public const AVATAR_DEFAULT = 'images/user-default.jpg';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firstname', 'patronymic', 'sex', 'birthday', 'phone', 'viber', 'telegram',
        'whatsapp', 'passport_serial', 'passport_number', 'passport_date', 'passport_subdivision',
        'passport_organization', 'position', 'company_inn', 'company_ogrn', 'company_name',
        'company_verification_status', 'company_kpp', 'company_address', 'company_registration_date', 'company_management_post',
        'company_management_name', 'statement_file', 'signature_file', 'avatar', 'is_confirmed', 'two_factor_code', 'two_factor_expires_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'passport_date',
        'company_registration_date',
    ];

    protected $appends = [
        'full_name',
        'avatar_url',
        'organization_name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'statement_file' => 'array',
        'signature_file' => 'array',
        'email_verified_at' => 'datetime',
        'two_factor_expires_at' => 'datetime',
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOperator($query)
    {
        $administratorRoles = Role::query()
            ->where('name', '=', 'operator')
            ->orWhere('name', '=', 'admin')
            ->get()
            ->pluck('id')
            ->toArray();

        if (count($administratorRoles) > 0)
        {
            return $query->whereIn('role_id', $administratorRoles);
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeAdministrator($query)
    {
        $administratorRoles = Role::query()
            ->orWhere('name', '=', 'admin')
            ->get()
            ->pluck('id')
            ->toArray();

        if (count($administratorRoles) > 0)
        {
            return $query->whereIn('role_id', $administratorRoles);
        }

        return $query;
    }

    public function scopeMinistry($query)
    {
        $ministryRole = Role::where('name', 'minuser')->first();

        if ($ministryRole) {
            return $query->where('role_id', $ministryRole->id);
        }

        return $query;
    }

    public function scopeUser($query)
    {
        $userRole = Role::where('name', 'user')->first();

        if ($userRole) {
            return $query->where('role_id', $userRole->id);
        }

        return $query;
    }

    /**
     * @return int
     */
    public static function newMessagesCount(): int
    {
        return Ticket::query()
            ->whereHas('formResult', function (Builder $query) {
                return $query->where('user_id', '=', auth()->id());
            })
            ->whereHas('newComments')
            ->count();
    }

    /**
     * Ссылка на файл заявления
     *
     * @return string
     */
    public function getStatementUrlAttribute(): string
    {
        return "/storage/{$this->statement_file['path']}";
    }

    /**
     * Ссылка на файл подписи заявления
     *
     * @return string
     */
    public function getSignatureUrlAttribute(): string
    {
        return "/storage/{$this->signature_file['path']}";
    }

    /**
     * Полное имя пользователя
     *
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return "{$this->firstname} {$this->name} {$this->patronymic}";
    }

    /**
     * @return HasOne
     */
    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }

    public function messages(): HasMany
    {
        return $this->hasMany(DiscussionMessage::class, 'user_owner_id');
    }

    /**
     * @return BelongsTo
     */
    public function window(): BelongsTo
    {
        return $this->belongsTo(Window::class);
    }

    public function inviterCompany(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar && $this->avatar !== self::AVATAR_DEFAULT) {
            if (Storage::disk('users')->exists($this->id . '/' . $this->avatar)) {
                return Storage::disk('users')->url($this->id . '/' . $this->avatar);
            } elseif (Storage::disk('public')->exists($this->avatar)) {
                return Storage::disk('public')->url($this->avatar);
            }
        }

        return url(self::AVATAR_DEFAULT);
    }

    public function getAvatarAttribute($value)
    {
        if ($value && $value !== self::AVATAR_DEFAULT) {
            if (Storage::disk('users')->exists($this->id . '/' . $value)) {
                return 'users/' . $this->id . '/' . $value;
            } elseif (Storage::disk('public')->exists($value)) {
                return $value;
            }
        }

        return self::AVATAR_DEFAULT;
    }

    public function setPasswordAttribute($value)
    {
        if (Hash::info($value)['algoName'] == 'unknown') {
            $this->attributes['password'] = Hash::make($value);
        } else {
            $this->attributes['password'] = $value;
        }
    }

    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        return $this->is_confirmed;
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'is_confirmed' => true,
        ])->save();
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        Mail::to($this)->send(new UserRegistered($this));
    }

    public function getOrganizationNameAttribute()
    {
        $name = 'Организация не указана';

        if ($company = $this->company) {
            $name = $company->name;
        } elseif ($window = $this->window) {
            $name = $window->name;
        } elseif ($this->role && $this->role->name === 'operator') {
            $name = 'АО "Корпорация развития Ульяновской области"';
        }

        return $name;
    }

    public function generateTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = rand(100000, 999999);
        $this->two_factor_expires_at = now()->addMinutes(10);
        $this->save();
    }

    public function resetTwoFactorCode()
    {
        $this->timestamps = false;
        $this->two_factor_code = null;
        $this->two_factor_expires_at = null;
        $this->save();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
