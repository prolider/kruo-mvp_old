<?php

namespace App\Models;

use TCG\Voyager\Models\Role as VoyagerRole;

class Role extends VoyagerRole
{
    public const ROLE_USER = 'user';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_OPERATOR = 'operator';
    public const ROLE_MINUSER = 'minuser';
    public const ROLE_INVITED = 'invited';
    public const ROLE_LEADER = 'leader';

    public const ROLES = [
        self::ROLE_USER => 'Обычный пользователь',
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_OPERATOR => 'Модератор',
        self::ROLE_MINUSER => 'Оператор',
        self::ROLE_INVITED => 'Наблюдатель',
        self::ROLE_LEADER => 'Руководитель отдела',
    ];
}
