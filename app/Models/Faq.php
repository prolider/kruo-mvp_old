<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Faq
 * @package App
 *
 * @property int $id
 * @property int $faq_category_id
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property int $window
 * @property int $sort
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property FaqCategory $category
 */
class Faq extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'faq_category_id',
        'name',
        'short_description',
        'description',
        'sort',
        'window',
        'locale',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(FaqCategory::class, 'faq_category_id');
    }
}
