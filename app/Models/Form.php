<?php

namespace App\Models;

use App\Services\FormBuilder\FormBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Form
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property string $is_public
 * @property string $recaptcha
 * @property array $form_data
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 */
class Form extends Model
{
    use SoftDeletes;

    const STATUS_ACTIVE = 'A';
    const STATUS_DISABLED = 'D';

    const IS_PUBLIC = 'Y';
    const IS_PRIVATE = 'N';

    const USE_RECAPTCHA = 'Y';
    const DONT_USE_RECAPTCHA = 'N';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'form_data', 'status', 'is_public', 'recaptcha',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'form_data' => 'array',
    ];

    public function formBuilder()
    {
        return new FormBuilder(is_array($this->form_data) ? $this->form_data : json_decode($this->form_data, 1));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function service()
    {
        return $this->hasOne(Service::class)->withTrashed();
    }

    /**
     * @return array
     */
    public function getFormPagesAttribute()
    {
        $formPages = is_array($this->form_data) ? $this->form_data : json_decode($this->form_data, true);
        return !is_null($formPages) ? $formPages : [];
    }

    public function getIsValidAttribute()
    {
        $form = is_array($this->form_data) ? $this->form_data : json_decode($this->form_data, true);

        $fieldCount = 0;
        foreach ($form['steps'] as $step)
        {
            foreach ($step['data'] as $row)
            {
                foreach ($row['cols'] as $col)
                {
                    foreach ($col['data'] as $field)
                    {
                        $fieldCount++;
                    }
                }
            }
        }

        return $fieldCount > 0;
    }
}
