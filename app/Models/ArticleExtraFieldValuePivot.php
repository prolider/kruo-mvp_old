<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\UploadedFile;

class ArticleExtraFieldValuePivot extends Pivot
{
    public function getValueAttribute()
    {
        if ($this->extraField->type === ExtraField::TYPE_FILE) {
            return json_decode($this->attributes['value']);
        } else {
            return $this->attributes['value'];
        }
    }
    
    public function setValueAttribute($value)
    {
        if (is_string($value) || is_null($value)) {
            $this->attributes['value'] = $value;
        } else {
            $values = is_array($value) ? $value : [$value];

            if (!head($values) instanceof UploadedFile) {
                $files = $values;
            } else {
                $files = $this->exists ? $this->value : [];

                foreach ($values as $value) {
                    $file = $value->store('extra-fields', [
                        'disk' => 'public',
                    ]);

                    $files[] = [
                        'name' => $value->getClientOriginalName(),
                        'original_name' => $value->getClientOriginalName(),
                        'download_link' => '/storage/' . $file,
                    ];
                }
            }
            $this->attributes['value'] = json_encode($files, JSON_UNESCAPED_UNICODE);
        }
    }

    public function extraField()
    {
        return $this->belongsTo(ExtraField::class);
    }
}