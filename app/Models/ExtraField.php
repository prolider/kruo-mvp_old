<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ExtraField
 * @package App
 *
 * @property int $id
 */
class ExtraField extends Model
{
    use SoftDeletes;

    const TYPE_URL = 0;
    const TYPE_DATE = 1;
    const TYPE_USER = 2;
    const TYPE_TEXT = 3;
    const TYPE_SELECT = 4;
    const TYPE_MULTISELECT = 5;
    const TYPE_TEXT_AREA = 6;
    const TYPE_FILE = 7;
    const TYPE_NUMBER = 8;

    const TYPE_URL_NAME = 'Ссылка';
    const TYPE_DATE_NAME = 'Дата';
    const TYPE_USER_NAME = 'Пользователь';
    const TYPE_TEXT_NAME = 'Текст';
    const TYPE_SELECT_NAME = 'Выбор';
    const TYPE_MULTISELECT_NAME = 'Множественный выбор';
    const TYPE_TEXT_AREA_NAME = 'Текстовый редактор';
    const TYPE_FILE_NAME = 'Файл';
    const TYPE_NUMBER_NAME = 'Число';

    const TYPES = [
        self::TYPE_URL => self::TYPE_URL_NAME,
        self::TYPE_DATE => self::TYPE_DATE_NAME,
        self::TYPE_TEXT => self::TYPE_TEXT_NAME,
        self::TYPE_SELECT => self::TYPE_SELECT_NAME,
        self::TYPE_MULTISELECT => self::TYPE_MULTISELECT_NAME,
        self::TYPE_TEXT_AREA => self::TYPE_TEXT_AREA_NAME,
        self::TYPE_FILE => self::TYPE_FILE_NAME,
        self::TYPE_NUMBER => self::TYPE_NUMBER_NAME,
//        self::TYPE_USER => self::TYPE_USER_NAME,
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'description',
        'is_required',
        'is_filterable',
        'default_value',
        'read_only',
        'extra_field_group_id',
        'step',
        'details',
        'container_class',
        'extra_field_step_id',
        'placeholder',
        'helper_message',
        'position',
        'is_hidden',
    ];

    protected $casts = [
        'details' => 'array',
        'is_hidden' => 'boolean',
    ];

    public function getTextTypeAttribute()
    {
        return self::TYPES[$this->attributes['type']] ?? '';
    }

    public function group()
    {
        return $this->belongsTo(ExtraFieldGroup::class, 'extra_field_group_id');
    }

    public function step()
    {
        return $this->belongsTo(ExtraFieldStep::class, 'extra_field_step_id');
    }

    public function articleValues()
    {
        return $this->belongsToMany(Article::class, 'article_extra_field_values')
            ->using(ArticleExtraFieldValuePivot::class)
            ->withPivot('value');
    }
}
