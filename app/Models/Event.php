<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class Event
 * @package App
 *
 * @property int $id
 */
class Event extends Model
{
    use SoftDeletes;

    const EVENT_TYPE_MEETING = 0;
    const EVENT_TYPE_CONF = 1;
    const EVENT_TYPE_VIDEO_CONF = 2;
    const EVENT_TYPE_SESSION = 3;

    const EVENT_TYPE_NAMES = [
        self::EVENT_TYPE_MEETING => 'Встреча',
        self::EVENT_TYPE_CONF => 'Конференция',
        self::EVENT_TYPE_VIDEO_CONF => 'Видео-конференц связь',
        self::EVENT_TYPE_SESSION => 'Совещание',
    ];

    const INVITE_TYPE_PUBLIC = 0;
    const INVITE_TYPE_PRIVATE = 1;

    const INVITE_TYPE_NAMES = [
        self::INVITE_TYPE_PUBLIC => 'Массовое',
        self::INVITE_TYPE_PRIVATE => 'Личное',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'text',
        'files',
        'end_at',
        'start_at',
        'address',
        'author_id',
        'invite_type',
        'event_type',
    ];

    protected $dates = [
        'end_at',
        'start_at',
    ];

    protected $casts = [
        'files' => 'array',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'event_users');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getEventTypeNameAttribute()
    {
        return self::EVENT_TYPE_NAMES[$this->attributes['event_type']] ?? $this->attributes['event_type'];
    }

    public function getInviteTypeNameAttribute()
    {
        return self::INVITE_TYPE_NAMES[$this->attributes['invite_type']] ?? $this->attributes['invite_type'];
    }

    public function scopeCurrent($query)
    {
        return $query->where('end_at', '>', now());
    }

    public function scopeForUser($query)
    {
        $user = auth()->user();

        $query->where('invite_type', self::INVITE_TYPE_PUBLIC)
            ->orWhere('invite_type', self::INVITE_TYPE_PRIVATE)
        ->whereHas('users', function($users) use ($user) {
            return $users->where('users.id', $user->id);
        })
        ->orWhere('author_id', $user->id);

        return $query;
    }

    public function getEventDatesAttribute()
    {
        if ($this->start_at->diffInDays($this->end_at) > 0) {
            return $this->start_at->format('d.m.Y') . ' — ' . $this->end_at->format('d.m.Y');
        } else {
            return $this->start_at->format('d.m.Y');
        }
    }

    public function getEventTimesAttribute()
    {
        if ($this->start_at->diffInDays($this->end_at) > 0) {
            return $this->start_at->format('d.m.Y H:i') . ' — ' . $this->end_at->format('d.m.Y H:i');
        } else {
            return $this->start_at->format('H:i') . ' — ' . $this->end_at->format('H:i');
        }
    }

    public function getFileUrlsAttribute()
    {
        $filesUrls = [];

        foreach ($this->files ?? [] as $file) {
            if (!is_string($file)) continue;

            $filesUrls[$file] = Storage::disk('events')->url($file);
        }

        return $filesUrls;
    }

    public function isPassed()
    {
        return $this->end_at <= now();
    }

    public function isCurrentUserIsAuthor()
    {
        $user = auth()->user();

        return $user && $this->author_id == $user->id;
    }
}
