<?php

namespace App\Models\Dictionaries;

use App\Models\Category;

class TagDictionary extends Dictionary
{
    public static function getDictionary(): array
    {
        return [
            Category::TAG_NEWS => 'Новое на портале',
            Category::TAG_MEASURES => 'Меры поддержки',
            Category::TAG_NONE => 'Без тега',
            Category::TAG_PARTNERS => 'Партнёры',
        ];
    }

    public static function getSearchables(): array
    {
        return [
            Category::TAG_NEWS => 'Новое на портале',
            Category::TAG_MEASURES => 'Меры поддержки',
            'services' => 'Получить услугу',
            Category::TAG_PARTNERS => 'Площадка кооперации',
        ];
    }
}
