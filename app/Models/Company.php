<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Company
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $address
 * @property string $registration_date
 * @property string $management_post
 * @property string $management_name
 * @property User $user
 */
class Company extends Model
{
    use SoftDeletes;

    const COMPANY_VERIFICATION_STATUS_NOT_VERIFIED = 'N';
    const COMPANY_VERIFICATION_STATUS_ON_CHECK = 'C';
    const COMPANY_VERIFICATION_STATUS_ACCEPT = 'A';
    const COMPANY_VERIFICATION_STATUS_REJECT = 'R';
    const COMPANY_VERIFICATION_STATUS_CANCEL_REQUEST = 'Q';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'inn',
        'ogrn',
        'kpp',
        'address',
        'email',
        'phone',
        'site',
        'registration_date',
        'management_post',
        'management_name',
        'verification_status',
        // 'statement_file',
        'signature_file',
        'is_sent_to_bitrix',
        'sent_to_bitrix_at',
    ];

    protected $dates = [
        'registration_date',
        'sent_to_bitrix_at',
    ];

    protected $casts = [
        'is_sent_to_bitrix' => 'boolean',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function invitedUsers(): HasMany
    {
        return $this->hasMany(User::class, 'inviter_company_id');
    }

    // public function getStatementUrlAttribute(): string
    // {
    //     $file = json_decode($this->statement_file)[0] ?? false;

    //     if (!$file) {
    //         return '';
    //     }

    //     return '/storage/' . $file->download_link ?? '';
    // }

    public function getSignatureUrlAttribute(): string
    {
        $file = json_decode($this->signature_file)[0] ?? false;

        if (!$file) {
            return '';
        }

        return '/storage/' . $file->download_link ?? '';
    }

    // public function getStatementNameAttribute(): string
    // {
    //     return json_decode($this->statement_file)[0]->original_name ?? '';
    // }

    public function getSignatureNameAttribute(): string
    {
        return json_decode($this->signature_file)[0]->original_name ?? '';
    }

    public function isVerified()
    {
        return $this->verification_status === self::COMPANY_VERIFICATION_STATUS_ACCEPT;
    }

    public function isOnCheck()
    {
        return $this->verification_status === self::COMPANY_VERIFICATION_STATUS_ON_CHECK;
    }

    public function isOnCancellation()
    {
        return $this->verification_status == self::COMPANY_VERIFICATION_STATUS_CANCEL_REQUEST;
    }
}
