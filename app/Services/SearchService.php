<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Dictionaries\TagDictionary;
use App\Models\Service;
use Illuminate\Support\Arr;

class SearchService
{
    public function searchBy(array $searchables, string $search)
    {
        if (empty($searchables) || empty($search)) {
            return collect();
        }

        $searchables = $this->filterSearchables($searchables);

        $searchCategories = collect();

        $search = 'LOWER("%' . trim($search) . '%")';

        foreach ($searchables as $key => $searchable) {
            if ($key === 'services') {
                $services = Service::where('service_category_id', '!=', null)
                            ->whereRaw("(LOWER(name) like {$search} OR LOWER(description) like {$search})")
                            ->get();

                $searchCategories = $this->formCategory($searchCategories, $key, $services);

                continue;
            }

            $articles = Article::published()->whereHas('category', function ($query) use ($key) {
                return $query->tag($key);
            })->where(function ($query) use ($search) {
                return $query->whereRaw("(LOWER(name) like {$search} OR LOWER(content) like {$search})");
            })->get();

            $searchCategories = $this->formCategory($searchCategories, $key, $articles);
        }

        return $searchCategories;
    }

    public function getSearchableChecked(array $searchable)
    {
        $tags = TagDictionary::getSearchables();
        $checked = [];

        foreach ($tags as $tag => $name) {
            $checked[$tag] = [
                'checked' => in_array($tag, $searchable) || array_key_exists($tag, $searchable),
                'name' => $name,
            ];
        }

        return $checked;
    }

    private function formCategory($searchCategories, $key, $data)
    {
        $categories = TagDictionary::getSearchables();

        return $searchCategories->put($key, ['key' => $categories[$key], 'data' => $data]);
    }

    private function filterSearchables(array $searchables)
    {
        return array_intersect_key(array_flip($searchables), TagDictionary::getSearchables());
    }
}
