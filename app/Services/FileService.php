<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileService
{
    /**
     * @param string $disk
     * @param UploadedFile[] $files
     * @param string $directory
     *
     * @return array
     */
    public function saveFiles(string $disk, $files, string $directory = '/')
    {
        $filenames = [];

        foreach ($files as $file) {
            $filenames[] = $this->saveFile($disk, $file, $directory);
        }

        return $filenames;
    }

    public function saveFile(string $diskName, UploadedFile $file, string $directory = '/', bool $overwrite = false): string
    {
        $disk = Storage::disk($diskName);
        $filename = str_replace(' ', '_', $file->getClientOriginalName());

        $filenames = $disk->files();
        $filenameArray = explode('.', $filename);
        $filenames = preg_grep("/" . $filenameArray[0] . "(\(\d+\))?\.\w+$/", $filenames);

        if ($filenames && !$overwrite) {
            $storagedFile = end($filenames);
            $storagedFile = prev($filenames) ?: $storagedFile;

            $matches = [];
            preg_match('(\(\d+\))', $storagedFile, $matches);
            $fileCounter = end($matches);

            $number = $fileCounter ? (int) substr($fileCounter, 1, -1) + 1 : 1;

            $filename = $this->formFilename($filenameArray, $number);
        }

        $file->storeAs($directory, $filename, $diskName);

        return $filename;
    }

    public function delete(string $diskName, string $filename, string $directory = '')
    {
        Storage::disk($diskName)->delete($directory . '/' . $filename);
    }

    public function formFilename(array $filenameArray, int $number = 1): string
    {
        return $filenameArray[0] . '(' . $number . ').' . $filenameArray[1];
    }
}
