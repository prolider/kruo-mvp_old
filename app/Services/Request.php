<?php

namespace App\Services;

class Request
{
    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @param array $options
     * @return array
     */
    private function sendRequest(string $method, string $url, array $data, array $options = []): array
    {
        $data_string = json_encode($data);

        $headers = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string)
        ];

        if (isset($options['auth']))
        {
            $headers[] = 'Authorization: ' . $options['auth']['type'] . ' ' . $options['auth']['value'];
        }

        if (isset($options['headers']))
        {
            foreach ($options['headers'] as $key => $value)
            {
                $headers[] = $key . ' ' . $value;
            }
        }

        $ch = curl_init($url);

        if ($method === 'post')
        {
            curl_setopt($ch, CURLOPT_POST, 1);
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 9);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50); //timeout in seconds
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);

        if (!empty($result))
        {
            $result = json_decode($result, true);
        }
        else
        {
            $result = [];
        }

        return $result;
    }

    /**
     * Выполняет GET запрос на указанный адрес с указанными параметрами
     *
     * @param string $url
     * @param array $data
     * @param array $options
     * @return array
     */
    public function get(string $url, array $data = [], array $options = []): array
    {
        return $this->sendRequest('get', $url, $data, $options);
    }

    /**
     * Выполняет POST запрос на указанный адрес с указанными параметрами
     *
     * @param string $url
     * @param array $data
     * @param array $options
     * @return array
     */
    public function post(string $url, array $data = [], array $options = []): array
    {
        return $this->sendRequest('post', $url, $data, $options);
    }
}
