<?php

namespace App\Services\Dadata;

use App\Services\Request;
use Carbon\Carbon;

class CompanyInfo
{
    /**
     * Подготавливает информацию о компании
     *
     * @param array $companyInfo
     * @return array
     */
    private function adaptCompanyInfo(array $companyInfo): array
    {
        $result = [
            'name'       => isset($companyInfo['data']['name']['short_with_opf']) ? $companyInfo['data']['name']['short_with_opf'] : '',
            'ogrn'       => isset($companyInfo['data']['ogrn']) ? $companyInfo['data']['ogrn'] : '',
            'kpp'        => isset($companyInfo['data']['kpp']) ? $companyInfo['data']['kpp'] : '',
            'inn'        => isset($companyInfo['data']['inn']) ? $companyInfo['data']['inn'] : '',
            'okved'      => isset($companyInfo['data']['okved']) ? $companyInfo['data']['okved'] : '',
            'okved_type' => isset($companyInfo['data']['okved_type']) ? $companyInfo['data']['okved_type'] : '',
            'chief'      => isset($companyInfo['data']['management']['name']) ? $companyInfo['data']['management']['name'] : '',
            'post'      => isset($companyInfo['data']['management']['post']) ? $companyInfo['data']['management']['post'] : '',
            'address'    => isset($companyInfo['data']['address']['value']) ? $companyInfo['data']['address']['value'] : '',
            'opf'        => isset($companyInfo['data']['opf']['short']) ? $companyInfo['data']['opf']['short'] : '',
            'country'    => '',
            'phone'      => '',
            'registration_date' => isset($companyInfo['data']['state'])
                ? Carbon::createFromTimestampMs($companyInfo['data']['state']['registration_date'])->format('Y-m-d')
                : '',
        ];

        if ($result['opf'] === 'ИП')
        {
            $result['chief'] = $companyInfo['data']['name']['full'];
        }

        return $result;
    }

    /**
     * Подготавливает список компаний
     *
     * @param array $companies
     * @return array
     */
    private function adaptCompanyList(array $companies): array
    {
        $result = [];

        foreach ($companies as $company)
        {
            $result[] = [
                'name' => $company['data']['name']['short_with_opf'],
                'inn' => $company['data']['inn'],
            ];
        }

        return $result;
    }

    /**
     * Возвращает информацию о компании по ИНН
     *
     * @param string $inn
     * @return array
     */
    public function getCompanyInfo(string $inn): array
    {
        $url = env('DADATA_ADDR') . '/suggestions/api/4_1/rs/findById/party';
        $data = [
            'query' => $inn
        ];

        $request = new Request();
        $data = $request->post($url, $data, [
            'auth' => [
                'type' => 'Token',
                'value' => env('DADATA_TOKEN')
            ],
            'headers' => [
                'X-SECRET' => env('DADATA_SECRET')
            ]
        ]);

        if (count($data['suggestions']) > 0)
        {
            return $this->adaptCompanyInfo(current($data['suggestions']));
        }
        else
        {
            return [];
        }
    }

    /**
     * Возвращает список компаний подходящие под параметры запроса
     *
     * @param string $query
     * @return array
     */
    public function findCompanyInfo(string $query)
    {
        $url = env('DADATA_ADDR') . '/suggestions/api/4_1/rs/suggest/party';
        $data = [
            'query' => $query
        ];

        $request = new Request();
        $data = $request->post($url, $data, [
            'auth' => [
                'type' => 'Token',
                'value' => env('DADATA_TOKEN')
            ],
            'headers' => [
                'X-SECRET' => env('DADATA_SECRET')
            ]
        ]);

        if (count($data['suggestions']) > 0)
        {
            return $this->adaptCompanyList($data['suggestions']);
        }
        else
        {
            return [];
        }
    }
}
