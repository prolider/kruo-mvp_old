<?php

namespace App\Observers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Ticket;
use App\Models\User;

class CommentObserver
{
    public function created(Comment $comment)
    {
        $ticket = $comment->ticket;

        $users = [];

        switch ($comment->visibility) {
            case Comment::VISIBILITY_ALL:
                if ($user = $ticket->formResult->user) $users[] = $user->id;
            case Comment::VISIBILITY_OPERATOR:
                if ($minUser = $ticket->minuser) $users[] = $minUser->id;
                if ($operator = $ticket->operator) $users[] = $operator->id;
                break;
            case Comment::VISIBILITY_MODERATOR:
                return;
        }

        $users = array_unique($users);

        foreach ($users as $userId) {
            if ($userId == auth()->id()) continue;

            Notification::create([
                'name' => "Добавлен новый комментарий в заявке #{$ticket->id}",
                'description' => "",
                'notificable_type' => Ticket::class,
                'notificable_id' => $ticket->id,
                'status' => Notification::STATUS_NEW,
                'user_id' => $userId,
            ]);
        }
    }
}
