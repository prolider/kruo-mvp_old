<?php

namespace App\Observers;

use App\Models\Company;
use App\Models\Notification;
use App\Models\User;

class CompanyObserver
{
    public function updated(Company $company)
    {
        $user = $company->user;

        if ($company->wasChanged('statement_file')) {
            User::administrator()->get()->each(function (User $admin) use ($user, $company) {
                Notification::create([
                    'name' => __('notifications.company_status.verification_' . $company->getOriginal('verification_status', 'N')),
                    'description' => "Пользователь {$user->full_name} отправил заявление на подтверждение компании {$company->name}. Проверить",
                    'notificable_type' => Company::class,
                    'notificable_id' => $company->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $admin->id,
                ]);
            });
        }

        if ($company->wasChanged('verification_status')) {
            Notification::create([
                'name' => 'Подтверждение компании',
                'description' => __('notifications.user.company.verification_' . $company->verification_status, [
                    'company' => $company->name,
                    'company_inn' => $company->inn,
                ]),
                'notificable_type' => User::class,
                'notificable_id' => $user->id,
                'status' => Notification::STATUS_NEW,
                'user_id' => $user->id,
            ]);
        }
    }

    public function creating(Company $company)
    {
        $company->verification_status = $company->verification_status ?: Company::COMPANY_VERIFICATION_STATUS_NOT_VERIFIED;
    }

    public function deleted(Company $company)
    {
        Notification::where('notificable_type', Company::class)
                    ->where('notificable_id', $company->id)
                    ->delete();
    }
}
