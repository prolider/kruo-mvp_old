<?php

namespace App\Observers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Notification;
use App\Models\User;

class ArticleObserver
{
    public function created(Article $article)
    {
        $category = $article->category;

        if ($category && $category->tag === Category::TAG_PARTNERS) {
            User::administrator()->get()->each(function (User $admin) use ($article) {
                Notification::create([
                    'name' => "Создана новая запись #{$article->id} в разделе Бизнес кооперация",
                    'description' => "",
                    'notificable_type' => Article::class,
                    'notificable_id' => $article->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $admin->id,
                ]);
            });
        }
    }

    public function saved(Article $article)
    {
        if (!$article->slug) {
            $article->generateSlug();
        }
    }
}
