<?php

namespace App\Observers;

use App\Models\Discussion;
use App\Models\DiscussionMessage;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class DiscussionMessageObserver
{
    public function created(DiscussionMessage $message)
    {
        $user = User::find($message->user_owner_id);
        $discussion = $message->discussion;

        if ($discussion->user_recipient_id === $user->id) {
            $notifiedUserId = $discussion->user_owner_id;
        } else {
            $notifiedUserId = $discussion->user_recipient_id;
        }

        Notification::create([
            'name' => 'Новое сообщение в обращении',
            'description' => "Пользователь {$user->fullName} отправил новое сообщение",
            'notificable_type' => Discussion::class,
            'notificable_id' => $discussion->id,
            'status' => Notification::STATUS_NEW,
            'user_id' => $notifiedUserId,
        ]);
    }

    public function creating(DiscussionMessage $message)
    {
        $message->status_changed_date = now();
        $message->user_owner_id = auth()->user()->id;
    }

    public function updating(DiscussionMessage $message)
    {
        if ($message->isDirty('filenames')) {
            $diffs = array_diff($message->filenames, $message->getOriginal('filenames', []));

            foreach (json_decode($diffs) as $diff) {
                $this->service->delete('discussions', $diff->name);
            }
        }
    }

    public function deleted(DiscussionMessage $message)
    {
        $notification = Notification::where('notificable_type', DiscussionMessage::class)
                        ->where('notificable_id', $message->id)
                        ->first();

        $notification->delete();

        $files = json_decode($message->filenames);

        foreach ((array) $files as $file) {
            Storage::delete($file->download_link);
        }
    }
}
