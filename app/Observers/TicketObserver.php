<?php

namespace App\Observers;

use App\Models\Notification;
use App\Models\Ticket;
use Carbon\Carbon;

class TicketObserver
{
    /**
     * Handle the ticket "created" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function created(Ticket $ticket)
    {
        if (in_array($ticket->status, ['C', 'R']))
        {
            $ticket->fact_end_date = Carbon::now();
        }
    }

    /**
     * Handle the ticket "updated" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function updated(Ticket $ticket)
    {
        if (in_array($ticket->status, ['C', 'R']))
        {
            $ticket->fact_end_date = Carbon::now();
        }
    }

    /**
     * Handle the ticket "deleted" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function deleted(Ticket $ticket)
    {
        $notification = Notification::where('notificable_type', Ticket::class)
                        ->where('notificable_id', $ticket->id)
                        ->first();

        if ($notification) {
            $notification->delete();
        }
    }

    /**
     * Handle the ticket "restored" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function restored(Ticket $ticket)
    {
        //
    }

    /**
     * Handle the ticket "force deleted" event.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return void
     */
    public function forceDeleted(Ticket $ticket)
    {
        //
    }
}
