<?php

namespace App\Observers;

use App\Mail\Notified;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class NotificationObserver
{
    public function created(Notification $notification)
    {
        try {
            $user = User::find($notification->user_id);

            Mail::to($user)->send(new Notified($notification));
        } catch (\Exception $e) {

        }
    }
}
