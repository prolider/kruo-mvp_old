<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'         => 'required|string',
            'description'  => 'required|string',
            'text'         => 'required|string',
            'users'        => 'required|array',
            'users.*'      => 'required|int|exists:users,id',
            'files'        => 'sometimes|nullable|array',
            'files.*.file' => 'file|max:5120',
            'end_at'       => 'required||date',
            'start_at'     => 'required|date',
            'address'      => 'required|string',
            'invite_type'  => 'required',
            'event_type'   => 'required',
        ];
    }
}
