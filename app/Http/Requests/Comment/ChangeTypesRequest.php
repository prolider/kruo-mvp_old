<?php

namespace App\Http\Requests\Comment;

use App\Models\Comment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeTypesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'comments' => 'array',
            'comments.*' => 'int|exists:comments,id',
            'type' => [
                'required',
                'string',
                Rule::in(Comment::VISIBILITY_TYPES),
            ],
        ];
    }
}
