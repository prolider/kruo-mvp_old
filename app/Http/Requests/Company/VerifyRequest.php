<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class VerifyRequest extends FormRequest
{
    public function rules()
    {
        return [
            // 'statement' => 'required|file',
            'signature' => 'required|file',
        ];
    }
}
