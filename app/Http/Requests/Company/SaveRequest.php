<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'              => 'required|string',
            'inn'               => 'nullable|sometimes|string',
            'ogrn'              => 'nullable|sometimes|string',
            'kpp'               => 'nullable|sometimes|string',
            'address'           => 'nullable|sometimes|string',
            'registration_date' => 'nullable|sometimes|date',
            'management_post'   => 'nullable|sometimes|string',
            'management_name'   => 'nullable|sometimes|string',
            'site' => 'nullable|sometimes|string',
            'phone' => 'nullable|sometimes|string',
            'email' => 'nullable|sometimes|email',
        ];
    }
}
