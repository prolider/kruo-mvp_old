<?php

namespace App\Http\Requests\ExtraField;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class UpdateStepRequest extends SaveStepRequest
{
    public function rules()
    {
        return Arr::except(parent::rules(), 'extra_field_group_id');
    }
}
