<?php

namespace App\Http\Requests\ExtraField;

use Illuminate\Foundation\Http\FormRequest;

class AttachRequest extends FormRequest
{
    public function rules()
    {
        return [
            'extra_field_id' => 'exists:extra_fields,id',
            'value'          => 'required',
        ];
    }
}
