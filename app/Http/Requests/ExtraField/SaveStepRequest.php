<?php

namespace App\Http\Requests\ExtraField;

use Illuminate\Foundation\Http\FormRequest;

class SaveStepRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'string',
            'helper_message' => 'string',
            'number' => 'required|integer',
            'extra_field_group_id' => 'required|exists:extra_field_groups,id',
        ];
    }
}
