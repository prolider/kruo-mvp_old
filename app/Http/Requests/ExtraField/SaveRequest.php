<?php

namespace App\Http\Requests\ExtraField;

use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'type' => 'required',
            'is_required' => 'boolean',
            'is_filterable' => 'boolean',
            'description' => 'nullable|string',
            'read_only' => 'boolean',
            'placeholder' => 'nullable|string',
            'default_value' => 'nullable|string',
            'helper_message' => 'nullable|string',
            'container_class' => 'nullable|string',
            'is_hidden' => 'boolean',
            'extra_field_group_id' => 'exists:extra_field_groups,id',
            'extra_field_step_id' => 'exists:extra_field_steps,id',
            'options' => 'array',
        ];
    }
}
