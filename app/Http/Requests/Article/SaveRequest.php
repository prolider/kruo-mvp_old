<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'        => 'required|string|max:200',
            'content'     => 'string',
            'category_id' => 'sometimes|nullable|exists:categories,id',
            'extraFields' => 'array',
        ];
    }
}
