<?php

namespace App\Http\Requests\Discussion;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required_without:file|string',
            'files' => 'required_without:text|array',
            'files.*' => 'file|max:5120|mimes:png,jpeg,jpg,docx,doc,xlsx,xls,sig',
        ];
    }
}
