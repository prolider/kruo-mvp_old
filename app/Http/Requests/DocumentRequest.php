<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required' => 'Требуется название файла',
            'name.string' => 'Название файла должно быть строкой',
            'sort.required' => 'Требуется указать позицию',
            'sort.integer' => 'Позиция должна быть числом',
            'file.*' => 'Требуется файл',
            'file.max' => 'Размер файла не должен превышать 15 мб.'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'sort' => 'required|integer',
            'file' => 'required|file|max:15360'
        ];
    }
}
