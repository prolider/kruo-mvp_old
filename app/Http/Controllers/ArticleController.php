<?php

namespace App\Http\Controllers;

use App\Http\Requests\Article\SaveRequest;
use App\Models\{
    Article,
    Category
};
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $category = false;

        if ($categoryId = $request->category_id) {
            $category = Category::with('extraFieldGroups.fields')->find($categoryId);
            $categories = $category->categories;
        } else {
            $categories = Category::rootCategories()->locale(app()->getLocale())->tag(null)->get();
        }

        return view('measures.index', compact('categories', 'category'));
    }

    public function category(Category $category)
    {
        $category->load('categories');

        $categories = $category->categories;

        if ($categories->count() === 0) {
            $articles = $category->articles()->published()->paginate();

            return view('articles.articles-list', compact('articles', 'category'));
        }

        return view('measures.index', compact('categories', 'category'));
    }

    public function show(Article $article)
    {
        $article->load('category.extraFieldGroups.fields');

        $parentCategory = optional($article->category)->category;
        $articles = null;

        if ($article->category && $article->category->tag === Category::TAG_MEASURES) {
            $articles = $article->category->articles;
        } elseif ($article->category && $article->category->tag === Category::TAG_NEWS) {
            $articles = $article->category->articles()->orderBy('created_at', 'desc')->take(10)->get();
        }

        return view('articles.show', compact('article', 'parentCategory', 'articles'));
    }

    public function news()
    {
        $articles = Article::whereHas('category', function ($query) {
            return $query->locale(app()->getLocale())->tag(Category::TAG_NEWS);
        })->paginate();

        $category = Category::rootCategories()->locale(app()->getLocale())->tag(Category::TAG_NEWS)->first();

        return view('articles.news', compact('articles', 'category'));
    }

    public function create(Request $request)
    {
        if ($tag = $request->get('tag', false)) {
            $categories = Category::tag($tag)->where('category_id', '!=', 'null')->get();
        } else {
            $categories = Category::all();
        }

        if ($tag === Category::TAG_PARTNERS) {
            $category = Category::rootCategories()->tag(Category::TAG_PARTNERS)->with('extraFieldGroups.fields')->first();

            return view('articles.partners.create', compact('category'));
        }

        return view('articles.create', compact('categories'));
    }

    public function createPartner()
    {
        $categories = Category::tag(Category::TAG_PARTNERS)->get();

        return view('articles.create', compact('categories'));
    }

    public function save(SaveRequest $request)
    {
        $article = Article::create($request->validated());

        $extraFieldsData = $request->get('extraFields', []);
        $extraFieldsData = $extraFieldsData + $request->file('extraFields', []);

        $extraFieldsData = array_filter($extraFieldsData, function ($item) {
            return $item['value'] != null;
        });

        $article->extraFields()->sync($extraFieldsData);

        if ($article->category && $article->category->tag == Category::TAG_PARTNERS) {
            return redirect()->route('partners.success');
        }

        return redirect()->route('slug', $article->getSlugHierarchy());
    }

    public function measures()
    {
        $categories = Category::rootCategories()->locale(app()->getLocale())->tag(Category::TAG_MEASURES)->orderBy('sort', 'ASC')->get();

        return view('measures.index', compact('categories'));
    }
}