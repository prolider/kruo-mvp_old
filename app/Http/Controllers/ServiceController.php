<?php

namespace App\Http\Controllers;

use App\Mail\Notified;
use App\Models\Notification;
use App\Models\ServiceCategory;
use App\Models\Ticket;
use App\Models\Service;
use App\Models\FormResult;
use App\Http\Requests\CustomFormRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;

class ServiceController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $services = Service::query()
            ->when($request->has('category_id'), function ($query) use ($request) {
                $query->where('service_category_id', '=', $request->input('category_id'));
            })
            ->paginate();
        $categories = ServiceCategory::locale(app()->getLocale())->orderBy('order', 'ASC')->get();

        return view('services.index', compact('services', 'categories'));
    }

    /**
     * @param Service $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(ServiceCategory $serviceCategory, Service $service)
    {
        return view('services.show', compact('service', 'serviceCategory'));
    }

    /**
     * @param Service $service
     * @param Ticket $ticket
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForm(Service $service, Ticket $ticket = null)
    {
        $draftTicket = Ticket::query()
            ->whereHas('formResult', function ($query) use ($service) {
                $query->where('form_id', '=', $service->id)
                    ->where('user_id', '=', auth()->id());
            })
            ->whereIn('status', [Ticket::STATUS_DRAFT, Ticket::STATUS_CLIENT_WRITE, Ticket::STATUS_NEED_DATA])
            ->first();

        if (is_null($draftTicket)) {
            $formResult = FormResult::create([
                'form_id' => $service->form_id,
                'user_id' => auth()->id(),
                'result' => [],
            ]);

            $window = $service->window_id;

            $draftTicket = Ticket::create([
                'form_result_id' => $formResult->id,
                'window_id' => $window,
                'status' => $service->use_button === 'N' ? Ticket::STATUS_DONE : Ticket::STATUS_DRAFT,
                'archived' => 'N',
                'plan_end_date' => Carbon::now(),
                'attachments' => null,
            ]);

	        if ($service->use_button === 'N') {
	            return view('services.form', compact('service', 'ticket'));
            }

            if (is_null($ticket)) {
                return redirect()
                    ->route('services.form', [$service, $draftTicket]);
            }
        }

        if (is_null($ticket)) {
            $draftTicket = Ticket::query()
                ->whereHas('formResult', function ($query) use ($service) {
                    $query->where('form_id', '=', $service->id)
                        ->where('user_id', '=', auth()->id());
                })
                ->where('status', '=', Ticket::STATUS_DRAFT)
                ->first();

            if (!is_null($draftTicket)) {
                return redirect()
                    ->route('services.form', [$service, $draftTicket]);
            }
        }

        return view('services.form', compact('service', 'ticket'));
    }

    /**
     * @param CustomFormRequest $request
     * @param Service $service
     * @param Ticket $ticket
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeForm(CustomFormRequest $request, Service $service, Ticket $ticket)
    {
        $end = Carbon::now();

        if ($service->service_period_type === 'D')
            $end->addDays($service->service_period);
        elseif ($service->service_period_type === 'W')
            $end->addWeeks($service->service_period);
        else
            $end->addMonths($service->service_period);

        if (!$ticket->id) {
            $formResultData = $request->except('_token');

            $formResult = FormResult::create([
                'form_id' => $service->form_id,
                'user_id' => $request->user()->id,
                'result' => json_encode($formResultData),
            ]);

            Ticket::create([
                'form_result_id' => $formResult->id,
                'status' => Ticket::STATUS_NEW,
                'archived' => 'N',
                'plan_end_date' => $end,
                'attachments' => null,
            ]);
        } else {
            $formResultData = $request->except('_token');
            if ($request->files->count() > 0)
            {
                foreach ($request->files->keys() as $key)
                {
                    $uploadedFiles = [];
                    $files = $request->file($key);
                    if (!is_array($files))
                    {
                        $files = [$files];
                    }

                    foreach ($files as $file)
                    {
                        /** @var UploadedFile $file */
                        $stored = $file->store("tickets/{$ticket->id}", ['disk' => 'public']);
                        if ($stored !== false)
                        {
                            $uploadedFiles[] = [
                                'name' => $file->getClientOriginalName(),
                                'path' => $stored
                            ];
                        }
                    }

                    $formResultData[$key] = $uploadedFiles;
                }
            }

            $ticket->formResult->result = json_encode($formResultData);
            $ticket->formResult->save();

            $ticket->status = Ticket::STATUS_NEW;
            $ticket->save();
        }

        // Оповещение для пользователя
        Notification::create([
            'name' => 'Новая заявка',
            'description' => "Вами подана заявки. Статус Заявки №{$ticket->id}. \"" . __('ticket.status.' . $ticket->status) . "\"",
            'notificable_type' => Ticket::class,
            'notificable_id' => $ticket->id,
            'status' => Notification::STATUS_NEW,
            'user_id' => $ticket->formResult->user_id,
        ]);

        // Оповещение для оператора
        User::operator()->get()->each(function (User $user) use ($ticket) {
             Notification::create([
                'name' => 'Новая заявка',
                'description' => "Подана новая заявка. Статус Заявки №{$ticket->id}. \"" . __('ticket.status.' . $ticket->status) . "\"",
                'notificable_type' => Ticket::class,
                'notificable_id' => $ticket->id,
                'status' => Notification::STATUS_NEW,
                'user_id' => $user->id,
            ]);
        });

        $window = $ticket->window;

        if ($windowEmail = optional($window)->email) {
            $notification = new Notification();
            $notification->fill([
                'name' => 'Новая заявка',
                'description' => "Подана новая заявка. Статус Заявки №{$ticket->id}. \"" . __('ticket.status.' . $ticket->status) . "\"",
                'notificable_type' => Ticket::class,
                'notificable_id' => $ticket->id,
                'status' => Notification::STATUS_NEW,
            ]);

            try {
                Mail::to($windowEmail)->send(new Notified($notification));
            } catch (\Exception $e) {

            }

        }


        return redirect()
            ->route('ticket.index')
            ->with('formSend', 'success');
    }

    /**
     * Сохраняет черновик формы
     *
     * @param Request $request
     * @param Service $service
     * @param Ticket $ticket
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeDraft(Request $request, Service $service, Ticket $ticket)
    {
        if (!$ticket->id)
        {
            $formResult = FormResult::create([
                'form_id' => $service->form_id,
                'user_id' => $request->user()->id,
                'result' => json_encode($request->except('_token')),
            ]);

            Ticket::create([
                'form_result_id' => $formResult->id,
                'status' => Ticket::STATUS_DRAFT,
                'archived' => 'N',
                'attachments' => null,
            ]);
        }
        else
        {
            $ticket->formResult->result = json_encode($request->except('_token'));
            $ticket->formResult->save();
        }

        return response()->json(['status' => true]);
    }
}
