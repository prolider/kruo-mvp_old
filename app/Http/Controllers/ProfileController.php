<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Models\User;
use App\Services\FileService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function profile()
    {
        $user = auth()->user();
        $company = $user->company;

        return view('profile.index', compact('user', 'company'));
    }

    /**
     * Обновление профиля
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        foreach ($user->getFillable() as $field) {
            if ($file = $request->file($field, null)) {
                $this->service->delete('users', $user->{$field}, '/' . $user->id);
                $user->{$field} = $this->service->saveFile('users', $file, '/' . $user->id, true);
            } else if ($request->has($field)) {
                $user->{$field} = $request->input($field);
            }
        }

        $user->save();

        return redirect()
            ->back()
            ->with('profile_update', 'success');
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $validated = $request->validated();

        auth()->user()->update($validated);

        return redirect()
            ->back()
            ->with('password_success', 'success');
    }

    public function company()
    {
        $company = auth()->user()->company;

        return view('profile.company', compact('company'));
    }

    public function contacts()
    {
        $user = auth()->user();

        return view('profile.contacts', compact('user'));
    }

    public function agent()
    {
        $user = auth()->user();
        $company = $user->company;

        return view('profile.agent', compact('user', 'company'));
    }

    public function changePassword()
    {
        $user = auth()->user();

        return view('profile.password', compact('user'));
    }

    public function settings()
    {
        $user = auth()->user();

        return view('profile.settings', compact('user'));
    }

    public function registered()
    {
        return view('auth.registered');
    }

    public function resetted()
    {
        return view('auth.resetted');
    }
}
