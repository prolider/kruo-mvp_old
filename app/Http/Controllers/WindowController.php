<?php

namespace App\Http\Controllers;

use App\Models\Window;

class WindowController extends Controller
{
    public function index()
    {
        $windows = Window::all();

        return view('windows.index', compact('windows'));
    }

    public function show(Window $window)
    {
        $window->load(['users', 'categories']);

        return view('windows.show', compact('window'));
    }
}
