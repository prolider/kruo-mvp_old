<?php

namespace App\Http\Controllers;

use App\Models\ServiceCategory;
use App\Models\Ticket;
use App\Models\Service;
use App\Models\FormResult;
use App\Http\Requests\CustomFormRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ServiceCategoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(ServiceCategory $serviceCategory)
    {
        $serviceCategory->load('services');
        $categories = ServiceCategory::ordered()->get();

        return view('categories.detail', compact('serviceCategory', 'categories'));
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category()
    {
        $categories = ServiceCategory::with('services')->locale(app()->getLocale())->ordered()->get();

        return view('categories.index', compact('categories'));
    }
}
