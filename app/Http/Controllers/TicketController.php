<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Window;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();

        $ticketsQuery = Ticket::with('formResult.form.service');

        // где operator - он же менеджер КРУО/модератор
        // а minuser - оператор министерства
        if ($user->role->name === 'operator' || $user->role->name === 'admin') {
            $tickets = $ticketsQuery->paginate(10);
            return view('tickets.operator-lk', compact('tickets'));
        } elseif ($user->role->name === 'minuser') {
            if ($user->window) {
                $tickets = $ticketsQuery->where('window_id', $user->window->id)->paginate(10);
            } else {
                $tickets = null;
            }
            return view('tickets.minuser-lk', compact('tickets'));
        }

        $tickets = $ticketsQuery->whereHas('formResult', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })->paginate(10);

        return view('tickets.user-lk', compact('tickets'));
    }

    /**
     * @param Ticket $ticket
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Ticket $ticket)
    {
        $ticket->load(['formResult.form.service', 'comments.user']);

        /** @var User $user */
        $user = auth()->user();
        if ($user->role->name === 'operator' || $user->role->name === 'admin')
        {
            $response = view('tickets.operator-lk', ['tickets' => []]);
        }
        elseif($user->role->name === 'minuser')
        {
            $response = view('tickets.minuser-lk', ['tickets' => []]);
        }
        else
        {
            $response = view('tickets.detail', compact('ticket'));
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function window(Request $request)
    {
        if ($request->has('window_id'))
        {
            $window = Window::query()
                ->where('id', '=', $request->input('window_id'))
                ->first();

            if (!is_null($window))
            {
                session()->put('window', $window);
            }
        }

        return redirect()
            ->back();
    }
}
