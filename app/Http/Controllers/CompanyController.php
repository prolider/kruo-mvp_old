<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\InviteRequest;
use App\Http\Requests\Company\SaveRequest;
use App\Http\Requests\Company\RemoveInvitedRequest;
use App\Mail\Invited;
use App\Models\Company;
use App\Models\Notification;
use App\Models\User;
use App\Services\Dadata\CompanyInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Role;

class CompanyController extends Controller
{
    public function save(SaveRequest $request)
    {
        $user = auth()->user();

        $company = $user->company;

        if (!$company) {
            $company = new Company();
            $company->user_id = $user->id;
            $company->save();
        }

        if ($company->isVerified() || $company->isOnCheck()) {
            return back();
        }

        $company->update($request->validated());

        return back()->with('profile_update', 'success');
    }

    public function show(Company $company)
    {
        $company->load('user.role');

        $creator = $company->user;

        return view('companies.show', compact('company', 'creator'));
    }

    public function invite(Company $company)
    {
        $company->load('invitedUsers');

        return view('companies.invite', compact('company'));
    }

    public function removeInvited(RemoveInvitedRequest $request, Company $company)
    {
        $validated = $request->get('invited');
        $company->invitedUsers()->whereIn('id', $validated)->forceDelete();

        return back();
    }

    public function sendInvitation(InviteRequest $request, Company $company)
    {
        $validated = $request->validated();
        $password = Str::random(10);
        $role = Role::where('name', 'invited')->first();

        $invited = User::make(
            [
                'firstname' => $validated['surname'],
                'name' => $validated['name'],
                'email' => $validated['email'],
                'password' => Hash::make($password)
            ]
        );

        $invited->role()->associate($role)->inviterCompany()->associate($company);
        $invited->save();

        Mail::to($invited)->send(new Invited($invited, $password));

        return redirect()->route('companies.invite', $company->id)
        ->with(
            'invitation',
            [
                'text' => 'Приглашение было отправлено.',
                'class' => 'alert-success'
            ]
        );
    }
}
