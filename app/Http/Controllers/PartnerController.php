<?php

namespace App\Http\Controllers;

use App\Http\Requests\Article\SaveRequest;
use App\Models\{
    Article,
    Category
};

class PartnerController extends Controller
{
    public function index()
    {
        $tag = Category::TAG_PARTNERS;

        $partners = Article::published()->orderBy('created_at', 'desc')->whereHas('category', function ($query) use ($tag) {
            return $query->locale(app()->getLocale())->tag($tag);
        })->paginate(10);

        $category = Category::rootCategories()->tag(Category::TAG_PARTNERS)->first();

        return view('articles.partners', compact('partners', 'tag', 'category'));
    }

    public function show(Article $article)
    {
        $article->load('category.extraFieldGroups.fields');

        $parentCategory = optional($article->category)->category;

        return view('articles.partners.show', compact('article', 'parentCategory'));
    }

    public function create()
    {
        $category = Category::rootCategories()->tag(Category::TAG_PARTNERS)->first();

        return view('articles.partners.create', compact('category'));
    }

    public function save(SaveRequest $request)
    {
        $article = Article::create($request->validated());

        $extraFieldsData = $request->get('extraFields', []);
        $extraFieldsData = $extraFieldsData + $request->file('extraFields', []);

        $extraFieldsData = array_filter($extraFieldsData, function ($item) {
            return $item['value'] != null;
        });

        $article->extraFields()->sync($extraFieldsData);

        return redirect()->route('partners.success')->with('status', 'success!');
    }

    public function success()
    {
        return view('articles.partners.success');
        return response()->with('status', 'success');
    }
}
