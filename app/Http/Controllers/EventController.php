<?php

namespace App\Http\Controllers;

use App\Http\Requests\Event\{
    SaveRequest,
};
use App\Models\Event;
use App\Models\Notification;
use App\Models\User;
use App\Services\FileService;
use Illuminate\Support\Arr;

class EventController extends Controller
{
    protected $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $events = Event::forUser()->orderBy('start_at', 'desc')->get();

        return view('events.index', compact('events'));
    }

    public function create()
    {
        $userGroups = User::with('company')->user()->get()->groupBy('company.name');

        return view('events.create', compact('userGroups'));
    }

    public function save(SaveRequest $request)
    {
        $data = $request->validated();

        $event = Event::create($data);

        $files = [];

        if ($requestFiles = Arr::get($data, 'files', [])) {
            $files = collect($requestFiles)->pluck('file');

            $files = $this->service->saveFiles('events', $files);
        }

        $event->update([
            'author_id' => auth()->user()->id,
            'files' => $files,
        ]);

        $event->users()->attach($request->users);

        if ($event->invite_type == Event::INVITE_TYPE_PRIVATE) {
            $event->users->each(function (User $user) use ($event) {
                Notification::create([
                    'name' => "Вас пригласили на событие: " . $event->name,
                    'description' => $event->text,
                    'notificable_type' => Event::class,
                    'notificable_id' => $event->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $user->id,
                ]);
            });
        } else {
            // TODO: use chunk
            User::all()->each(function(User $user) use ($event) {
                Notification::create([
                    'name' => "Новое массовое событие: " . $event->name,
                    'description' => $event->text,
                    'notificable_type' => Event::class,
                    'notificable_id' => $event->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $user->id,
                ]);
            });
        }

        return redirect()->route('events.show', $event);
    }

    public function edit(Event $event)
    {
        $event->load('users');

        $userGroups = User::with('company')->user()->get()->groupBy('company.name');

        return view('events.edit', compact('event', 'userGroups'));
    }

    public function update(Event $event, SaveRequest $request)
    {
        $data = $request->validated();

        $event->update($data);

        $files = [];

        if ($requestFiles = Arr::get($data, 'files', [])) {
            $files = collect($requestFiles)->pluck('file');

            $files = $this->service->saveFiles('events', $files);
        }

        $event->update([
            'author_id' => auth()->user()->id,
            'files' => $files,
        ]);

        $event->users()->attach($request->users);

        return redirect()->route('events.show', $event);
    }

    public function show(Event $event)
    {
        return view('events.show', compact('event'));
    }
}
