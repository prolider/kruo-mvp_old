<?php

namespace App\Http\Controllers;

use App\Models\MainCompany;

class MainCompanyController extends Controller
{
    public function index()
    {
        $mainCompany = MainCompany::with('operators')->first();

        return view('main-companies.index', compact('mainCompany'));
    }
}
