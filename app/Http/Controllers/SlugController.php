<?php

namespace App\Http\Controllers;

use App\Models\{
    Article,
    Category
};
use Illuminate\Support\Arr;

class SlugController extends Controller
{
    protected const TAG_CONTROLLERS = [
        Category::TAG_MEASURES => [
            'controller' => ArticleController::class,
            'method' => 'measures',
        ],
        Category::TAG_PARTNERS => [
            'controller' => PartnerController::class,
            'method' => 'index',
        ],
        Category::TAG_NEWS => [
            'controller' => ArticleController::class,
            'method' => 'news',
        ],
    ];

    public function slugWithTag($tag, $slugs = '')
    {
        return $this->slug($slugs, $tag);
    }

    public function slugWithoutTag($slugs = '')
    {
        return $this->slug($slugs);
    }

    public function slug($slugs, $tag = false)
    {
        $slugs = explode('/', $slugs);

        $firstSlug = Arr::first($slugs);

        if (!$tag && optional(Category::where('slug', $firstSlug)->first())->slug != null) {
            abort(404);
        }

        $lastSlug = Arr::last($slugs);

        if ($category = Category::where('slug', $lastSlug)->first()) {
            return app(ArticleController::class)->category($category);
        } else if ($article = Article::where('slug', $lastSlug)->first()) {
            if ($tag === Category::TAG_PARTNERS) {
                return app(PartnerController::class)->show($article);
            }
            return app(ArticleController::class)->show($article);
        }

        if ($tag) {
            return $this->callController($tag);
        }

        return abort(404);
    }

    protected function callController($tag)
    {
        $methodInfo = Arr::get(self::TAG_CONTROLLERS, $tag);

        $controller = $methodInfo['controller'];
        $method = $methodInfo['method'];

        if (method_exists($controller, $method) && Category::rootCategories()->tag($tag)->count() == 0) {
            return app($controller)->$method();
        }

        return abort(404);
    }
}
