<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Notification;
use App\Models\ServiceCategory;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Builder;

class HomeController extends Controller
{
    public function index()
    {
        $mainNews = Article::published()->whereHas('category', function ($query) {
            return $query->tag(Category::TAG_NEWS);
        })->take(5)->get();

        $serviceCategories = ServiceCategory::all();

        return view('welcome', compact('mainNews', 'serviceCategories'));
    }

    public function messages()
    {
        $tickets = Ticket::query()
            ->whereHas('formResult', function (Builder $query) {
                return $query->where('user_id', '=', auth()->id());
            })
            ->whereHas('newComments')
            ->with(['formResult.form.service'])
            ->withCount('newComments')
            ->get();

        return view('messages.index', compact('tickets'));
    }

    public function notices()
    {
        $notifications = Notification::user()
            ->with('notificable')
            ->latest()
            ->get();
        return view('notices.index', compact('notifications'));
    }
}
