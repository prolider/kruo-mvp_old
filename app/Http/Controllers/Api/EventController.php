<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    public function deleteFile(Event $event, Request $request)
    {
        $file = $request->file;

        if (in_array($file, $event->files)) {
            Storage::disk('events')->delete($file);

            $files = array_filter($event->files, function($eventFile) use ($file) {
                return $eventFile != $file;
            });

            $event->update(compact('files'));
        }

        return response()->json();
    }
}
