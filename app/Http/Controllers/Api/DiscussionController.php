<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Discussion;
use Illuminate\Http\Request;

class DiscussionController extends Controller
{
    public function paginatedIndex(Request $request)
    {
        $user = auth()->user();

        $discussions = Discussion::withCount('messages');

        if (in_array($user->role->name, ['user', 'invited'])) {
            $discussions = $discussions->where('user_owner_id', $user->id);
        } else {
            $discussions = $discussions->where('user_recipient_id', $user->id);
        }

        $discussions = $discussions->paginate(
            $request->get('per_page', 50),
        );

        return response()->json($discussions);
    }
}
