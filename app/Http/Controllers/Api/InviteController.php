<?php

namespace App\Http\Controllers\Api;

use App\Models\Invite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviteController extends Controller
{
    /**
     * Возвращает список приглашений
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Invite::query();

        if ($request->has('date'))
        {
            $start = Carbon::createFromFormat('Y-m-d', $request->input('date'))->startOfDay();
            $end = Carbon::createFromFormat('Y-m-d', $request->input('date'))->endOfDay();
            $invites = $query->whereBetween('invite_time', [$start, $end])->get();
        }
        else
        {
            $invites = $query->paginate(12);
        }

        return response()->json(compact('invites'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function available(Request $request)
    {
        $start = Carbon::createFromFormat('Y-m-d', $request->input('date'))
            ->setTime(9, 0, 0, 0);
        $end = Carbon::createFromFormat('Y-m-d', $request->input('date'))
            ->setTime(18, 0, 0, 0);

        $invites = Invite::query()
            ->whereBetween('invite_time', [$start, $end])
            ->where('ticket_id', '<>', $request->input('ticket_id'))
            ->get()
            ->pluck('invite_time')
            ->map->format('H:i')
            ->toArray();

        $times = [];

        while ($end->diffInMinutes($start) > 0)
        {
            $times[$start->format('H') . ':00'][] = [
                'time' => $start->format('r'),
                'status' => !in_array($start->format('H:i'), $invites),
            ];
            $start->addMinutes(15);
        }

        return response()->json(['invites' => $times]);
    }
}
