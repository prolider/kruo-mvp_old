<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function search(Request $request)
    {
        $articles = Article::published()->filter($request)->paginate()->setPath($request->path);

        $view = view('articles.list', compact('articles'))->render();

        return response()->json(compact('view'));
    }

    public function getExtraFields(Category $category)
    {
        $category->load('extraFieldGroups.fields');

        $view = view('articles.extra-fields', compact('category'))->render();

        return response()->json(compact('view'));
    }
}
