<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Models\Invite;
use App\Models\Notification;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Window;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $ticketsQuery = Ticket::with('formResult.form.service', 'formResult.user.company', 'formResult.user.role', 'window', 'operator')->filter($request);

        if ($user->role->name === 'minuser') {
            if (!$user->window) {
                $tickets = null;

                return response()->json(compact('tickets'));
            }
            $ticketsQuery->where('window_id', $user->window->id);
        }

        if ($user->role->name === 'admin' || $user->role->name === 'minuser') {
            $tickets = $ticketsQuery
                ->where('tickets.status', '<>', Ticket::STATUS_DRAFT);
        } elseif ($user->role->name === 'operator') {
            $tickets = $ticketsQuery->where('tickets.status', '<>', Ticket::STATUS_DRAFT)
                ->where('operator_id', '=', $user->id);
        } else {
            $tickets = $ticketsQuery->whereHas('formResult', function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            });
        }

        $tickets = $tickets->paginate(10);

        return response()->json(compact('tickets'));
    }

    /**
     * @param Ticket $ticket
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Ticket $ticket)
    {
        $ticket->load([
            'formResult.form.service.documents',
            'comments' => function ($query) {
                if (auth()->user()->role->name === 'minuser') {
                    return $query->where('visibility', '!=', Comment::VISIBILITY_OPERATOR);
                }

                return $query;
            },
            'comments.user.company',
            'formResult.user.company',
            'formResult.user.role',
            'operator.role',
            'minuser.role',
            'invite',
            'window.users',
        ]);

        return response()->json(compact('ticket'));
    }

    /**
     * Создание комментария пользователем
     *
     * @param Ticket $ticket
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Ticket $ticket, Request $request)
    {
        $comment = [
            'ticket_id' => $ticket->id,
            'user_id' => auth()->id(),
            'status' => Comment::STATUS_NEW,
            'visibility' => $request->input('visibility', Comment::VISIBILITY_ALL),
            'text' => strip_tags($request->input('text'), '<p><br>'),
        ];

        if ($request->hasFile('attachment')) {
            $files = $request->file('attachment');
            $attachments = [];
            foreach ($files as $file)
            {
                $filePath = "comments/ticket/{$ticket->id}/";
                $path = $file->store($filePath, ['disk' => 'public']);
                $attachments[] = [
                    'path' => $path,
                    'name' => $file->getClientOriginalName()
                ];
            }

            $comment['attachment'] = $attachments;
        }

        Comment::create($comment);

        $ticket->load(['formResult.form.service.documents', 'comments.user', 'formResult.user']);

        $token = csrf_token();

        return response()->json(compact('ticket', 'token'));
    }

    public function changeStatus(Ticket $ticket, Request $request)
    {
        $invite = null;

        if ($request->input('status') === Ticket::STATUS_INVITE)
        {
            /** @var Invite $invite */
            $invite = Invite::query()
                ->where('ticket_id', '=', $ticket->id)
                ->first();

            if (is_null($invite))
            {
                Invite::create([
                    'window_id' => $request->input('invite.window'),
                    'user_id' => $ticket->formResult->user_id,
                    'operator_id' => auth()->id(),
                    'invite_time' => Carbon::createFromFormat('Y-m-d H:i', $request->input('invite.time')),
                    'ticket_id' => $ticket->id,
                ]);
            }
            else
            {
                $invite->invite_time = Carbon::createFromFormat('Y-m-d H:i', $request->input('invite.time'));
                $invite->window_id = $request->input('invite.window');
                $invite->save();
            }
        }

        if ($request->input('operator_id'))
        {
            $ticket->operator_id = $request->input('operator_id');
        }

        if ($request->input('status'))
        {
            $ticket->status = $request->input('status');
        }


        if ($request->input('window_id'))
        {
            $ticket->window_id = $request->input('window_id');
        }

        if ($request->input('minuser_id'))
        {
            $ticket->minuser_id = $request->input('minuser_id');
        }

        $ticket->save();
        $ticket->load(['formResult.form.service.documents', 'comments.user', 'formResult.user', 'operator.role', 'invite', 'minuser.role']);
        $token = csrf_token();

        $this->sendNotifications($ticket, $invite);

        if ($request->ajax())
        {
            return response()->json(compact('ticket', 'token'));
        }
        else
        {
            return redirect()->back();
        }
    }

    public function returnToManager(Ticket $ticket)
    {
        $operator = auth()->user();

        $ticket->update([
            'status' => Ticket::STATUS_NEW,
        ]);

        if ($window = optional(optional(optional($ticket->formResult)->form)->service)->window) {
            foreach ($window->users as $user) {
                Notification::create([
                    'status' => Notification::STATUS_NEW,
                    'name' => 'Возврат заявки',
                    'description' => "Оператор {$operator->full_name} {$window->name} вернул заявку №{$ticket->id}",
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'user_id' => $user->id,
                ]);
            }
        }

        $token = csrf_token();

        return response()->json(compact('ticket', 'token'));
    }

    public function sendInvite(Ticket $ticket, Request $request)
    {
        /** @var Invite $invite */
        $invite = Invite::where('ticket_id', '=', $ticket->id)->first();
        $operator = auth()->user();
        $investor = $ticket->formResult->user;

        if (is_null($invite)) {
            Invite::create([
                'user_id' => $investor->id,
                'operator_id' => $operator->id,
                'invite_time' => Carbon::parse($request->input('date')),
                'ticket_id' => $ticket->id,
            ]);

            $notificationDescription = "Оператор {$operator->full_name} отправил вам приглашение";
        } else {
            $notificationDescription = "Оператор {$operator->full_name} изменил время в приглашении по заявке";
            $invite->invite_time = Carbon::parse($request->input('date'));
            $invite->save();
        }

        Notification::create([
            'status' => Notification::STATUS_NEW,
            'name' => 'Приглашение',
            'description' => 'Заявителю отправлено приглашение',
            'notificable_type' => Ticket::class,
            'notificable_id' => $ticket->id,
            'user_id' => $operator->id,
        ]);

        Notification::create([
            'status' => Notification::STATUS_NEW,
            'name' => 'Приглашение',
            'description' => $notificationDescription,
            'notificable_type' => Ticket::class,
            'notificable_id' => $ticket->id,
            'user_id' => $investor->id,
        ]);

        return response()->json();
    }

    private function sendNotifications(Ticket $ticket, ?Invite $invite)
    {
        $statusChanged = $ticket->wasChanged('status');

        $inviteWasChanged =  optional($invite)->wasChanged('invite_time');
        $operator = auth()->user();

        if ($inviteWasChanged) {
            if ($invite->wasRecentlyCreated) {
                $notificationDescription = "Оператор {$operator->full_name} отправил вам приглашение";
            } else {
                $notificationDescription = "Оператор {$operator->full_name} изменил время в приглашении по заявке";
            }

            Notification::create([
                'status' => Notification::STATUS_NEW,
                'name' => 'Приглашение',
                'description' => $notificationDescription,
                'notificable_type' => Ticket::class,
                'notificable_id' => $ticket->id,
                'user_id' => $ticket->formResult->user_id,
            ]);
        }

        if ($statusChanged) {
            // Оповещение для пользователя
            if ($ticket->status === Ticket::STATUS_INVITE) {
                Notification::create([
                    'name' => 'Изменен статус заявки',
                    'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"" . __('ticket.status.' . $ticket->status) . '"',
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $ticket->formResult->user_id,
                ]);
            }

            if ($ticket->operator) {
                Notification::create([
                    'name' => 'Изменен статус заявки',
                    'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"" . __('ticket.status.' . $ticket->status) . '"',
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $ticket->operator->id,
                ]);
            }

            if ($ticket->minuser) {
                Notification::create([
                    'name' => 'Изменен статус заявки',
                    'description' => "Изменен статус Заявки №{$ticket->id}. Новый статус \"" . __('ticket.status.' . $ticket->status) . '"',
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $ticket->minuser->id,
                ]);
            }
        } else {
            // Если из в запросе есть
            $minuserChanged = $ticket->wasChanged('minuser_id');
            $windowChanged = $ticket->wasChanged('window_id');

            if ($minuserChanged || $windowChanged) {

                $windowsName = Window::select('name')->where('id', '=', $ticket->window_id)->first()->toArray();

                Notification::create([
                    'name' => 'Изменен статус заявки',
                    'description' => "Заявка №{$ticket->id}. Передана в министерство \"" .  $windowsName["name"] . "\" для дальнейшей обработки",
                    'notificable_type' => Ticket::class,
                    'notificable_id' => $ticket->id,
                    'status' => Notification::STATUS_NEW,
                    'user_id' => $ticket->formResult->user_id,
                ]);

                User::operator()->get()->each(function (User $user) use ($ticket, $windowsName) {
                    Notification::create([
                        'name' => 'Изменен статус заявки',
                        'description' => "Заявка №{$ticket->id}. Передана в министерство \"" .  $windowsName["name"] . "\" для дальнейшей обработки",
                        'notificable_type' => Ticket::class,
                        'notificable_id' => $ticket->id,
                        'status' => Notification::STATUS_NEW,
                        'user_id' => $user->id,
                    ]);
                });
            }

        }
    }
}
