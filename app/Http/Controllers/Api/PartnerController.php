<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function search(Request $request)
    {
        $partners = Article::published()->filter($request)->paginate(10)->withPath(route('partners.index'));

        $view = view('articles.partners.list', compact('partners'))->render();

        return response()->json(compact('view'));
    }
}
