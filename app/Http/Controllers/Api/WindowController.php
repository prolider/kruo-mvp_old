<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Window;

class WindowController extends Controller
{
    public function list()
    {
        $windows = Window::all();

        return response()->json(compact('windows'));
    }
}
