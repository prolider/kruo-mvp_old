<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ExtraFieldStep;
use App\Http\Requests\ExtraField\{AttachRequest, UpdateRequest, SaveRequest, SaveStepRequest, UpdateStepRequest};
use App\Models\ExtraField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExtraFieldController extends Controller
{
    public function get(ExtraField $field)
    {
        return response()->json($field);
    }

    public function save(SaveRequest $request)
    {
        $extraField = ExtraField::create($request->validated());

        $extraField->update([
            'details' => [
                'options' => $request->get('options', [])
            ],
        ]);

        return back();
    }

    public function saveStep(SaveStepRequest $request)
    {
        $step = ExtraFieldStep::create($request->validated());

        return back();
    }

    public function updateStep(UpdateStepRequest $request, ExtraFieldStep $step)
    {
        $step->update($request->validated());

        return back();
    }


    public function deleteStep(ExtraFieldStep $step)
    {
        $step->delete();

        return response()->json();
    }

    public function delete(ExtraField $field)
    {
        $field->delete();

        return response()->json();
    }

    public function update(ExtraField $field, UpdateRequest $request)
    {
        $data = $request->validated();

        $checkboxes = ['is_hidden', 'is_required', 'is_filterable', 'read_only'];

        foreach ($checkboxes as $checkboxName) {
            $data[$checkboxName] = $request->has($checkboxName) && $request->get($checkboxName, false);
        }

        $field->update($data);

        $field->update([
            'details' => [
                'options' => $request->get('options', [])
            ],
        ]);

        return response()->json();
    }

    public function attach(Article $article, AttachRequest $request)
    {
        $article->extraFields()->attach($request->extra_field_id, $request->validated());

        return back();
    }

    public function sync(Article $article, Request $request)
    {
        $extraFieldsData = $request->get('extraFields', []);

        foreach ($extraFieldsData as $key => $data) {
            if (array_key_exists('url', $data) && array_key_exists('name', $data)) {
                if ($data['url'] != null) {
                    $extraFieldsData[$key] = ["value" => json_encode($data)];
                } else {
                    $extraFieldsData[$key] = ["value" => json_encode(['name' => '', 'url' => ''])];
                }
            }
        }

        $extraFieldsData = array_filter($extraFieldsData, function ($item) {
            return $item['value'] != null;
        });

        $extraFieldsData = $extraFieldsData + $request->file('extraFields', []);

        $article->extraFields()->syncWithoutDetaching($extraFieldsData);

        return back();
    }

    public function detach(Article $article, ExtraField $field)
    {
        $article->extraFields()->detach($field);

        return response()->json();
    }


    public function deleteFile(Article $article, ExtraField $field, Request $request)
    {
        $articleField = $article->extraFields()->find($field);

        $files = $articleField->pivot->value;

        $files = array_filter($files, function($item) use ($request){
            if ($item->original_name == $request->fileName) {
                Storage::delete($item->download_link);

                return false;
            }

            return true;
        });

        $article->extraFields()->updateExistingPivot($field, ['value' => $files]);

        return response()->json();
    }
}
