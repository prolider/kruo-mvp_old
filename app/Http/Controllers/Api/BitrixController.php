<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BitrixSetting;
use App\Models\Company;
use Illuminate\Support\Facades\Http;

class BitrixController extends Controller
{
    public function leadAdd(Company $company)
    {
        $bitrixSetting = BitrixSetting::where('name', BitrixSetting::CRM_LEAD_ADD)->first();

        if (!$bitrixSetting) {
            $success = false;

            return response()->json(compact('success'));
        }

        $user = $company->user;

        $data = [
            'fields' => [
                'TITLE' => $company->name . "({$company->inn})",
                'NAME' => $user->name,
                'SECOND_NAME' => $user->patronymic,
                'LAST_NAME' => $user->firstname,
                'EMAIL' => [
                    [
                        'VALUE' => $user->email,
                        'VALUE_TYPE' => 'WORK'
                    ],
                ],
                'PHONE' => [
                    [
                        'VALUE' => $user->phone,
                        'VALUE_TYPE' => 'WORK'
                    ],
                ],
            ],
        ];

        $response = Http::post($bitrixSetting->url, $data);

        $company->update([
            'is_sent_to_bitrix' => true,
            'sent_to_bitrix_at' => now(),
        ]);

        $success = true;

        return response()->json(compact('success'));
    }
}
