<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentRequest;
use App\Models\Service;
use App\Models\ServiceDocument;
use App\Services\FileService;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /** @var FileService */
    protected $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }
    /**
     * Добавление документа
     *
     * @param Request $request
     * @param Service $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Service $service, DocumentRequest $request)
    {
        $filePath = $this->service->saveFile('public', $request->file('file'), ServiceDocument::DIRECTORY);

        $service_document = ServiceDocument::create([
            'service_id' => $service->id,
            'name' => $request->input('name'),
            'file' => $filePath,
            'sort' => $request->input('sort')
        ]);

        $service->load('documents');
        $table = view('vendor.voyager.services.partials.documents-table', [
            'documents' => $service->documents,
        ])->render();

        return response()->json(compact('service_document', 'table'));
    }

    /**
     * Обновление документа
     *
     * @param Service $service
     * @param ServiceDocument $serviceDocument
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Service $service, ServiceDocument $serviceDocument, Request $request)
    {
        if ($request->hasFile('file'))
        {
            $serviceDocument->file = $this->service->saveFile('public', $request->file('file'), ServiceDocument::DIRECTORY);
        }

        $serviceDocument->name = $request->input('name');
        $serviceDocument->sort = $request->input('sort');

        $serviceDocument->save();

        $service->load('documents');

        $table = view('vendor.voyager.services.partials.documents-table', [
            'documents' => $service->documents,
        ])->render();

        return response()->json(['service_document' => $serviceDocument, 'table' => $table]);

    }

    /**
     * Удаление документа
     *
     * @param Service $service
     * @param ServiceDocument $serviceDocument
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Service $service, ServiceDocument $serviceDocument)
    {
        $this->service->delete('public', $serviceDocument->file, ServiceDocument::DIRECTORY);
        $serviceDocument->delete();

        return response()->json();
    }
}
