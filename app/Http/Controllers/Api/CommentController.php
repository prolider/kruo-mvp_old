<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\ChangeTypesRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function changeStatus(Comment $comment, Request $request)
    {
        $status = $request->get('status');

        $comment->update(compact('status'));

        $token = csrf_token();

        return response()->json(compact('comment', 'token'));
    }

    public function changeTypes(ChangeTypesRequest $request)
    {
        Comment::whereIn('id', $request->comments)->update(['visibility' => $request->type]);

        return response()->json();
    }
}
