<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Notification;

class NotificationController extends Controller
{
    /**
     * @param Notification $notification
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Notification $notification)
    {
        $notification->delete();
        $token = csrf_token();
        return response()->json(compact('token'));
    }
}
