<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Window;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query();

        if ($request->input('role') === 'operator')
        {
            $operatorRoles = Role::query()
                ->where('name', '=', 'operator')
                ->get();

            if ($operatorRoles->count() > 0)
            {
                $query->whereIn('role_id', $operatorRoles->pluck('id')->toArray());
            }
            else
            {
                return response()->json(['users' => []]);
            }
        }

        $users = $query->get();

        // вернуть в компонент через /api/user?role=operator: window_id и id текущего аутентифицированного пользователя
        $windows = Window::query()->get();
        $user = Auth::id();

        return response()->json(compact('users', 'windows', 'user'));
    }

    public function getCurrentUser()
    {
        $user = auth()->user();

        return response()->json(compact('user'));
    }
}
