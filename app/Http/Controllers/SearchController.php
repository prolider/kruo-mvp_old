<?php

namespace App\Http\Controllers;

use App\Models\Dictionaries\TagDictionary;
use App\Services\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var \App\Services\SearchService
     */
    private $service;

    public function __construct(SearchService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $search = $request->get('search', '');
        $searchable = $request->get('categories', array_keys(TagDictionary::getSearchables()));

        $searchCategories = $this->service->searchBy($searchable, $search);
        $searchable = $this->service->getSearchableChecked($searchable);

        return view('search.index', compact('search', 'searchCategories', 'searchable'));
    }
}
