<?php

namespace App\Http\Controllers;

use App\Models\Company;

class ProjectController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $companies = Company::where('user_id', $user->id)->get();

        if ($user->role && $user->role->name === 'admin') {
            $companies = Company::all();
        }

        return view('projects.index', compact('companies'));
    }
}
