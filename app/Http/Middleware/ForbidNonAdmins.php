<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;

class ForbidNonAdmins
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if ($user && $user->role && in_array($user->role->name, ['operator', 'admin', 'leader'])) {
            return $next($request);
        }

        return redirect(RouteServiceProvider::HOME);
    }
}
