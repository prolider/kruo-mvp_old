<?php

namespace App\Http\View\Composers;

use App\Models\Discussion;
use App\Models\DiscussionMessage;
use Illuminate\View\View;

class UnreadMessagesComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (!auth()->user()) {
            return $view;
        }

        $user = auth()->user();

        $unreadMessages = Discussion::withCount(
            [
                'messages' => function ($query) use ($user) {
                        return $query->where('user_owner_id', '!=', $user->id)
                                    ->where('status', DiscussionMessage::STATUS_UNREAD);
                }
            ]
        );

        if (in_array($user->role->name, ['user', 'invited'])) {
            $unreadMessages = $unreadMessages->where('user_owner_id', $user->id)->get()->sum('messages_count');
        } else {
            $unreadMessages = $unreadMessages->where('user_recipient_id', $user->id)->get()->sum('messages_count');
        }

        return $view->with('message_count', $unreadMessages);
    }
}
