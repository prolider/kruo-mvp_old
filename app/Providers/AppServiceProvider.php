<?php

namespace App\Providers;

use App\Http\View\Composers\UnreadMessagesComposer;
use App\Models\Article;
use App\Models\Company;
use App\Models\Discussion;
use App\Models\DiscussionMessage;
use App\Models\Notification;
use App\Observers\ArticleObserver;
use App\Observers\CompanyObserver;
use App\Observers\ServiceObserver;
use App\Observers\TicketObserver;
use App\Observers\UserObserver;
use App\Models\Service;
use App\Models\Ticket;
use App\Models\User;
use App\Observers\DiscussionMessageObserver;
use App\Observers\DiscussionObserver;
use App\Observers\NotificationObserver;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        URL::forceScheme('https');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('auth', function () {
            return auth()->user() && !auth()->user()->two_factor_code;
        });

        View::composer('layouts.app', UnreadMessagesComposer::class);
    }
}
