<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class GateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('canCreateTickets', function (?User $user) {
            if (!$user || !$user->role || $user->role->name !== 'user') {
                return false;
            }

            if (!$user->company || !$user->company->isVerified()) {
                return false;
            }

            return true;
        });

        Gate::define('canCreateDiscussions', function (?User $user) {
            if (!$user || !$user->role || $user->role->name !== 'user') {
                return false;
            }
            if (!$user->company) {
                return false;
            }

            return true;
        });

        Gate::define('canCreatePartners', function (?User $user) {
            if (!$user) {
                return true;
            }

            if ($user->role && $user->role->name !== 'user') {
                return false;
            }

            return true;
        });

        Gate::define('canSeeMinusers', function (?User $user) {
            if (!$user || !in_array($user->role->name, ['admin', 'operator'])) {
                return false;
            }

            return true;
        });
    }
}
