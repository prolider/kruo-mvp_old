<?php

namespace App\Providers;

use App\Models\{
    Article,
    Company,
    Discussion,
    DiscussionMessage,
    Notification,
    Service,
    Ticket,
    User,
    Comment
};
use App\Observers\{
    ArticleObserver,
    CompanyObserver,
    DiscussionMessageObserver,
    DiscussionObserver,
    NotificationObserver,
    ServiceObserver,
    CommentObserver,
    TicketObserver,
    UserObserver
};
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Service::observe(ServiceObserver::class);
        Ticket::observe(TicketObserver::class);
        User::observe(UserObserver::class);
        Company::observe(CompanyObserver::class);
        Notification::observe(NotificationObserver::class);
        Discussion::observe(DiscussionObserver::class);
        DiscussionMessage::observe(DiscussionMessageObserver::class);
        Article::observe(ArticleObserver::class);
        Comment::observe(CommentObserver::class);
    }
}
