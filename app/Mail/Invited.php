<?php

namespace App\Mail;

use App\Models\Company;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Invited extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var App\Models\User
     */
    protected $user;
    public $subject = 'Приглашение в "Личный кабинет инвестора"';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $password)
    {
        $user->load('inviterCompany');

        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $password = $this->password;

        return $this->markdown('email.invited', compact('user', 'password'));
    }
}
