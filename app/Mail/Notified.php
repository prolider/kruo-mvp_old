<?php

namespace App\Mail;

use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Notified extends Mailable
{
    use Queueable, SerializesModels;

    protected $notification;
    public $subject = 'Новое уведомление на портале "Личный кабинет инвестора"';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $notification = $this->notification;

        return $this->markdown('email.notification', compact('notification'));
    }
}
