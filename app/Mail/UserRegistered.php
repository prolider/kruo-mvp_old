<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    public $subject = 'Регистрация на портале "Личный кабинет инвестора"';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $logoData = json_decode(setting('site.logo'));
        $logo = $logoData[0]->download_link ?? asset('theme/assets/media/logos/logo-1.svg');

        return $this->markdown('email.registered', compact('user', 'logo'));
    }
}
