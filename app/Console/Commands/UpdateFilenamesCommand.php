<?php

namespace App\Console\Commands;

use App\Models\ServiceDocument;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class UpdateFilenamesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voyager:documents-fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix filenames for ServiceDocument';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $documents = ServiceDocument::all();
        $fixedRows = 0;

        $this->info("Fixing service documents' filenames");

        foreach ($documents as $document) {
            $document->file = Str::replaceArray(ServiceDocument::DIRECTORY, [''], $document->file);

            if ($document->getOriginal('file') !== $document->file) {
                $fixedRows++;
            }

            $document->save();
        }

        $this->info("Fixed rows: " . $fixedRows);

        return 0;
    }
}
