<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\ExtraField;
use Illuminate\Console\Command;

class ChangeArticleLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:article:links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $articles = Article::with('extraFields')->get();

        foreach ($articles as $article)
        {
            foreach ($article->extraFields->where('type', ExtraField::TYPE_URL) as $linkExtraField)
            {
                if (json_decode($linkExtraField->pivot->value) == null){
                    $linkExtraField->pivot->value = json_encode(['name' => null, 'url' => $linkExtraField->pivot->value]);
                    $linkExtraField->pivot->save();
                }
            }
        }

        return 0;
    }
}
