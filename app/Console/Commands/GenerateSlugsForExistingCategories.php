<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;

class GenerateSlugsForExistingCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:slugs:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate slugs for existing categories';

    public function handle()
    {
        $categories = Category::where('slug', null)->get();

        foreach($categories as $category) {
            $category->update(['slug' => '']);
        }
    }
}
