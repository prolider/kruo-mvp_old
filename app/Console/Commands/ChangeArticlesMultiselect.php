<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\ExtraField;
use Illuminate\Console\Command;

class ChangeArticlesMultiselect extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:article:multiselect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $articles = Article::with('extraFields')->get();

        foreach ($articles as $article)
        {
            foreach ($article->extraFields->where('type', ExtraField::TYPE_MULTISELECT) as $linkExtraField)
            {
                $data = json_decode($linkExtraField->pivot->value);
                $linkExtraField->pivot->value = json_encode($data,JSON_UNESCAPED_UNICODE);
                $linkExtraField->pivot->save();
            }
        }

        return 0;
    }
}
