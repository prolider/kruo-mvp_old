<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class GenerateSlugsForExistingArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:slugs:articles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate slugs for existing articles';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $articles = Article::where('slug', null)->get();

        foreach($articles as $article) {
            $article->update(['slug' => '']);
        }
    }
}
