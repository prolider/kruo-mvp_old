<?php

namespace App\Policies;

use App\Models\{
    Article,
    User
};
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Article $article)
    {
        //TODO
    }

    public function create()
    {
        //TODO
    }
}
