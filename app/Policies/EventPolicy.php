<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;

class EventPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function canEditEvent(User $user, Event $event)
    {
        if ($user->id !== $event->author_id && optional($user->role)->name !== 'admin') {
            return $this->deny('', 403);
        }

        return $this->allow();
    }

    public function canShowEvent(User $user, Event $event)
    {
        if (optional($user->role)->name === 'admin' || $user->id === $event->author_id) {
            return $this->allow();
        }

        if ($event->invite_type === Event::INVITE_TYPE_PRIVATE && !$event->users->find($user->id)) {
            return $this->deny('', 403);
        }

        return $this->allow();
    }

    public function canCreateEvents(User $user)
    {
        if (!in_array(optional($user->role)->name, ['admin', 'operator'])) {
            return $this->deny('', 403);
        }

        return $this->allow();
    }
}
