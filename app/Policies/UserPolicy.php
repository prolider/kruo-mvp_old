<?php

namespace App\Policies;

use App\Models\Category;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy extends \TCG\Voyager\Policies\UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAppeals()
    {
        $user = auth()->user();

        if (!$user || $user->role->name === 'invited') {
            return $this->deny('Зайдите за инвестора или зарегистрируйтесь', 401);
        }

        if (!$user->company && $user->role->name === 'user') {
            return $this->deny('Персональный менеджер не указан', 419);
        }

        return $this->allow(null, 200);
    }

    public function createUserRelated(User $user)
    {
        if (!$user->role || $user->role->name !== 'user') {
            return $this->deny(null, 403);
        }

        return $this->allow(null, 200);
    }

    public function createBusinessPartner(?User $user)
    {
        if ($user && (!$user->role || in_array($user->role->name, ['admin', 'minuser', 'operator']) && request('tag') === Category::TAG_PARTNERS)) {
            return $this->deny(null, 403);
        }

        return $this->allow(null, 200);
    }
}
