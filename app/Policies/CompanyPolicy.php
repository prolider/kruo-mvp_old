<?php

namespace App\Policies;

use App\Models\Company;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;

class CompanyPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function see(User $user, Company $company)
    {
        $user = optional($user);

        if (optional($user->role)->name === 'admin') {
            return $this->allow();
        }

        return $this->deny();
    }

}
