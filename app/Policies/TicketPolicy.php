<?php

namespace App\Policies;

use App\Models\Category;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use TCG\Voyager\Policies\BasePolicy;

class TicketPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public function viewTicket(User $user, Ticket $ticket)
    {
        if (($user->role->name == 'admin') ||
            ($user->window_id === $ticket->window_id) ||
            ($user->id === $ticket->formResult->user_id) ||
            ($user->role->name == 'operator' && $ticket->operator_id === $user->id) ||
            ($user->role->name == 'invited' && $user->inviterCompany && $user->inviterCompany->id === $ticket->formResult->user->company->id)
        ) {
            return $this->allow(null, 200);
        }

        return $this->deny(null, 403);
    }
}
