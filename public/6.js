(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/list.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/minuser-lk/tickets/list.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _paginate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../paginate */ "./resources/js/components/paginate.vue");
/* harmony import */ var _services_statuses_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/statuses.js */ "./resources/js/services/statuses.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ticket.index',
  components: {
    Paginate: _paginate__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      user: [],
      operators: [],
      tickets: {
        data: [],
        page: 0,
        pages: 0,
        windows: [],
        users: []
      },
      filter: {
        operator: '',
        status: '',
        start_date: '',
        end_date: ''
      },
      sort: {
        id: '',
        name: '',
        date: '',
        status: '',
        manager: '',
        window: '',
        company: ''
      }
    };
  },
  methods: {
    ruDateFormat: function ruDateFormat(date) {
      return date ? moment__WEBPACK_IMPORTED_MODULE_0___default()(date).format("DD.MM.YYYY") : '';
    },
    ruStatus: function ruStatus(status) {
      return _services_statuses_js__WEBPACK_IMPORTED_MODULE_2__["statuses"][status];
    },
    ruStatusStyle: function ruStatusStyle(status) {
      return Object(_services_statuses_js__WEBPACK_IMPORTED_MODULE_2__["statusesStyles"])(status);
    },
    changePage: function changePage(val) {
      var _this = this;

      var params = this.getParams();
      $.ajax("/api/ticket?page=".concat(val, "&").concat(params.length > 0 ? params.join('&') : ''), {
        success: function success(_ref) {
          var tickets = _ref.tickets;
          _this.tickets.data = tickets.data;
          _this.tickets.page = tickets.current_page;
          _this.tickets.pages = tickets.last_page;
        }
      });
    },
    setInWorkStatus: function setInWorkStatus(t) {
      var _this2 = this;

      $.ajax({
        url: "/api/ticket/".concat(t.id, "/status"),
        data: {
          operator_id: this.user.id,
          status: 'W',
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        success: function success(_ref2) {
          var ticket = _ref2.ticket,
              token = _ref2.token;
          $('meta[name="csrf-token"]').attr('content', token);
          t.status = ticket.status;
          t.operator_id = ticket.operator_id;

          _this2.$nextTick(function () {
            return _this2.$forceUpdate();
          });

          _this2.$flash_success('Параметры заявки успешно обновлены');
        }
      });
    },
    returnToManager: function returnToManager(t) {
      var _this3 = this;

      $.ajax({
        url: "/api/ticket/".concat(t.id, "/return"),
        data: {
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        success: function success(_ref3) {
          var ticket = _ref3.ticket,
              token = _ref3.token;
          $('meta[name="csrf-token"]').attr('content', token);
          t.status = ticket.status;
          t.operator_id = ticket.operator_id;

          _this3.$nextTick(function () {
            return _this3.$forceUpdate();
          });

          _this3.$flash_success('Заявка успешно возвращена');
        }
      });
    },
    getParams: function getParams() {
      var params = [];

      if (this.filter.search) {
        params.push("search=".concat(this.filter.search));
      }

      if (this.filter.status) {
        params.push("status=".concat(this.filter.status));
      }

      if (this.filter.window) {
        params.push("window=".concat(this.filter.window));
      }

      if (this.filter.operator) {
        params.push("operator=".concat(this.filter.operator));
      }

      if (this.filter.start_date) {
        params.push("start_date=".concat(this.filter.start_date));
      }

      if (this.filter.end_date) {
        params.push("end_date=".concat(this.filter.end_date));
      }

      if (this.sort.type) {
        params.push("".concat(this.sort.field, "=").concat(this.sort.type));
      }

      return params;
    },
    sortField: function sortField(field, event) {
      if (this.sort.field != field) this.sort.type = '';
      this.sort.type = this.guessSortingType(field, event);
      this.sort.field = field;
      $(event.currentTarget).find('i').remove();
      $(event.currentTarget).attr('data-sort', this.sort.type);

      if (this.sort.type == 'asc') {
        $(event.currentTarget).find('span').append('<i class="flaticon2-arrow-up"></i>');
      } else {
        $(event.currentTarget).find('span').append('<i class="flaticon2-arrow-down"></i>');
      }

      this.loadTickets();
    },
    guessSortingType: function guessSortingType(field, event) {
      $('.datatable-cell').removeClass('datatable-cell-sorted');
      $('.datatable-cell').find('i').remove();
      $('.datatable-cell').removeAttr('data-sort');
      event.currentTarget.classList.add('datatable-cell-sorted');

      if (this.sort.type === 'desc') {
        return 'asc';
      }

      return 'desc';
    },
    loadTickets: function loadTickets() {
      var _this4 = this;

      var params = this.getParams();
      $.ajax("/api/ticket".concat(params.length > 0 ? '?' + params.join('&') : ''), {
        success: function success(_ref4) {
          var tickets = _ref4.tickets;

          if (tickets === null) {
            _this4.tickets.data = [];
            _this4.tickets.page = 1;
            _this4.tickets.pages = 1;
            return;
          }

          _this4.tickets.data = tickets.data;
          _this4.tickets.page = tickets.current_page;
          _this4.tickets.pages = tickets.last_page;
        }
      });
    }
  },
  computed: {
    statuses: function statuses() {
      return _services_statuses_js__WEBPACK_IMPORTED_MODULE_2__["statuses"];
    }
  },
  created: function created() {
    var _this5 = this;

    this.loadTickets();
    $.ajax("/api/user?role=operator", {
      success: function success(_ref5) {
        var users = _ref5.users;
        _this5.operators = users;
      }
    });
    $.ajax("/api/user/current", {
      success: function success(_ref6) {
        var user = _ref6.user;
        _this5.user = user;
      }
    });
    var datatable = $('#kt_datatable').DataTable();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/list.vue?vue&type=template&id=f64fddaa&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/minuser-lk/tickets/list.vue?vue&type=template&id=f64fddaa& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "card card-custom" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("div", { staticClass: "mb-7" }, [
            _c("div", { staticClass: "row align-items-center" }, [
              _c(
                "div",
                {
                  staticClass: "row",
                  staticStyle: {
                    "margin-bottom": "10px",
                    "align-items": "center"
                  }
                },
                [
                  _c("div", { staticClass: "col-md-3" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Оператор")]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.filter.operator,
                              expression: "filter.operator"
                            }
                          ],
                          staticClass: "form-control",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.filter,
                                "operator",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        [
                          _c("option", { attrs: { value: "" } }, [
                            _vm._v("Все")
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.operators, function(operator) {
                            return _c(
                              "option",
                              { domProps: { value: operator.id } },
                              [
                                _vm._v(
                                  _vm._s(
                                    (
                                      (operator.first_name || "") +
                                      " " +
                                      (operator.name || "") +
                                      " " +
                                      (operator.patronymic || "")
                                    ).trim()
                                  )
                                )
                              ]
                            )
                          })
                        ],
                        2
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-3" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Статус")]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.filter.status,
                              expression: "filter.status"
                            }
                          ],
                          staticClass: "form-control",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.filter,
                                "status",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        [
                          _c("option", { attrs: { value: "" } }, [
                            _vm._v("Все")
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.statuses, function(name, val) {
                            return _c("option", { domProps: { value: val } }, [
                              _vm._v(_vm._s(name))
                            ])
                          })
                        ],
                        2
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Дата завершения (c, до)")]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-md-6" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.filter.start_date,
                                expression: "filter.start_date"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: { type: "date", placeholder: "Дата начала" },
                            domProps: { value: _vm.filter.start_date },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.filter,
                                  "start_date",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-md-6" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.filter.end_date,
                                expression: "filter.end_date"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "date",
                              placeholder: "Дата завершения"
                            },
                            domProps: { value: _vm.filter.end_date },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.filter,
                                  "end_date",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-2" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary",
                        on: { click: _vm.loadTickets }
                      },
                      [_vm._v("Применить")]
                    )
                  ])
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dataTables_wrapper dt-bootstrap4 no-footer",
              attrs: { id: "kt_datatable_wrapper" }
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "datatable datatable-bordered datatable-head-custom table-hover datatable-default datatable-primary datatable-loaded",
                  attrs: { id: "kt_datatable" }
                },
                [
                  _c("table", { staticClass: "dataTable datatable-table" }, [
                    _c("thead", { staticClass: "datatable-head" }, [
                      _c(
                        "tr",
                        {
                          staticClass: "datatable-row",
                          staticStyle: { left: "0px" }
                        },
                        [
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: { "data-field": "OrderID" },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("id_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "50px" } }, [
                                _vm._v("№")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: {
                                "data-field": "CompanyName",
                                "data-sort": "desc"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("name_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "250px" } }, [
                                _vm._v(
                                  "\n                          Наименование услуги\n                      "
                                )
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: { "data-field": "ShipDate" },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("date_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "80px" } }, [
                                _vm._v("Дата")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: { "data-field": "Status" },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("status_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "150px" } }, [
                                _vm._v("Статус")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: { "data-field": "Minuser" },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("manager_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "150px" } }, [
                                _vm._v("Менеджер корпорации")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: { "data-field": "Window" },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("window_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "150px" } }, [
                                _vm._v("Министерство")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            {
                              staticClass: "datatable-cell datatable-cell-sort",
                              attrs: { "data-field": "Window" },
                              on: {
                                click: function($event) {
                                  return _vm.sortField("company_s", $event)
                                }
                              }
                            },
                            [
                              _c("span", { staticStyle: { width: "150px" } }, [
                                _vm._v("Заявитель")
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _vm._m(1)
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _vm.tickets.data.length > 0
                      ? _c(
                          "tbody",
                          { staticClass: "datatable-body" },
                          _vm._l(_vm.tickets.data, function(ticket) {
                            return _c(
                              "tr",
                              { key: ticket.id, staticClass: "datatable-row" },
                              [
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "50px" },
                                    attrs: { "data-field": "OrderID" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "50px" } },
                                      [_vm._v(_vm._s(ticket.id))]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "250px" },
                                    attrs: { "data-field": "CompanyName" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "250px" } },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "ticket.show",
                                                params: { id: ticket.id }
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                ticket.form_result.form.service
                                                  .name
                                              )
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "80px" },
                                    attrs: { "data-field": "ShipDate" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "80px" } },
                                      [
                                        _vm._v(
                                          _vm._s(
                                            _vm.ruDateFormat(ticket.created_at)
                                          )
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "120px" },
                                    attrs: { "data-field": "Status" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "120px" } },
                                      [
                                        _c(
                                          "span",
                                          {
                                            staticClass:
                                              "label font-weight-bold label-lg label-inline",
                                            class: _vm.ruStatusStyle(
                                              ticket.status
                                            ),
                                            staticStyle: { width: "120px" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          " +
                                                _vm._s(
                                                  _vm.ruStatus(ticket.status)
                                                ) +
                                                "\n                        "
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "150px" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "150px" } },
                                      [
                                        ticket.operator
                                          ? _c(
                                              "div",
                                              {
                                                staticStyle: { width: "150px" }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    ticket.operator.firstname
                                                  ) +
                                                    " " +
                                                    _vm._s(ticket.operator.name)
                                                )
                                              ]
                                            )
                                          : _c(
                                              "div",
                                              {
                                                staticStyle: { width: "80px" }
                                              },
                                              [_vm._v("Оператор не назначен")]
                                            )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "150px" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "150px" } },
                                      [
                                        ticket.window
                                          ? _c(
                                              "div",
                                              {
                                                staticStyle: { width: "150px" }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                          " +
                                                    _vm._s(ticket.window.name) +
                                                    "\n                        "
                                                )
                                              ]
                                            )
                                          : _c(
                                              "div",
                                              {
                                                staticStyle: { width: "150px" }
                                              },
                                              [
                                                _vm._v(
                                                  "Министерство не назначено"
                                                )
                                              ]
                                            )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "150px" }
                                  },
                                  [
                                    _c(
                                      "span",
                                      { staticStyle: { width: "150px" } },
                                      [
                                        ticket.form_result.user.company
                                          ? _c(
                                              "div",
                                              {
                                                staticStyle: { width: "150px" }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                          " +
                                                    _vm._s(
                                                      ticket.form_result.user
                                                        .company.name
                                                    ) +
                                                    "\n                        "
                                                )
                                              ]
                                            )
                                          : _c(
                                              "div",
                                              {
                                                staticStyle: { width: "150px" }
                                              },
                                              [_vm._v("Заявитель не указан")]
                                            )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  {
                                    staticClass: "datatable-cell",
                                    staticStyle: { width: "30px" },
                                    attrs: {
                                      "data-field": "Actions",
                                      "data-autohide-disabled": "false"
                                    }
                                  },
                                  [
                                    _c(
                                      "span",
                                      {
                                        staticStyle: {
                                          overflow: "visible",
                                          position: "relative",
                                          width: "30px"
                                        }
                                      },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "dropdown dropdown-inline"
                                          },
                                          [
                                            _c(
                                              "a",
                                              {
                                                staticClass:
                                                  "btn btn-sm btn-light btn-text-primary btn-icon mr-2",
                                                attrs: {
                                                  "data-toggle": "dropdown"
                                                }
                                              },
                                              [
                                                _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "svg-icon svg-icon-md"
                                                  },
                                                  [
                                                    _c(
                                                      "svg",
                                                      {
                                                        attrs: {
                                                          xmlns:
                                                            "http://www.w3.org/2000/svg",
                                                          "xmlns:xlink":
                                                            "http://www.w3.org/1999/xlink",
                                                          width: "24px",
                                                          height: "24px",
                                                          viewBox: "0 0 24 24",
                                                          version: "1.1"
                                                        }
                                                      },
                                                      [
                                                        _c(
                                                          "g",
                                                          {
                                                            attrs: {
                                                              stroke: "none",
                                                              "stroke-width":
                                                                "1",
                                                              fill: "none",
                                                              "fill-rule":
                                                                "evenodd"
                                                            }
                                                          },
                                                          [
                                                            _c("rect", {
                                                              attrs: {
                                                                x: "0",
                                                                y: "0",
                                                                width: "24",
                                                                height: "24"
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c("circle", {
                                                              attrs: {
                                                                fill: "#000000",
                                                                cx: "12",
                                                                cy: "5",
                                                                r: "2"
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c("circle", {
                                                              attrs: {
                                                                fill: "#000000",
                                                                cx: "12",
                                                                cy: "12",
                                                                r: "2"
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c("circle", {
                                                              attrs: {
                                                                fill: "#000000",
                                                                cx: "12",
                                                                cy: "19",
                                                                r: "2"
                                                              }
                                                            })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "dropdown-menu dropdown-menu-left"
                                              },
                                              [
                                                ticket.status === "N"
                                                  ? _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "dropdown-item",
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.setInWorkStatus(
                                                              ticket
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Взять в работу")]
                                                    )
                                                  : _vm._e(),
                                                _vm._v(" "),
                                                ticket.operator_id != null
                                                  ? _c(
                                                      "button",
                                                      {
                                                        staticClass:
                                                          "dropdown-item",
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.returnToManager(
                                                              ticket
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          "Вернуть менеджеру"
                                                        )
                                                      ]
                                                    )
                                                  : _vm._e()
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ]
                            )
                          }),
                          0
                        )
                      : _c("tbody", [_vm._m(2)])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "datatable-pager datatable-paging-loaded mt-3" },
                [
                  _c("paginate", {
                    attrs: {
                      value: _vm.tickets.page,
                      "page-count": _vm.tickets.pages,
                      "margin-pages": 2,
                      "page-range": 5,
                      "no-li-surround": true,
                      "first-last-button": false,
                      "hide-prev-next": false,
                      "container-class": "pagination",
                      "active-class": "btn-hover-primary active",
                      "prev-icon-class": "ki ki-bold-arrow-back icon-xs",
                      "next-icon-class": "ki ki-bold-arrow-next icon-xs",
                      "prev-text": null,
                      "next-text": null,
                      "prev-class": "page-item",
                      "prev-link-class":
                        "btn btn-icon btn-sm border-0 btn-light mr-2 my-1",
                      "page-class": "page-item",
                      "page-link-class":
                        "btn btn-icon btn-sm border-0 btn-light mr-2 my-1",
                      "next-class": "page-item",
                      "next-link-class":
                        "btn btn-icon btn-sm border-0 btn-light mr-2 my-1"
                    },
                    on: { input: _vm.changePage }
                  })
                ],
                1
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row mt-4" }, [
      _c("div", { staticClass: "col-md-10" }, [
        _c("h1", { staticClass: "mb-4" }, [_vm._v("Заявки")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "th",
      {
        staticClass: "datatable-cell datatable-cell-sort",
        attrs: { "data-field": "Action", "data-autohide-disabled": "false" }
      },
      [_c("span", { staticStyle: { width: "30px" } })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", { staticClass: "text-center", attrs: { colspan: "7" } }, [
        _vm._v("Пока нет заявок")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/list.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/list.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _list_vue_vue_type_template_id_f64fddaa___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./list.vue?vue&type=template&id=f64fddaa& */ "./resources/js/components/minuser-lk/tickets/list.vue?vue&type=template&id=f64fddaa&");
/* harmony import */ var _list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./list.vue?vue&type=script&lang=js& */ "./resources/js/components/minuser-lk/tickets/list.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _list_vue_vue_type_template_id_f64fddaa___WEBPACK_IMPORTED_MODULE_0__["render"],
  _list_vue_vue_type_template_id_f64fddaa___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/minuser-lk/tickets/list.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/list.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/list.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./list.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/list.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/list.vue?vue&type=template&id=f64fddaa&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/list.vue?vue&type=template&id=f64fddaa& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_f64fddaa___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./list.vue?vue&type=template&id=f64fddaa& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/list.vue?vue&type=template&id=f64fddaa&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_f64fddaa___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_f64fddaa___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);