function update(e, isEdit) {
    e.preventDefault();

    var form = $('form.form-edit-add')[0];

    var formData = new FormData(form);

    if (!isEdit) {
        formData.append('_tagging', '1');
        $.ajax({
            url: form.action,
            data: formData,
            type: 'POST',
            processData: false,
            contentType: false,

            success: function(response) {
                form.action = response.route;
                toastr.success(response.message);

                if (!formData._method) {
                    var methodInput = document.createElement('input');
                    methodInput.type = 'hidden';
                    methodInput.name = '_method';
                    methodInput.value = 'PUT';
                    form.appendChild(methodInput);
                }
            },

            error: function(response) {
                toastr.error(response.message);
            }
        });

        return;
    }

    formData.append('model_slug', window.location.pathname.split('/').reverse()[2]);

    $.ajax({
        url: window.location.origin + '/admin/model/' + window.location.pathname.split('/').reverse()[1] + '/save',
        data: formData,
        type: 'POST',
        processData: false,
        contentType: false,

        success: function(response) {
            toastr.success(response.message);
        },

        error: function(response) {
            toastr.error(response.message);
        }
    });
}
