(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _services_statuses__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/statuses */ "./resources/js/services/statuses.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ticket.show',
  components: {
    VSelect: vue_select__WEBPACK_IMPORTED_MODULE_1___default.a,
    quillEditor: vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__["quillEditor"]
  },
  props: ['id'],
  data: function data() {
    return {
      tab: 'form',
      ticket: null,
      currentUser: null,
      cticket: null,
      operator: null,
      commentForm: {
        comment: '',
        files: [],
        visibility: 'M'
      },
      invite: {
        date: null,
        time: null,
        "new": true
      },
      operators: [],
      organizations: [],
      showPopup: false,
      editorOption: {
        modules: {
          toolbar: false
        },
        placeholder: "Введите текст..."
      }
    };
  },
  methods: {
    makeAttachmentName: function makeAttachmentName(file) {
      var tmp = file.path.split('.');
      var ext = tmp[tmp.length - 1];
      return "".concat(file.name.replace("." + ext, ''), ".").concat(ext);
    },
    statusChange: function statusChange() {
      if (this.ticket.status === 'I') {
        this.showPopup = true;
      }
    },
    applyInviteDate: function applyInviteDate(_ref) {
      var date = _ref.date,
          window = _ref.window;
      this.invite.date = date;
      this.invite.window = window;
    },
    findFieldValue: function findFieldValue(field) {
      if (field.options) {
        if (Array.isArray(this.formData[field.name])) {
          return this.formData[field.name].map(function (res) {
            return "<p>".concat(field.options[res], "</p>");
          }).join('');
        } else {
          return field.options[this.formData[field.name]];
        }
      }

      return this.formData[field.name];
    },
    ruStatus: function ruStatus(status) {
      return _services_statuses__WEBPACK_IMPORTED_MODULE_2__["statuses"][status];
    },
    formatDate: function formatDate(date, format) {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(date).format(format);
    },
    ruDateFormat: function ruDateFormat(date) {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(date).format('DD.MM.YYYY HH:mm');
    },
    changeFiles: function changeFiles(event) {
      this.commentForm.files = event.target.files;
    },
    sendComment: function sendComment() {
      var _this = this;

      var fd = new FormData();
      fd.append('_token', $('meta[name="csrf-token"]').attr('content'));
      fd.append('text', this.commentForm.comment.split('\n').join('<br>'));
      fd.append('visibility', this.commentForm.visibility);
      Array.from(this.commentForm.files).forEach(function (file) {
        return fd.append('attachment[]', file);
      });
      $.ajax({
        url: "/api/ticket/".concat(this.ticket.id, "/comment"),
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function success(_ref2) {
          var ticket = _ref2.ticket,
              token = _ref2.token;
          _this.commentForm.comment = '';
          $('meta[name="csrf-token"]').attr('content', token);
          _this.ticket = ticket;
          _this.cticket = _objectSpread({}, ticket);
          _this.commentForm.files = [];

          _this.$nextTick(function () {
            return _this.$forceUpdate();
          });
        }
      });
    },
    saveStatus: function saveStatus() {
      var _this2 = this;

      var data;

      if (this.ticket.status === 'I') {
        if (!this.invite.date) {
          this.$flash_danger("Не выбрано время визита");
          return;
        }

        data = {
          minuser_id: this.ticket.minuser_id,
          status: this.ticket.status,
          invite: {
            window: this.invite.window,
            time: moment__WEBPACK_IMPORTED_MODULE_0___default()(this.invite.date).format("YYYY-MM-DD HH:mm"),
            "new": true
          },
          _token: $('meta[name="csrf-token"]').attr('content')
        };
      } else {
        data = {
          minuser_id: this.ticket.minuser_id,
          status: this.ticket.status,
          window_id: $('#organization').val(),
          _token: $('meta[name="csrf-token"]').attr('content')
        };
      }

      $.ajax({
        url: "/api/ticket/".concat(this.ticket.id, "/status"),
        data: data,
        type: 'POST',
        success: function success(_ref3) {
          var ticket = _ref3.ticket,
              token = _ref3.token;
          $('meta[name="csrf-token"]').attr('content', token);
          _this2.ticket = ticket;
          _this2.cticket = _objectSpread({}, ticket);

          _this2.$nextTick(function () {
            return _this2.$forceUpdate();
          });

          _this2.$flash_success('Параметры заявки успешно обновлены');
        },
        error: function error() {
          _this2.$flash_danger('При изменении статуса произошла ошибка. Попробуйте обновить страницу и повторить попытку.');
        }
      });
    },
    setCommentStatus: function setCommentStatus(comment) {
      var _this3 = this;

      var data = {
        status: comment.status,
        _token: $('meta[name="csrf-token"]').attr('content')
      };
      $.ajax({
        url: "/api/comments/".concat(comment.id, "/change-status"),
        data: data,
        type: 'POST',
        success: function success(_ref4) {
          var token = _ref4.token;
          $('meta[name="csrf-token"]').attr('content', token);

          _this3.$nextTick(function () {
            return _this3.$forceUpdate();
          });

          _this3.$flash_success('Статус комментария успешно обновлен');
        },
        error: function error() {
          _this3.$flash_danger('При изменении статуса произошла ошибка. Попробуйте обновить страницу и повторить попытку.');
        }
      });
    },
    returnToManager: function returnToManager() {
      var _this4 = this;

      $.ajax({
        url: "/api/ticket/".concat(this.ticket.id, "/return"),
        data: {
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        success: function success(_ref5) {
          var ticket = _ref5.ticket,
              token = _ref5.token;
          $('meta[name="csrf-token"]').attr('content', token);
          _this4.ticket.status = ticket.status;
          _this4.ticket.operator_id = ticket.operator_id;

          _this4.$nextTick(function () {
            return _this4.$forceUpdate();
          });

          _this4.$flash_success('Заявка успешно возвращена');
        }
      });
    }
  },
  computed: {
    availableStatuses: function availableStatuses() {
      if (this.cticket) {
        return Object(_services_statuses__WEBPACK_IMPORTED_MODULE_2__["availableStatuses"])(_objectSpread({}, this.cticket).status);
      }

      return {};
    },
    form: function form() {
      if (!this.ticket) return {};
      var form = typeof this.ticket.form_result.form.form_data === 'string' ? JSON.parse(this.ticket.form_result.form.form_data) : this.ticket.form_result.form.form_data;
      var formData = [];
      form.steps.forEach(function (step) {
        step.data.forEach(function (stepData) {
          stepData.cols.forEach(function (col) {
            col.data.forEach(function (elm) {
              if (elm.tag !== 'custom') {
                formData.push({
                  name: elm.name,
                  title: elm.title,
                  tag: elm.tag,
                  type: elm.type || false,
                  options: elm.options ? elm.options.reduce(function (a, c) {
                    a[c.value] = c.name;
                    return a;
                  }, {}) : null
                });
              }
            });
          });
        });
      });
      return formData;
    },
    formData: function formData() {
      if (!this.ticket) return {};
      return typeof this.ticket.form_result.result === 'string' ? JSON.parse(this.ticket.form_result.result) : this.ticket.form_result.result;
    },
    filesText: function filesText() {
      if (this.commentForm.files.length > 0) {
        return Array.from(this.commentForm.files).map(function (f) {
          return f.name;
        }).join(', ');
      } else {
        return 'Выберите файлы';
      }
    },
    operatorOptions: function operatorOptions() {
      return this.operators.map(function (u) {
        return {
          label: "".concat(u.first_name || '', " ").concat(u.name || '', " ").concat(u.patronymic || '').trim(),
          code: u.id
        };
      });
    }
  },
  created: function created() {
    var _this5 = this;

    $.ajax("/api/ticket/".concat(this.id), {
      success: function success(_ref6) {
        var ticket = _ref6.ticket;
        _this5.ticket = ticket;
        _this5.cticket = _objectSpread({}, ticket);
        _this5.invite = {
          date: ticket.invite ? _this5.formatDate(ticket.invite.invite_time, 'YYYY-MM-DDTHH:MM') : null,
          window: ticket.invite ? ticket.invite.window_id : null,
          "new": ticket.invite ? false : true
        };
        _this5.operators = ticket.window.users;
        $.ajax("/api/user?role=operator", {
          success: function success(_ref7) {
            var users = _ref7.users,
                windows = _ref7.windows,
                user = _ref7.user;
            _this5.organizations = windows;
            _this5.currentUser = user;

            _this5.$nextTick(function () {
              _this5.operator = _this5.operators.find(function (o) {
                return o.code == _this5.ticket.operator_id;
              });
            });
          }
        });

        _this5.$nextTick(function () {
          return _this5.$forceUpdate();
        });
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".tab-content .tab-pane[data-v-2070f40a] {\n  display: block;\n}\n.quill-editor[data-v-2070f40a] {\n  height: 200px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=template&id=2070f40a&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=template&id=2070f40a&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.ticket === null
    ? _c("div", [_c("h1", [_vm._v("Загрузка...")])])
    : _c("div", [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex flex-column-fluid" }, [
          _c("div", { staticClass: "col-lg-6" }, [
            _c(
              "div",
              {
                staticClass: "card card-custom gutter-b example example-compact"
              },
              [
                _c("div", { staticClass: "card-header" }, [
                  _c("h3", { staticClass: "card-title" }, [
                    _vm._v(
                      "\n              Заявка #" +
                        _vm._s(_vm.ticket.id) +
                        " от: " +
                        _vm._s(
                          _vm.formatDate(_vm.ticket.created_at, "MM.DD.YYYY")
                        ) +
                        "\n            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "d-flex align-items-center mr-2" }, [
                    _c(
                      "span",
                      {
                        class:
                          "label label-light-success label-lg font-weight-bold label-inline " +
                          _vm.ticket.status_class
                      },
                      [_vm._v(_vm._s(_vm.ticket.status_name))]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("form", { staticClass: "form" }, [
                  _c(
                    "div",
                    { staticClass: "card-body" },
                    [
                      _c("div", { staticClass: "form-group form-group-last" }, [
                        _c("h4", { staticClass: "card-title" }, [
                          _vm._v(
                            _vm._s(_vm.ticket.form_result.form.service.name)
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.form, function(field) {
                        return _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v(_vm._s(field.title))]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "custom-file ml-2" },
                            _vm._l(_vm.findFieldValue(field), function(file) {
                              return field.tag === "input" &&
                                field.type === "file"
                                ? _c(
                                    "a",
                                    {
                                      staticClass: "py-1",
                                      attrs: {
                                        href: "/storage/" + file.path,
                                        target: "_blank",
                                        download: _vm.makeAttachmentName(file)
                                      }
                                    },
                                    [
                                      _vm._m(1, true),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "navi-text" }, [
                                        _vm._v(_vm._s(file.name))
                                      ])
                                    ]
                                  )
                                : _c("span", {
                                    domProps: {
                                      innerHTML: _vm._s(
                                        _vm.findFieldValue(field)
                                      )
                                    }
                                  })
                            }),
                            0
                          )
                        ])
                      })
                    ],
                    2
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "card card-custom gutter-b example example-compact"
              },
              [
                _vm._m(2),
                _vm._v(" "),
                _vm._l(_vm.ticket.comments, function(comment) {
                  return _c("div", { staticClass: "mb-3" }, [
                    _c(
                      "div",
                      {
                        staticClass: "shadow-xs",
                        attrs: { "data-inbox": "message" }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "d-flex align-items-center card-spacer-x py-6"
                          },
                          [
                            _c(
                              "span",
                              { staticClass: "symbol symbol-50 mr-4" },
                              [
                                _c("span", {
                                  staticClass: "symbol-label",
                                  style:
                                    "background-image: url('" +
                                    comment.user.avatar_url +
                                    "')"
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "d-flex flex-column flex-grow-1 flex-wrap mr-2"
                              },
                              [
                                _c("div", { staticClass: "d-flex" }, [
                                  _c(
                                    "span",
                                    {
                                      staticClass:
                                        "font-size-lg font-weight-bolder text-dark-75 mr-2"
                                    },
                                    [
                                      _vm._v(
                                        "\n                      " +
                                          _vm._s(comment.user.firstname) +
                                          " " +
                                          _vm._s(comment.user.name)
                                      ),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c(
                                        "span",
                                        {
                                          staticClass:
                                            "font-size-base font-weight-normal"
                                        },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(
                                                comment.user.organization_name
                                              ) +
                                              "\n                      "
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center" },
                              [
                                comment.user_id != _vm.currentUser
                                  ? _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: comment.status,
                                            expression: "comment.status"
                                          }
                                        ],
                                        staticClass:
                                          "form-control btn btn-sm btn-light-primary",
                                        on: {
                                          change: [
                                            function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.$set(
                                                comment,
                                                "status",
                                                $event.target.multiple
                                                  ? $$selectedVal
                                                  : $$selectedVal[0]
                                              )
                                            },
                                            function($event) {
                                              return _vm.setCommentStatus(
                                                comment
                                              )
                                            }
                                          ]
                                        }
                                      },
                                      [
                                        _c(
                                          "option",
                                          { attrs: { value: "R" } },
                                          [_vm._v("Прочитано")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "option",
                                          { attrs: { value: "N" } },
                                          [_vm._v("Не прочитано")]
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              ]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "card-spacer-x py-3" }, [
                          _c(
                            "div",
                            { staticClass: "font-weight-bold text-muted mr-3" },
                            [
                              _vm._v(
                                "\n                  " +
                                  _vm._s(_vm.ruDateFormat(comment.created_at)) +
                                  "\n                "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("div", {
                            domProps: { innerHTML: _vm._s(comment.text) }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "d-flex align-items-center py-3" },
                            _vm._l(comment.attachment, function(attachment) {
                              return _c("div", { staticClass: "mr-4" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "py-1",
                                    attrs: {
                                      target: "_blank",
                                      download: _vm.makeAttachmentName(
                                        attachment
                                      ),
                                      href: "/storage/" + attachment.path
                                    }
                                  },
                                  [
                                    _vm._m(3, true),
                                    _vm._v(
                                      "\n                      " +
                                        _vm._s(attachment.name) +
                                        "\n                    "
                                    )
                                  ]
                                )
                              ])
                            }),
                            0
                          )
                        ])
                      ]
                    )
                  ])
                }),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "mb-3", attrs: { id: "kt_inbox_reply" } },
                  [
                    _c("div", { staticClass: "card-body p-0" }, [
                      _c(
                        "form",
                        {
                          attrs: { id: "kt_inbox_reply_form" },
                          on: {
                            submit: function($event) {
                              $event.preventDefault()
                              return _vm.sendComment()
                            }
                          }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "d-block" },
                            [
                              _c("quill-editor", {
                                attrs: { options: _vm.editorOption },
                                model: {
                                  value: _vm.commentForm.comment,
                                  callback: function($$v) {
                                    _vm.$set(_vm.commentForm, "comment", $$v)
                                  },
                                  expression: "commentForm.comment"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top"
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "align-items-center mr-3" },
                                [
                                  _vm._m(4),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    {
                                      staticClass:
                                        "btn btn-light-primary mr-2 mt-2",
                                      attrs: { type: "reset" }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "la la-paperclip"
                                      }),
                                      _vm._v(
                                        _vm._s(_vm.filesText) +
                                          "\n                      "
                                      ),
                                      _c("input", {
                                        staticStyle: { display: "none" },
                                        attrs: {
                                          type: "file",
                                          id: "customFile",
                                          name: "attachment[]",
                                          multiple: ""
                                        },
                                        on: { change: _vm.changeFiles }
                                      })
                                    ]
                                  )
                                ]
                              )
                            ]
                          )
                        ]
                      )
                    ])
                  ]
                )
              ],
              2
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "col-lg-4 flex-row-auto",
              attrs: { id: "kt_profile_aside" }
            },
            [
              _c("div", { staticClass: "card card-custom gutter-b" }, [
                _vm._m(5),
                _vm._v(" "),
                _c("div", { staticClass: "card-body pt-4" }, [
                  _c("div", { staticClass: "d-flex align-items-center py-5" }, [
                    _c(
                      "div",
                      {
                        staticClass:
                          "symbol symbol-75 mr-5 align-self-start align-self-xxl-top"
                      },
                      [
                        _c("div", {
                          staticClass: "symbol-label",
                          style:
                            "background-image:url('" +
                            _vm.ticket.form_result.user.avatar_url +
                            "')"
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", [
                      _c(
                        "div",
                        {
                          staticClass:
                            "font-weight-bolder font-size-h5 text-dark-75"
                        },
                        [
                          _vm._v(
                            "\n                  " +
                              _vm._s(_vm.ticket.form_result.user.name) +
                              " " +
                              _vm._s(_vm.ticket.form_result.user.firstname) +
                              "\n                "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm.ticket.form_result.user.company
                        ? _c("div", { staticClass: "text-muted" }, [
                            _vm._v(
                              _vm._s(_vm.ticket.form_result.user.company.name) +
                                "\n                "
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "d-flex align-items-center mb-2" },
                        [
                          _c(
                            "a",
                            {
                              staticClass:
                                "text-primary text-hover-primary font-weight-bold",
                              attrs: { href: "#" }
                            },
                            [_vm._v(_vm._s(_vm.ticket.form_result.user.phone))]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass:
                            "navi navi-bold navi-hover navi-active navi-link-rounded"
                        },
                        [
                          _c("div", { staticClass: "navi-item mt-2" }, [
                            _c(
                              "div",
                              { staticClass: "d-flex align-items-center mb-2" },
                              [
                                _vm.ticket.form_result.user.company
                                  ? _c(
                                      "a",
                                      {
                                        staticClass:
                                          "text-hover-primary font-weight-bold navi-link py-4 active",
                                        attrs: {
                                          href:
                                            "/companies/" +
                                            _vm.ticket.form_result.user.company
                                              .id
                                        }
                                      },
                                      [_vm._v("Профиль компании")]
                                    )
                                  : _vm._e()
                              ]
                            )
                          ])
                        ]
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "card card-custom gutter-b" }, [
                _vm._m(6),
                _vm._v(" "),
                _c("div", { staticClass: "card-body pt-4" }, [
                  _c("label", { staticClass: "form-label" }, [
                    _vm._v("Оператор")
                  ]),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.ticket.minuser_id,
                          expression: "ticket.minuser_id"
                        }
                      ],
                      staticClass: "form-control",
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.ticket,
                            "minuser_id",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    _vm._l(_vm.operatorOptions, function(item) {
                      return _c("option", { domProps: { value: item.code } }, [
                        _vm._v(
                          "\n                " +
                            _vm._s(item.label) +
                            "\n              "
                        )
                      ])
                    }),
                    0
                  ),
                  _vm._v(" "),
                  _c("input", {
                    attrs: { id: "current-user", type: "hidden" },
                    domProps: { value: _vm.currentUser }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "btn-group mr-3 mt-3" }, [
                    _c(
                      "button",
                      {
                        staticClass:
                          "btn btn-primary btn-shadow font-weight-bold px-6",
                        on: { click: _vm.saveStatus }
                      },
                      [_vm._v("Применить")]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-footer" }, [
                  this.ticket.minuser
                    ? _c(
                        "div",
                        { staticClass: "d-flex align-items-end py-2 mb-5" },
                        [
                          _c(
                            "div",
                            { staticClass: "d-flex align-items-center" },
                            [
                              _c(
                                "div",
                                { staticClass: "d-flex flex-shrink-0 mr-5" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "symbol symbol-lg-75" },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src: this.ticket.minuser.avatar_url,
                                          alt: "image"
                                        }
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "d-flex flex-column" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "font-weight-bolder font-size-h5 text-dark-75"
                                  },
                                  [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(this.ticket.minuser.name) +
                                        " " +
                                        _vm._s(this.ticket.minuser.firstname) +
                                        "\n                  "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "font-weight-bold" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        this.ticket.minuser.role.display_name
                                      )
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass: "text-muted font-weight-bold"
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        this.ticket.minuser.organization_name
                                      )
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  this.ticket.operator
                    ? _c(
                        "div",
                        { staticClass: "d-flex align-items-end py-2 mb-5" },
                        [
                          _c(
                            "div",
                            { staticClass: "d-flex align-items-center" },
                            [
                              _c(
                                "div",
                                { staticClass: "d-flex flex-shrink-0 mr-5" },
                                [
                                  _c(
                                    "div",
                                    { staticClass: "symbol symbol-lg-75" },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src: this.ticket.operator.avatar_url,
                                          alt: "image"
                                        }
                                      })
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "d-flex flex-column" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "font-weight-bolder font-size-h5 text-dark-75"
                                  },
                                  [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(this.ticket.operator.name) +
                                        " " +
                                        _vm._s(this.ticket.operator.firstname) +
                                        "\n                  "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "font-weight-bold" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        this.ticket.operator.role.display_name
                                      )
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    staticClass: "text-muted font-weight-bold"
                                  },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        this.ticket.operator.organization_name
                                      )
                                    )
                                  ]
                                )
                              ])
                            ]
                          )
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "navi navi-bold navi-hover navi-active navi-link-rounded"
                    },
                    [
                      _c("div", { staticClass: "navi-item mt-2" }, [
                        _c(
                          "div",
                          { staticClass: "d-flex align-items-center mb-2" },
                          [
                            _c(
                              "button",
                              {
                                staticClass:
                                  "btn btn-primary text-hover-primary btn-shadow font-weight-bold",
                                on: {
                                  click: function($event) {
                                    return _vm.returnToManager()
                                  }
                                }
                              },
                              [_vm._v("Передать модератору")]
                            )
                          ]
                        )
                      ])
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "accordion accordion-solid accordion-panel accordion-svg-toggle pb-5",
                  attrs: { id: "accordionExample8" }
                },
                [
                  _c("div", { staticClass: "card" }, [
                    _c(
                      "div",
                      {
                        staticClass: "card-header",
                        attrs: { id: "headingThree8" }
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass: "card-title collapsed",
                            attrs: {
                              "data-toggle": "collapse",
                              "data-target": "#collapseThree8"
                            }
                          },
                          [
                            _c("div", { staticClass: "card-label" }, [
                              _vm._v("История изменений")
                            ]),
                            _vm._v(" "),
                            _c("span", { staticClass: "svg-icon" }, [
                              _c(
                                "svg",
                                {
                                  attrs: {
                                    xmlns: "http://www.w3.org/2000/svg",
                                    "xmlns:xlink":
                                      "http://www.w3.org/1999/xlink",
                                    width: "24px",
                                    height: "24px",
                                    viewBox: "0 0 24 24",
                                    version: "1.1"
                                  }
                                },
                                [
                                  _c(
                                    "g",
                                    {
                                      attrs: {
                                        stroke: "none",
                                        "stroke-width": "1",
                                        fill: "none",
                                        "fill-rule": "evenodd"
                                      }
                                    },
                                    [
                                      _c("polygon", {
                                        attrs: { points: "0 0 24 0 24 24 0 24" }
                                      }),
                                      _vm._v(" "),
                                      _c("path", {
                                        attrs: {
                                          d:
                                            "M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z",
                                          fill: "#000000",
                                          "fill-rule": "nonzero"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("path", {
                                        attrs: {
                                          d:
                                            "M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z",
                                          fill: "#000000",
                                          "fill-rule": "nonzero",
                                          opacity: "0.3",
                                          transform:
                                            "translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"
                                        }
                                      })
                                    ]
                                  )
                                ]
                              )
                            ])
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "collapse",
                        attrs: {
                          id: "collapseThree8",
                          "data-parent": "#accordionExample8"
                        }
                      },
                      [
                        _c("div", { staticClass: "card-body" }, [
                          _c("p", { staticClass: "font-weight-bold" }, [
                            _vm._v("Дата создания"),
                            _c("br"),
                            _vm._v(" "),
                            _c("span", { staticClass: "text-dark-50" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.ticket.created_at
                                    ? _vm.ruDateFormat(_vm.ticket.created_at)
                                    : "Неизвестно"
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "font-weight-bold" }, [
                            _vm._v("Дата изменения"),
                            _c("br"),
                            _vm._v(" "),
                            _c("span", { staticClass: "text-dark-50" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.ticket.updated_at
                                    ? _vm.ruDateFormat(_vm.ticket.updated_at)
                                    : "Неизвестно"
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "font-weight-bold" }, [
                            _vm._v("Дата отправки"),
                            _c("br"),
                            _vm._v(" "),
                            _c("span", { staticClass: "text-dark-50" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.ticket.plan_end_date
                                    ? _vm.ruDateFormat(_vm.ticket.plan_end_date)
                                    : "Неизвестно"
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "font-weight-bold" }, [
                            _vm._v("Дата принятия в работу"),
                            _c("br"),
                            _vm._v(" "),
                            _c("span", { staticClass: "text-dark-50" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.ticket.plan_end_date
                                    ? _vm.ruDateFormat(_vm.ticket.plan_end_date)
                                    : "Неизвестно"
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "font-weight-bold" }, [
                            _vm._v("Плановая дата закрытия"),
                            _c("br"),
                            _vm._v(" "),
                            _c("span", { staticClass: "text-dark-50" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.ticket.plan_end_date
                                    ? _vm.ruDateFormat(_vm.ticket.plan_end_date)
                                    : "Неизвестно"
                                )
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "font-weight-bold" }, [
                            _vm._v("Фактическая дата закрытия"),
                            _c("br"),
                            _vm._v(" "),
                            _vm.ticket.fact_end_date
                              ? _c("span", { staticClass: "text-dark-50" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm.ruDateFormat(_vm.ticket.fact_end_date)
                                    )
                                  )
                                ])
                              : _vm._e()
                          ])
                        ])
                      ]
                    )
                  ])
                ]
              )
            ]
          )
        ])
      ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "subheader py-6 py-lg-8 subheader-transparent",
        attrs: { id: "kt_subheader" }
      },
      [
        _c(
          "div",
          {
            staticClass:
              "container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap"
          },
          [
            _c(
              "div",
              { staticClass: "d-flex align-items-center flex-wrap mr-1" },
              [
                _c(
                  "div",
                  { staticClass: "d-flex align-items-baseline flex-wrap mr-5" },
                  [
                    _c(
                      "ul",
                      {
                        staticClass:
                          "breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm"
                      },
                      [
                        _c(
                          "li",
                          { staticClass: "breadcrumb-item text-muted" },
                          [
                            _c(
                              "a",
                              {
                                staticClass: "text-muted",
                                attrs: { href: "/ticket" }
                              },
                              [_vm._v("Мои заявки")]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          { staticClass: "breadcrumb-item text-muted" },
                          [
                            _c(
                              "a",
                              {
                                staticClass: "disabled text-muted",
                                attrs: { href: "#" }
                              },
                              [_vm._v("Заявка")]
                            )
                          ]
                        )
                      ]
                    )
                  ]
                )
              ]
            )
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "mr-0" }, [
      _c("i", { staticClass: "icon-xl text-info la la-file-download" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex card-header align-items-center justify-content-between flex-wrap card-spacer-x py-5"
      },
      [
        _c("div", { staticClass: "d-flex align-items-center mr-2 py-2" }, [
          _c("div", { staticClass: "font-weight-bold font-size-lg mr-3" }, [
            _vm._v("Комментарии")
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "mr-0" }, [
      _c("i", { staticClass: "icon-xl text-info la la-file-download" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "btn-group mr-3" }, [
      _c(
        "button",
        { staticClass: "btn btn-primary btn-shadow font-weight-bold px-6" },
        [_vm._v("Отправить")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("div", { staticClass: "card-title" }, [
        _c("h3", { staticClass: "card-label" }, [_vm._v("Заявитель")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("div", { staticClass: "card-title" }, [
        _c("h3", { staticClass: "card-label" }, [_vm._v("Управление заявкой")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/show.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/show.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _show_vue_vue_type_template_id_2070f40a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./show.vue?vue&type=template&id=2070f40a&scoped=true& */ "./resources/js/components/minuser-lk/tickets/show.vue?vue&type=template&id=2070f40a&scoped=true&");
/* harmony import */ var _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show.vue?vue&type=script&lang=js& */ "./resources/js/components/minuser-lk/tickets/show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true& */ "./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _show_vue_vue_type_template_id_2070f40a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _show_vue_vue_type_template_id_2070f40a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "2070f40a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/minuser-lk/tickets/show.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/show.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/show.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true&":
/*!***********************************************************************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true& ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=style&index=0&id=2070f40a&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_style_index_0_id_2070f40a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/minuser-lk/tickets/show.vue?vue&type=template&id=2070f40a&scoped=true&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/minuser-lk/tickets/show.vue?vue&type=template&id=2070f40a&scoped=true& ***!
  \********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_2070f40a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./show.vue?vue&type=template&id=2070f40a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/minuser-lk/tickets/show.vue?vue&type=template&id=2070f40a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_2070f40a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_2070f40a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/statuses.js":
/*!*******************************************!*\
  !*** ./resources/js/services/statuses.js ***!
  \*******************************************/
/*! exports provided: statuses, availableStatuses, statusesStyles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "statuses", function() { return statuses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "availableStatuses", function() { return availableStatuses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "statusesStyles", function() { return statusesStyles; });
var statuses = {
  N: 'Отправлена',
  W: 'Взята в работу',
  O: 'Оформление',
  C: 'Отменена',
  D: 'Услуга оказана',
  R: 'Черновик',
  L: 'Клиент заполняет',
  P: 'Запрос на отмену',
  E: 'Требует уточнения',
  F: 'Отказ',
  I: 'Приглашение'
}; // Возвращает доступные статусы по текущему статусу заявки

var availableStatuses = function availableStatuses(currentStatus) {
  switch (currentStatus) {
    case 'N':
      return {
        N: statuses.N,
        W: statuses.W
      };

    case 'W':
      return {
        W: statuses.W,
        E: statuses.E,
        F: statuses.F,
        I: statuses.I,
        D: statuses.D
      };

    case 'R':
      return {
        R: statuses.R,
        L: statuses.L
      };

    case 'E':
      return {
        E: statuses.E,
        L: statuses.L
      };

    case 'F':
      return {
        F: statuses.F,
        W: statuses.W
      };

    case 'P':
      return {
        P: statuses.P,
        C: statuses.C
      };

    case 'I':
      return {
        I: statuses.I,
        F: statuses.F,
        D: statuses.D
      };

    default:
      if (statuses[currentStatus]) {
        var obj = {};
        obj[currentStatus] = statuses[currentStatus];
        return obj;
      }

      return {};
  }
};

var statusesStyles = function statusesStyles(value) {
  switch (value) {
    case 'N':
      return 'label-light-primary';

    case 'C':
      return 'label-light-danger';

    case 'D':
      return 'label-light-success';

    case 'W':
      return 'label-light-warning';

    default:
      return '';
  }
};



/***/ })

}]);