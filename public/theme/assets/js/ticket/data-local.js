'use strict';
// Class definition

var KTDatatableDataLocalDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {
        var dataJSONArray = JSON.parse('[{"OrderID":"15","CompanyName":"Получение статуса особо значимого инвестиционного проекта","ShipDate":"21.02.2021","Status":1,"Actions":null},\n' +
            '{"OrderID":"16","CompanyName":"Статус регионального инвестиционного проекта","ShipDate":"01.03.2021","Status":2,"Actions":null},\n' +
			'{"OrderID":"17","CompanyName":"Заявление на получение субсидий","ShipDate":"11.03.2021","Status":5,"Actions":null},\n' +
            '{"OrderID":"04","CompanyName":"Получение статуса особо значимого инвестиционного проекта","ShipDate":"25.01.2021","Status":6,"Actions":null}]');

        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },

            // columns definition
            columns: [{
                field: 'OrderID',
                title: '№',
				width: 50,
            }, 
				{
                field: 'CompanyName',
                title: 'Наименование услуги',
				width: 250,
            },  {
                field: 'ShipDate',
                title: 'Дата',
				width: 80,
                type: 'date',
                format: 'DD/MM/YYYY',
            },
				{
                field: 'Status',
                title: 'Статус',
				width: 150,
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        1: {
                            'title': 'Отправлена',
                            'class': 'label-light-primary'
                        },
                        2: {
                            'title': 'На рассмотрении',
                            'class': ' label-light-warning'
                        },
                        3: {
                            'title': 'Взята в работу',
                            'class': ' label-light-primary'
                        },
                        4: {
                            'title': 'Требует уточнения',
                            'class': ' label-light-warning'
                        },
                        5: {
                            'title': 'Оказана',
                            'class': ' label-light-success'
                        },
                        6: {
                            'title': 'Отменена',
                            'class': ' label-light-danger'
                        },
                    };
                    return '<span class="label font-weight-bold label-lg ' + status[row.Status].class + ' label-inline">' + status[row.Status].title + '</span>';
                },
            }, {
                field: 'Actions',
                title: '',
                sortable: false,
                width: 30,
                overflow: 'visible',
                autoHide: false,
                template: function() {
                    return '\
							<div class="dropdown dropdown-inline">\
                            <a href="javascript:;" class="btn btn-sm btn-light btn-text-primary btn-icon mr-2">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
        <rect x="0" y="0" width="24" height="24"/>\
        <circle fill="#000000" cx="12" cy="5" r="2"/>\
        <circle fill="#000000" cx="12" cy="12" r="2"/>\
        <circle fill="#000000" cx="12" cy="19" r="2"/>\
    </g>\
                                    </svg>\
                                </span>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                            </div>\
                        </div>\
						';
                },
            }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableDataLocalDemo.init();
});
