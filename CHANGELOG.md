    Журнал изменений:
        1. Первый коммит в рамках задачи из trello "Отмена Верификации пользователя" (DQPEaR12)
         - routes/web.php - новый маршрут (name=profile.cancel-verification);
         - app/Http/Controllers/HomeController.php - новый метод, соответственно (cancelVerification);
         - resources/views/profile/particles/company.blade.php - в шаблоне форма со скрытым инпутом для отправки заявки на отмену верификации
         - resources/views/profile/index.blade.php - сюда редиректим и выводим информационное сообщение;
         - resources/lang/ru/notifications.php - добавил кастомные языковые переменные.
        2. Второй коммит в рамках задачи из trello "Отмена Верификации пользователя" (DQPEaR12).Заключительный.
         - app/Http/Controllers/HomeController.php - переписал метод cancelVerification(). Добавил создание Notification для админа,
            если был запрос на отмену верифкации компании.
        3. bugfix(на самом деле это не баг, идентификатор задачи в trello DRfv7Jcf)
         - app/Observers/UserObserver.php - добавил company_inn для того чтобы вернуть его значение в уведомлении для обычного юзера;
         - app/User.php - edit PhpDocBlock;
         - resources/lang/ru/notifications.php - правка языковых переменных для сообщений юзера, в зависимости от статуса верификации компании.
        4. Создать шаблон кабинета Министерства (идентификатор задачи в trello r9cycVGJ)
         - app/Http/Controllers/TicketController.php - в индексном методе, если роль пользователя === minuser, то подулючаем другой шаблон. 
            Также описана пагинация для заявок.
         - resources/views/layouts/site.blade.php - правки в навигационном меню с учётом новой роли министерства (minuser);
         - resources/views/tickets/minuser-lk.blade.php - новый шаблон личного кабинета оператора министерства (на момент текущего коммита, 
          это полная копия шаблона модератора с использованием Vue.js. !!!ВНИМАНИЕ!!! Компоненты и маршрутизация в текущем коммите не работают!!!)
        5. Создать шаблон кабинета Министерства (идентификатор задачи в trello r9cycVGJ) Второй коммит в рамках задачи.
         - app/Http/Controllers/Api/TicketController.php - добавлена роль нового пользователя при выборке тикетов через Api/TicketController по статусу заявки
         - app/Http/Controllers/TicketController.php - в методе show() вывод тикетов а шаблон для ЛК минситерства с учётом роли последнего;
         - resources/views/tickets/minuser-lk.blade.php - шаблон ЛК министерства (в текущем коммите полная копия шаблона ЛК оператора/модератора);
         - Остальные файлы сбилдены по npm prod (на v14.16.0).
        6. Создать шаблон кабинета Министерства (идентификатор задачи в trello r9cycVGJ) Третий коммит в рамках задачи. Попытка скопировать ЛК оператора/модератора
         - app/Http/Controllers/Api/UserController.php - выборка ролей для Api/UserController, правки в запросе с учётом роли minuser;
         - webpack.mix.js - инструкция для Laravel Mix, чтобы подцепить инициализацию minuser-lk;
         - resources/views/tickets/minuser-lk.blade.php - правки в шаблоне с учётом ЛК Министерства;
         - public/mix-manifest.json - создан при помощи Laravel Mix;
         - resources/js/components/minuser-lk/ - папка компонентов ЛК министерства;
         - resources/js/minuser-lk.js - инициализация Vue приложения ЛК министерства;
         - resources/js/routes/minuser-lk.js - маршрутизация Vue приложения ЛК министерства;
         - Прочие файлы - результат билда npm run prod (на v14.16.0).
        7. Коммиты:
            a) 3c91bf824bdfe61ba59ade0f74cd0a7a7e5cf108 (изменение цветов);
            b) 23694214367fb506240ae9ccd8f38cecba4f7e1c (Изменение шрифтов, удаление комментированных строк в стилях, добавление шрифта в шаблон, правка текста в шаблоне);
            c) 001f4a189ee5290455 (Изменения в стилях на фронте и в админке).
           Выполнены с изменениями от Ramil <mr.prolider@gmail.com>
        8. Синхронизация изменений с bitbucker и продом, редактирую CHANGELOG.md. Далее работа только по git flow
        9. hotfix исправлен путь до логотипа:
         - resources/views/layouts/site.blade.php - в шаблоне исправлен путь до логотипа;
        10.Создать шаблон кабинета Министерства (идентификатор задачи в trello r9cycVGJ) Четвёртый коммит в рамках задачи. Изменение таблицы comments через новую миграцию
         - resources/js/components/minuser-lk/tickets/show.vue - оставил только <option value="M">Виден только представителю министерства и модераторам</option> в форме создания комментария в ЛК Министерства;
         - resources/views/layouts/site.blade.php - опять путь до логотипа. Теперь используется полный путь с учётом переменнной окружения APP_URL;
         - database/migrations/2021_03_12_140416_edit_field_visibility_for_comment_table.php - новая миграция на изменения enum visibility в таблице comments;
         - Остальные файлы - результат билда npm rum prod;
        11.Создать шаблон кабинета Министерства (идентификатор задачи в trello r9cycVGJ) Заключительный.
         - resources/js/components/minuser-lk/tickets/show.vue в компоненте show ЛК Министерства, ограничиваем отрисовку комментариев, едназначенных исключительно только менеджера КРУО;
         - Остальные файлы - результат билда.
        12.Создать шаблон кабинета Министерства (идентификатор задачи в trello r9cycVGJ) Вернуть возможнсоть министру, отсавлять комментарий видимый для всех.
         - resources/js/components/minuser-lk/tickets/show.vue - просто откатили <option value="A">Виден всем</option> в селектке;
         - Остальные файлы - результат билда.
        13.Новые индексы для сущности тикет: minuser_id and window_id
         - database/migrations/2021_03_15_105643_add_new_fields_to_tickets_table.php - модель Ticket, таблица tickets;
        14. Назначение оператора (Министра) к заявке (идентификатор задачи в trello xVXV13Fj). Вывести в блок управления заявкой селект с представителями Министерств.
         - app/Http/Controllers/Api/UserController.php - выборка только пользователей с ролью admin и operator (как было);
         - resources/js/components/operator-lk/tickets/show.vue - новый select, правка в компоненте с учётом роли minuser для ЛК оператора;
         - routes/api.php - доавлен маршрут внутреенего API /minuser;
         - routes/web.php - добавлен маршрут для получение пользователей с role_id=4 или minuser для MinuserController@index;
         - app/Http/Controllers/Api/MinuserController.php - индексный метод, для ajax на запроса /api/minuser?role=minuser;
         - сгенерированные билдом файлы.
        15. Назначение оператора (Министра) к заявке (идентификатор задачи в trello xVXV13Fj). Заключительный.
         - app/Http/Controllers/Api/MinuserController.php - файл удалён, не нужен дополнительный апи, это был костыль;
         - app/Http/Controllers/Api/TicketController.php - обработка window_id и minuser_id из запроса и нотификация;
         - app/Http/Controllers/Api/UserController.php - правка в запросе на выборку ролей. Теперь просим только operator;
         - app/Models/Ticket.php - в модель добавлены свойства window_id и minuser_id;
         - package-lock.json - npm настойчиво просил обновить список браузеров;
         - resources/js/components/minuser-lk/tickets/list.vue и resources/js/components/minuser-lk/tickets/show.vue - вернул имена переменным как это было до того, 
           как мы копировали лк оператора;
         - resources/js/components/operator-lk/tickets/show.vue - селект с министерствами, подготовка данных перед отправкой в контролёр обработчик;
         - routes/web.php - убрал свой ошибочный маршрут;
         - сгенерированные билдом файлы.
       16. Правки:
         - config/database.php - выключен строгий режим;
         - database/migrations/2021_03_15_105643_add_new_fields_to_tickets_table.php - правки в down() методе - на удаление ключей;
         - .gitignore - новые инструкции.
